﻿<?php

return [

    'password' => 'Las contraseñas deben tener al menos seis caracteres y corresponder a la confirmación.',
    'reset' => '¡Su contraseña ha sido restablecida!',
    'sent' => '¡Enviamos su enlace de restablecimiento de contraseña por e-mail!',
    'token' => 'Este token de restablecimiento de contraseña no es válido.',
    'user' => "No hemos encontrado un usuario con esta dirección de correo electrónico.",
    'reset_password' => 'Restablecer contraseña',
    'send_password_reset_link' => 'Enviar enlace de borrar contraseña',

];
