﻿<?php

return array(


    'page_of_accessibility' => [
        'p1' => 'La accesibilidad de la Web significa que las personas con alguna forma de discapacidad pueden usar la Web. En particular, cuando se habla de accesibilidad de la Web se está encaminando hacia un diseño web que permitirá a esas personas percibir, comprender, navegar y interactuar con la comunicación Web, mientras proporcionan contenido.',
        'p2' => 'Hay millones de personas con discapacidad que no pueden usar la Web. Actualmente, la mayoría de los sitios web y software de la Web tienen barreras de accesibilidad, haciendo difícil o imposible utilizar la Web para muchas personas con discapacidad. El software más accesible y los sitios web disponibles, las personas con más discapacidad pueden usar la Web y contribuir de forma más eficiente.',
        'p3' => 'En el caso del sitio CADAP, están planeadas:',
        'p4' => '1. Facilidad de uso y accesibilidad: estructura de navegación, etiquetado simple y claro, diseño reconocible y comprensible. Grado en el que todas las personas pueden usar un sitio, independientemente de sus capacidades físicas o derivadas de su contexto de uso.',
        'p5' => 'El sitio tiene un diseño estándar que facilita la exploración de contenidos por los usuarios',
        'p6' => 'La oferta del sitio y las secciones que lo componen se distinguen claramente.',
        'p7' => 'De acuerdo con la revisión automática en Tawdis: sin errores.',
        'p8' => 'Hay comandos para "ampliar" [Alt + +] o "reducir" [Alt + -] el tamaño de las fuentes que permite la adaptabilidad para los usuarios con necesidades especiales. Los textos también pueden volver al tamaño original [Alt + 0].',
        'p9' => 'Hay comando para "alto contraste" [Alt + S] que permite la adaptabilidad para usuarios con necesidades visuales.',
        'p10' => 'El acceso directo "ir al contenido" [Alt + T] está presente como una guía para los usuarios que utilizan el software de lectura del sitio.',
        'p11' => 'La hoja de estilo es validada por los estándares w3.org.',
        'p12' => 'Existen herramientas útiles para contribuir a la percepción de la estructura del sitio por usuarios, como: mapa del sitio [Alt + Q], saltar al contenido [Alt + T], saltar al menú [Alt + W], y saltar a el pie de página [Alt + A].',
        'p13' => '2. Diseño visual: apariencia, estética visual del sitio, aspectos gráficos, uso de iconos y colores.',
        'p14' => 'Los textos tienen un buen contraste que facilita la lectura.',
        'p15' => 'Además, los enlaces tienen un color estándar para identificarse como hipervínculos, lo que favorece la comprensión de las interacciones por los usuarios.',
        'p16' => 'El sitio mantiene un diseño similar en la mayoría de sus páginas, haciendo que la navegación sea relativamente fácil.',
        'p17' => 'El tamaño de fuente predeterminado es adecuado para la lectura de los usuarios.',

    ],


);
