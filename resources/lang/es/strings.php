﻿<?php

return [

    'email' => 'Correo electrónico',
    'none' => 'Ninguna',
    'name' => 'Nombre',
    'all_rights_reserved' => 'Todos los derechos reservados',
    'whoops' => 'Opa',
    'hello' => 'Holla',
    'regards' => 'Saludos',
    'active' => 'Activo',
    'action' => 'Acción',
    'edit' => 'Editar',
    'yes' => 'Sí',
    'no' => 'No',
    'save' => 'Guardar',
    'admin' => 'Administrador',
    'collaborator' => 'Colaborador',
    'block' => 'Bloquear',
    'unblock' => 'Desbloquear',
    'users' => 'Usuarios',
    'create' => 'Crear',
    'role' => 'Papel',
    'password' => 'Contrasenã',
    'dashboard' => 'Tablero',
    'lessons' => 'Lecciones',
    'title' => 'Título',
    'knowledge_area' => 'Área de conocimento',
    'created_by' => 'Creado por',
    'description' => 'descripción',
    'outro' => 'Mensaje de agradecimiento',
    'video_type' => 'Tipo del video',
    'youtube' => 'Youtube',
    'file' => 'Archivo',
    'send_file' => 'Enviar archivo',
    'video_url' => 'URL del video',
    'video_id_youtube' => 'ID del vídeo no Youtube',
    'questions' => 'Preguntas',
    'question' => 'Pregunta',
    'options' => 'Opciones',


    'add_lessons' => 'Añadir lecciones',
    'classes' => 'Turmas',
    'add_students' => 'Añadir usuarios',
    'delete_class' => 'Borrar turma',

    'add_question' => 'Añadir preguntas',
    'add_option' => 'Añadir opción',
    'remove' => 'quitar',
    'is_right' => '¿correcta?',
    'video_timestamp' => 'Tiempo en el vídeo',
    'update' => 'Acceder',
    'video' => 'Video',
    'change_video' => 'cambiar video',
    'cancel' => 'Deshacer',
    'change_questions' => 'cambiar preguntas',
    'youtube_video_id' => 'ID do video no Youtube',
    'delete' => 'Borrar',
    'lesson_link' => 'Enlace de la clase',
    'see_ils_form' => 'Ver el formulario ILS',
    'ils_form' => 'Formulario ILS',
    'close' => 'Cerrar',
    'close_classe' => 'Cerrar Turma',
    'contents' => 'Contenidos',
    'content' => 'Contenido',
    'add_content' => 'Añadir contenido',
    'content_tags' => 'Tags de contenido',
    'add_tag' => 'Añadir tag',
    'start_time' => 'Tiempo inicial',
    'end_time' => 'Tiempo final',
    'change_contents' => 'Cambiar contenido',
    'category' => 'Categoría',
    'next' => 'Siguiente',
    'age' => 'Edad',
    'birth' => 'Fecha de nacimiento',
    'birth_date' => 'Fecha de nacimiento',
    'gender' => 'Sexo',
    'male' => 'Hombre',
    'female' => 'Mujer',
    'race' => 'Etnicidad',
    'college' => 'Universidad',
    'course' => 'Curso',
    'special_need' => 'Necessidad especial',
    'handicap' => 'Soy portador de necesidades especiales',
    'handicap_type' => '¿Qué tipo de necesidad?',
    'handicap_custom_type' => 'Especifique su necesidad especial',
    'students' => 'Estudiantes',
    'student' => 'Estudiante',
    'details' => 'Detalles',
    'student_profile' => 'Perfil de estudiante',
    'learning_profile' => 'Perfil de aprendizaje',
    'tries' => 'Pruebas',
    'try' => 'Prueba',
    'hits' => 'Acierto',
    'total_questions' => 'Total de preguntas',
    'back' => 'Volver al Inicio',
    'handicap_types' => [

        1 => 'Ciego',
        2 => 'Deficiencia visual (Baja Visión)',
        3 => 'Sordo/Mudo',
        4 => 'Deficiencia auditiva (Pérdida auditiva)',
        5 => 'Deficiencia Motora',
        6 => 'Problemas de comunicación (Disturbios de la Habla y Lenguaje)',
        7 => 'Otros. ¿Cuál?',

    ],
    'musical_instrument' => 'Instrumento musical',
    'play_musical_instrument' => 'Yo toco algún instrumento musical',
    'which_instrument' => '¿Qué instrumento toca?',
    'introduction' => 'Introducción',
    'previous' => 'Anterior',
    'continue' => 'Continuar',
    'thanks_for_watching' => 'Gracias por ver la clase',
    'check_results_below' => 'Compruebe sus resultados abajo',
    'percentage_right' => 'Porcentaje de cuestiones correctas',
    'have_to_accept_camera' => 'Para continuar con la clase debes permitir el uso de la webcam. Actualice la página y permita el uso de la cámara',
    'impossible_initialize_lesson' => 'No se puede iniciar la clase',
    'loading_lesson' => 'La clase se está cargando. Esto puede tardar unos minutos',
    'results' => 'Resultados',

    'you_are_receiving_this_password_reset' => '
Usted está recibiendo este email porque recibimos una petición para resetear la contraseña de su cuenta.',
    'if_you_did_not_request_password_reset' => '
Si usted no pidió para resetear su contraseña, usted no necesita hacer nada más.',
    'if_you_are_having_rouble_clicking' => '
Si está teniendo problemas al hacer clic en el botón de ":actionText", 
copie y pegue la URL abajo en su navegador: [:actionUrl](:actionUrl)',
    'delete_this_lesson' => 'Desea borrar esta clase?',
    'separate_tags_by_comma' => 'Separe las etiquetas por coma',
    'you_need_flash' => 'Usted necesita Flash Player 8+ y Javascript habilitado para ver este vídeo',

    'test' => [

        'step1' => [
            'info_email' => 'Ingrese su correio para verificarmos se usted esta cadastrado',
        ],

        'step2' => [
            'instructions' => 'Llene todos los campos abajo para completar su registro antes del inicio de la clase.',
        ],

        'step3' => [
            'instructions' => 'Llene todos los campos abajo para continuar con la clase.',
        ],
        'step5' => [
            'instructions' => 'Responde todas las preguntas abajo antes de continuar',
        ]

    ],

    'metrics' => [

        'views' => 'la cantidad de veces que la clase fue asistida',
        'finalized' => 'cantidad de veces que la clase fue finalizada',

    ],

    'internal_server_error' => 'Error interno del servidor',
    'ils_result_error' => 'Responde todas las preguntas del formulario antes de continuar',
    'ils_question_empty' => 'Responde todas las preguntas antes de continuar',
    'ils_exists_already' => 'Usted ya ha llenado este formulario una vez. Intente iniciar la clase de nuevo para saltar este paso.',
    'question_time' => 'Hora de la pregunta',
    'error_loading_lesson' => 'Error al cargar la clase. Por favor intenta actualizar la página',
    'download' => 'Descargar',
    'download_last_frames' => 'Marcos .csv',
    'download_last_frames_xml' => 'Marcos .xml',
    'time' => 'Tiempo',
    'joy' => 'Alegría',
    'fear' => 'Miedo',
    'anger' => 'Rabia',
    'disgust' => 'Disgusto',
    'sadness' => 'Tristeza',
    'valence' => 'Valencia',
    'contempt' => 'Desprecio',
    'surprise' => 'Sorpresa',
    'engagement' => 'Compromiso',
    'during_video' => 'Durante el video',
    'see_impressions' => 'Ver impresiones',
    'impressions' => 'Impresiones',
    'see_all_lessons' => 'Ver todas las clases',
    'see_my_lessons' => 'Ver mis lecciones',
    'manage_lessons' => 'Gestionar clases',
    'skip' => 'Saltar etapa',
    'brown' => 'Marrón',
    'black' => 'Negro',
    'white' => 'Blanco',
    'indigenous' => 'Indio',
    'all' => 'Todos',
    'loose' => 'Separado',
    'ok' => 'Ok',
    'register_self' => 'Registrar hasta',

    'accessibility' => 'Accesibilidad',
    'map' => 'Mapa del Sitio',

    'language_portuguese' => 'Portugués',
    'language_spanish' => 'Español',

    'go_to_map' => 'Mapa del sitio',
    'go_to_home' => 'Ir al oo Inicio',
    'go_to_menu' => 'Ir al menú',
    'go_to_footer' => 'Ir al pie de página',
    'hight_contrast' => 'Alto contraste',
    'go_to_content' => 'Ir al contenido',

    'manual' => 'Manual',
    'teacher' => 'Profesor',


    'my_results' => 'Mis resultados',
    'graphics' => 'Gráficos',
    'statistic_module' => 'Estadística',


    'variation_of_emotions_per_lesson' => 'Variación de emociones',
    'average_chart_of_emotions_per_lesson' => 'Promedio de emociones',
    'hit_rate_graph_per_lesson' => 'Tasa de aciertos',

    'select_a_lesson' => 'Seleccione una clase',
    'select_a_type_of_graphic' => 'Seleccione el gráfico',
    'select_a_student' => 'Seleccione un estudiante',
    'select_a_class' => 'Seleccione una clase',


    'plot' => 'Mostrar Grafico',


    'variation_of_emotions_of_students_during_a_classroom' => 'Variación de las emociones de un estudiante durante una clase',
    'average_emotions' => 'Media de las Emociones',
    'hit_hate' => 'Tasa de resultados',


    'lesson_name' => 'Nombre de la clase',
    'student_name' => 'Nombre del estudiante',
    'class_name' => 'Nombre de la clase',



    'by_student' => 'Por Estudiante',
    'by_lesson' => 'Por Aula',
    'by_class' => 'Por Clase',


    'select_a_degree_of_schooling' => 'Seleccione el grado de escolaridad',

    'degree_of_schooling' => 'Grado de escolaridad (cursando)',

    'degree' => [
        'fundamental' => 'Fundamental',
        'medium' => 'Promedio',
        'higher' => 'Superior',
        'master' => 'Mestre',
        'doctor' => 'Doutor'
    ],


    'school' => 'Escuela',
    'series_year' => 'Serie / Año',

    'select_your_handicap' => 'Seleccione su necesidad especial',

    'instrument' => [
        '1' => 'Cuerdas',
        '2' => 'Sopro',
        '3' => 'Percusión',
        '4' => 'Ilaves',
    ],


    'kind_of_instrument' => '¿Qué tipo de instrumento tocas?',
    'select_a_kind_of_instrument' => 'Seleccione el tipo de instrumento',

    'end_pwd' => 'Usted ha cambiado su contraseña con éxito :)',
    'ready' => 'Listo!',



    'no_prints' => 'Sin impresiones',


    'no_graphics' => '¡Hasta el momento no tenemos resultados para traer!',
    'no_statistics' => '¡Hasta el momento todavía no tenemos resultados para el análisis.',



    'lessons_with_higher_and_lower_indexes_of_emotion' => 'Clases con mayor y menor índice de emoción',
    'student_with_higher_and_lower_index_of_emotion' => 'Alumno con mayor y menor indices de emoción',
    'individual_per_student' => 'Individual por alumno',
    'individual_per_lesson' => 'Individual por Aula',
    'lesson_upper_lower_for_each_emotion' => 'Aula con mayor y menor índice para cada emoción',

    'select_a_emotion' => 'Seleccione una emoción',


    'show' => 'Mostrar',


    'higher_index' => 'Mayor índice',
    'lower_index' => 'Menor índice',
    'id' => 'Id',
    'views' => 'Vistas',

    'average_emotion' => 'Media de la emoción',
    'average_student' => 'Media del alumno',
    'class_average' => 'Media de la clase',
    'averages' => 'Promedios',

    'watch' => 'Asistir',

    'details_about_the_student' => 'Detalles del alumno',
    'details_about_the_lesson' => 'Detalles del Aula',

    'class_with_higher_index' => 'Aula con mayor índice',
    'class_with_lower_index' => 'Aula con menor índice',

    'student_with_higher_index' => 'Alumno con mayor índice',
    'student_with_lower_index' => 'Alumno con menor índice',


    'user_id' => 'Id de Usuário',
    'student_id' => 'Id de Estudiante',
    'lesson_id' => 'Id del Aula',
    'number_of_classes_watched' => 'Cantidad de clases asistidas',




    'lesson' => 'Clase',
    'evaluation' => 'Evaluación',

    'evidences' => 'Evidencia',
    'add_evidence'=> 'Agregar evidencia',

    'competences' => 'Competencias',
    'competence' => 'Competencia',
    'add_competence'=> 'Agregar competencia',


    'abilities' => 'Habilidades',
    'ability' => 'Habilidade',
    'add_ability'=> 'Agregar Habilidade',


    'knowledge_s' => 'Conocimientos',
    'knowledge' => 'Conocimiento',
    'add_knowledge'=> 'Añadir Conocimiento',



    'register_evaluation' => 'Incluir Evaluación',
    'learning_assessment' => 'Evaluación de Aprendizaje',

    'result_evaluation_by_student_lesson' => 'Resultado de la Evaluación Individual de alumno / aula',
    'result_evaluation_by_lesson' => 'Resultado de la evaluación por aula',
    'results_of_evaluation' => 'Resultados de la evaluación',


    'developed' => 'Desarrollado',
    'partially_developed' => 'Desarrollado Parcialmente',
    'did_not_develop' => 'No ha desarrollado',


    'change_abilities' => 'Cambiar las Habilidades',
    'change_knowledge_s' => 'Cambiar los conocimientos',
    'change_competencies' => 'Cambiar las competencias',



];
