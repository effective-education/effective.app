﻿<?php
    return [
        /*
        |--------------------------------------------------------------------------
        | Validation Language Lines
        |--------------------------------------------------------------------------
        |
        | The following language lines contain the default error messages used by
        | the validator class. Some of these rules have multiple versions such
        | as the size rules. Feel free to tweak each of these messages here.
        |
        */
        'accepted' => ':attribute debe ser aceptado.',
        'active_url' => ':attribute no es un enlace valido',
        'after' => ':attribute debe ser una fecha después de :date.',
        'alpha' => ':attribute debe contener sólo letras.',
        'alpha_dash' => ':attribute debe contener sólo letras y números y trazos',
        'alpha_num' => ':attribute debe contener sólo letras y números.',
        'array' => ':attribute debe ser um array.',
        'before' => ':attribute debe ser una fecha antes de :date.',
        'between' => [
            'numeric' => ':attribute debe estar entre :min e :max.',
            'file' => ':attribute debe estar entre :min e :max kilobytes.',
            'string' => ':attribute debe estar entre :min e :max caracteres.',
            'array' => ':attribute debe tener entre :min e :max itens.',
        ],
        'boolean' => ':attribute debe ser verdadero o falso.',
        'confirmed' => 'A confirmação de :attribute não confere.',
        'date' => ':attribute no es una fecha válida.',
        'date_format' => ':attribute no confiere con el formato :format.',
        'different' => ':attribute e :other deben ser diferentes.',
        'digits' => ':attribute deve tner :digits dígitos.',
        'digits_between' => ':attribute debe tener entre :min y :max dígitos.',
        'email' => ':attribute debe ser una dirección de correo electrónico válida.',
        'exists' => 'O :attribute seleccionado no es válido.',
        'filled' => ':attribute es un campo obligatorio.',
        'image' => ':attribute debe ser una imagen.',
        'in' => ':attribute es inválido.',
        'integer' => ':attribute debe ser un entero.',
        'ip' => ':attribute debe ser una dirección IP válida.',
        'json' => ':attribute debe ser un JSON válido.',
        'max' => [
            'numeric' => ':attribute no debe ser mayor que :max.',
            'file' => ':attribute no puede tener más que :max kilobytes.',
            'string' => ':attribute no puede tener más que :max caracteres.',
            'array' => ':attribute no puede tener más que :max itens.',
        ],
        'mimes' => ':attribute debe ser un archivo de tipo: :values.',
        'min' => [
            'numeric' => ':attribute debe tener como mínimo :min.',
            'file' => ':attribute debe tener como mínimo :min kilobytes.',
            'string' => ':attribute debe tener como mínimo :min caracteres.',
            'array' => ':attribute debe tener como mínimo :min itens.',
        ],
        'not_in' => 'El :attribute seleccionado no es valido.',
        'numeric' => ':attribute deve ser um número.',
        'regex' => 'El formato de :attribute no es valido.',
        'required' => 'El campo :attribute  es obligatorio.',
        'required_if' => 'El campo :attribute  es obligatorio cuando :other es :value.',
        'required_with' => 'El campo :attribute  es obligatorio cuando :values está presente.',
        'required_with_all' => 'El campo :attribute  es obligatorio cuando :values están presentes.',
        'required_without' => 'El campo :attribute es obligatorio cuando :values no está presente.',
        'required_without_all' => 'El campo :attribute es obligatorio cuando ninguno de éstos está presente: :values.',
        'same' => ':attribute e :other deben ser iguales.',
        'size' => [
            'numeric' => ':attribute debe ser :size.',
            'file' => ':attribute debe tener :size kilobytes.',
            'string' => ':attribute debe tener :size caracteres.',
            'array' => ':attribute debe contener :size itens.',
        ],
        'string' => ':attribute debe ser una string',
        'timezone' => ':attribute debe ser una timezone valida.',
        'unique' => ':attribute ya esta en uso.',
        'url' => 'El formato de :attribute es invalido.',
        'not_closed' => "Falla al cerrar la clase",


        /*
        |--------------------------------------------------------------------------
        | Custom Validation Language Lines
        |--------------------------------------------------------------------------
        |
        | Here you may specify custom validation messages for attributes using the
        | convention "attribute.rule" to name the lines. This makes it quick to
        | specify a specific custom language line for a given attribute rule.
        |
        */
        'custom' => [

            'questions.*.title' => [

                'required' => 'Algunas preguntas no se han cumplido',
                'required_if' => 'Algunas preguntas no se han cumplido',

            ],

            'questions.*.options.*.text' => [

                'required' => 'Algunas preguntas están con opciones no rellenadas',
                'required_if' => 'Algunas preguntas están con opciones no rellenadas',

            ],

            'questions.*.timestamp' => [

                'required' => 'Algunas preguntas sin un tiempo especificado',
                'required_if' => 'Algunas preguntas sin un tiempo especificado',
                'date_format' => 'Algunas preguntas tienen un tiempo no válido',

            ],

            'questions.*.options' => [

                'required' => 'Algunas preguntas no tienen opciones',
                'required_if' => 'Algunas preguntas no tienen opciones',

            ],

            'contents.*.start_time' => [

                'required' => 'Algunos contenidos están sin un tiempo inicial',
                'required_if' => 'Algunos contenidos están sin un tiempo inicial',
                'date_format' => 'Algunos contenidos tienen un tiempo inicial no válido',

            ],

            'contents.*.end_time' => [

                'required' => 'Algunos contenidos están sin un tiempo final',
                'required_if' => 'Algunos contenidos están sin un tiempo final',
                'date_format' => 'Algunos contenidos tienen un tiempo final no válido',

            ],

            'contents.*.tags' => [

                'required' => 'Todos los contenidos deben tener al menos una etiqueta',
                'required_if' => 'Todos los contenidos deben tener al menos una etiqueta',

            ],


            'competences.*.description' => [

                'required' => 'Algunas competencias no se cumplieron',
                'required_if' => 'Algunas competencias no se cumplieron',

            ],

            'competences.*.evidences' => [

                'required' => 'Algunas competencias están sin evidencia',
                'required_if' => 'Algunas competencias están sin evidencia',

            ],

            'competences.*.evidences.*.description' => [

                'required' => 'Algunas evidencias de competencia no se cumplen',
                'required_if' => 'Algunas evidencias de competencia no se cumplen',

            ],



            'abilities.*.description' => [

                'required' => 'Algunas Habilidades no se llenaron',
                'required_if' => 'Algunas Habilidades no se llenaron',

            ],

            'abilities.*.evidences' => [

                'required' => 'Algunas Habilidades están sin evidencia',
                'required_if' => 'Algunas Habilidades están sin evidencia',

            ],

            'abilities.*.evidences.*.description' => [

                'required' => 'Algunas evidencias de Habilidad no se cumplen',
                'required_if' => 'Algunas evidencias de Habilidad no se cumplen',

            ],



            'knowledge_s.*.description' => [

                'required' => 'Algunos Conocimientos no se cumplieron',
                'required_if' => 'Algunos Conocimientos no se cumplieron',

            ],

            'knowledge_s.*.evidences' => [

                'required' => 'Algunos Conocimientos están sin evidencia',
                'required_if' => 'Algunos Conocimientos están sin evidencia',

            ],

            'knowledge_s.*.evidences.*.description' => [

                'required' => 'Algunas evidencias de conocimiento no se cumplen',
                'required_if' => 'Algunas evidencias de conocimiento no se cumplen',

            ],



            'video_type' => [

                'required_if' => 'El campo de tipo de vídeo es obligatorio',

            ],

            'video_id' => [

                'required_if' => 'El campo ID de vídeo en Youtube es obligatorio',

            ],

            'video_file' => [

                'required_if' => 'El campo Enviar archivo es obligatorio',

            ],

            'handicap_type' => [

                'required_if' => 'Elija el tipo de necesidad especial que usted posee',

            ],

            'handicap_custom_type' => [

                'required_if' => 'Especifique qué tipo de necesidad especial tiene usted',

            ],

            'which_instrument' => [

                'required_if' => 'Diga qué instrumento musical usted toca',

            ],
        ],
        /*
        |--------------------------------------------------------------------------
        | Custom Validation Attributes
        |--------------------------------------------------------------------------
        |
        | The following language lines are used to swap attribute place-holders
        | with something more reader friendly such as E-Mail Address instead
        | of "email". This simply helps us make messages a little cleaner.
        |
        */
        'attributes' => [

            'name' => trans('strings.name'),
            'email' => trans('auth.email_address'),
            'password' => trans('auth.password'),
            'confpwd' => trans('auth.confirm_password'),
            'knowledge_area' => trans('strings.knowledge_area'),
            'title' => trans('strings.title'),
            'description' => trans('strings.description'),
            'outro' => trans('strings.outro'),
            'video_type' => trans('strings.video_type'),
            'video_id' => trans('strings.youtube_video_id'),
            'video_file' => trans('strings.send_file'),
            'questions' => trans('strings.questions'),
            'contents' => trans('strings.contents'),
            'age' => trans('strings.age'),
            'gender' => trans('strings.gender'),
            'college' => 'Escola/Universidad',
            'course' => 'Ano/Curso',
            'handicap' => trans('strings.handicap'),
            'handicap_type' => trans('strings.handicap_type'),
            'handicap_custom_type' => trans('strings.handicap_custom_type'),
            'play_musical_instrument' => trans('strings.play_musical_instrument'),
            'which_instrument' => trans('strings.which_instrument'),
            'educational_level' => 'Grado de escolaridad',
            'birth_date' => 'Fecha de nacimiento',

        ],

        'at_least_one_right' => 'Todas las preguntas necesitan tener al menos una opción marcada como cierta',

    ];
