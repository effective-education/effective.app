﻿<?php

return [

    'types' => [

        'surprised' => 'Me sorprendí',
        'proud' => 'Me sentía orgulloso',
        'sad' => 'Por algún motivo me sentí triste',
        'excited' => 'Me quedé entusiasmado',
        'interesting' => 'Me pareció interesante',
        'devalued' => 'Me sentía desvalorizado',
        'ashamed' => 'Por algún motivo me avergonzaba',
        'happy' => 'Me quedé contento',
        'sorry' => 'Me sentí arrepentido',
        'expectancy' => 'He creado una expectativa',
        'fear' => 'Me sentí miedo',
        'quiet' => 'Me quedo tranquilo',
        'aversion' => 'Sentí una cierta aversión',
        'satisfied' => 'Me sentía satisfecho',
        'angry' => 'Me quedé enojado',
        'jealous' => 'Por algún motivo sentí celos',
        'indifferent' => 'Me quedé indiferente',

    ],

    'degrees' => [

        \App\Models\Impression::VERY_WEAK => 'muy débil',
        \App\Models\Impression::WEAK => 'débil',
        \App\Models\Impression::NOR_WEAK_OR_STRONG => 'ni débil, ni fuerte',
        \App\Models\Impression::STRONG => 'fuerte',
        \App\Models\Impression::VERY_STRONG => 'muy fuerte',

    ],

];
