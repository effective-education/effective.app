﻿<?php

return [

    'user' => [

        'created' => 'Un e-mail fue enviado al usuario avisando sobre su registro',
        'updated' => 'Usuario actualizado con éxito',
        'blocked' => 'Se ha bloqueado su acceso',
        'none' => 'Ningún usuario encontrado',

    ],

    'lessons' => [

        'created' => 'Aula creada con éxito!',
        'updated' => 'Aula actualizada con suceso!',
        'deleted' => 'Aula excluida con éxito!',
        'none' => 'Ninguna clase encontrada',


    ],



    'classes' => [

        'created' => 'Turma creada con éxito!',
        'updated' => 'Turma actualizada con suceso!',
        'deleted' => 'Turma excluida con éxito!',
        'none' => 'Ninguna turma encontrada',
        'closed' => "Turma cerrada con éxito",
        'already_closed' => 'Esta clase ya está cerrada'

    ],


    'class_student' => [

        'add' => 'Estudiantes agregados!',
        'deleted' => 'Estudiante excluido de la turma!',
        'none' => 'Ningún estudiante encontrado',

    ],


    'class_lesson' => [

        'add' => 'Aulas agregadas!',
        'deleted' => 'Aula excluida de la turma',
        'none' => 'Ningún aula encontrada',

    ],



    'dont_support_video' => 'Su navegador no soporta la etiqueta de vídeo.',



    'competence' => [
        'none' => 'Ninguna competencia encontrada'
    ] ,

    'ability' => [
        'none' => 'Ninguna Habilidad encontrada'
    ] ,

    'knowledge' => [
        'none' => 'Ninguno Conocimiento encontrado'
    ] ,


    'evaluation_results' => [

        'created' => '¡Salva con éxito!',
        'updated' => '¡Evaluación actualizada con éxito!',
    ],


];