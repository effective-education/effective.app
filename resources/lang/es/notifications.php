﻿<?php

    return [

        'welcome_to_effective' => [

            'greeting' => 'Sea bienvenido :app!',
            'intro' => 'Sus credenciales de acceso a la aplicación:',
            'email' => 'correo eletrónico: :email',
            'password' => 'contraseña: :password',
            'action' => 'Acesar',

        ],

    ];