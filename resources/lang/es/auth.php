﻿<?php

    return [

        'failed' => 'Estas credenciales no existen en nuestro sistema.',
        'throttle' => 'Muchos intentos de acceso. Pruebe de nuevo en: 3 segundos ',
        'register' => 'Registrarse',
        'login' => 'Login',
        'logout' => 'Salir',
        'email_address' => 'Correo Electrónico',
        'password' => 'contraseña',
        'confirm_password' => 'Confirmar contraseña',
        'remember_me' => 'Acordarse de mi',
        'forgot_password' => 'Olvidaste tu contraseña?',
    	'change_password' => 'Cambiar contraseña',
        'new_password' => 'nueva contraseña',

    ];
