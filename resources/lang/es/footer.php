﻿<?php

return [


    'developer' => 'Todos los derechos reservados. Desarrollado por ',
    'version' => 'Versión ',
    'tested_on' => 'Revisión automática en',
    'contact' => 'Contacto',
    'home' => 'Casa',
    'accessibility' => 'Accesibilidad'

];
