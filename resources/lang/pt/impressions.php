<?php

return [

    'types' => [

        'surprised' => 'Fiquei surpreso',
        'proud' => 'Senti-me orgulhoso',
        'sad' => 'Por algum motivo fiquei triste',
        'excited' => 'Fiquei entusiasmado',
        'interesting' => 'Achei interessante',
        'devalued' => 'Senti-me desvalorizado',
        'ashamed' => 'Por algum motivo fiquei envergonhado',
        'happy' => 'Fiquei feliz',
        'sorry' => 'Senti-me arrependido',
        'expectancy' => 'Criei expectativa',
        'fear' => 'Senti medo',
        'quiet' => 'Fiquei tranquilo',
        'aversion' => 'Senti uma certa aversão',
        'satisfied' => 'Senti-me satisfeito',
        'angry' => 'Fiquei irritado',
        'jealous' => 'Por algum motivo senti ciúmes',
        'indifferent' => 'Fiquei indiferente',

    ],

    'degrees' => [

        \App\Models\Impression::VERY_WEAK => 'muito fraco',
        \App\Models\Impression::WEAK => 'fraco',
        \App\Models\Impression::NOR_WEAK_OR_STRONG => 'nem fraco, nem forte',
        \App\Models\Impression::STRONG => 'forte',
        \App\Models\Impression::VERY_STRONG => 'muito forte',

    ],

];
