<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    'accepted' => ':attribute deve ser aceito.',
    'active_url' => ':attribute não é uma URL válida.',
    'after' => ':attribute deve ser uma data depois de :date.',
    'alpha' => ':attribute deve conter somente letras.',
    'alpha_dash' => ':attribute deve conter letras, números e traços.',
    'alpha_num' => ':attribute deve conter somente letras e números.',
    'array' => ':attribute deve ser um array.',
    'before' => ':attribute deve ser uma data antes de :date.',
    'between' => [
        'numeric' => ':attribute deve estar entre :min e :max.',
        'file' => ':attribute deve estar entre :min e :max kilobytes.',
        'string' => ':attribute deve estar entre :min e :max caracteres.',
        'array' => ':attribute deve ter entre :min e :max itens.',
    ],
    'boolean' => ':attribute deve ser verdadeiro ou falso.',
    'confirmed' => 'A confirmação de :attribute não confere.',
    'date' => ':attribute não é uma data válida.',
    'date_format' => ':attribute não confere com o formato dd/mm/yyyy.',
    'different' => ':attribute e :other devem ser diferentes.',
    'digits' => ':attribute deve ter :digits dígitos.',
    'digits_between' => ':attribute deve ter entre :min e :max dígitos.',
    'email' => ':attribute deve ser um endereço de e-mail válido.',
    'exists' => 'O :attribute selecionado é inválido.',
    'filled' => ':attribute é um campo obrigatório.',
    'image' => ':attribute deve ser uma imagem.',
    'in' => ':attribute é inválido.',
    'integer' => ':attribute deve ser um inteiro.',
    'ip' => ':attribute deve ser um endereço IP válido.',
    'json' => ':attribute deve ser um JSON válido.',
    'max' => [
        'numeric' => ':attribute não deve ser maior que :max.',
        'file' => ':attribute não deve ter mais que :max kilobytes.',
        'string' => ':attribute não deve ter mais que :max caracteres.',
        'array' => ':attribute não pode ter mais que :max itens.',
    ],
    'mimes' => ':attribute deve ser um arquivo do tipo: :values.',
    'min' => [
        'numeric' => ':attribute deve ser no mínimo :min.',
        'file' => ':attribute deve ter no mínimo :min kilobytes.',
        'string' => ':attribute deve ter no mínimo :min caracteres.',
        'array' => ':attribute deve ter no mínimo :min itens.',
    ],
    'not_in' => 'O :attribute selecionado é inválido.',
    'numeric' => ':attribute deve ser um número.',
    'regex' => 'O formato de :attribute é inválido.',
    'required' => 'O campo :attribute é obrigatório.',
    'required_if' => 'O campo :attribute é obrigatório quando :other é :value.',
    'required_with' => 'O campo :attribute é obrigatório quando :values está presente.',
    'required_with_all' => 'O campo :attribute é obrigatório quando :values estão presentes.',
    'required_without' => 'O campo :attribute é obrigatório quando :values não está presente.',
    'required_without_all' => 'O campo :attribute é obrigatório quando nenhum destes estão presentes: :values.',
    'same' => ':attribute e :other devem ser iguais.',
    'size' => [
        'numeric' => ':attribute deve ser :size.',
        'file' => ':attribute deve ter :size kilobytes.',
        'string' => ':attribute deve ter :size caracteres.',
        'array' => ':attribute deve conter :size itens.',
    ],
    'string' => ':attribute deve ser uma string',
    'timezone' => ':attribute deve ser uma timezone válida.',
    'unique' => ':attribute já está em uso.',
    'url' => 'O formato de :attribute é inválido.',
    'not_closed' => "Falha ao fechar a turma",


    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */
    'custom' => [

        'questions.*.title' => [

            'required' => 'Algumas perguntas não foram preenchidas',
            'required_if' => 'Algumas perguntas não foram preenchidas',

        ],

        'questions.*.options.*.text' => [

            'required' => 'Algumas perguntas estão com opções não preenchidas',
            'required_if' => 'Algumas perguntas estão com opções não preenchidas',

        ],

        'questions.*.timestamp' => [

            'required' => 'Algumas perguntas sem um tempo especificado',
            'required_if' => 'Algumas perguntas sem um tempo especificado',
            'date_format' => 'Algumas perguntas estão com um tempo inválido',

        ],

        'questions.*.options' => [

            'required' => 'Algumas perguntas estão sem opções',
            'required_if' => 'Algumas perguntas estão sem opções',

        ],

        'contents.*.start_time' => [

            'required' => 'Alguns conteúdos estão sem um tempo inicial',
            'required_if' => 'Alguns conteúdos estão sem um tempo inicial',
            'date_format' => 'Alguns conteúdos estão com um tempo inicial inválido',

        ],

        'contents.*.end_time' => [

            'required' => 'Alguns conteúdos estão sem um tempo final',
            'required_if' => 'Alguns conteúdos estão sem um tempo final',
            'date_format' => 'Alguns conteúdos estão com um tempo final inválido',

        ],

        'contents.*.tags' => [

            'required' => 'Todos os conteúdos devem ter ao menos uma tag',
            'required_if' => 'Todos os conteúdos devem ter ao menos uma tag',

        ],



        'competences.*.description' => [

            'required' => 'Algumas Competências não foram preenchidas',
            'required_if' => 'Algumas Competências não foram preenchidas',

        ],

        'competences.*.evidences' => [

            'required' => 'Algumas Competências estão sem Evidências',
            'required_if' => 'Algumas Competências estão sem Evidências',

        ],

        'competences.*.evidences.*.description' => [

            'required' => 'Algumas evidências de Competência não estão preenchidas',
            'required_if' => 'Algumas evidências de Competência não estão preenchidas',

        ],



        'abilities.*.description' => [

            'required' => 'Algumas Habilidades não foram preenchidas',
            'required_if' => 'Algumas Habilidades não foram preenchidas',

        ],

        'abilities.*.evidences' => [

            'required' => 'Algumas Habilidades estão sem Evidências',
            'required_if' => 'Algumas Habilidades estão sem Evidências',

        ],

        'abilities.*.evidences.*.description' => [

            'required' => 'Algumas evidências de Habilidade não estão preenchidas',
            'required_if' => 'Algumas evidências de Habilidade não estão preenchidas',

        ],



        'knowledge_s.*.description' => [

            'required' => 'Alguns Conhecimentos não foram preenchidas',
            'required_if' => 'Alguns Conhecimentos não foram preenchidas',

        ],

        'knowledge_s.*.evidences' => [

            'required' => 'Alguns Conhecimentos estão sem Evidências',
            'required_if' => 'Alguns Conhecimentos estão sem Evidências',

        ],

        'knowledge_s.*.evidences.*.description' => [

            'required' => 'Algumas evidências de Conhecimento não estão preenchidas',
            'required_if' => 'Algumas evidências de Conhecimento não estão preenchidas',

        ],



        'video_type' => [

            'required_if' => 'O campo tipo de vídeo é obrigatório',

        ],

        'video_id' => [

            'required_if' => 'O campo ID do vídeo no Youtube é obrigatório',

        ],

        'video_file' => [

            'required_if' => 'O campo Enviar arquivo é obrigatório',

        ],

        'handicap_type' => [

            'required_if' => 'Escolha qual o tipo de necessidade especial você possui',

        ],

        'handicap_custom_type' => [

            'required_if' => 'Especifique qual o tipo de necessidade especial você possui',

        ],

        'which_instrument' => [

            'required_if' => 'Diga qual instrumento musical você toca',

        ],

    ],
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */
    'attributes' => [

        'name' => trans('strings.name'),
        'email' => trans('auth.email_address'),
        'password' => trans('auth.password'),
        'confpwd' => trans('auth.confirm_password'),
        'knowledge_area' => trans('strings.knowledge_area'),
        'title' => trans('strings.title'),
        'description' => trans('strings.description'),
        'outro' => trans('strings.outro'),
        'video_type' => trans('strings.video_type'),
        'video_id' => trans('strings.youtube_video_id'),
        'video_file' => trans('strings.send_file'),
        'questions' => trans('strings.questions'),
        'contents' => trans('strings.contents'),
        'age' => trans('strings.age'),
        'gender' => trans('strings.gender'),
        'college' => 'Escola/Universidade',
        'course' => 'Ano/Curso',
        'handicap' => trans('strings.handicap'),
        'handicap_type' => trans('strings.handicap_type'),
        'handicap_custom_type' => trans('strings.handicap_custom_type'),
        'play_musical_instrument' => trans('strings.play_musical_instrument'),
        'which_instrument' => trans('strings.which_instrument'),
        'educational_level' => 'Grau de escolaridade',
        'grade_average' => trans('strings.grade_average'),
        'birth_date' => 'Data de Nascimento',

    ],

    'at_least_one_right' => 'Todas as perguntas precisam ter ao menos uma opção marcada como certa',

];
