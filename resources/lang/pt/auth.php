<?php

    return [

        'failed' => 'Estas credenciais não existem em nosso sistema.',
        'throttle' => 'Muitas tentativas de login. Tente novamente em :seconds segundos.',
        'register' => 'Registrar',
        'login' => 'Login',
        'logout' => 'Sair',
        'email_address' => 'Endereço de e-mail',
        'password' => 'Senha',
        'confirm_password' => 'Confirmar senha',
        'new_password' => 'Nova senha',
        'remember_me' => 'Lembrar de mim',
        'forgot_password' => 'Esqueceu sua senha?',
	    'change_password' => 'Alterar senha'

    ];
