<?php

return [

    'password' => 'As senhas devem ter pelo menos seis caracteres e corresponder à confirmação.',
    'reset' => 'Sua senha foi resetada!',
    'sent' => 'Enviamos o seu link de redefinição de senha por e-mail!',
    'token' => 'Este token de redefinição de senha é inválido.',
    'user' => "Não encontramos um usuário com esse endereço de e-mail.",
    'reset_password' => 'Resetar senha',
    'send_password_reset_link' => 'Enviar link de resetar senha',

];
