﻿<?php

return array(


    'page_of_accessibility' => [
        'p1' => 'A acessibilidade da Web significa que as pessoas com alguma forma de deficiência poderão usar a Web. Em particular, quando se fala sobre acessibilidade da Web está sendo encaminhado para um design da Web que permitirá que essas pessoas percebam, compreendam, naveguem e interajam com a Web, enquanto fornecem conteúdo.',
        'p2' => 'Existem milhões de pessoas com deficiência que não podem usar a Web. Atualmente, a maioria dos sites e software da Web tem barreiras de acessibilidade, tornando difícil ou impossível usar a Web para muitas pessoas com deficiência. O software mais acessível e os sites da Web disponíveis, as pessoas com mais deficiência poderão usar a Web e contribuir de forma mais eficiente.',
        'p3' => 'No caso do site CADAP, estão planejadas:',
        'p4' => '1. Facilidade de uso e acessibilidade: estrutura de navegação, rotulagem simples e clara, layout reconhecível e compreensível. Grau em que todas as pessoas podem usar um site, independentemente de suas capacidades físicas ou derivadas do seu contexto de uso.',
        'p5' => 'O site possui um layout padrão que facilita a exploração de conteúdos pelos usuários',
        'p6' => 'A oferta do site e as seções que o compõem se distinguem claramente.',
        'p7' => 'De acordo com a revisão automática em Tawdis: sem erros.',
        'p8' => 'Existem comandos para "ampliar" [Alt + +] ou "reduzir" [Alt + -] o tamanho das fontes que permite a adaptabilidade para usuários com necessidades especiais. Os textos também podem voltar ao tamanho original [Alt + 0].',
        'p9' => 'Existe comando para "alto contraste" [Alt + S] que permite a adaptabilidade para usuários com necessidades visuais.',
        'p10' => 'O atalho “ir para o conteúdo” [Alt + T] está presente como um guia para usuários que usam o software de leitura do site.',
        'p11' => 'A folha de estilo é validada pelos padrões w3.org.',
        'p12' => 'Existem ferramentas úteis para contribuir com a apreensão da estrutura do site por usuários, tais como: mapa do site [Alt + Q], pular para o conteúdo [Alt + T], pular para o menu [Alt + W], e pular para o rodapé [Alt + A].',
        'p13' => 'Design visual: aparência, estética visual do site, aspectos gráficos, uso de ícones e cores.',
        'p14' => 'Os textos têm um bom contraste que facilita a leitura.',
        'p15' => 'Além disso, os links têm uma cor padrão para se identificar como hiperlinks, o que favorece a compreensão das interações pelos usuários.',
        'p16' => 'O site mantém um layout semelhante na maioria de suas páginas, tornando a navegação relativamente fácil.',
        'p17' => 'O tamanho de fonte padrão é adequado para leitura de usuários.',

    ],


);
