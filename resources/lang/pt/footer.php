﻿<?php

return [


    'developer' => 'Todos os diretos reservados. Desenvolvido por ',
    'version' => 'Versão ',
    'tested_on' => 'Revisão automática em' ,
    'contact' => 'Contato',
    'home' => 'Home',
    'accessibility' => 'Acessibilidade'

];
