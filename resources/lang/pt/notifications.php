<?php

    return [

        'welcome_to_effective' => [

            'greeting' => 'Seja bem vindo ao :app!',
            'intro' => 'Suas credenciais de acesso à aplicação:',
            'email' => 'E-mail: :email',
            'password' => 'Senha: :password',
            'action' => 'Acessar',

        ],

    ];