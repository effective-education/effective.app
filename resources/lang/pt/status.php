<?php

return [

    'user' => [

        'created' => 'Um e-mail foi enviado para o usuário avisando sobre seu cadastro',
        'updated' => 'Usuário atualizado com sucesso',
        'blocked' => 'O seu acesso foi bloqueado',
        'none' => 'Nenhum usuário encontrado',

    ],

    'student' => [

        'created' => 'Um e-mail foi enviado para o estudante avisando sobre seu cadastro',
        'updated' => 'Estudante atualizado com sucesso',
        'blocked' => 'O seu acesso foi bloqueado',
        'none' => 'Nenhum estudante encontrado',

    ],

    'lessons' => [

        'created' => 'Aula criada com sucesso!',
        'updated' => 'Aula atualizada com sucesso!',
        'deleted' => 'Aula excluída com sucesso!',
        'none' => 'Nenhuma aula encontrada',

    ],

    'classes' => [

        'created' => 'Turma criada com sucesso!',
        'updated' => 'Turma atualizada com sucesso!',
        'deleted' => 'Turma excluída com sucesso!',
        'none' => 'Nenhuma turma encontrada',
        'closed' => "Turma fechada com sucesso",
        'already_closed' => 'Esta turma já está fechada'


    ],


    'class_student' => [

        'add' => 'Alunos adicionados!',
        'deleted' => 'Aluno excluido da turma!',
        'none' => 'Nenhum estudante encontrado',

    ],


    'class_lesson' => [

        'add' => 'Aulas adicionadas!',
        'deleted' => 'Aula excluida da turma!',
        'none' => 'Nenhuma aula encontrada',

    ],



    'dont_support_video' => 'Your browser does not support the video tag.',


   'competence' => [
      'none' => 'Nenhuma Competência encontrada'
   ] ,

    'ability' => [
        'none' => 'Nenhuma Habilidade encontrada'
    ] ,

    'knowledge' => [
        'none' => 'Nenhum Conhecimento encontrado'
    ] ,


    'evaluation_results' => [

        'created' => 'Avaliação Salva com Sucesso!',
        'updated' => 'Avaliação Atualizada com Sucesso!',
    ],


];