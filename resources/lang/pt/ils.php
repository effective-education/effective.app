<?php

return [

    '1' => [

        'text' => 'Eu compreendo melhor alguma coisa depois de',
        'options' => [

            'a' => 'experimentar.',
            'b' => 'refletir sobre ela. ',

        ],

    ],

    '2' => [

        'text' => 'Eu me considero',
        'options' => [

            'a' => 'realista.',
            'b' => 'inovador(a)',

        ],

    ],

    '3' => [

        'text' => 'Quando eu penso sobre o que fiz ontem, é mais provável que aflorem',
        'options' => [

            'a' => 'figuras.',
            'b' => 'palavras.',

        ],

    ],

    '4' => [

        'text' => 'Eu tendo a ',
        'options' => [

            'a' => 'compreender os detalhes de um assunto, mas a estrutura geral pode ficar imprecisa.',
            'b' => 'compreender a estrutura geral de um assunto, mas os detalhes podem ficar imprecisos.',

        ],

    ],

    '5' => [

        'text' => 'Quando estou aprendendo algum assunto novo, me ajuda',
        'options' => [

            'a' => 'falar sobre ele.',
            'b' => 'refletir sobre ele.',

        ],

    ],

    '6' => [

        'text' => 'Se eu fosse um professor, eu preferiria ensinar uma disciplina',
        'options' => [

            'a' => 'que trate com fatos e situações reais.',
            'b' => 'que trate com idéias e teorias.',

        ],

    ],

    '7' => [

        'text' => 'Eu prefiro obter novas informações através de',
        'options' => [

            'a' => 'figuras, diagramas, gráficos ou mapas.',
            'b' => 'instruções escritas ou informações verbais.',

        ],

    ],

    '8' => [

        'text' => 'Quando eu compreendo',
        'options' => [

            'a' => 'todas as partes, consigo entender o todo.',
            'b' => 'o todo, consigo ver como as partes se encaixam.',

        ],

    ],

    '9' => [

        'text' => 'Em um grupo de estudo, trabalhando um material difícil, eu provavelmente',
        'options' => [

            'a' => 'tomo a iniciativa e contribuo com idéias.',
            'b' => 'assumo uma posição discreta e escuto.',

        ],

    ],

    '10' => [

        'text' => 'Acho mais fácil',
        'options' => [

            'a' => 'aprender fatos.',
            'b' => 'aprender conceitos.',

        ],

    ],

    '11' => [

        'text' => 'Em um livro com uma porção de figuras e desenhos, eu provavelmente',
        'options' => [

            'a' => 'observo as figuras e desenhos cuidadosamente.',
            'b' => 'atento para o texto escrito.',

        ],

    ],

    '12' => [

        'text' => 'Quando resolvo problemas de matemática, eu',
        'options' => [

            'a' => 'usualmente trabalho de maneira a resolver uma etapa de cada vez.',
            'b' => 'freqüentemente antevejo as soluções, mas tenho que me esforçar muito para conceber as etapas para
chegar a elas.',

        ],

    ],

    '13' => [

        'text' => 'Nas disciplinas que cursei eu',
        'options' => [

            'a' => 'em geral fiz amizade com muitos dos colegas.',
            'b' => 'raramente fiz amizade com muitos dos colegas.',

        ],

    ],

    '14' => [

        'text' => 'Em literatura de não-ficção, eu prefiro',
        'options' => [

            'a' => 'algo que me ensine fatos novos ou me indique como fazer alguma coisa.',
            'b' => 'algo que me apresente novas idéias para pensar.',

        ],

    ],

    '15' => [

        'text' => 'Eu gosto de professores',
        'options' => [

            'a' => 'que colocam uma porção de diagramas no quadro.',
            'b' => 'que gastam bastante tempo explicando.',

        ],

    ],

    '16' => [

        'text' => 'Quando estou analisando uma estória ou novela eu',
        'options' => [

            'a' => 'penso nos incidentes e tento colocá-los juntos para identificar os temas.',
            'b' => 'tenho consciência dos temas quando termino a leitura e então tenho que voltar atrás para encontrar os
incidentes que os confirmem.',

        ],

    ],

    '17' => [

        'text' => 'Quando inicio a resolução de um problema para casa, normalmente eu',
        'options' => [

            'a' => 'começo a trabalhar imediatamente na solução.',
            'b' => 'primeiro tento compreender completamente o problema.',

        ],

    ],

    '18' => [

        'text' => 'Prefiro a idéia do',
        'options' => [

            'a' => 'certo.',
            'b' => 'teórico.',

        ],

    ],

    '19' => [

        'text' => 'Relembro melhor',
        'options' => [

            'a' => 'o que vejo.',
            'b' => 'o que ouço.',

        ],

    ],

    '20' => [

        'text' => 'É mais importante para mim que o professor',
        'options' => [

            'a' => 'apresente a matéria em etapas seqüências claras.',
            'b' => 'apresente um quadro geral e relacione a matéria com outros assuntos.',

        ],

    ],

    '21' => [

        'text' => 'Eu prefiro estudar',
        'options' => [

            'a' => 'em grupo.',
            'b' => 'sozinho(a).',

        ],

    ],

    '22' => [

        'text' => 'Eu costumo ser considerado(a)',
        'options' => [

            'a' => 'cuidadoso(a) com os detalhes do meu trabalho.',
            'b' => 'criativo(a) na maneira de realizar meu trabalho.',

        ],

    ],

    '23' => [

        'text' => 'Quando busco orientação para chegar a um lugar desconhecido, eu prefiro',
        'options' => [

            'a' => 'um mapa.',
            'b' => 'instruções por escrito.',

        ],

    ],

    '24' => [

        'text' => 'Eu aprendo',
        'options' => [

            'a' => 'num ritmo bastante regular. Se estudar pesado, eu “chego lá”.',
            'b' => 'em saltos. Fico totalmente confuso(a) por algum tempo, e então, repentinamente eu tenho um “estalo”.',

        ],

    ],

    '25' => [

        'text' => 'Eu prefiro primeiro',
        'options' => [

            'a' => 'experimentar as coisas.',
            'b' => 'pensar sobre como é que eu vou fazer.',

        ],

    ],

    '26' => [

        'text' => 'Quando estou lendo como lazer, eu prefiro escritores que',
        'options' => [

            'a' => 'explicitem claramente o que querem dizer.',
            'b' => 'dizem as coisas de maneira criativa, interessante.',

        ],

    ],

    '27' => [

        'text' => 'Quando vejo um diagrama ou esquema em uma aula. Relembro mais facilmente',
        'options' => [

            'a' => 'a figura.',
            'b' => 'o que o(a) professor(a) disse a respeito dela.',

        ],

    ],

    '28' => [

        'text' => 'Quando considero um conjunto de informações, provavelmente eu',
        'options' => [

            'a' => 'presto mais atenção nos detalhes e não percebo o quadro geral.',
            'b' => 'procuro compreender o quadro geral antes de atentar para os detalhes.',

        ],

    ],

    '29' => [

        'text' => 'Relembro mais facilmente.',
        'options' => [

            'a' => 'algo que fiz.',
            'b' => 'algo sobre o que pensei bastante.',

        ],

    ],

    '30' => [

        'text' => 'Quando tenho uma tarefa para executar, eu prefiro',
        'options' => [

            'a' => 'dominar uma maneira para a execução da tarefa.',
            'b' => 'encontrar novas maneiras para a execução da tarefa.',

        ],

    ],

    '31' => [

        'text' => 'Quando alguém está me mostrando dados, eu prefiro',
        'options' => [

            'a' => 'diagramas e gráficos.',
            'b' => 'texto sumarizando os resultados.',

        ],

    ],

    '32' => [

        'text' => 'Quando escrevo um texto, eu prefiro trabalhar (pensar a respeito ou escrever)',
        'options' => [

            'a' => 'a parte inicial do texto e avançar ordenadamente.',
            'b' => 'diferentes partes do texto e ordená-las depois.',

        ],

    ],

    '33' => [

        'text' => 'Quando tenho que trabalhar em um projeto em grupo, eu prefiro que se faça primeiro',
        'options' => [

            'a' => 'um debate (brainstorming) em grupo, onde todos contribuem com idéias.',
            'b' => 'um brainstorming individual, seguido de reunião do grupo para comparar idéias.',

        ],

    ],

    '34' => [

        'text' => 'Considero um elogio chamar alguém de',
        'options' => [

            'a' => 'sensível.',
            'b' => 'imaginativo.',

        ],

    ],

    '35' => [

        'text' => 'Das pessoas que conheço em uma festa, provavelmente eu me recordo melhor',
        'options' => [

            'a' => 'de sua aparência.',
            'b' => 'do que elas disseram de si mesmas.',

        ],

    ],

    '36' => [

        'text' => 'Quando estou aprendendo um assunto novo, eu prefiro',
        'options' => [

            'a' => 'concentrar-me no assunto, aprendendo o máximo possível.',
            'b' => 'tentar estabelecer conexões entre o assunto e outros com ele relacionados.',

        ],

    ],

    '37' => [

        'text' => 'Mais provavelmente sou considerado(a)',
        'options' => [

            'a' => 'expansivo(a).',
            'b' => 'reservado(a).',

        ],

    ],

    '38' => [

        'text' => 'Prefiro disciplinas que enfatizam',
        'options' => [

            'a' => 'material concreto (fatos, dados).',
            'b' => 'material abstrato (conceitos, teorias).',

        ],

    ],

    '39' => [

        'text' => 'Para entretenimento, eu prefiro',
        'options' => [

            'a' => 'assistir televisão.',
            'b' => 'ler um livro.',

        ],

    ],

    '40' => [

        'text' => 'Alguns professores iniciam suas preleções com um resumo do que irão cobrir. Tais resumos são',
        'options' => [

            'a' => 'de alguma utilidade para mim.',
            'b' => 'muito úteis para mim.',

        ],

    ],

    '41' => [

        'text' => 'A idéia de fazer o trabalho de casa em grupo, com a mesma nota para todos do grupo,',
        'options' => [

            'a' => 'me agrada.',
            'b' => 'não me agrada.',

        ],

    ],

    '42' => [

        'text' => 'Quando estou fazendo cálculo longos',
        'options' => [

            'a' => 'tendo a repetir todos os passos e conferir meu trabalho cuidadosamente.',
            'b' => 'acho cansativo conferir o meu trabalho e tenho que me esforçar para fazê-lo.',

        ],

    ],

    '43' => [

        'text' => 'Tendo a descrever os lugares onde estive',
        'options' => [

            'a' => 'com facilidade e com bom detalhamento.',
            'b' => 'com dificuldade e sem detalhamento.',

        ],

    ],

    '44' => [

        'text' => 'Quando estou resolvendo problemas em grupo, mais provavelmente eu',
        'options' => [

            'a' => 'penso nas etapas do processo de solução.',
            'b' => 'penso nas possíveis conseqüências, ou sobre a aplicações da solução para uma ampla faixa de áreas.',

        ],

    ],

];