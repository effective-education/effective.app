<?php

    return [

        'email' => 'Email',
        'none' => 'Nenhuma',
        'name' => 'Nome',
        'all_rights_reserved' => 'Todos os direitos reservados',
        'whoops' => 'Opa',
        'hello' => 'Olá',
        'regards' => 'Saudações',
        'active' => 'Ativo',
        'action' => 'Ação',
        'edit' => 'Editar',
        'yes' => 'Sim',
        'no' => 'Não',
        'save' => 'Salvar',
        'admin' => 'Admin',
        'collaborator' => 'Colaborador',
        'block' => 'Bloquear',
        'unblock' => 'Desbloquear',
        'users' => 'Usuários',
        'students' => 'Estudantes',
        'student' => 'Estudante',
        'create' => 'Criar',
        'role' => 'Função',
        'password' => 'Senha',
        'dashboard' => 'Dashboard',
        'lessons' => 'Aulas',
        'my_lessons' => 'Minhas Aulas',
        'title' => 'Título',
        'knowledge_area' => 'Área de conhecimento',
        'created_by' => 'Criada por',
        'description' => 'Descrição',
        'outro' => 'Mensagem de agradecimento',
        'video_type' => 'Tipo do vídeo',
        'youtube' => 'Youtube',
        'file' => 'Arquivo',
        'send_file' => 'Enviar arquivo',
        'video_url' => 'URL do video',
        'video_id_youtube' => 'ID do vídeo no Youtube',
        'questions' => 'Perguntas',
        'question' => 'Pergunta',
        'options' => 'Opções',

        'add_students' => 'Adicionar estudante',
        'add_lessons' => 'Adicionar aula',
        'classes' => 'Turmas',
        'all_classes' => 'Todas as turmas',
        'my_classes' => 'Minhas Turmas',
        'delete_class' => 'Deletar turma',


        'add_question' => 'Adicionar pergunta',
        'add_option' => 'Adicionar opção',
        'remove' => 'Remover',
        'is_right' => 'Certa?',
        'video_timestamp' => 'Tempo no vídeo',
        'update' => 'Atualizar',
        'video' => 'Video',
        'change_video' => 'Alterar vídeo',
        'cancel' => 'Cancelar',
        'change_questions' => 'Alterar perguntas',
        'youtube_video_id' => 'ID do vídeo no Youtube',
        'delete' => 'Deletar',
        'lesson_link' => 'Link da aula',
        'see_ils_form' => 'Ver formulário ILS',
        'ils_form' => 'Formulário ILS',
        'close' => 'Fechar',
        'close_classe' => 'Fechar Turma',
        'contents' => 'Conteúdos',
        'content' => 'Conteúdo',
        'add_content' => 'Adicionar conteúdo',
        'content_tags' => 'Tags de conteúdo',
        'add_tag' => 'Adicionar tag',
        'start_time' => 'Tempo inicial',
        'end_time' => 'Tempo final',
        'change_contents' => 'Alterar conteúdo',
        'category' => 'Categoria',
        'next' => 'Próximo',
        'age' => 'Idade',
        'birth' => 'Data de Nascimento',
        'birth_date' => 'Data de Nascimento',
        'gender' => 'Sexo',
        'male' => 'Masculino',
        'female' => 'Feminino',
        'race'=>'Etnia',
        'college' => 'Universidade',
        'course' => 'Curso',
        'special_need' => 'Necessidade especial',
        'handicap' => 'Eu sou portador de necessidades especiais',
        'handicap_type' => 'Qual tipo de necessidade?',
        'handicap_custom_type' => 'Especifique sua necessidade especial',
        'details' => 'Detalhes',
        'student_profile' => 'Perfil de estudante',
        'learning_profile' => 'Perfil de aprendizado',
        'tries' => 'Tentativas',
        'try' => 'Tentativa',
        'hits' => 'Acertos',
        'total_questions' => 'Total de questões',
        'back' => 'Voltar',
        'handicap_types' => [

            1 => 'Cego',
            2 => 'Deficiência visual (Baixa Visão)',
            3 => 'Surdo/Mudo',
            4 => 'Deficiência auditiva (Perda Auditiva)',
            5 => 'Deficiência Motora',
            6 => 'Problemas de comunicação (Distúrbios da Fala e Linguagem)',
            7 => 'Outros. Qual ?',

        ],
        'musical_instrument' => 'Instrumento musical',
        'play_musical_instrument' => 'Eu toco algum instrumento musical',
        'which_instrument' => 'Qual instrumento você toca?',
        'introduction' => 'Introdução',
        'previous' => 'Anterior',
        'continue' => 'Continuar',
        'thanks_for_watching' => 'Obrigado por assistir a aula',
        'check_results_below' => 'Verifique seus resultados abaixo',
        'percentage_right' => 'Porcentagem de questões certas',
        'have_to_accept_camera' => 'Para continuar com a aula você deve permitir o uso da webcam. Atualize a página e permita o uso da câmera.',
        'impossible_initialize_lesson' => 'Impossível iniciar aula (erro no Affective)',
        'loading_lesson' => 'A aula está sendo carregada. Isso pode demorar alguns minutos',
        'results' => 'Resultados',

        'you_are_receiving_this_password_reset' => 'Você está recebendo este email porque nós recebemos uma requisição para resetar a senha de sua conta.',
        'if_you_did_not_request_password_reset' => 'Se você não pediu para resetar sua senha, você não precisa fazer mais nada.',
        'if_you_are_having_rouble_clicking' => 'Se você estiver tendo problemas em clicar no botão de ":actionText", copie e cole a URL abaixo em seu navegador: [:actionUrl](:actionUrl)',
        'delete_this_lesson' => 'Deseja deletar esta aula?',
        'separate_tags_by_comma' => 'Separe as tags por vírgula',
        'you_need_flash' => 'Você precisa do Flash Player 8+ e Javascript habilitado para ver este vídeo',

        'test' => [

            'step1' => [
                'info_email' => 'Informe seu e-mail para verificarmos se você já fez algum teste em nossa plataforma',
            ],

            'step2' => [
                'instructions' => 'Preencha todos os campos abaixo para completar seu registro antes do início da aula.',
            ],

            'step3' => [
                'instructions' => 'Preencha todos os campos abaixo para continuar com a aula.',
            ],
            'step5' => [
                'instructions' => 'Responda todas as perguntas abaixo antes de continuar',
            ]

        ],

        'metrics' => [

            'views' => 'quantidade de vezes que a aula foi assistida',
            'finalized' => 'quantidade de vezes que a aula foi finalizada',

        ],

        'internal_server_error' => 'Erro interno de servidor',
        'ils_result_error' => 'Responda todas as perguntas do formulário antes de continuar',
        'ils_question_empty' => 'Responda todas as perguntas antes de continuar',
        'ils_exists_already' => 'Você já preencheu este formulário uma vez. Tente iniciar a aula novamente para pular esta etapa.',
        'question_time' => 'Hora da pergunta',
        'error_loading_lesson' => 'Erro ao carregar aula. Por favor tente atualizar a página',
        'download' => 'Baixar',
        'download_last_frames' => 'Frames .csv',
        'download_last_frames_xml' => 'Frames .xml',
        'time' => 'Tempo',
        'joy' => 'Alegria',
        'fear' => 'Medo',
        'anger' => 'Raiva',
        'disgust' => 'Desgosto',
        'sadness' => 'Tristeza',
        'valence' => 'Valência',
        'contempt' => 'Desprezo',
        'surprise' => 'Surpresa',
        'engagement' => 'Compromisso',
        'during_video' => 'Durante vídeo',
        'see_impressions' => 'Ver impressões',
        'impressions' => 'Impressões',
        'see_all_lessons' => 'Ver todas as aulas',
        'all_lessons' => 'Todas as aulas',
        'see_my_lessons' => 'Ver minhas aulas',
        'manage_lessons' => 'Gerenciar aulas',
        'skip' => 'Pular Formulário',
        'brown'=>'Pardo',
        'black'=>'Negro',
        'white'=>'Branco',
        'indigenous' => 'Indígena',
        'all'=>'Todos',
        'loose'=>'Avulso',
    	'ok' => 'Ok',
        'register_self' => 'Cadastrar-se',

        'accessibility' => 'Acessibilidade',
        'map' => 'Mapa do Site',

        'language_portuguese' => 'Português',
        'language_spanish' => 'Espanhol',

        'go_to_map' => 'Mapa do Site',
        'go_to_home' => 'Ir para o Inicio',
        'go_to_menu' => 'Ir para o menu',
        'go_to_footer' => 'Ir para o Rodapé',
        'hight_contrast' => 'Alto contraste',
        'go_to_content' => 'Ir para o conteúdo',

        'manual' => 'Manual',
        'teacher' => 'Professor',

        'my_results' => 'Meus resultados',
        'graphics' => 'Gráficos',
        'statistic_module' => 'Estatísticas',


        'variation_of_emotions_per_lesson' => 'Variação de emoções',
        'average_chart_of_emotions_per_lesson' => 'Média de emoções',
        'hit_rate_graph_per_lesson' => 'Taxa de acertos',

        'select_a_lesson' => 'Selecione uma aula',
        'select_a_type_of_graphic' => 'Selecione o gráfico',
        'select_a_student' => 'Selecione um Estudante',
        'select_a_class' => 'Selecione uma Turma',


        'plot' => 'Mostrar Grafico',


        'variation_of_emotions_of_students_during_a_classroom' => 'Variação das Emoções de um Estudante durante uma Aula',
        'average_emotions' => 'Média das Emoções',
        'hit_hate' => 'Taxa de Acertos',


        'lesson_name' => 'Nome da Aula',
        'student_name' => 'Nome do Estudante',
        'class_name' => 'Nome da Turma',



        'by_student' => 'Por Estudante',
        'by_lesson' => 'Por Aula',
        'by_class' => 'Por Turma',


        'select_a_degree_of_schooling' => 'Selecione o grau de escolaridade',

        'degree_of_schooling' => 'Grau de escolaridade (cursando)',

        'degree' => [
            'fundamental' => 'Fundamental',
            'medium' => 'Médio',
            'higher' => 'Superior',
            'master' => 'Mestre',
            'doctor' => 'Doutor'
        ],


        'school' => 'Escola',
        'series_year' => 'Série/Ano',

        'select_your_handicap' => 'Selecione sua necessidade especial',

        'instrument' => [
            '1' => 'Cordas',
            '2' => 'Sopro',
            '3' => 'Percursão',
            '4' => 'Teclas',
        ],


        'kind_of_instrument' => 'Qual tipo de instrumento você toca?',
        'select_a_kind_of_instrument' => 'Selecione o tipo de instrumento',

        'end_pwd' => 'Você alterou sua senha com sucesso :)',
        'ready' => 'Prontinho!',

        'no_prints' => 'Sem impressões',


        'no_graphics' => 'Até o momento não temos resultados para plotar!',
        'no_statistics' => 'Até o momento ainda não temos resultados para análise!',


        'lessons_with_higher_and_lower_indexes_of_emotion' => 'Aulas com maior e menor indices de emoção',
        'student_with_higher_and_lower_index_of_emotion' => 'Aluno com maior e menor indices de emoção',
        'individual_per_student' => 'Individual por Aluno',
        'individual_per_lesson' => 'Individual por Aula',
        'lesson_upper_lower_for_each_emotion' => 'Aula com maior e menor indice para cada emoção',

        'select_a_emotion' => 'Selecione uma Emoção',


        'show' => 'Mostrar',


        'higher_index' => 'Maior índice',
        'lower_index' => 'Menor índice',
        'id' => 'Id',
        'views' => 'Visualizações',

        'average_emotion' => 'Média da Emoção',
        'average_student' => 'Média do Aluno',
        'class_average' => 'Média da Aula',
        'averages' => 'Médias',

        'watch' => 'Assistir',

        'details_about_the_student' => 'Detalhes sobre o Aluno',
        'details_about_the_lesson' => 'Detalhes sobre a Aula',

        'class_with_higher_index' => 'Aula com maior índice',
        'class_with_lower_index' => 'Aula com menor índice',

        'student_with_higher_index' => 'Aluno com maior índice',
        'student_with_lower_index' => 'Aluno com menor índice',


        'user_id' => 'Id de Usuário',
        'student_id' => 'Id de Estudante',
        'lesson_id' => 'Id da Aula',
        'number_of_classes_watched' => 'Qtd. Aulas assistidas',





        'lesson' => 'Aula',
        'evaluation' => 'Avaliação',

        'evidences' => 'Evidências',
        'add_evidence'=> 'Adicionar Evidência',

        'competences' => 'Competências',
        'competence' => 'Competência',
        'add_competence'=> 'Adicionar Competência',


        'abilities' => 'Habilidades',
        'ability' => 'Habilidade',
        'add_ability'=> 'Adicionar Habilidade',


        'knowledge_s' => 'Conhecimentos',
        'knowledge' => 'Conhecimento',
        'add_knowledge'=> 'Adicionar Conhecimento',



        'register_evaluation' => 'Incluir Avaliação',
        'learning_assessment' => 'Avaliação de Aprendizagem',

        'result_evaluation_by_student_lesson' => 'Resultado da Avaliação Individual de aluno/aula',
        'result_evaluation_by_lesson' => 'Resultado da Avaliação por aula',
        'results_of_evaluation' => 'Resultados da Avaliação',


        'developed' => 'Desenvolveu',
        'partially_developed' => 'Desenvolveu Parcialmente',
        'did_not_develop' => 'Não Desenvolveu',



        'change_abilities' => 'Alterar Habilidades',
        'change_knowledge_s' => 'Alterar Conhecimentos',
        'change_competencies' => 'Alterar Competências',





    ];
