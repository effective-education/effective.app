@extends('layouts.full')

@section('heading', Auth::user()->name))

@section('content')
    <div class="form-group">

        <p><label>@lang('strings.ready')</label></p>

        <!--<div v-html="student"></div>-->

        <div class="alert alert-success">
            <p>@lang('strings.end_pwd')</p>
        </div>

        <a href="{{route('dashboard.index')}}" class="btn btn-primary">@lang('strings.ok')</a>

    </div>
@endsection