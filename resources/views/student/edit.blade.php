@extends('layouts.app')

@section('heading', trans('strings.edit'))

@section('content')

    <form action="{{ route('student.update', [$student->uuid]) }}" method="POST" class="form" id="form">

        {{ method_field('PATCH') }}
        {{ csrf_field() }}

        <div class="form-group">
            <label>@lang('auth.email_address')</label>
            <input type="text" class="form-control" value="{{ $student->email }}" readonly>
        </div>

        <div class="form-group">
            <label>@lang('strings.name')</label>
            <input type="text" class="form-control" name="name" value="{{ old('name') ?? $student->name }}" required>
        </div>


        <div class="form-group">
            <label>@lang('strings.birth')</label>
            <input type="text" class="form-control" onkeypress="mascaraData(this)" name="birth_date"
                   value="{{ old('birth_date') ?? $data_nasc }}" required>
        </div>


        <div class="form-group">
            <label>@lang('strings.gender')</label>
            <select name="gender" class="form-control select2">
                @if($user->gender == 'M')
                    <option selected value="M">@lang('strings.male')</option>
                    <option value="F">@lang('strings.female')</option>
                @elseif($user->gender == 'F')
                    <option value="M">@lang('strings.male')</option>
                    <option selected value="F">@lang('strings.female')</option>
                @endif
            </select>
        </div>


        <div class="form-group">
            <label>@lang('strings.degree_of_schooling')</label>
            <select id="educational_level" name="educational_level" class="form-control select2"
                    style="width: 100%" onchange="educ_level(this.value)" required>
                {{--<option value="-1">Selecione o grau de escolaridade</option>--}}
                {{--<option value="">Selecione o grau de escolaridade</option>--}}

                @if($student->educational_level == 'elementary')
                    <option selected value="0">@lang('strings.degree.fundamental')</option>
                    <option value="1">@lang('strings.degree.medium')</option>
                    <option value="2">@lang('strings.degree.higher')</option>
                    <option value="3">@lang('strings.degree.master')</option>
                    <option value="4">@lang('strings.degree.doctor')</option>
                @elseif($student->educational_level == 'high school')
                    <option value="0">@lang('strings.degree.fundamental')</option>
                    <option selected value="1">@lang('strings.degree.medium')</option>
                    <option value="2">@lang('strings.degree.higher')</option>
                    <option value="3">@lang('strings.degree.master')</option>
                    <option value="4">@lang('strings.degree.doctor')</option>
                @elseif($student->educational_level == 'bachelor')
                    <option value="0">@lang('strings.degree.fundamental')</option>
                    <option value="1">@lang('strings.degree.medium')</option>
                    <option selected value="2">@lang('strings.degree.higher')</option>
                    <option value="3">@lang('strings.degree.master')</option>
                    <option value="4">@lang('strings.degree.doctor')</option>
                @elseif($student->educational_level == 'master')
                    <option value="0">@lang('strings.degree.fundamental')</option>
                    <option value="1">@lang('strings.degree.medium')</option>
                    <option value="2">@lang('strings.degree.higher')</option>
                    <option selected value="3">@lang('strings.degree.master')</option>
                    <option value="4">@lang('strings.degree.doctor')</option>
                @elseif($student->educational_level == 'doctor')
                    <option value="0">@lang('strings.degree.fundamental')</option>
                    <option value="1">@lang('strings.degree.medium')</option>
                    <option value="2">@lang('strings.degree.higher')</option>
                    <option value="3">@lang('strings.degree.master')</option>
                    <option selected value="4">@lang('strings.degree.doctor')</option>
                @else
                    <option value="">@lang('strings.select_a_degree_of_schooling')</option>
                    <option value="0">@lang('strings.degree.fundamental')</option>
                    <option value="1">@lang('strings.degree.medium')</option>
                    <option value="2">@lang('strings.degree.higher')</option>
                    <option value="3">@lang('strings.degree.master')</option>
                    <option value="4">@lang('strings.degree.doctor')</option>
                @endif
            </select>
        </div>


        <div class="form-group" id="if_college" style="margin-left: 1rem">
            <div class="form-group">
                @if($student->educational_level == 'elementary' || $student->educational_level == 'high school')
                    <label>@lang('strings.school')</label>
                @else
                    <label>@lang('strings.college')</label>
                @endif
                <input type="text" class="form-control" name="college_default"
                       value="{{ old('college') ?? $student->college }}" id="college_default" required>
            </div>

            <div class="form-group">
                @if($student->educational_level == 'elementary' || $student->educational_level == 'high school')
                    <label>@lang('strings.series_year')</label>
                @else
                    <label>@lang('strings.course')</label>
                @endif
                <input type="text" class="form-control" name="course_default"
                       value="{{ old('course') ?? $student->course }}" id="course_default" required>
            </div>
        </div>


        <div class="form-group" id="college" style="display: none; margin-left: 1rem">
            <div class="form-group">
                <label>@lang('strings.school')</label>
                <input type="text" class="form-control" name="col_name" id="col_name">
            </div>

            <div class="form-group">
                <label>@lang('strings.series_year')</label>
                <input type="text" class="form-control" name="col_serie" id="col_serie">
            </div>
        </div>


        <div class="form-group" id="university" style="display: none; margin-left: 1rem">
            <div class="form-group">
                <label>@lang('strings.college')</label>
                <input type="text" class="form-control" name="uni_name" id="uni_name">
            </div>

            <div class="form-group">
                <label>@lang('strings.course')</label>
                <input type="text" class="form-control" name="uni_course" id="uni_course">
            </div>
        </div>


        <div class="form-group">
            <div class="checkbox" id="check_handicap">
                <label>
                    @if($has_disability)
                        <input type="checkbox" id="if_v_hand" checked onchange="if_handicap_js(this.value)">
                    @else
                        <input type="checkbox" id="v_hand" onchange="handicap_js(this.value)">
                    @endif
                    @lang('strings.handicap')
                </label>
            </div>
        </div>


        @if($has_disability)
            <div class="form-group" id="if_values_of_handicap" style=" margin-left: 1rem">
                <label>@lang('strings.handicap_type')</label>
                <select id="if_opt_handicap" name="default_handicap" class="form-control select2"
                        style="width: 100%" onchange="other_disability(this.value)">
                    @if($student->handicap == 'blind')
                        <option selected value="blind">@lang('strings.handicap_types.1')</option>
                        <option value="visual">@lang('strings.handicap_types.2')</option>
                        <option value="deaf_mute">@lang('strings.handicap_types.3')</option>
                        <option value="auditory">@lang('strings.handicap_types.4')</option>
                        <option value="motor">@lang('strings.handicap_types.5')</option>
                        <option value="comunication">@lang('strings.handicap_types.6')</option>
                        <option value="other">@lang('strings.handicap_types.7')</option>
                    @elseif($student->handicap == 'visual')
                        <option value="blind">@lang('strings.handicap_types.1')</option>
                        <option selected value="visual">@lang('strings.handicap_types.2')</option>
                        <option value="deaf_mute">@lang('strings.handicap_types.3')</option>
                        <option value="auditory">@lang('strings.handicap_types.4')</option>
                        <option value="motor">@lang('strings.handicap_types.5')</option>
                        <option value="comunication">@lang('strings.handicap_types.6')</option>
                        <option value="other">@lang('strings.handicap_types.7')</option>
                    @elseif($student->handicap == 'deaf_mute')
                        <option value="blind">@lang('strings.handicap_types.1')</option>
                        <option value="visual">@lang('strings.handicap_types.2')</option>
                        <option selected value="deaf_mute">@lang('strings.handicap_types.3')</option>
                        <option value="auditory">@lang('strings.handicap_types.4')</option>
                        <option value="motor">@lang('strings.handicap_types.5')</option>
                        <option value="comunication">@lang('strings.handicap_types.6')</option>
                        <option value="other">@lang('strings.handicap_types.7')</option>
                    @elseif($student->handicap == 'auditory')
                        <option value="blind">@lang('strings.handicap_types.1')</option>
                        <option value="visual">@lang('strings.handicap_types.2')</option>
                        <option value="deaf_mute">@lang('strings.handicap_types.3')</option>
                        <option selected value="auditory">@lang('strings.handicap_types.4')</option>
                        <option value="motor">@lang('strings.handicap_types.5')</option>
                        <option value="comunication">@lang('strings.handicap_types.6')</option>
                        <option value="other">@lang('strings.handicap_types.7')</option>
                    @elseif($student->handicap == 'motor')
                        <option value="blind">@lang('strings.handicap_types.1')</option>
                        <option value="visual">@lang('strings.handicap_types.2')</option>
                        <option value="deaf_mute">@lang('strings.handicap_types.3')</option>
                        <option value="auditory">@lang('strings.handicap_types.4')</option>
                        <option selected value="motor">@lang('strings.handicap_types.5')</option>
                        <option value="comunication">@lang('strings.handicap_types.6')</option>
                        <option value="other">@lang('strings.handicap_types.7')</option>
                    @elseif($student->handicap == 'comunication')
                        <option value="blind">@lang('strings.handicap_types.1')</option>
                        <option value="visual">@lang('strings.handicap_types.2')</option>
                        <option value="deaf_mute">@lang('strings.handicap_types.3')</option>
                        <option value="auditory">@lang('strings.handicap_types.4')</option>
                        <option value="motor">@lang('strings.handicap_types.5')</option>
                        <option selected value="comunication">@lang('strings.handicap_types.6')
                        </option>
                        <option value="other">@lang('strings.handicap_types.7')</option>
                    @elseif($has_disability)
                        <option value="blind">@lang('strings.handicap_types.1')</option>
                        <option value="visual">@lang('strings.handicap_types.2')</option>
                        <option value="deaf_mute">@lang('strings.handicap_types.3')</option>
                        <option value="auditory">@lang('strings.handicap_types.4')</option>
                        <option value="motor">@lang('strings.handicap_types.5')</option>
                        <option value="comunication">@lang('strings.handicap_types.6')</option>
                        <option selected value="other">@lang('strings.handicap_types.7')</option>
                    @endif

                </select>


                <div class="form-group" id="other_disability"
                     style=" @if(!$custom_handicap) display: none; @endif margin-top: 1rem">

                    <label>@lang('strings.handicap_custom_type')</label>
                    <input type="text" class="form-control"
                           value="{{ old('default_handicap_custom_type') ?? $handicap }}" id="if_ot_dis"
                           name="default_handicap_custom_type">

                </div>

            </div>
        @endif


        <div class="form-group" id="values_of_handicap" style="display: none; margin-left: 1rem">
            <label>@lang('strings.handicap_type')</label>
            <select id="opt_handicap" name="handicap" class="form-control select2"
                    style="width: 100%" onchange="other_disability(this.value)">
                <option value="">@lang('strings.select_your_handicap')</option>
                <option value="blind">@lang('strings.handicap_types.1')</option>
                <option value="visual">@lang('strings.handicap_types.2')</option>
                <option value="deaf_mute">@lang('strings.handicap_types.3')</option>
                <option value="auditory">@lang('strings.handicap_types.4')</option>
                <option value="motor">@lang('strings.handicap_types.5')</option>
                <option value="comunication">@lang('strings.handicap_types.6')</option>
                <option value="other">@lang('strings.handicap_types.7')</option>
            </select>

            <div class="form-group" id="other_disability" style="display: none; margin-top: 1rem">

                <label>@lang('strings.handicap_custom_type')</label>
                <input type="text" class="form-control" id="ot_dis" name="handicap_custom_type">

            </div>
        </div>


        <div class="form-group">
            <div class="checkbox" id="check_musical">
                <label>
                    @if($has_musical)
                        <input type="checkbox" checked id="if_v_musical" onchange="if_musical(this.value)">
                    @else
                        <input type="checkbox" id="v_musical" onchange="musical(this.value)">
                    @endif
                   @lang('strings.play_musical_instrument')
                </label>
            </div>
        </div>


        @if($has_musical)
            <div class="form-group" id="if_values_of_musical" style=" margin-left: 1rem">
                <label>@lang('strings.which_instrument')</label>
                <select id="if_opt_musical" name="default_musical" class="form-control select2"
                        style="width: 100%">
                    @if($musical == 'strings')
                        <option selected value="strings">@lang('strings.instrument.1')</option>
                        <option value="wind">@lang('strings.instrument.2')</option>
                        <option value="percursion">@lang('strings.instrument.3')</option>
                        <option value="keys">@lang('strings.instrument.4')</option>
                     @elseif($musical == 'wind')
                        <option value="strings">@lang('strings.instrument.1')</option>
                        <option selected value="wind">@lang('strings.instrument.2')</option>
                        <option value="percursion">@lang('strings.instrument.3')</option>
                        <option value="keys">@lang('strings.instrument.4')</option>
                     @elseif($musical == 'percursion')
                        <option value="strings">@lang('strings.instrument.1')</option>
                        <option value="wind">@lang('strings.instrument.2')</option>
                        <option selected value="percursion">@lang('strings.instrument.3')</option>
                        <option value="keys">@lang('strings.instrument.4')</option>
                     @elseif($musical == 'keys')
                        <option value="strings">@lang('strings.instrument.1')</option>
                        <option value="wind">@lang('strings.instrument.2')</option>
                        <option value="percursion">@lang('strings.instrument.3')</option>
                        <option selected value="keys">@lang('strings.instrument.4')</option>
                    @endif
                </select>
            </div>
        @endif


        <div class="form-group" id="values_of_musical" style="display: none; margin-left: 1rem">
            <label>@lang('strings.kind_of_instrument')</label>
            <select id="opt_musical" name="musical_instrument" class="form-control select2"
                    style="width: 100%">
                <option value="">@lang('strings.select_a_kind_of_instrument')</option>
                <option value="strings">@lang('strings.instrument.1')</option>
                <option value="wind">@lang('strings.instrument.2')</option>
                <option value="percursion">@lang('strings.instrument.3')</option>
                <option value="keys">@lang('strings.instrument.4')</option>
            </select>
        </div>


        <input type="submit" onclick="submete()" class="btn btn-block btn-success" value="@lang('strings.save')">

    </form>

    <br>

    <form action="{{ route('students.status', [$student->id]) }}" method="POST">

        {{ csrf_field() }}
        {{ method_field('PATCH') }}
        <input
                type="submit"
                class="btn btn-block {{ $student->isActive() ? 'btn-danger' : 'btn-primary' }}"
                value="{{ $student->isActive() ? trans('strings.block') : trans('strings.unblock') }}"
        >

    </form>


    <script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>


    <script type="text/javascript">
        function edu() {
            if (document.getElementById('educational_level').value == '') {
                //alert('Grau de Escolaridade está vazio!');
                $("#educational_level").focus();
                document.getElementById('educational_level').required = true;
                return false;
            } else {
                document.getElementById('educational_level').required = false;
                $("#form").submit(true);
                return true;
            }
        }
    </script>


    <script>
        function educ_default() {
            if (document.getElementById('educational_level').value > '0') {
                if (document.getElementById('college_default').value == '') {
                    document.getElementById('college_default').required = true;
                    return false;
                } else if (document.getElementById('course_default').value == '') {
                    document.getElementById('college_default').required = true;
                    return false;
                } else {
                    return true;
                }
            }
        }
    </script>


    <script>
        function educ_uni() {
            if (document.getElementById('educational_level').value > '1') {
                if (document.getElementById('uni_name').value == '') {
                    //alert('Nome da Universidade está vazio!');
                    //$("#uni_name").focus();
                    document.getElementById('uni_name').required = true;
                    return false;
                } else if (document.getElementById('uni_course').value == '') {
                    //alert('Nome do Curso está vazio!');
                    //$("#uni_course").focus();
                    document.getElementById('uni_course').required = true;
                    return false;
                } else {
                    return true;
                }
            }
        }
    </script>

    <script>
        function educ_col() {
            if (document.getElementById('educational_level').value < '2' && document.getElementById('educational_level').value >= '0') {
                if (document.getElementById('col_name').value == '') {
                    //alert('Nome da Escola está vazio!');
                    document.getElementById('col_name').required = true;
                    //$("#col_name").focus();
                    return false;
                } else if (document.getElementById('col_serie').value == '') {
                    //alert('Série/Ano está vazio!');
                    document.getElementById('col_serie').required = true;
                    //$("#col_serie").focus();
                    return false;
                } else {
                    return true;
                }
            }
        }
    </script>


    <script type="text/javascript">
        function submete() {

            $("#form").submit(function () {

                if (document.getElementById('educational_level').value > '1') {
                    return (educ_uni() || educ_default()) && disability_other();
                } else if (document.getElementById('educational_level').value < '2' && document.getElementById('educational_level').value >= '0') {
                    return (educ_col() || educ_default()) && disability_other();
                } else if (document.getElementById('educational_level').value == '') {
                    return edu();
                }
            });
        }
    </script>



    <script type="text/javascript">
        function educ_level(valor) {

            var col = document.getElementById('if_college');
            var uni = document.getElementById('if_university');

            if (valor > '1') {
                document.getElementById("university").style.display = "block";
                document.getElementById("college").style.display = "none";

                document.getElementById("if_college").style.display = "none";

                document.getElementById('uni_name').required = true;
                document.getElementById('uni_course').required = true;
                document.getElementById('col_name').required = false;
                document.getElementById('col_serie').required = false;
                document.getElementById('college_default').required = false;
                document.getElementById('course_default').required = false;


                document.getElementById('col_serie').value = '';
                document.getElementById('col_name').value = '';
                document.getElementById('uni_name').value = '';
                document.getElementById('uni_course').value = '';
                document.getElementById('college_default').value = '';
                document.getElementById('course_default').value = '';


                //col.parentNode.removeChild( col );
            } else if (valor < '2' && valor >= '0') {
                document.getElementById("university").style.display = "none";
                document.getElementById("college").style.display = "block";

                document.getElementById("if_college").style.display = "none";

                document.getElementById('uni_name').required = false;
                document.getElementById('uni_course').required = false;
                document.getElementById('col_name').required = true;
                document.getElementById('col_serie').required = true;
                document.getElementById('college_default').required = false;
                document.getElementById('course_default').required = false;


                document.getElementById('col_serie').value = '';
                document.getElementById('col_name').value = '';
                document.getElementById('uni_name').value = '';
                document.getElementById('uni_course').value = '';
                document.getElementById('college_default').value = '';
                document.getElementById('course_default').value = '';

            } else if (valor == '') {
                document.getElementById("college").style.display = "none";
                document.getElementById("university").style.display = "none";


                document.getElementById('educational_level').required = true;
                document.getElementById('uni_name').required = true;
                document.getElementById('uni_course').required = true;
                document.getElementById('col_name').required = true;
                document.getElementById('col_serie').required = true;
                document.getElementById('college_default').required = true;
                document.getElementById('course_default').required = true;

                document.getElementById('col_serie').value = '';
                document.getElementById('col_name').value = '';
                document.getElementById('uni_name').value = '';
                document.getElementById('uni_course').value = '';
                document.getElementById('college_default').value = '';
                document.getElementById('course_default').value = '';

            }
        }
    </script>

    <script>
        function other_disability(valor) {

            if (valor == 'other') {
                document.getElementById("other_disability").style.display = "block";
            } else {
                document.getElementById("other_disability").style.display = "none";
            }

        }
    </script>


    <script type="text/javascript">
        function handicap_js(valor) {

            if (document.getElementById("v_hand").checked) {
                document.getElementById("values_of_handicap").style.display = "block";
                document.getElementById("v_hand").checked = true;
            } else {
                document.getElementById("values_of_handicap").style.display = "none";
                document.getElementById("v_hand").checked = false;
                document.getElementById('opt_handicap').value = '';
            }

        }
    </script>


    <script type="text/javascript">
        function if_handicap_js(valor) {

            if (document.getElementById("if_v_hand").checked) {
                document.getElementById("if_values_of_handicap").style.display = "block";
                document.getElementById("if_v_hand").checked = true;
            } else {
                document.getElementById("if_values_of_handicap").style.display = "none";
                document.getElementById("if_v_hand").checked = false;
                document.getElementById('if_opt_handicap').value = '';
            }

        }
    </script>

    <script type="text/javascript">
        function disability_other() {


            if (document.getElementById('opt_handicap').value == 'other') {

                if (document.getElementById('ot_dis').value == '') {
                    document.getElementById('ot_dis').required = true;
                    return false;
                } else {
                    document.getElementById('ot_dis').required = false;
                    return true;
                }
            } else if (document.getElementById('if_opt_handicap').value == 'other') {

                if (document.getElementById('if_ot_dis').value == '') {
                    document.getElementById('if_ot_dis').required = true;
                    return false;
                } else {
                    document.getElementById('if_ot_dis').required = false;
                    return true;
                }
            } else {
                return true;
            }
        }
    </script>



    <script type="text/javascript">
        function musical(valor) {

            if (document.getElementById("v_musical").checked) {
                document.getElementById("values_of_musical").style.display = "block";
                document.getElementById("v_musical").checked = true;
            } else {
                document.getElementById("values_of_musical").style.display = "none";
                document.getElementById("v_musical").checked = false;
                document.getElementById('opt_musical').value = '';
            }

        }
    </script>

    <script type="text/javascript">
        function if_musical(valor) {

            if (document.getElementById("if_v_musical").checked) {
                document.getElementById("if_values_of_musical").style.display = "block";
                document.getElementById("if_v_musical").checked = true;
            } else {
                document.getElementById("if_values_of_musical").style.display = "none";
                document.getElementById("if_v_musical").checked = false;
                document.getElementById('if_opt_musical').value = '';
            }

        }
    </script>


    <script>
        function mascaraData(val) {
            var pass = val.value;
            var expr = /[0123456789]/;

            for (i = 0; i < pass.length; i++) {
                // charAt -> retorna o caractere posicionado no índice especificado
                var lchar = val.value.charAt(i);
                var nchar = val.value.charAt(i + 1);

                if (i == 0) {
                    // search -> retorna um valor inteiro, indicando a posição do inicio da primeira
                    // ocorrência de expReg dentro de instStr. Se nenhuma ocorrencia for encontrada o método retornara -1
                    // instStr.search(expReg);
                    if ((lchar.search(expr) != 0) || (lchar > 3)) {
                        val.value = "";
                    }

                } else if (i == 1) {

                    if (lchar.search(expr) != 0) {
                        // substring(indice1,indice2)
                        // indice1, indice2 -> será usado para delimitar a string
                        var tst1 = val.value.substring(0, (i));
                        val.value = tst1;
                        continue;
                    }

                    if ((nchar != '/') && (nchar != '')) {
                        var tst1 = val.value.substring(0, (i) + 1);

                        if (nchar.search(expr) != 0)
                            var tst2 = val.value.substring(i + 2, pass.length);
                        else
                            var tst2 = val.value.substring(i + 1, pass.length);

                        val.value = tst1 + '/' + tst2;
                    }

                } else if (i == 4) {

                    if (lchar.search(expr) != 0) {
                        var tst1 = val.value.substring(0, (i));
                        val.value = tst1;
                        continue;
                    }

                    if ((nchar != '/') && (nchar != '')) {
                        var tst1 = val.value.substring(0, (i) + 1);

                        if (nchar.search(expr) != 0)
                            var tst2 = val.value.substring(i + 2, pass.length);
                        else
                            var tst2 = val.value.substring(i + 1, pass.length);

                        val.value = tst1 + '/' + tst2;
                    }
                }

                if (i >= 6) {
                    if (lchar.search(expr) != 0) {
                        var tst1 = val.value.substring(0, (i));
                        val.value = tst1;
                    }
                }
            }

            if (pass.length > 10)
                val.value = val.value.substring(0, 9);
            return true;
        }
    </script>

@endsection