@extends('layouts.full')

@section('heading', Auth::user()->name))

@section('content')
    <form method="POST" action="{{ route('change.password') }}" class="form">

        {{ csrf_field() }}

        <div class="form-group">
            <label>@lang('auth.new_password')</label>
            <input type="password" name="password" class="form-control">
        </div>

        <div class="form-group">
            <label>@lang('auth.confirm_password')</label>
            <input type="password" name="confpwd" class="form-control">
        </div>

        <button class="btn btn-primary btn-block">@lang('auth.change_password')</button>
    </form>

@endsection
