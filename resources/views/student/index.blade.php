@extends('layouts.app')

@section('heading', trans('strings.students'))

@section('actions')
    <a href="{{ route('student.create') }}" class="btn btn-sm btn-success">@lang('strings.create')</a>
@endsection

@section('content')

    @if(! $students->isEmpty())

        <div class="table-responsive">

            <table class="table">

                <thead>
                <tr>
                    <th>@lang('strings.name')</th>
                    <th>@lang('auth.email_address')</th>
                    <th>@lang('strings.active')</th>
                    <th>@lang('strings.action')</th>
                </tr>
                </thead>

                <tbody>
                @foreach($students as $student)
                    @if($student->name != null or $student->name != '')
                    <tr>
                        <td>{{ $student->name }}</td>
                        <td>{{ $student->email }}</td>
                        <td>{{ $student->isActive() ? trans('strings.yes') : trans('strings.no') }}</td>
                        <td><a href="{{ route('student.edit', [$student->id]) }}">@lang('strings.edit')</a></td>
                    </tr>
                    @endif
                @endforeach
                </tbody>

            </table>

        </div>

    @else

        <div class="alert alert-warning">@lang('status.student.none')</div>

    @endif

@endsection