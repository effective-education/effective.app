@extends('layouts.app')

@section('heading', trans('strings.hit_rate_graph_per_lesson'))

@section('content')

    <label>Certas</label>
    {{$certas}}<br>
    <label>Erradas</label>
    {{$erradas}}<br>
    <label>Total</label>
    {{$total}}<br>



    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script type="application/javascript" src="{{ asset('js/graphic/highcharts.js') }}"></script>

    <div id="container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>



<script>

    Highcharts.chart('container', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Taxa de acertos'
        },
        subtitle: {
            text: '{{$subtitle}}'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Taxa',
            colorByPoint: true,
            data: [{
                name: 'Certas',
                y: {{$certas}},
                sliced: true,
                selected: true
            }, {
                name: 'Erradas',
                y: {{$erradas}}
            }]
        }]
    });
</script>







    {{--<div id="piechart" style="width: 900px; height: 500px;"></div>--}}




    {{--<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>--}}
    {{--<script type="text/javascript">--}}
        {{--google.charts.load('current', {'packages':['corechart']});--}}
        {{--google.charts.setOnLoadCallback(drawChart);--}}

        {{--function drawChart() {--}}

            {{--var data = google.visualization.arrayToDataTable([--}}
                {{--['Resultado', 'Quantidade'],--}}
                {{--['Certas',     {{$certas}}],--}}
                {{--['Erradas',      {{$erradas}}]--}}
            {{--]);--}}

            {{--var options = {--}}
                {{--title: 'Taxa de Acertos'--}}
            {{--};--}}

            {{--var chart = new google.visualization.PieChart(document.getElementById('piechart'));--}}

            {{--chart.draw(data, options);--}}
        {{--}--}}
    {{--</script>--}}



@endsection