@extends('layouts.app')

@section('heading', trans('strings.my_results'))

@section('content')

    <form method="POST" action="{{ route('plot.student') }}" class="form">

        {{ csrf_field() }}

        @if(! $lessons->isEmpty())
            <h5>@lang('strings.select_a_lesson')</h5>

            <div class="form-group">
                <select id="framework" name="lesson_id" class="form-control select2"
                        style="width: 100%" onchange="plot(this.value)" required>
                    <option value="0">Selecione uma aula</option>
                    @foreach($lessons as $lesson)
                        <option value="{{ $lesson->id }}">
                            {{ $lesson->title }}
                        </option>
                    @endforeach
                </select>
            </div>


            <div id="graficos" style="display: none">
                <h5>@lang('strings.select_a_type_of_graphic')</h5>

                <div class="form-group">
                    <select id="framework" name="type" class="form-control select2"
                            style="width: 100%" required>
                        <option value="1">@lang('strings.variation_of_emotions_per_lesson')</option>
                        <option value="2">@lang('strings.average_chart_of_emotions_per_lesson')</option>
                        <option value="3">@lang('strings.hit_rate_graph_per_lesson')</option>

                    </select>
                </div>

                <input type="submit" value="@lang('strings.plot')" class="btn btn-block btn-success">

            </div>
        @else

            <div class="alert alert-warning">@lang('status.lessons.none')</div>

        @endif


    </form>



    <script type="text/javascript">
        function plot(valor) {
            if (valor != "0") {
                document.getElementById("graficos").style.display = "block";
            } else
                document.getElementById("graficos").style.display = "none";
        }
    </script>


@endsection
