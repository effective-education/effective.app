@extends('layouts.app')

@section('heading', trans('strings.average_chart_of_emotions_per_lesson'))

@section('content')


    {{--Verifica se teve algum resultado--}}
    @if($subtitle)

        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/modules/export-data.js"></script>
        <script type="application/javascript" src="{{ asset('js/graphic/highcharts.js') }}"></script>


        <script>
            Highcharts.chart('container', {

                chart: {
                    type: 'bar'
                },
                title: {
                    text: 'Média de emoções'
                },
                subtitle: {
                    text: '{{$subtitle}}'
                },
                xAxis: {
                    categories: ['Alegria 😄', 'Medo 😱', 'Raiva 😡', 'Nojo 😖', 'Tristeza 😞', 'Desprezo 🙄', 'Surpresa 😲'],
                    title: {
                        text: 'Emoções'
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: '[ 0 ~ 100 ]',
                        verticalAlign: 'middle'

                    },
                    labels: {
                        overflow: 'justify'
                    }
                },
                tooltip: {
                    valueSuffix: null
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'top',
                    x: -40,
                    y: 80,
                    floating: true,
                    borderWidth: 1,
                    backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                    shadow: true,
                },
                credits: {
                    enabled: false
                },
                series: [{
                    name: 'Média ao longo da aula',
                    data: {{$str_Media}}
                }]
            });

        </script>


        <div id="container" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>

    @else

        <div class="alert alert-warning">@lang('strings.no_graphics')</div>
        <div align="center">
            <img height="40em" src="{{url('images/dot2.png')}}">
        </div>

    @endif

@endsection