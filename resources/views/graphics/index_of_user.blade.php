@extends('layouts.app')

@section('heading', trans('strings.graphics'))

@section('content')


    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <label class="panel-title">
                    <a style="text-decoration: none" class="accordion-toggle" data-toggle="collapse"
                       data-parent="#accordion" href="#collapseOne">
                        {{--<i class="glyphicon glyphicon-cloud"></i>--}}
                        <i class="fas fa-chart-line" ></i>
                        @lang('strings.variation_of_emotions_of_students_during_a_classroom')
                    </a>
                </label>
            </div>
            <div id="collapseOne" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="section">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="control-label">@lang('strings.student_name')</label>
                                <select id="student_v2" name="" class="form-control select2"
                                        style="width: 100%">
                                    <option value="">@lang('strings.select_a_student')</option>
                                    @foreach($students as $student)
                                        <option value="{{$student->id}}">{{$student->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label class="control-label">@lang('strings.lesson_name')</label>
                                <select id="session_v2" name="" class="form-control select2"
                                        style="width: 100%">
                                    <option value="">@lang('strings.select_a_lesson')</option>
                                </select>
                            </div>

                            <div class="">
                                <button id="plotGraphicBtn_v2" type="button" name="button" class="btn btn-success"
                                        onclick="plotGraphic_v2()">@lang('strings.plot')
                                </button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <label class="panel-title">
                    <a style="text-decoration: none" class="accordion-toggle" data-toggle="collapse"
                       data-parent="#accordion" href="#collapseTwo">
                        <i class="fas fa-chart-bar" ></i>
                       @lang('strings.average_emotions')
                    </a>
                </label>
            </div>

            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="section">

                        {{--Por Estudante--}}
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <label class="panel-title">
                                    <a style="text-decoration: none" class="accordion-toggle" data-toggle="collapse"
                                       data-parent="#collapseTwo" href="#collapseTwoOne">
                                        <i class="fas fa-user" ></i>
                                        @lang('strings.by_student')
                                    </a>
                                </label>
                            </div>

                            <div id="collapseTwoOne" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="section">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="control-label">@lang('strings.student_name')</label>
                                                <select id="student_v3" name="" class="form-control select2"
                                                        style="width: 100%">
                                                    <option value="">@lang('strings.select_a_student')</option>
                                                    @foreach($students as $student)
                                                        <option value="{{$student->id}}">{{$student->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="">
                                                <button id="plotGraphicBtn_v3" type="button" name="button"
                                                        class="btn btn-success" onclick="plotGraphic_v3()">
                                                    @lang('strings.plot')
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{--Por aula--}}
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <label class="panel-title">
                                    <a style="text-decoration: none" class="accordion-toggle" data-toggle="collapse"
                                       data-parent="#collapseTwo" href="#collapseTwoTwo">
                                        <i class="fab fa-youtube" ></i>
                                        @lang('strings.by_lesson')
                                    </a>
                                </label>
                            </div>

                            <div id="collapseTwoTwo" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="section">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="control-label"> @lang('strings.lesson_name') </label>
                                                <select id="lesson_v2" name="" class="form-control select2"
                                                        style="width: 100%">
                                                    <option value="">@lang('strings.select_a_lesson')</option>
                                                    @foreach($lessons as $lesson)
                                                        <option value="{{$lesson->id}}">{{$lesson->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="">
                                                <button id="plotGraphicBtn_v4" type="button" name="button"
                                                        class="btn btn-success" onclick="plotGraphic_v4()">
                                                    @lang('strings.plot')
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{--Por turma--}}
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <label class="panel-title">
                                    <a style="text-decoration: none" class="accordion-toggle" data-toggle="collapse"
                                       data-parent="#collapseTwo" href="#collapseTwoThree">
                                        <i class="fas fa-users" ></i>
                                        @lang('strings.by_class')
                                    </a>
                                </label>
                            </div>

                            <div id="collapseTwoThree" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="section">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="control-label">@lang('strings.class_name')</label>
                                                <select id="class" name="" class="form-control select2"
                                                        style="width: 100%">
                                                    <option value="">@lang('strings.select_a_class')</option>
                                                    @foreach($classes as $classe)
                                                        <option value="{{$classe->id}}">{{$classe->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="">
                                                <button id="plotGraphicBtn_v5" type="button" name="button"
                                                        class="btn btn-success" onclick="plotGraphic_v5()">
                                                    @lang('strings.plot')
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

             </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <label class="panel-title">
                    <a style="text-decoration: none" class="accordion-toggle" data-toggle="collapse"
                       data-parent="#accordion" href="#collapseThree">
                        <i class="fas fa-chart-pie" ></i>
                        @lang('strings.hit_hate')
                    </a>
                </label>
            </div>

            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="section">

                        {{--Por Estudante--}}
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <label class="panel-title">
                                    <a style="text-decoration: none" class="accordion-toggle" data-toggle="collapse"
                                       data-parent="#collapseThree" href="#collapseThreeOne">
                                        <i class="fas fa-user" ></i>
                                        @lang('strings.by_student')
                                    </a>
                                </label>
                            </div>

                            <div id="collapseThreeOne" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="section">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="control-label"> @lang('strings.student_name')</label>
                                                <select id="student_v6" name="" class="form-control select2"
                                                        style="width: 100%">
                                                    <option value=""> @lang('strings.select_a_student')</option>
                                                    @foreach($students as $student)
                                                        <option value="{{$student->id}}">{{$student->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="">
                                                <button id="plotGraphicBtn_v6" type="button" name="button"
                                                        class="btn btn-success" onclick="plotGraphic_v6()">
                                                    @lang('strings.plot')
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{--Por aula--}}
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <label class="panel-title">
                                    <a style="text-decoration: none" class="accordion-toggle" data-toggle="collapse"
                                       data-parent="#collapseThree" href="#collapseThreeTwo">
                                        <i class="fab fa-youtube" ></i>
                                        @lang('strings.by_lesson')
                                    </a>
                                </label>
                            </div>

                            <div id="collapseThreeTwo" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="section">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="control-label"> @lang('strings.lesson_name')</label>
                                                <select id="lesson_v7" name="" class="form-control select2"
                                                        style="width: 100%">
                                                    <option value=""> @lang('strings.select_a_lesson')</option>
                                                    @foreach($lessons as $lesson)
                                                        <option value="{{$lesson->id}}">{{$lesson->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="">
                                                <button id="plotGraphicBtn_v7" type="button" name="button"
                                                        class="btn btn-success" onclick="plotGraphic_v7()">
                                                    @lang('strings.plot')
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{--Por turma--}}
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <label class="panel-title">
                                    <a style="text-decoration: none" class="accordion-toggle" data-toggle="collapse"
                                       data-parent="#collapseThree" href="#collapseThreeThree">
                                        <i class="fas fa-users" ></i>
                                        @lang('strings.by_class')
                                    </a>
                                </label>
                            </div>

                            <div id="collapseThreeThree" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="section">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="control-label"> @lang('strings.class_name')</label>
                                                <select id="class_v8" name="" class="form-control select2"
                                                        style="width: 100%">
                                                    <option value=""> @lang('strings.select_a_class')</option>
                                                    @foreach($classes as $classe)
                                                        <option value="{{$classe->id}}">{{$classe->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="">
                                                <button id="plotGraphicBtn_v8" type="button" name="button"
                                                        class="btn btn-success" onclick="plotGraphic_v8()">
                                                    @lang('strings.plot')
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>



    </div>




@endsection

@push('after-scripts')
    <script type="application/javascript" src="{{ asset('js/graphic/data_graph.js') }}"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">

    {{--<script type="application/javascript" src="{{ asset('js/graphic/ofuscated.js') }}"></script>--}}
@endpush