@extends('layouts.app')

@section('heading', trans('strings.variation_of_emotions_per_lesson'))

@section('content')


    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/series-label.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script type="application/javascript" src="{{ asset('js/graphic/highcharts.js') }}"></script>
    <script>

        Highcharts.chart('container', {

            title: {
                text: 'Variação de emoções'
            },

            subtitle: {
                text: '{{$subtitle}}'
            },

            yAxis: {
                title: {
                    text: 'Ranking'
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },

            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: true,
                    },
                    pointStart: 0,
                }
            },

            series: [{
                name: 'Alegria 😄 ',
                data: {{$str_Joy}}
            }, {
                name: 'Medo 😱',
                data: {{$str_Fear}}
            }, {
                name: 'Raiva 😡',
                data: {{$str_Anger}}
            }, {
                name: 'Nojo 😖',
                data: {{$str_Disgust}}
            }, {
                name: 'Tristeza 😞',
                data: {{$str_Sadness}}
            }, {
                name: 'Desprezo 🙄',
                data: {{$str_Contempt}}
            }, {
                name: 'Surpresa 😲',
                data: {{$str_Surprise}}
            }],

            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }

        });
    </script>




    <style type="text/css">
        #container {
            min-width: 310px;
            max-width: 800px;
            height: 400px;
            margin: 0 auto
        }
    </style>



    <div id="container"></div>


@endsection

