@extends('layouts.full')

@section('heading', $lesson->title)

@section('right_content')
    <div id="affectiva-webcam-frame"></div>
@endsection

@section('content')

    @if(Auth::check())
        @if((Auth::user()->isStudent()))
            {{! $email = Auth::user()->email}}
            <div id="test">

                <test email="{{$email}}"></test>

            </div>
        @endif
    @else
        <div id="test">
            <test></test>
        </div>
    @endif

@endsection

@push('before-scripts')

    <script>

        function onYouTubeIframeAPIReady() {

            window.YoutubePlayer = YT;

            console.log('Youtube Iframe API loaded');

        }

    </script>


    {{--<script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>--}}
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous">
    </script>
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">--}}
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://cdn.pubnub.com/sdk/javascript/pubnub.4.8.0.js"></script>
    {{--<link href='http://fonts.googleapis.com/css?family=Cinzel' rel='stylesheet' type='text/css'>--}}

    <script src="https://www.youtube.com/iframe_api"></script>
    <script type="application/javascript" src="{{ asset('js/affectiva/affdex.js')}}"></script>
    <script type="application/javascript" src="{{ asset('js/affectiva/affective.js')}}"></script>
    {{--<script type="text/javascript" src="{{ asset('js/API_YouTube.js') }}"></script>--}}


        {{--Originais--}}
    {{--<script src="https://www.youtube.com/iframe_api"></script>--}}
    {{--<script src="https://download.affectiva.com/js/3.2/affdex.js"></script>--}}


@endpush

@push('after-scripts')

    <script>

        window.onbeforeunload = function (e) {

            e = e || window.event;

            if (e) e.returnValue = 'confirm';

            return 'confirm';

        };

    </script>

    <script type="application/javascript" src="{{ asset('js/show-test.js') }}"></script>

@endpush