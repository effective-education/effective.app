@extends('layouts.app')

@section('heading', trans('strings.map'))

@section('content')

    <a style="align-items: center;  display: flex;  flex-direction: row;  flex-wrap: wrap;  justify-content: center;">
        <img height="100px" src="{{url('images/logo.png')}}">
    </a>

    <div class="page-content" id="map">
        <div class="row">

            <!--Right Side-->
            <div class="col-sm-5">
                <ul>
                    <li class="menu">
                        <a href="{{route('dashboard.index')}}">
                            {{config('app.name')}}
                        </a>
                        <ul>
                            <li class="sub_menu">
                                <a href="{{ route('lessons.all') }}">@lang('strings.see_all_lessons')</a>
                            </li>
                            @if(Auth::check())
                                @if(Auth::user()->isStudent())
                                    <li class="sub_menu">
                                        <a href="{{ route('lessons') }}">@lang('strings.see_my_lessons')</a>
                                    </li>

                                    <li class="menu">
                                        <a href="{{ route('graphics_of_student') }}">@lang('strings.graphics')</a>
                                    </li>


                                @endif
                            @endif

                        </ul>

                    </li>

                    @if(Auth::check())
                        @if(Auth::user()->isAdmin())
                            <li class="menu">
                                <a href="{{ route('users.index') }}">@lang('strings.users')</a>
                                <ul>
                                    <li class="sub_menu">
                                        <a href="{{ route('users.create') }}">@lang('strings.create')</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu">
                                <a href="{{ route('classes.index') }}">@lang('strings.classes')</a>
                                <ul>
                                    <li class="sub_menu">
                                        <a href="{{ route('classes.create') }}">@lang('strings.create')</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu">
                                <a href="{{ route('lessons.index') }}">@lang('strings.lessons')</a>
                                <ul>
                                    <li class="sub_menu">
                                        <a href="{{ route('lessons.create') }}">@lang('strings.create')</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="menu">
                                <a href="{{ route('graphics_of_user') }}">@lang('strings.graphics')</a>
                            </li>

                            <li class="menu">
                                <a href="{{ route('statistic_module') }}">@lang('strings.statistic_module')</a>
                            </li>


                        @endif

                        @if(Auth::user()->isCollaborator())
                            <li class="menu">
                                <a href="{{ route('classes.index') }}">@lang('strings.classes')</a>
                                <ul>
                                    <li class="sub_menu">
                                        <a href="{{ route('classes.create') }}">@lang('strings.create')</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu">
                                <a href="{{ route('lessons.index') }}">@lang('strings.lessons')</a>
                                <ul>
                                    <li class="sub_menu">
                                        <a href="{{ route('lessons.create') }}">@lang('strings.create')</a>
                                    </li>
                                </ul>
                            </li>

                                <li class="menu">
                                    <a href="{{ route('graphics_of_user') }}">@lang('strings.graphics')</a>
                                </li>

                                <li class="menu">
                                    <a href="{{ route('statistic_module') }}">@lang('strings.statistic_module')</a>
                                </li>

                        @endif

                    @else
                        <li class="menu">
                            <a href="{{ route('student.register') }}">@lang('strings.register_self')</a>
                        </li>
                    @endif

                </ul>
            </div>

            <!-- Center-->

            <div class="col-sm-4">

                <ul>
                    <li class="menu">
                        <a href="{{route('map.index')}}">
                            @lang('strings.go_to_map')<br>
                        </a>
                    </li>
                </ul>
                <ul>
                    <li class="menu">
                        <a href="{{route('accessibility.index')}}">
                            @lang('footer.accessibility')<br>
                        </a>
                    </li>
                </ul>
                <ul>
                    <li class="menu">
                        <a href="mailto:carlamarina@gmail.com?subject=Contato%20Acacia">
                            @lang('footer.contact')<br>
                        </a>
                    </li>
                </ul>

            </div>

            <!-- Right Side-->

            <div class="col-sm-3">
                <ul>
                    <li class="menu">
                        <a href="{{route('setlocale', 'pt')}}">
                            @lang('strings.language_portuguese')<br>
                        </a>
                    </li>
                </ul>

                <ul>
                    <li class="menu">
                        <a href="{{route('setlocale', 'es')}}">
                            @lang('strings.language_spanish')<br>
                        </a>
                    </li>
                </ul>
            </div>

        </div>

    </div>

    <link rel="stylesheet" href="{{ asset('css/map.css') }}">


    {{--.sitemap-menu li li > a::before {
          border-left: 2px solid #6b8894;
          border-top: 2px solid green;
          }--}}


@endsection
