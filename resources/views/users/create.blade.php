@extends('layouts.app')

@section('heading', trans('strings.create'))

@section('content')

    <form method="POST" action="{{ route('users.store') }}" class="form">

        {{ csrf_field() }}

        <div class="form-group">
            <label>@lang('strings.name')</label>
            <input type="text" name="name" class="form-control">
        </div>

        <div class="form-group">
            <label>@lang('strings.birth')</label>
            <input type="text" name="birth_date" class="form-control">
        </div>

        <div class="form-group">
            <label>@lang('strings.gender')</label>
            <select name="gender" class="form-control">
                <option value="Male">@lang('strings.male')</option>
                <option value="Female">@lang('strings.female')</option>
            </select>
        </div>

        <div class="form-group">
            <label>@lang('auth.email_address')</label>
            <input type="email" name="email" class="form-control">
        </div>

        <div class="form-group">
            <label>@lang('strings.password')</label>
            <input type="text" name="password" value="{{ str_random() }}" class="form-control">
        </div>

        <div class="form-group">
            <label>@lang('strings.role')</label>
            <select name="role" class="form-control">
                <option value="{{ \App\Models\User::ADMIN }}">@lang('strings.admin')</option>
                <option value="{{ \App\Models\User::COLLABORATOR }}">@lang('strings.collaborator')</option>
            </select>
        </div>

        <input type="submit" value="@lang('strings.create')" class="btn btn-block btn-success">

    </form>

@endsection