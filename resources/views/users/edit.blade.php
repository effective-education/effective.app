@extends('layouts.app')

@section('heading', $user->name)

@section('content')

    <form action="{{ route('users.update', [$user->id]) }}" method="POST" class="form">

        {{ method_field('PATCH') }}
        {{ csrf_field() }}

        <div class="form-group">
            <label>@lang('auth.email_address')</label>
            <input type="text" class="form-control" value="{{ $user->email }}" readonly>
        </div>

        <div class="form-group">
            <label>@lang('strings.role')</label>
            <select name="role" class="form-control">
                <option value="{{ App\Models\User::ADMIN }}" {{ $user->isAdmin() ? 'selected' : '' }} >@lang('strings.admin')</option>
                <option value="{{ App\Models\User::COLLABORATOR }}" {{ $user->isCollaborator() ? 'selected' : '' }} >@lang('strings.collaborator')</option>
            </select>
        </div>

        <input type="submit" class="btn btn-block btn-success" value="@lang('strings.save')">

    </form>

    <br>

    <form action="{{ route('users.status', [$user->id]) }}" method="POST">

        {{ csrf_field() }}
        {{ method_field('PATCH') }}
        <input
                type="submit"
                class="btn btn-block {{ $user->isActive() ? 'btn-danger' : 'btn-primary' }}"
                value="{{ $user->isActive() ? trans('strings.block') : trans('strings.unblock') }}"
        >

    </form>

@endsection