@extends('layouts.app')

@section('heading', trans('strings.users'))

@section('actions')
    <a href="{{ route('users.create') }}" class="btn btn-sm btn-success">@lang('strings.create')</a>
@endsection

@section('content')

    @if(! $users->isEmpty())

        <div class="table-responsive">

            <table class="table">

                <thead>
                <tr>
                    <th>@lang('strings.name')</th>
                    <th>@lang('auth.email_address')</th>
                    <th>@lang('strings.active')</th>
                    <th>@lang('strings.action')</th>
                </tr>
                </thead>

                <tbody>
                @foreach($users as $user)
                    @if(!$user->isStudent())
                        <tr>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->isActive() ? trans('strings.yes') : trans('strings.no') }}</td>
                            <td><a href="{{ route('users.edit', [$user->id]) }}">@lang('strings.edit')</a></td>
                        </tr>
                    @endif
                @endforeach
                </tbody>

            </table>

        </div>

    @else

        <div class="alert alert-warning">@lang('status.user.none')</div>

    @endif

@endsection