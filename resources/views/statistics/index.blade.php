@extends('layouts.app')

@section('heading', trans('strings.statistic_module'))

@section('content')


    <div class="panel-group" id="accordion">

        {{--Maior e Menor Emoção--}}
        <div class="panel panel-default">
            <div class="panel-heading">
                <label class="panel-title">
                    <a style="text-decoration: none;" class="accordion-toggle" data-toggle="collapse"
                       data-parent="#accordion" href="#collapseOne">
                        {{--<i class="glyphicon glyphicon-cloud"></i>--}}

                        <i class="fa fa-video"></i>
                        @lang('strings.lessons_with_higher_and_lower_indexes_of_emotion')
                    </a>
                </label>
            </div>
            <div id="collapseOne" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="section">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label class="control-label">@lang('strings.select_a_emotion')</label>
                                <select id="emotion" name="" class="form-control select2"
                                        style="width: 100%">
                                    <option value="">@lang('strings.select_a_emotion')</option>
                                    <option value="joy">@lang('strings.joy')</option>
                                    <option value="fear">@lang('strings.fear')</option>
                                    <option value="anger">@lang('strings.anger')</option>
                                    <option value="disgust">@lang('strings.disgust')</option>
                                    <option value="sadness">@lang('strings.sadness')</option>
                                    <option value="contempt">@lang('strings.contempt')</option>
                                    <option value="surprise">@lang('strings.surprise')</option>

                                </select>
                            </div>


                            <div class="">
                                <button id="larger_smaller_search" type="button" name="button" class="btn btn-success"
                                        onclick="botaoBuscaMaiorMenorEmotion()">@lang('strings.show')
                                </button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>


            {{--Aluno com Maior e Menor Emoção numa Aula--}}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <label class="panel-title">
                        <a style="text-decoration: none;" class="accordion-toggle" data-toggle="collapse"
                           data-parent="#accordion" href="#collapseTwo">
                            {{--<i class="glyphicon glyphicon-cloud"></i>--}}
                            <i class="fa fa-users"></i>
                            @lang('strings.student_with_higher_and_lower_index_of_emotion')
                        </a>
                    </label>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="section">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <div class="col-lg-5">
                                        <div class="form-group">
                                            <label class="control-label">@lang('strings.select_a_emotion')</label>
                                            <select id="emotion_2" name="" class="form-control select2"
                                                    style="width: 100%">
                                                <option value="">@lang('strings.select_a_emotion')</option>
                                                <option value="joy">@lang('strings.joy')</option>
                                                <option value="fear">@lang('strings.fear')</option>
                                                <option value="anger">@lang('strings.anger')</option>
                                                <option value="disgust">@lang('strings.disgust')</option>
                                                <option value="sadness">@lang('strings.sadness')</option>
                                                <option value="contempt">@lang('strings.contempt')</option>
                                                <option value="surprise">@lang('strings.surprise')</option>

                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-7">

                                        <label class="control-label">@lang('strings.select_a_lesson')</label>
                                        <select id="lesson" name="" class="form-control select2"
                                                style="width: 100%">
                                            <option value="">@lang('strings.select_a_lesson')</option>

                                            @foreach($lessons as $lesson)
                                                <option value="{{$lesson->id}}">{{$lesson->title}}</option>
                                            @endforeach

                                        </select>
                                    </div><br>

                                    <div class="col-lg-12">
                                        <div class="">
                                            <button id="student_larger_smaller_emotion_search" type="button"
                                                    name="button"
                                                    class="btn btn-success"
                                                    onclick="botaoBuscaAlunoMaiorMenorEmotionAula()">@lang('strings.show')
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>


            </div>


            {{--individual por aluno--}}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <label class="panel-title">
                        <a style="text-decoration: none;" class="accordion-toggle" data-toggle="collapse"
                           data-parent="#accordion" href="#collapseThree">
                            {{--<i class="glyphicon glyphicon-cloud"></i>--}}

                            <i class="fas fa-user-circle"></i>
                            @lang('strings.individual_per_student')
                        </a>
                    </label>
                </div>
                <div id="collapseThree" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="section">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <div class="form-group">
                                        <label class="control-label">@lang('strings.select_a_student')</label>
                                        <select id="student" name="" class="form-control select2"
                                                style="width: 100%">
                                            <option value="">@lang('strings.select_a_student')</option>

                                            @foreach($students as $student)
                                                <option value="{{$student->id}}">{{$student->name}}</option>
                                            @endforeach

                                        </select>
                                    </div>


                                    <div class="">
                                        <button id="individual_student_search" type="button" name="button"
                                                class="btn btn-success"
                                                onclick="botaoIndividualAluno()">@lang('strings.show')
                                        </button>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>




        {{--individual por Aula--}}
        <div class="panel panel-default">
            <div class="panel-heading">
                <label class="panel-title">
                    <a style="text-decoration: none;" class="accordion-toggle" data-toggle="collapse"
                       data-parent="#accordion" href="#collapseSix">
                        {{--<i class="glyphicon glyphicon-cloud"></i>--}}

                        <i class="fas fa-play-circle"></i>
                        @lang('strings.individual_per_lesson')
                    </a>
                </label>
            </div>
            <div id="collapseSix" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="section">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <div class="form-group">
                                    <label class="control-label">@lang('strings.select_a_lesson')</label>
                                    <select id="lesson_3" name="" class="form-control select2"
                                            style="width: 100%">
                                        <option value="">@lang('strings.select_a_lesson')</option>

                                        @foreach($lessons as $lesson)
                                            <option value="{{$lesson->id}}">{{$lesson->title}}</option>
                                        @endforeach

                                    </select>
                                </div>


                                <div class="">
                                    <button id="individual_lesson_search" type="button" name="button"
                                            class="btn btn-success"
                                            onclick="botaoIndividualAula()">@lang('strings.show')
                                    </button>
                                </div>


                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>


            {{--Aula que o aluno ficou maior/menor indice cada emoção--}}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <label class="panel-title">
                        <a style="text-decoration: none;" class="accordion-toggle" data-toggle="collapse"
                           data-parent="#accordion" href="#collapseFour">
                            {{--<i class="glyphicon glyphicon-cloud"></i>--}}

                            <i class="fas fa-project-diagram"></i>
                            @lang('strings.lesson_upper_lower_for_each_emotion')
                        </a>
                    </label>
                </div>
                <div id="collapseFour" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="section">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <div class="form-group">
                                        <label class="control-label">@lang('strings.select_a_student')</label>
                                        <select id="student_2" name="" class="form-control select2"
                                                style="width: 100%">
                                            <option value="">@lang('strings.select_a_student')</option>

                                            @foreach($students as $student)
                                                <option value="{{$student->id}}">{{$student->name}}</option>
                                            @endforeach

                                        </select>
                                    </div>


                                    <div class="">
                                        <button id="lesson_upper_lower_emotion_search" type="button" name="button"
                                                class="btn btn-success"
                                                onclick="botaoMaiorMenorForEachEmotion()">@lang('strings.show')
                                        </button>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>



        {{--Avaliação de aluno/Aula--}}
        <div class="panel panel-default">
            <div class="panel-heading">
                <label class="panel-title">
                    <a style="text-decoration: none;" class="accordion-toggle" data-toggle="collapse"
                       data-parent="#accordion" href="#collapseFive">
                        <i class="fas fa-chalkboard-teacher"></i>
                        @lang('strings.result_evaluation_by_student_lesson')
                    </a>
                </label>
            </div>
            <div id="collapseFive" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="section">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <label class="control-label">@lang('strings.select_a_student')</label>
                                        <select id="student_3" name="" class="form-control select2"
                                                style="width: 100%">
                                            <option value="">@lang('strings.select_a_student')</option>

                                            @foreach($students as $student)
                                                <option value="{{$student->id}}">{{$student->name}}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-7">

                                    <label class="control-label">@lang('strings.select_a_lesson')</label>
                                    <select id="lesson_2" name="" class="form-control select2"
                                            style="width: 100%">
                                        <option value="">@lang('strings.select_a_lesson')</option>

{{--                                        tá fazendo a busca via ajax de aula assistidas pelo aluno selecionado --}}
{{--                                        @foreach($lessons as $lesson)--}}
{{--                                            <option value="{{$lesson->id}}">{{$lesson->title}}</option>--}}
{{--                                        @endforeach--}}

                                    </select>
                                </div><br>

                                <div class="col-lg-12">
                                    <div class="">
                                        <button id="evaluation_button" type="button"
                                                name="button"
                                                class="btn btn-success"
                                                onclick="botaoAvaliacaoAlunoAula()">@lang('strings.show')
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>


        {{--Avaliação por Aula--}}
        <div class="panel panel-default">
            <div class="panel-heading">
                <label class="panel-title">
                    <a style="text-decoration: none;" class="accordion-toggle" data-toggle="collapse"
                       data-parent="#accordion" href="#collapseSeven">
                        <i class="fa fa-chalkboard"></i>
                        @lang('strings.result_evaluation_by_lesson')
                    </a>
                </label>
            </div>
            <div id="collapseSeven" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="section">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <div class="form-group">
                                    <label class="control-label">@lang('strings.select_a_lesson')</label>
                                    <select id="lesson_4" name="" class="form-control select2"
                                            style="width: 100%">
                                        <option value="">@lang('strings.select_a_lesson')</option>

                                        @foreach($lessons as $lesson)
                                            <option value="{{$lesson->id}}">{{$lesson->title}}</option>
                                        @endforeach

                                    </select>
                                </div>


                                <div class="">
                                    <button id="lesson_report_results" type="button" name="button"
                                            class="btn btn-success"
                                            onclick="botaoAvaliacaoAula()">@lang('strings.show')
                                    </button>
                                </div>


                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>



    </div>

@endsection

@push('after-scripts')
    <script type="application/javascript" src="{{ asset('js/statistics/modules.js') }}"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
@endpush