@extends('layouts.app')

@section('heading', trans('strings.lessons_with_higher_and_lower_indexes_of_emotion'))

@section('content')



    @if( $aula_menor )


        <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <iframe width="100%" height="350" src="" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>



        <h3 align="center">
            <i class="fas fa-angle-double-right"></i>
            {{$emotion_name}}
            <i class="fas fa-angle-double-left"></i>
        </h3><br>

        <form>

{{--Maior indice--}}
            <h4>@lang('strings.higher_index')</h4>

            <div class="table-responsive">
            <table class="table table-hover table-bordered">
                <thead>
                <tr>
                    <th style="vertical-align: middle; text-align: center" scope="col"><i class="fas fa-angle-right"></i></th>
                    <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.id')</th>
                    <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.lesson_name')</th>
                    <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.views')</th>
                    <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.average_emotion')</th>
                    <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.action')</th>

                </tr>
                </thead>
                <tbody>
                <tr>
                    <th style="vertical-align: middle; text-align: center" scope="row">@lang('strings.results')</th>
                    <td style="vertical-align: middle; text-align: center">{{$aula_maior->lesson_id}}</td>
                    <td style="vertical-align: middle; text-align: center">{{$aula_maior->title}}</td>
                    <td style="vertical-align: middle; text-align: center">{{$qtd_views_maior}}</td>
                    <td style="vertical-align: middle; text-align: center">{{$maior}}</td>
                    <td style="vertical-align: middle; text-align: center">
                        <a class="btn btn-block video" data-video="https://www.youtube.com/embed/{{$aula_maior->video_uri}}?controls=0" data-toggle="modal" data-target="#videoModal">@lang('strings.watch')</a>
                    </td>
                </tr>

                </tbody>
            </table>
            </div>
        </form>

        <form>

            {{--Menor indice--}}
            <h4>@lang('strings.lower_index')</h4>

            <div class="table-responsive">
            <table class="table table-hover table-bordered">
                <thead>
                <tr>
                    <th style="vertical-align: middle; text-align: center" scope="col"><i class="fas fa-angle-left"></i></th>
                    <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.id')</th>
                    <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.lesson_name')</th>
                    <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.views')</th>
                    <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.average_emotion')</th>
                    <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.action')</th>

                </tr>
                </thead>
                <tbody>
                <tr>
                    <th style="vertical-align: middle; text-align: center" scope="row">@lang('strings.results')</th>
                    <td style="vertical-align: middle; text-align: center">{{$aula_menor->lesson_id}}</td>
                    <td style="vertical-align: middle; text-align: center">{{$aula_menor->title}}</td>
                    <td style="vertical-align: middle; text-align: center">{{$qtd_views_menor}}</td>
                    <td style="vertical-align: middle; text-align: center">{{$menor}}</td>
                    <td style="vertical-align: middle; text-align: center">
                        <a class="btn btn-block video" data-video="https://www.youtube.com/embed/{{$aula_menor->video_uri}}?controls=0" data-toggle="modal" data-target="#videoModal">@lang('strings.watch')</a>
                    </td>
                </tr>

                </tbody>
            </table>
            </div>
        </form>


    @else
        <div class="alert alert-warning">@lang('strings.no_statistics')</div>
        <div align="center">
            <img height="40em" src="{{url('images/dot2.png')}}">
        </div>
    @endif




@endsection

@push('after-scripts')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <script>
        $(function() {
            $(".video").click(function () {
                var theModal = $(this).data("target"),
                    videoSRC = $(this).attr("data-video"),
                    videoSRCauto = videoSRC + "?modestbranding=1&rel=0&controls=0&showinfo=0&html5=1&autoplay=1";
                $(theModal + ' iframe').attr('src', videoSRCauto);
                $(theModal + ' button.close').click(function () {
                    $(theModal + ' iframe').attr('src', videoSRC);
                });
            });
        });
    </script>
@endpush