@extends('layouts.app')

@section('heading', trans('strings.student_with_higher_and_lower_index_of_emotion'))

@section('content')

    {{--Só verifica se teve resultado para mostrar--}}
    @if( $maior)

        <h3 align="center">
            <i class="fas fa-angle-double-right"></i>
            {{$emotion_name}}
            <i class="fas fa-angle-double-left"></i>
        </h3><br>


        <iframe width="100%" height="350" src="https://www.youtube.com/embed/{{$lesson_details->video_uri}}?controls=0"
                frameborder="0" allowfullscreen></iframe>
        <form>
            <h4>@lang('strings.details_about_the_lesson')</h4>
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.id')</th>
                        <th style="vertical-align: middle; text-align: center; width: 20%" scope="col">@lang('strings.lesson_name')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.views')</th>
                        <th style="vertical-align: middle; text-align: center; width: 50%" scope="col">@lang('strings.description')</th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="vertical-align: middle; text-align: center">{{$lesson_details->id}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$lesson_details->title}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$qtd_views}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$lesson_details->description}}</td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </form>

        <form>
            <h4>@lang('strings.student_with_higher_index')</h4>

            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.user_id')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.student_id')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.student_name')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.email')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.average_emotion')</th>


                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="vertical-align: middle; text-align: center">{{$aluno_maior->id}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$aluno_maior->student_id}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$aluno_maior->name}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$aluno_maior->email}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$maior->$emotion}}</td>
                    </tr>

                    </tbody>
                </table>
            </div>

        </form>

        <form>
            <h4>@lang('strings.student_with_lower_index')</h4>

            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.user_id')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.student_id')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.student_name')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.email')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.average_emotion')</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="vertical-align: middle; text-align: center">{{$aluno_menor->id}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$aluno_menor->student_id}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$aluno_menor->name}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$aluno_menor->email}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$menor->$emotion}}</td>
                    </tr>

                    </tbody>
                </table>
            </div>

        </form>


    @else
        <div class="alert alert-warning">@lang('strings.no_statistics')</div>
        <div align="center">
            <img height="40em" src="{{url('images/dot2.png')}}">
        </div>
    @endif



@endsection
