@extends('layouts.app')

@section('heading', trans('strings.lesson_upper_lower_for_each_emotion'))

@section('content')

    {{--Só verifica se teve resultado para mostrar--}}
    @if( $maior_alegria)


        <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <iframe width="100%" height="350" src="" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>


        <h3 align="center">
            <i class="fas fa-angle-double-right"></i>
            {{$user->name}}
            <i class="fas fa-angle-double-left"></i>
        </h3><br>

        <form>
            <h4>@lang('strings.details_about_the_student')</h4>

            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.id')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.email')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.gender')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.birth')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.active')</th>


                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="vertical-align: middle; text-align: center">{{$user->student_id}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$user->email}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$user->gender}}</td>
                        <td style="vertical-align: middle; text-align: center">
                            {{ date( 'd/m/Y' , strtotime($user->birth_date))}}
                        </td>
                        <td style="vertical-align: middle; text-align: center">

                            @if($user->active)
                                @lang('strings.yes')
                            @else
                                @lang('strings.no')
                            @endif
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>

        </form><br>


        {{--ALEGRIA--}}
        <form>
            <h4 align="center">
                <i class="fas fa-angle-double-right"></i>
                @lang('strings.joy')
                <i class="fas fa-angle-double-left"></i>
            </h4>

            <h4>@lang('strings.class_with_higher_index')</h4>
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th style="vertical-align: middle; text-align: center" scope="col"><i class="fas fa-angle-right"></i></th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.id')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.lesson_name')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.average_student')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.class_average')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.action')</th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th style="vertical-align: middle; text-align: center" scope="row">@lang('strings.results')</th>
                        <td style="vertical-align: middle; text-align: center">{{$maior_alegria->lesson_id}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$maior_alegria->title}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$maior_alegria->joy}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$media_emocao_aula_maior_alegria}}</td>
                        <td style="vertical-align: middle; text-align: center">
                            <a class="btn btn-block video" data-video="https://www.youtube.com/embed/{{$maior_alegria->video_uri}}?controls=0" data-toggle="modal" data-target="#videoModal">@lang('strings.watch')</a>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>

            <h4>@lang('strings.class_with_lower_index')</h4>
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th style="vertical-align: middle; text-align: center" scope="col"><i class="fas fa-angle-left"></i></th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.id')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.lesson_name')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.average_student')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.class_average')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.action')</th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th style="vertical-align: middle; text-align: center" scope="row">@lang('strings.results')</th>
                        <td style="vertical-align: middle; text-align: center">{{$menor_alegria->lesson_id}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$menor_alegria->title}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$menor_alegria->joy}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$media_emocao_aula_menor_alegria}}</td>
                        <td style="vertical-align: middle; text-align: center">
                            <a class="btn btn-block video" data-video="https://www.youtube.com/embed/{{$menor_alegria->video_uri}}?controls=0" data-toggle="modal" data-target="#videoModal">@lang('strings.watch')</a>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>

        </form><br>


        {{--MEDO--}}
        <form>
            <h4 align="center">
                <i class="fas fa-angle-double-right"></i>
                @lang('strings.fear')
                <i class="fas fa-angle-double-left"></i>
            </h4>

            <h4>@lang('strings.class_with_higher_index')</h4>
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th style="vertical-align: middle; text-align: center" scope="col"><i class="fas fa-angle-right"></i></th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.id')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.lesson_name')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.average_student')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.class_average')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.action')</th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th style="vertical-align: middle; text-align: center" scope="row">@lang('strings.results')</th>
                        <td style="vertical-align: middle; text-align: center">{{$maior_medo->lesson_id}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$maior_medo->title}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$maior_medo->fear}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$media_emocao_aula_maior_medo}}</td>
                        <td style="vertical-align: middle; text-align: center">
                            <a class="btn btn-block video" data-video="https://www.youtube.com/embed/{{$maior_medo->video_uri}}?controls=0" data-toggle="modal" data-target="#videoModal">@lang('strings.watch')</a>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>

            <h4>@lang('strings.class_with_lower_index')</h4>
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th style="vertical-align: middle; text-align: center" scope="col"><i class="fas fa-angle-left"></i></th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.id')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.lesson_name')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.average_student')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.class_average')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.action')</th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th style="vertical-align: middle; text-align: center" scope="row">@lang('strings.results')</th>
                        <td style="vertical-align: middle; text-align: center">{{$menor_medo->lesson_id}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$menor_medo->title}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$menor_medo->fear}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$media_emocao_aula_menor_medo}}</td>
                        <td style="vertical-align: middle; text-align: center">
                            <a class="btn btn-block video" data-video="https://www.youtube.com/embed/{{$menor_medo->video_uri}}?controls=0" data-toggle="modal" data-target="#videoModal">@lang('strings.watch')</a>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>

        </form><br>


        {{--RAIVA--}}
        <form>
            <h4 align="center">
                <i class="fas fa-angle-double-right"></i>
                @lang('strings.anger')
                <i class="fas fa-angle-double-left"></i>
            </h4>

            <h4>@lang('strings.class_with_higher_index')</h4>
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th style="vertical-align: middle; text-align: center" scope="col"><i class="fas fa-angle-right"></i></th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.id')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.lesson_name')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.average_student')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.class_average')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.action')</th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th style="vertical-align: middle; text-align: center" scope="row">@lang('strings.results')</th>
                        <td style="vertical-align: middle; text-align: center">{{$maior_raiva->lesson_id}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$maior_raiva->title}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$maior_raiva->rage}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$media_emocao_aula_maior_raiva}}</td>
                        <td style="vertical-align: middle; text-align: center">
                            <a class="btn btn-block video" data-video="https://www.youtube.com/embed/{{$maior_raiva->video_uri}}?controls=0" data-toggle="modal" data-target="#videoModal">@lang('strings.watch')</a>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>

            <h4>@lang('strings.class_with_lower_index')</h4>
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th style="vertical-align: middle; text-align: center" scope="col"><i class="fas fa-angle-left"></i></th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.id')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.lesson_name')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.average_student')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.class_average')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.action')</th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th style="vertical-align: middle; text-align: center" scope="row">@lang('strings.results')</th>
                        <td style="vertical-align: middle; text-align: center">{{$menor_raiva->lesson_id}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$menor_raiva->title}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$menor_raiva->rage}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$media_emocao_aula_menor_raiva}}</td>
                        <td style="vertical-align: middle; text-align: center">
                            <a class="btn btn-block video" data-video="https://www.youtube.com/embed/{{$menor_raiva->video_uri}}?controls=0" data-toggle="modal" data-target="#videoModal">@lang('strings.watch')</a>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>

        </form><br>


        {{--DESGOSTO--}}
        <form>
            <h4 align="center">
                <i class="fas fa-angle-double-right"></i>
                @lang('strings.disgust')
                <i class="fas fa-angle-double-left"></i>
            </h4>

            <h4>@lang('strings.class_with_higher_index')</h4>
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th style="vertical-align: middle; text-align: center" scope="col"><i class="fas fa-angle-right"></i></th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.id')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.lesson_name')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.average_student')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.class_average')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.action')</th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th style="vertical-align: middle; text-align: center" scope="row">@lang('strings.results')</th>
                        <td style="vertical-align: middle; text-align: center">{{$maior_desgosto->lesson_id}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$maior_desgosto->title}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$maior_desgosto->disgust}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$media_emocao_aula_maior_desgosto}}</td>
                        <td style="vertical-align: middle; text-align: center">
                            <a class="btn btn-block video" data-video="https://www.youtube.com/embed/{{$maior_desgosto->video_uri}}?controls=0" data-toggle="modal" data-target="#videoModal">@lang('strings.watch')</a>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>

            <h4>@lang('strings.class_with_lower_index')</h4>
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th style="vertical-align: middle; text-align: center" scope="col"><i class="fas fa-angle-left"></i></th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.id')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.lesson_name')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.average_student')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.class_average')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.action')</th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th style="vertical-align: middle; text-align: center" scope="row">@lang('strings.results')</th>
                        <td style="vertical-align: middle; text-align: center">{{$menor_desgosto->lesson_id}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$menor_desgosto->title}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$menor_desgosto->disgust}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$media_emocao_aula_menor_desgosto}}</td>
                        <td style="vertical-align: middle; text-align: center">
                            <a class="btn btn-block video" data-video="https://www.youtube.com/embed/{{$menor_desgosto->video_uri}}?controls=0" data-toggle="modal" data-target="#videoModal">@lang('strings.watch')</a>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>

        </form><br>



        {{--TRISTEZA--}}
        <form>
            <h4 align="center">
                <i class="fas fa-angle-double-right"></i>
                @lang('strings.sadness')
                <i class="fas fa-angle-double-left"></i>
            </h4>

            <h4>@lang('strings.class_with_higher_index')</h4>
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th style="vertical-align: middle; text-align: center" scope="col"><i class="fas fa-angle-right"></i></th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.id')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.lesson_name')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.average_student')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.class_average')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.action')</th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th style="vertical-align: middle; text-align: center" scope="row">@lang('strings.results')</th>
                        <td style="vertical-align: middle; text-align: center">{{$maior_tristeza->lesson_id}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$maior_tristeza->title}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$maior_tristeza->sadness}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$media_emocao_aula_maior_tristeza}}</td>
                        <td style="vertical-align: middle; text-align: center">
                            <a class="btn btn-block video" data-video="https://www.youtube.com/embed/{{$maior_tristeza->video_uri}}?controls=0" data-toggle="modal" data-target="#videoModal">@lang('strings.watch')</a>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>

            <h4>@lang('strings.class_with_lower_index')</h4>
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th style="vertical-align: middle; text-align: center" scope="col"><i class="fas fa-angle-left"></i></th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.id')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.lesson_name')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.average_student')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.class_average')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.action')</th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th style="vertical-align: middle; text-align: center" scope="row">@lang('strings.results')</th>
                        <td style="vertical-align: middle; text-align: center">{{$menor_tristeza->lesson_id}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$menor_tristeza->title}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$menor_tristeza->sadness}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$media_emocao_aula_menor_tristeza}}</td>
                        <td style="vertical-align: middle; text-align: center">
                            <a class="btn btn-block video" data-video="https://www.youtube.com/embed/{{$menor_tristeza->video_uri}}?controls=0" data-toggle="modal" data-target="#videoModal">@lang('strings.watch')</a>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>

        </form><br>



        {{--DESPREZO--}}
        <form>
            <h4 align="center">
                <i class="fas fa-angle-double-right"></i>
                @lang('strings.contempt')
                <i class="fas fa-angle-double-left"></i>
            </h4>

            <h4>@lang('strings.class_with_higher_index')</h4>
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th style="vertical-align: middle; text-align: center" scope="col"><i class="fas fa-angle-right"></i></th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.id')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.lesson_name')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.average_student')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.class_average')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.action')</th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th style="vertical-align: middle; text-align: center" scope="row">@lang('strings.results')</th>
                        <td style="vertical-align: middle; text-align: center">{{$maior_desprezo->lesson_id}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$maior_desprezo->title}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$maior_desprezo->contempt}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$media_emocao_aula_maior_desprezo}}</td>
                        <td style="vertical-align: middle; text-align: center">
                            <a class="btn btn-block video" data-video="https://www.youtube.com/embed/{{$maior_desprezo->video_uri}}?controls=0" data-toggle="modal" data-target="#videoModal">@lang('strings.watch')</a>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>

            <h4>@lang('strings.class_with_lower_index')</h4>
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th style="vertical-align: middle; text-align: center" scope="col"><i class="fas fa-angle-left"></i></th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.id')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.lesson_name')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.average_student')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.class_average')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.action')</th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th style="vertical-align: middle; text-align: center" scope="row">@lang('strings.results')</th>
                        <td style="vertical-align: middle; text-align: center">{{$menor_desprezo->lesson_id}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$menor_desprezo->title}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$menor_desprezo->contempt}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$media_emocao_aula_menor_desprezo}}</td>
                        <td style="vertical-align: middle; text-align: center">
                            <a class="btn btn-block video" data-video="https://www.youtube.com/embed/{{$menor_desprezo->video_uri}}?controls=0" data-toggle="modal" data-target="#videoModal">@lang('strings.watch')</a>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>

        </form><br>

        {{--SURPRESA--}}
        <form>
            <h4 align="center">
                <i class="fas fa-angle-double-right"></i>
                @lang('strings.surprise')
                <i class="fas fa-angle-double-left"></i>
            </h4>

            <h4>@lang('strings.class_with_higher_index')</h4>
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th style="vertical-align: middle; text-align: center" scope="col"><i class="fas fa-angle-right"></i></th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.id')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.lesson_name')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.average_student')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.class_average')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.action')</th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th style="vertical-align: middle; text-align: center" scope="row">@lang('strings.results')</th>
                        <td style="vertical-align: middle; text-align: center">{{$maior_surpresa->lesson_id}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$maior_surpresa->title}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$maior_surpresa->surprise}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$media_emocao_aula_maior_surpresa}}</td>
                        <td style="vertical-align: middle; text-align: center">
                            <a class="btn btn-block video" data-video="https://www.youtube.com/embed/{{$maior_surpresa->video_uri}}?controls=0" data-toggle="modal" data-target="#videoModal">@lang('strings.watch')</a>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>

            <h4>@lang('strings.class_with_lower_index')</h4>
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th style="vertical-align: middle; text-align: center" scope="col"><i class="fas fa-angle-left"></i></th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.id')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.lesson_name')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.average_student')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.class_average')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.action')</th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th style="vertical-align: middle; text-align: center" scope="row">@lang('strings.results')</th>
                        <td style="vertical-align: middle; text-align: center">{{$menor_surpresa->lesson_id}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$menor_surpresa->title}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$menor_surpresa->surprise}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$media_emocao_aula_menor_surpresa}}</td>
                        <td style="vertical-align: middle; text-align: center">
                            <a class="btn btn-block video" data-video="https://www.youtube.com/embed/{{$menor_surpresa->video_uri}}?controls=0" data-toggle="modal" data-target="#videoModal">@lang('strings.watch')</a>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>

        </form><br>


            @else
                <div class="alert alert-warning">@lang('strings.no_statistics')</div>
                <div align="center">
                    <img height="40em" src="{{url('images/dot2.png')}}">
                </div>
    @endif



@endsection


@push('after-scripts')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <script>
        $(function() {
            $(".video").click(function () {
                var theModal = $(this).data("target"),
                    videoSRC = $(this).attr("data-video"),
                    videoSRCauto = videoSRC + "?modestbranding=1&rel=0&controls=0&showinfo=0&html5=1&autoplay=1";
                $(theModal + ' iframe').attr('src', videoSRCauto);
                $(theModal + ' button.close').click(function () {
                    $(theModal + ' iframe').attr('src', videoSRC);
                });
            });
        });
    </script>
@endpush