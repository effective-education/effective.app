@extends('layouts.app')

@section('heading', trans('strings.individual_per_lesson'))

@section('content')

{{--Só verifica se teve resultado para mostrar--}}
@if( ! $student->isEmpty())

        <h4 align="center">
                <i class="fas fa-angle-double-right"></i>
                {{$lesson->title}}
                <i class="fas fa-angle-double-left"></i>
        </h4><br>


        <iframe width="100%" height="350" src="https://www.youtube.com/embed/{{$lesson->video_uri}}?controls=0"
                frameborder="0" allowfullscreen></iframe>

<hr>
        <h4>
                @lang('strings.students')
        </h4>
<hr>


<form>

        @foreach($student as $info)
                <h4 align="center">
                        <i class="fas fa-angle-double-right"></i>
                        {{$info->name}}
                        <i class="fas fa-angle-double-left"></i>
                </h4>

                <div class="table-responsive">
                        <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                        <th style="vertical-align: middle; text-align: center" scope="col">#</th>
                                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.lesson_id')</th>
                                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.joy')</th>
                                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.fear')</th>
                                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.anger')</th>
                                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.disgust')</th>
                                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.sadness')</th>
                                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.contempt')</th>
                                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.surprise')</th>

                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                        <th style="vertical-align: middle; text-align: center" scope="row">@lang('strings.averages')</th>
                                        <td style="vertical-align: middle; text-align: center">{{$info->lesson_id}}</td>
                                        <td style="vertical-align: middle; text-align: center">{{$info->joy}}</td>
                                        <td style="vertical-align: middle; text-align: center">{{$info->fear}}</td>
                                        <td style="vertical-align: middle; text-align: center">{{$info->rage}}</td>
                                        <td style="vertical-align: middle; text-align: center">{{$info->disgust}}</td>
                                        <td style="vertical-align: middle; text-align: center">{{$info->sadness}}</td>
                                        <td style="vertical-align: middle; text-align: center">{{$info->contempt}}</td>
                                        <td style="vertical-align: middle; text-align: center">{{$info->surprise}}</td>
                                </tr>
                                </tbody>
                        </table>
                </div>
               <hr>
        @endforeach

</form>

@else
        <div class="alert alert-warning">@lang('strings.no_statistics')</div>
        <div align="center">
                <img height="40em" src="{{url('images/dot2.png')}}">
        </div>
@endif


@endsection


@push('after-scripts')

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

        <script>
                $(function() {
                        $(".video").click(function () {
                                var theModal = $(this).data("target"),
                                        videoSRC = $(this).attr("data-video"),
                                        videoSRCauto = videoSRC + "?modestbranding=1&rel=0&controls=0&showinfo=0&html5=1&autoplay=1";
                                $(theModal + ' iframe').attr('src', videoSRCauto);
                                $(theModal + ' button.close').click(function () {
                                        $(theModal + ' iframe').attr('src', videoSRC);
                                });
                        });
                });
        </script>
@endpush