@extends('layouts.app')

@section('heading', trans('strings.individual_per_student'))

@section('content')

{{--Só verifica se teve resultado para mostrar--}}
@if( ! $student->isEmpty())


        <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                        <div class="modal-content">
                                <div class="modal-body">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <iframe width="100%" height="350" src="" frameborder="0" allowfullscreen></iframe>
                                </div>
                        </div>
                </div>
        </div>



        <h3 align="center">
                <i class="fas fa-angle-double-right"></i>
                {{$user->name}}
                <i class="fas fa-angle-double-left"></i>
        </h3><br>



        <form>
                <h4>@lang('strings.details_about_the_student')</h4>

                <div class="table-responsive">
                        <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.user_id')</th>
                                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.student_id')</th>
                                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.email')</th>
                                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.gender')</th>
                                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.birth')</th>
                                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.active')</th>
                                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.number_of_classes_watched')</th>


                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                        <td style="vertical-align: middle; text-align: center">{{$user->id}}</td>
                                        <td style="vertical-align: middle; text-align: center">{{$user->student_id}}</td>
                                        <td style="vertical-align: middle; text-align: center">{{$user->email}}</td>
                                        <td style="vertical-align: middle; text-align: center">{{$user->gender}}</td>
                                        <td style="vertical-align: middle; text-align: center">
                                                {{ date( 'd/m/Y' , strtotime($user->birth_date))}}
                                        </td>
                                        <td style="vertical-align: middle; text-align: center">

                                                @if($user->active)
                                                        @lang('strings.yes')
                                                @else
                                                        @lang('strings.no')
                                                @endif
                                        </td>
                                        <td style="vertical-align: middle; text-align: center">{{$qtd_aulas_assistidas}}</td>
                                </tr>

                                </tbody>
                        </table>
                </div>

        </form><br>

<form>

        @foreach($student as $info)

                <h4 align="center">
                        <i class="fas fa-angle-double-right"></i>
                        {{$info->title}}
                        <i class="fas fa-angle-double-left"></i>
                </h4>

                <h4>@lang('strings.details_about_the_lesson')</h4>

                <div class="table-responsive">
                        <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                        <th style="vertical-align: middle; text-align: center" scope="col">#</th>
                                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.lesson_id')</th>
                                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.joy')</th>
                                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.fear')</th>
                                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.anger')</th>
                                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.disgust')</th>
                                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.sadness')</th>
                                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.contempt')</th>
                                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.surprise')</th>

                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                        <th style="vertical-align: middle; text-align: center" scope="row">@lang('strings.averages')</th>
                                        <td style="vertical-align: middle; text-align: center">{{$info->lesson_id}}</td>
                                        <td style="vertical-align: middle; text-align: center">{{$info->joy}}</td>
                                        <td style="vertical-align: middle; text-align: center">{{$info->fear}}</td>
                                        <td style="vertical-align: middle; text-align: center">{{$info->rage}}</td>
                                        <td style="vertical-align: middle; text-align: center">{{$info->disgust}}</td>
                                        <td style="vertical-align: middle; text-align: center">{{$info->sadness}}</td>
                                        <td style="vertical-align: middle; text-align: center">{{$info->contempt}}</td>
                                        <td style="vertical-align: middle; text-align: center">{{$info->surprise}}</td>
                                </tr>
                                </tbody>
                        </table>
                </div>
                <a class="btn btn-primary video"
                   data-video="https://www.youtube.com/embed/{{$info->video_uri}}?controls=0"
                   data-toggle="modal" data-target="#videoModal">
                        @lang('strings.watch')
                </a>
                <br><br><br>


        @endforeach

</form>

@else
        <div class="alert alert-warning">@lang('strings.no_statistics')</div>
        <div align="center">
                <img height="40em" src="{{url('images/dot2.png')}}">
        </div>
@endif


@endsection


@push('after-scripts')

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

        <script>
                $(function() {
                        $(".video").click(function () {
                                var theModal = $(this).data("target"),
                                        videoSRC = $(this).attr("data-video"),
                                        videoSRCauto = videoSRC + "?modestbranding=1&rel=0&controls=0&showinfo=0&html5=1&autoplay=1";
                                $(theModal + ' iframe').attr('src', videoSRCauto);
                                $(theModal + ' button.close').click(function () {
                                        $(theModal + ' iframe').attr('src', videoSRC);
                                });
                        });
                });
        </script>
@endpush