@extends('layouts.app')

@section('heading', trans('strings.result_evaluation_by_lesson'))

@section('content')

    {{--Só verifica se teve resultado para mostrar--}}
    @if( ! $lesson == null)

        <h3 align="center"> {{$lesson->title}}</h3>

        {{--Verifica se teve algum resultado--}}
        @if($subtitle)
            <hr>

            <div class="plotgraph" id="grafico" style="width:100%; height:400px;">
            </div>

        @endif

        <hr>

        <form>
            <h3 align="center"> @lang('strings.students')</h3>
            <hr>

            <div class="table-responsive">

                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>@lang('strings.name')</th>
                        <th>@lang('strings.email')</th>
                        <th>@lang('strings.age')</th>
                        <th>@lang('strings.active')</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($students as $student)
                        <tr>
                            <td>{{$student->name}}</td>
                            <td>{{$student->email}}</td>
                            <td>{{$student->age}}</td>
                            <td>{{ $student->isActive() ? trans('strings.yes') : trans('strings.no') }}</td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>
            </div>

        </form>




        <form>

            {{--Habilidades--}}
            <hr>
            <h2 align="center">@lang('strings.abilities')</h2>
            <hr>

            @if(! $lesson->abilities->isEmpty())
                @foreach($lesson->abilities as $ability)

                    <div>

                        <h5 align="center" style="margin-top: 2rem">
                            <i class="fas fa-angle-double-right"></i>
                            {{ $ability->description }}
                            <i class="fas fa-angle-double-left"></i>
                        </h5>


                        <div class="table-responsive">

                            <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th style="text-align: center;" scope="col">@lang('strings.evidences')</th>
                                    <th style="text-align: center;" scope="col">@lang('strings.developed')</th>
                                    <th style="text-align: center;" scope="col">@lang('strings.partially_developed')</th>
                                    <th style="text-align: center;" scope="col">@lang('strings.did_not_develop')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($ability->evidences as $evidence)
                                    <?php
                                    $desenvolveu = 0;
                                    $des_parcial = 0;
                                    $nao_des = 0;
                                    ?>

                                    <tr>

                                        <td>{{ $evidence->description }}</td>

                                        <td style="text-align: center;">
                                            @foreach($evidence->results as $result)
                                                @if($result->result == '2')
                                                    <?php $desenvolveu++; ?>
                                                @endif
                                            @endforeach
                                            {{$desenvolveu}}
                                        </td>

                                        <td style="text-align: center;">
                                            @foreach($evidence->results as $result)
                                                @if($result->result == '1')
                                                    <?php $des_parcial++; ?>
                                                @endif
                                            @endforeach
                                            {{$des_parcial}}
                                        </td>

                                        <td style="text-align: center;">
                                            @foreach($evidence->results as $result)
                                                @if($result->result == '0')
                                                    <?php $nao_des++; ?>
                                                @endif
                                            @endforeach
                                            {{$nao_des}}
                                        </td>

                                    </tr>

                                @endforeach

                                </tbody>
                            </table>

                        </div>
                    </div>
                @endforeach
            @else
                <div class="alert alert-warning">@lang('status.ability.none')</div>
            @endif


            {{--Conhecimento--}}
            <hr>
            <h2 align="center">@lang('strings.knowledge_s')</h2>
            <hr>

            @if(! $lesson->knowledge_s->isEmpty())
                @foreach($lesson->knowledge_s as $knowledge)

                    <div>

                        <h5 align="center" style="margin-top: 2rem">
                            <i class="fas fa-angle-double-right"></i>
                            {{ $knowledge->description }}
                            <i class="fas fa-angle-double-left"></i>
                        </h5>


                        <div class="table-responsive">

                            <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th style="text-align: center;" scope="col">@lang('strings.evidences')</th>
                                    <th style="text-align: center;" scope="col">@lang('strings.developed')</th>
                                    <th style="text-align: center;" scope="col">@lang('strings.partially_developed')</th>
                                    <th style="text-align: center;" scope="col">@lang('strings.did_not_develop')</th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($knowledge->evidences as $evidence)
                                    <?php
                                    $desenvolveu = 0;
                                    $des_parcial = 0;
                                    $nao_des = 0;
                                    ?>

                                    <tr>

                                        <td>{{ $evidence->description }}</td>

                                        <td style="text-align: center;">
                                            @foreach($evidence->results as $result)
                                                @if($result->result == '2')
                                                    <?php $desenvolveu++; ?>
                                                @endif
                                            @endforeach
                                            {{$desenvolveu}}
                                        </td>

                                        <td style="text-align: center;">
                                            @foreach($evidence->results as $result)
                                                @if($result->result == '1')
                                                    <?php $des_parcial++; ?>
                                                @endif
                                            @endforeach
                                            {{$des_parcial}}
                                        </td>

                                        <td style="text-align: center;">
                                            @foreach($evidence->results as $result)
                                                @if($result->result == '0')
                                                    <?php $nao_des++; ?>
                                                @endif
                                            @endforeach
                                            {{$nao_des}}
                                        </td>

                                    </tr>

                                @endforeach

                                </tbody>
                            </table>

                        </div>
                    </div>
                @endforeach
            @else
                <div class="alert alert-warning">@lang('status.knowledge.none')</div>
            @endif


            {{--Competencias--}}
            <hr>
            <h2 align="center">@lang('strings.competences')</h2>
            <hr>

            @if(! $lesson->competencies->isEmpty())
                @foreach($lesson->competencies as $competence)

                    <div>

                        <h5 align="center" style="margin-top: 2rem">
                            <i class="fas fa-angle-double-right"></i>
                            {{ $competence->description }}
                            <i class="fas fa-angle-double-left"></i>
                        </h5>


                        <div class="table-responsive">

                            <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th style="text-align: center;" scope="col">@lang('strings.evidences')</th>
                                    <th style="text-align: center;" scope="col">@lang('strings.developed')</th>
                                    <th style="text-align: center;" scope="col">@lang('strings.partially_developed')</th>
                                    <th style="text-align: center;" scope="col">@lang('strings.did_not_develop')</th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($competence->evidences as $evidence)

                                    <?php
                                    $desenvolveu = 0;
                                    $des_parcial = 0;
                                    $nao_des = 0;
                                    ?>

                                    <tr>

                                        <td>{{ $evidence->description }}</td>

                                        <td style="text-align: center;">
                                            @foreach($evidence->results as $result)
                                                @if($result->result == '2')
                                                    <?php $desenvolveu++; ?>
                                                @endif
                                            @endforeach
                                            {{$desenvolveu}}
                                        </td>

                                        <td style="text-align: center;">
                                            @foreach($evidence->results as $result)
                                                @if($result->result == '1')
                                                    <?php $des_parcial++; ?>
                                                @endif
                                            @endforeach
                                            {{$des_parcial}}
                                        </td>

                                        <td style="text-align: center;">
                                            @foreach($evidence->results as $result)
                                                @if($result->result == '0')
                                                    <?php $nao_des++; ?>
                                                @endif
                                            @endforeach
                                            {{$nao_des}}
                                        </td>

                                    </tr>

                                @endforeach

                                </tbody>
                            </table>

                        </div>
                    </div>
                @endforeach
            @else
                <div class="alert alert-warning">@lang('status.competence.none')</div>
            @endif


        </form>

        <hr>

        <form>

            <iframe width="100%" height="350" src="https://www.youtube.com/embed/{{$lesson->video_uri}}?controls=0"
                    frameborder="0" allowfullscreen></iframe>
        </form>



    @else
        <div class="alert alert-warning">@lang('strings.no_statistics')</div>
        <div align="center">
            <img height="40em" src="{{url('images/dot2.png')}}">
        </div>
    @endif


@endsection


@push('after-scripts')

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script type="application/javascript" src="{{ asset('js/graphic/highcharts.js') }}"></script>


    <script>
        Highcharts.chart('grafico', {

            chart: {
                type: 'bar'
            },
            title: {
                text: 'Média de emoções'
            },
            subtitle: {
                text: '{{$subtitle}}'
            },
            xAxis: {
                categories: ['Alegria 😄', 'Medo 😱', 'Raiva 😡', 'Nojo 😖', 'Tristeza 😞', 'Desprezo 🙄', 'Surpresa 😲'],
                title: {
                    text: 'Emoções'
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: '[ 0 ~ 100 ]',
                    verticalAlign: 'middle'

                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
                valueSuffix: null
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: -40,
                y: 80,
                floating: true,
                borderWidth: 1,
                backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                shadow: true,
            },
            credits: {
                enabled: false
            },
            series: [{
                name: 'Média ao longo da aula',
                data: {{$str_Media}}
            }],
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }
        });

    </script>

@endpush


@push('before-scripts')


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>


@endpush