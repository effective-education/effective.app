@extends('layouts.app')

@section('heading', trans('strings.result_evaluation_by_student_lesson'))

@section('content')

    {{--Só verifica se teve resultado para mostrar--}}
    @if( ! $student->isEmpty())


        <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <iframe width="100%" height="350" src="" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>





        <form>
            <h4>@lang('strings.details_about_the_student')</h4>

            <h5 align="center" style="margin-top: 2rem">
                <i class="fas fa-angle-double-right"></i>
                {{$user->name}}
                <i class="fas fa-angle-double-left"></i>
            </h5>

            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.email')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.gender')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.birth')</th>
                        <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.active')</th>


                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="vertical-align: middle; text-align: center">{{$user->email}}</td>
                        <td style="vertical-align: middle; text-align: center">{{$user->gender}}</td>
                        <td style="vertical-align: middle; text-align: center">
                            {{ date( 'd/m/Y' , strtotime($user->birth_date))}}
                        </td>
                        <td style="vertical-align: middle; text-align: center">

                            @if($user->active)
                                @lang('strings.yes')
                            @else
                                @lang('strings.no')
                            @endif
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>

        </form>
        <hr>

        <form>

            @foreach($student as $info)

                <h4>@lang('strings.details_about_the_lesson')</h4>

                <h5 align="center" style="margin-top: 2rem">
                    <i class="fas fa-angle-double-right"></i>
                    {{$info->title}}
                    <i class="fas fa-angle-double-left"></i>
                </h5>

                <div class="table-responsive">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th style="vertical-align: middle; text-align: center" scope="col">#</th>
                            <th style="vertical-align: middle; text-align: center"
                                scope="col">@lang('strings.lesson_id')</th>
                            <th style="vertical-align: middle; text-align: center" scope="col">@lang('strings.joy')</th>
                            <th style="vertical-align: middle; text-align: center"
                                scope="col">@lang('strings.fear')</th>
                            <th style="vertical-align: middle; text-align: center"
                                scope="col">@lang('strings.anger')</th>
                            <th style="vertical-align: middle; text-align: center"
                                scope="col">@lang('strings.disgust')</th>
                            <th style="vertical-align: middle; text-align: center"
                                scope="col">@lang('strings.sadness')</th>
                            <th style="vertical-align: middle; text-align: center"
                                scope="col">@lang('strings.contempt')</th>
                            <th style="vertical-align: middle; text-align: center"
                                scope="col">@lang('strings.surprise')</th>

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th style="vertical-align: middle; text-align: center"
                                scope="row">@lang('strings.averages')</th>
                            <td style="vertical-align: middle; text-align: center">{{$info->lesson_id}}</td>
                            <td style="vertical-align: middle; text-align: center">{{$info->joy}}</td>
                            <td style="vertical-align: middle; text-align: center">{{$info->fear}}</td>
                            <td style="vertical-align: middle; text-align: center">{{$info->rage}}</td>
                            <td style="vertical-align: middle; text-align: center">{{$info->disgust}}</td>
                            <td style="vertical-align: middle; text-align: center">{{$info->sadness}}</td>
                            <td style="vertical-align: middle; text-align: center">{{$info->contempt}}</td>
                            <td style="vertical-align: middle; text-align: center">{{$info->surprise}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <a class="btn btn-primary video"
                   data-video="https://www.youtube.com/embed/{{$info->video_uri}}?controls=0"
                   data-toggle="modal" data-target="#videoModal">
                    @lang('strings.watch')
                </a>

            @endforeach

        </form>
        <hr>


        <form>
            <h4>@lang('strings.results_of_evaluation')</h4>

            {{--Habilidades--}}
            <hr>
            <h2 align="center">@lang('strings.abilities')</h2>
            <hr>

            @if(! $lesson->abilities->isEmpty())
                @foreach($lesson->abilities as $ability)

                    <div>

                        <h5 align="center" style="margin-top: 2rem">
                            <i class="fas fa-angle-double-right"></i>
                            {{ $ability->description }}
                            <i class="fas fa-angle-double-left"></i>
                        </h5>


                        <div class="table-responsive">

                            <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th style="text-align: center;" scope="col">@lang('strings.evidences')</th>
                                    <th style="text-align: center;" scope="col">@lang('strings.results')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($ability->evidences as $evidence)
                                    <tr>

                                        <td>{{ $evidence->description }}</td>

                                        <td>
                                            @if($evidence->results->isEmpty())
                                                <p>-</p>
                                            @else
                                                @foreach($evidence->results as $result)
                                                    @if($result->student_id == $user->student_id)
                                                        @if($result->result == '2')
                                                            @lang('strings.developed')
                                                        @elseif($result->result == '1')
                                                            @lang('strings.partially_developed')
                                                        @elseif($result->result == '0')
                                                            @lang('strings.did_not_develop')
                                                        @endif
                                                    @endif
                                                @endforeach
                                            @endif
                                        </td>

                                    </tr>

                                @endforeach

                                </tbody>
                            </table>

                        </div>
                    </div>
                @endforeach
            @else
                <div class="alert alert-warning">@lang('status.ability.none')</div>
            @endif


            {{--Conhecimento--}}
            <hr>
            <h2 align="center">@lang('strings.knowledge_s')</h2>
            <hr>

            @if(! $lesson->knowledge_s->isEmpty())
                @foreach($lesson->knowledge_s as $knowledge)

                    <div>

                        <h5 align="center" style="margin-top: 2rem">
                            <i class="fas fa-angle-double-right"></i>
                            {{ $knowledge->description }}
                            <i class="fas fa-angle-double-left"></i>
                        </h5>


                        <div class="table-responsive">

                            <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th style="text-align: center;" scope="col">@lang('strings.evidences')</th>
                                    <th style="text-align: center;" scope="col">@lang('strings.results')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($knowledge->evidences as $evidence)
                                    <tr>

                                        <td>{{ $evidence->description }}</td>

                                        <td>
                                            @if($evidence->results->isEmpty())
                                                <p>-</p>
                                            @else
                                                @foreach($evidence->results as $result)
                                                    @if($result->student_id == $user->student_id)
                                                        @if($result->result == '2')
                                                            @lang('strings.developed')
                                                        @elseif($result->result == '1')
                                                            @lang('strings.partially_developed')
                                                        @elseif($result->result == '0')
                                                            @lang('strings.did_not_develop')
                                                        @endif
                                                    @endif
                                                @endforeach
                                            @endif
                                        </td>


                                    </tr>

                                @endforeach

                                </tbody>
                            </table>

                        </div>
                    </div>
                @endforeach
            @else
                <div class="alert alert-warning">@lang('status.knowledge.none')</div>
            @endif


            {{--Competencias--}}
            <hr>
            <h2 align="center">@lang('strings.competences')</h2>
            <hr>

            @if(! $lesson->competencies->isEmpty())
                @foreach($lesson->competencies as $competence)

                    <div>

                        <h5 align="center" style="margin-top: 2rem">
                            <i class="fas fa-angle-double-right"></i>
                            {{ $competence->description }}
                            <i class="fas fa-angle-double-left"></i>
                        </h5>


                        <div class="table-responsive">

                            <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th style="text-align: center;" scope="col">@lang('strings.evidences')</th>
                                    <th style="text-align: center;" scope="col">@lang('strings.results')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($competence->evidences as $evidence)
                                    <tr>

                                        <td>{{ $evidence->description }}</td>

                                        <td>
                                            @if($evidence->results->isEmpty())
                                                <p>-</p>
                                            @else
                                                @foreach($evidence->results as $result)
                                                    @if($result->student_id == $user->student_id)
                                                        @if($result->result == '2')
                                                            @lang('strings.developed')
                                                        @elseif($result->result == '1')
                                                            @lang('strings.partially_developed')
                                                        @elseif($result->result == '0')
                                                            @lang('strings.did_not_develop')
                                                        @endif
                                                    @endif
                                                @endforeach
                                            @endif
                                        </td>

                                    </tr>

                                @endforeach

                                </tbody>
                            </table>

                        </div>
                    </div>
                @endforeach
            @else
                <div class="alert alert-warning">@lang('status.competence.none')</div>
            @endif


        </form>


    @else
        <div class="alert alert-warning">@lang('strings.no_statistics')</div>
        <div align="center">
            <img height="40em" src="{{url('images/dot2.png')}}">
        </div>
    @endif


@endsection


@push('after-scripts')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <script>
        $(function () {
            $(".video").click(function () {
                var theModal = $(this).data("target"),
                    videoSRC = $(this).attr("data-video"),
                    videoSRCauto = videoSRC + "?modestbranding=1&rel=0&controls=0&showinfo=0&html5=1&autoplay=1";
                $(theModal + ' iframe').attr('src', videoSRCauto);
                $(theModal + ' button.close').click(function () {
                    $(theModal + ' iframe').attr('src', videoSRC);
                });
            });
        });
    </script>
@endpush