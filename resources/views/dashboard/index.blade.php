@extends('layouts.app')

@section('heading', trans('strings.dashboard'))

@section('content')

    <a style="align-items: center;  display: flex;  flex-direction: row;  flex-wrap: wrap;  justify-content: center;">
        <img height="100px" src="{{url('images/logo.png')}}">
    </a>

    @lang('notifications.welcome_to_effective.greeting', ['app' => config('app.name')])

    <br><br>

    @if(Auth::user()->isAdmin() or Auth::user()->isCollaborator())
        <p><a href="{{ route('lessons') }}" class="btn btn-primary btn-block btn-next"
              style="width: 100%">@lang('strings.see_all_lessons')</a></p>
    @elseif(Auth::user()->isStudent())
        <p><a href="{{ route('lessons') }}" class="btn btn-primary btn-block btn-next"
              style="width: 100%">@lang('strings.see_my_lessons')</a></p>
        <p><a href="{{ route('lessons.all') }}"
              class="btn btn-primary btn-block btn-next">@lang('strings.see_all_lessons')
            </a></p>
    @endif



@endsection
