@extends('layouts.app')

@section('heading', $class->name)

@section('content')

    <button type="button" name="id_class" class="btn btn-primary btn-block" data-toggle="modal" data-target="#ilsModal">
        @lang('strings.add_students')
    </button>

    <form id="student">

        {{--Estudantes--}}
        <label><h3>@lang('strings.students')</h3></label>

        @if(! $std->isEmpty())
            <div class="table-responsive">

                <table class="table">

                    <thead>
                    <tr>
                        <th>@lang('strings.name')</th>
                    </tr>
                    </thead>

                    <tbody>


                    @foreach($std as $st)
                        <tr>
                            <td>{{ $st->name }}</td>

                        </tr>
                    @endforeach
                    </tbody>

                </table>
                <a href="{{route('classes.index')}}" class="btn btn-block btn-default"> @lang('strings.back')</a>
            </div>

            {{ $std->links() }}

        @else

            <div class="alert alert-warning">@lang('status.class_student.none')</div>

        @endif

    </form>


    <form method="POST" action="{{ route('class_student.store') }}" class="form" id="framework_form">


        {{ csrf_field() }}


        <div class="modal fade" id="ilsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

            <div class="modal-dialog" role="document">

                <div class="modal-content">




                    {{--pega o id da turma--}}
                    <div class="form-group">
                        <input style="display: none" name="classe_id" type="text" class="form-control"
                               value="{{ $class->id }}" readonly>
                    </div>


                    <div class="modal-body" align="center">
                        <label style="text-align: center">@lang('strings.students')</label>
                        <br>

                        <div class="form-group">
                            <select id="framework" name="student_id[]" multiple class="form-control select2"
                                    style="width: 100%" required>
                                @if(! $students->isEmpty())
                                    @foreach($students as $student)
                                        <option value="{{ $student->id }}">
                                            {{ $student->name }}
                                        </option>
                                    @endforeach
                                @else
                                    <div class="alert alert-warning">@lang('status.class_student.none')</div>
                                @endif
                            </select>
                        </div>

                    </div>

                    <input type="submit" value="@lang('strings.save')" class="btn btn-block btn-success">

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default"
                                data-dismiss="modal">@lang('strings.close')</button>
                    </div>

                </div>

            </div>

        </div>

    </form>

@endsection
