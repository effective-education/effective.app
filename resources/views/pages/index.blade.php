@extends('layouts.app')

@section('heading')
    <a style="align-items: center;  display: flex;  flex-direction: row;  flex-wrap: wrap;  justify-content: center;">
        <img height="100px" src="{{url('images/logo.png')}}">
    </a>

    <div class="title text-center">
        {{ config('app.name') }}
    </div>
@endsection

@section('content')

    <div class="links text-center">
        <p><a href="{{ route('lessons.all') }}"
              class="btn btn-primary btn-block btn-next">@lang('strings.see_all_lessons')
            </a></p>
    </div>

@endsection