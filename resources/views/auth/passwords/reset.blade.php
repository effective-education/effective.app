@extends('layouts.app')

@section('heading', trans('passwords.reset_password'))

@section('content')

    <form class="form-horizontal" role="form" method="POST" action="{{ route('password.request') }}">

        {{ csrf_field() }}

        <input type="hidden" name="token" value="{{ $token }}">

        <div class="form-group">

            <label for="email" class="col-md-4 control-label">@lang('auth.email_address')</label>

            <div class="col-md-6">
                <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}"
                       required autofocus>
            </div>

        </div>

        <div class="form-group">
            <label for="password" class="col-md-4 control-label">@lang('auth.password')</label>

            <div class="col-md-6">
                <input id="password" type="password" class="form-control" name="password" required>
            </div>
        </div>

        <div class="form-group">
            <label for="password-confirm" class="col-md-4 control-label">@lang('auth.confirm_password')</label>
            <div class="col-md-6">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    @lang('passwords.reset_password')
                </button>
            </div>
        </div>
    </form>

@endsection