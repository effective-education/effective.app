@extends('layouts.app')

@section('heading', trans('passwords.reset_password'))

@section('content')

    <form class="form-horizontal" role="form" method="POST" action="{{ route('password.email') }}">

        {{ csrf_field() }}

        <div class="form-group">

            <label for="email" class="col-md-4 control-label">@lang('auth.email_address')</label>

            <div class="col-md-6">
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
            </div>

        </div>

        <div class="form-group">

            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    @lang('passwords.send_password_reset_link')
                </button>
            </div>

        </div>
    </form>

@endsection
