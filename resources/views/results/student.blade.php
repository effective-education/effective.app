@extends('layouts.app')

@section('heading', trans('strings.results'))

@section('content')

    <h2>{{ $student->name }}</h2>
    <label>{{ $lesson->title  }}</label>

    <hr>

    <h2>@lang('strings.student_profile')</h2>

    <div class="form-group">
        <label>@lang('strings.name')</label>
        <input type="text" class="form-control" readonly value="{{ $student->name }}">
    </div>

    <div class="form-group">
        <label>@lang('strings.email')</label>
        <input type="text" class="form-control" readonly value="{{ $student->email }}">
    </div>

    <div class="form-group">
        <label>@lang('strings.age')</label>
        <input type="text" class="form-control" readonly value="{{ $student->age }}">
    </div>

    <div class="form-group">
        <label>@lang('strings.gender')</label>
        <input type="text" class="form-control" readonly value="{{ $student->gender }}">
    </div>

    <div class="form-group">
        <label>@lang('strings.college')</label>
        <input type="text" class="form-control" readonly value="{{ $student->college }}">
    </div>

    <div class="form-group">
        <label>@lang('strings.course')</label>
        <input type="text" class="form-control" readonly value="{{ $student->course }}">
    </div>

    <div class="form-group">
        <label>@lang('strings.special_need')</label>
        <input type="text" class="form-control" readonly value="{{ $student->readable_handicap }}">
    </div>

    <div class="form-group">
        <label>@lang('strings.musical_instrument')</label>
        <input type="text" class="form-control" readonly value="{{ $student->readable_musical_instrument }}">
    </div>

    <hr>


    @if($student->ils !== null)

    <h2>@lang('strings.learning_profile')</h2>

    <div class="row text-center">

        <div class="col-md-3 well">
            <p>ACT/REF</p>
            <p>{{ $student->ils->act_ref }}</p>
        </div>

        <div class="col-md-3 well">
            <p>SEN/INT</p>
            <p>{{ $student->ils->sen_int }}</p>
        </div>

        <div class="col-md-3 well">
            <p>VIS/VER</p>
            <p>{{ $student->ils->vis_ver }}</p>
        </div>

        <div class="col-md-3 well">
            <p>SEQ/GLO</p>
            <p>{{ $student->ils->seq_glo }}</p>
        </div>

    </div>

    @endif

    <hr>

    <h2>@lang('strings.tries')</h2>

    <table class="table">

        <thead>
        <tr>
            <th>@lang('strings.try')</th>
            <th>@lang('strings.total_questions')</th>
            <th>@lang('strings.hits')</th>
            <th>@lang('strings.action')</th>
        </tr>
        </thead>

        <tbody>
            <?php $counter = 1; ?>
            @foreach($results as $result)
                <tr>
                    <td>{{ '#' . $counter }}</td>
                    <td>{{ $result->total_questions }}</td>
                    <td>{{ $result->right_answers }}</td>
                    @if($result->impression !== null)
                        <td><a href="{{ route('impressions.show', $result->impression->id) }}" target="_blank">@lang('strings.see_impressions')</a></td>
                    @else
                        <td>@lang('strings.no_prints')</td>
                    @endif
                </tr>
                <?php $counter++; ?>
            @endforeach
        </tbody>

    </table>

    <a href="{{ route('lesson.results', [$lesson->id]) }}" class="btn btn-default btn-block">@lang('strings.back')</a>

@endsection