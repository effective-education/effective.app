@extends('layouts.app')

@section('heading', trans('strings.results'))

@section('content')

    <h2>{{ $lesson->title }}</h2>
    <label>@lang('strings.created_by') {{  $lesson->creator->name }}</label>

    <hr>

    <div class="row text-center">

        <div class="col-md-6 well">
            <h3>{{ $lesson->metric->views }}</h3>
            <p>@lang('strings.metrics.views')</p>
        </div>

        <div class="col-md-6 well">
            <h3>{{ $lesson->metric->finalized }}</h3>
            <p>@lang('strings.metrics.finalized')</p>
        </div>

    </div>

    <h2>@lang('strings.students')</h2>

    <hr>

    <form id="filter-result" name="filter_result" class="form-group" method="GET" action="{{route('lesson.results', $lesson->id)}}">
        <label>
            <input id="all" type="radio" name="showby" value="all">
            @lang('strings.all')

        </label> &nbsp;

        <label>

            <input id="classex" type="radio" name="showby" value="classe">
            @lang('strings.classes')
        </label> &nbsp;

        <label>

            <input id="loose" type="radio" name="showby" value="loose">
            @lang('strings.loose')
        </label><br>

        <div id="classe-select" style='display:none'>
            <select id="selectclasse" name="selected_classe" style="width: 100%" class="form-control select2" onselect="submit()">
                {{$classes = \App\Models\Classe::all()}}
                <option value="0">
                    Selecione uma Turma
                </option>
                @foreach($classes as $classe)
                    <option value="{{ $classe->id }}">
                        {{ $classe->name }}
                    </option>
                @endforeach
            </select><br>

            <button style="margin-top: 1rem" id="btnclasse" class="btn btn-default" onclick="submit()"> Filtrar</button>
        </div>

    </form>

    <table class="table">

        <thead>
        <tr>
            <th>@lang('strings.student')</th>
            <th>@lang('strings.college')</th>
            <th>@lang('strings.course')</th>
            <th>@lang('strings.action')</th>
            <th>@lang('strings.download')</th>
        </tr>
        </thead>

        {{--MOSTRAR TODOS--}}

        <div>
            <tbody id='div-all' style='display:none'>
            @foreach($lesson->students() as $student)
                <tr>
                    <td>{{ $student->name }}</td>
                    <td>{{ $student->college }}</td>
                    <td>{{ $student->course }}</td>
                    <td>
                        <a href="{{ route('lesson.results.student', [$lesson->id, $student->id]) }}">@lang('strings.details')</a><br>

                        @if(\Illuminate\Support\Facades\Auth::user()->id == $lesson->user_id)
                            <a href="{{ route('lesson.results.evaluation_results.route', [$lesson->id, $student->id]) }}">@lang('strings.register_evaluation')</a>
                        @endif

                    </td>
                    <td>
                        <a href="{{ route('lesson.results.student.download.lastFrames', [$lesson->id, $student->id]) }}">@lang('strings.download_last_frames')</a><br>
                        <a href="{{ route('lesson.results.student.download.lastFramesXml', [$lesson->id, $student->id]) }}">@lang('strings.download_last_frames_xml')</a></br>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </div>

        {{--MOSTRAR POR CLASSE--}}
        <div >
            @if($_GET)
                <p style="display: none">{{!$classe = \App\Models\Classe::find($_GET['selected_classe'])}}</p>

            @if($classe)
                {{!$students = $classe->students->whereIn('id', $lesson->studentsIds())}}

                <tbody id="div-classe">
                @foreach($students as $student)

                    <tr>
                        <td>{{ $student->name }}</td>
                        <td>{{ $student->college }}</td>
                        <td>{{ $student->course }}</td>
                        <td>
                            <a href="{{ route('lesson.results.student', [$lesson->id, $student->id]) }}">@lang('strings.details')</a><br>
                            @if(\Illuminate\Support\Facades\Auth::user()->id == $lesson->user_id)
                                <a href="{{ route('lesson.results.evaluation_results.route', [$lesson->id, $student->id]) }}">@lang('strings.register_evaluation')</a>
                            @endif

                        </td>
                        <td>
                            <a href="{{ route('lesson.results.student.download.lastFrames', [$lesson->id, $student->id]) }}">@lang('strings.download_last_frames')</a><br>
                            <a href="{{ route('lesson.results.student.download.lastFramesXml', [$lesson->id, $student->id]) }}">@lang('strings.download_last_frames_xml')</a></br>
                        </td>

                    </tr>
                @endforeach
                </tbody>
                @endif
            @endif
        </div>

        {{--MOSTRAR APENAS OS AVULSOS--}}
        <div>
            <tbody id='div-loose' style='display:none'>
            @foreach($lesson->students() as $student)
                @if(!$student->hasClasse())
                    <tr>
                        <td>{{ $student->name }}</td>
                        <td>{{ $student->college }}</td>
                        <td>{{ $student->course }}</td>
                        <td>
                            <a href="{{ route('lesson.results.student', [$lesson->id, $student->id]) }}">@lang('strings.details')</a><br>
                            @if(\Illuminate\Support\Facades\Auth::user()->id == $lesson->user_id)
                                <a href="{{ route('lesson.results.evaluation_results.route', [$lesson->id, $student->id]) }}">@lang('strings.register_evaluation')</a>
                            @endif

                        </td>
                        <td>
                            <a href="{{ route('lesson.results.student.download.lastFrames', [$lesson->id, $student->id]) }}">@lang('strings.download_last_frames')</a><br>
                            <a href="{{ route('lesson.results.student.download.lastFramesXml', [$lesson->id, $student->id]) }}">@lang('strings.download_last_frames_xml')</a></br>
                        </td>

                    </tr>
                @endif
            @endforeach
            </tbody>
        </div>
    </table>

    <a href="{{ route('lessons.index') }}" class="btn btn-default btn-block">@lang('strings.back')</a>

@endsection

@push('after-scripts')
    <script>
        $(document).ready(function () {
            $('input[type="radio"]').click(function () {
                if ($(this).attr('id') == 'all') {
                    $('#div-all').show();
                    $('#div-classe').hide();
                } else if ($(this).attr('id') != 'all') {
                    $('#div-all').hide();
                }

                if ($(this).attr('id') == 'classex') {
                    $('#classe-select').show();

                } else if ($(this).attr('id') != 'classex') {
                    $('#classe-select').hide();

                }

                if ($(this).attr('id') == 'loose') {
                    $('#div-loose').show();
                    $('#div-classe').hide();
                } else if ($(this).attr('id') != 'loose') {
                    $('#div-loose').hide();
                }

            });
        });
    </script>
@endpush
