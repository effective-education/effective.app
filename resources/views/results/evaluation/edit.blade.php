@extends('layouts.app')

@section('heading', trans('strings.learning_assessment'))

@section('content')


    <form method="POST" action="{{ route('evaluation_results.update', [$lesson->id]) }}" class="form">

        {{ csrf_field() }}

        {{ method_field('PUT') }}

        <div class="row bg-info">
            <div class="col-md-6">
                <h5 style="margin-top: 2.5rem; text-align: center">@lang('strings.student')</h5>
                <h4 style="margin-bottom: 2.5rem; text-align: center">{{$student->name}}</h4>

                {{--pega o id do Aluno--}}
                <div class="form-group">
                    <input style="display: none" name="student_id" type="text" class="form-control"
                           value="{{ $student->id }}" readonly>
                </div>
            </div>
            <div class="col-md-6">
                <h5 style="margin-top: 2.5rem; text-align: center">@lang('strings.lesson')</h5>
                <h4 style="margin-bottom: 2.5rem; text-align: center">{{$lesson->title}}</h4>
                {{--pega o id da Aula--}}
                <div class="form-group">
                    <input style="display: none" name="lesson_id" type="text" class="form-control"
                           value="{{ $lesson->id }}" readonly>
                </div>
            </div>

        </div>

        {{--Habilidades--}}
        <hr>
        <h2 align="center">@lang('strings.abilities')</h2>
        <hr>

        @if(! $lesson->abilities->isEmpty())
            @foreach($lesson->abilities as $ability)

                <div class="well">

                    <p align="center"><strong>{{ $ability->description }}</strong></p>


                    <div class="table-responsive">

                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th style="text-align: center; width: 45%;" scope="col">@lang('strings.evidences')</th>
                                <th style="text-align: center;" scope="col" colspan="2">@lang('strings.evaluation')</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php
                            $contAbility = 0;
                            ?>

                            @foreach($ability->evidences as $evidence)

                                <?php $contAbility ++  ?>

                                <tr>

                                    {{--pega o id da abilidade--}}
                                    <div class="form-group">
                                        <input style="display: none" name="ability[]" type="text" class="form-control"
                                               value="{{ $ability->id }}" readonly>
                                    </div>

                                    <td>{{ $evidence->description }}</td>

                                    <td style="width: 20%; min-width: 50px">
                                        <div class="form-group">
                                            @if(! ($evidence->results)->isEmpty())
                                                @foreach($evidence->results as $result)
                                                    @if($result->student_id == $student->id)
                                                        <input type="range" min="0" max="2" step="1" value="{{ old('result') ?? $result->result }}"
                                                               id="rangeAbility{{$evidence->id}}{{$ability->id}}{{$contAbility}}" name="range[]"
                                                               class="form-control-range">
                                                    @endif
                                                @endforeach
                                            @else
                                                <input type="range" min="0" max="2" step="1" value="{{ old('result') ?? $result->result }}"
                                                       id="rangeAbility{{$evidence->id}}{{$ability->id}}{{$contAbility}}" name="range[]"
                                                       class="form-control-range">
                                            @endif
                                        </div>
                                    </td>

                                    <td style="width: 22rem; text-align: center">
                                        {{--pega o id da evidencia--}}
                                        <div class="form-group">
                                            <input style="display: none" name="evidence_id[]" type="text" class="form-control"
                                                   value="{{$evidence->id}}" readonly>
                                        </div>
                                        <p><span id="outAbility{{$evidence->id}}{{$ability->id}}{{$contAbility}}"></span></p>
                                    </td>

                                    <script>
                                        var sliderAbility{{$evidence->id}}{{$ability->id}}{{$contAbility}} = document.getElementById("rangeAbility{{$evidence->id}}{{$ability->id}}{{$contAbility}}");
                                        var outputAbility{{$evidence->id}}{{$ability->id}}{{$contAbility}} = document.getElementById("outAbility{{$evidence->id}}{{$ability->id}}{{$contAbility}}");

                                        if (sliderAbility{{$evidence->id}}{{$ability->id}}{{$contAbility}}.value == 0) {
                                            outputAbility{{$evidence->id}}{{$ability->id}}{{$contAbility}}.style.color = "rgb(202, 19, 19)";
                                            outputAbility{{$evidence->id}}{{$ability->id}}{{$contAbility}}.innerHTML = 'Não Desenvolveu';
                                        } else if (sliderAbility{{$evidence->id}}{{$ability->id}}{{$contAbility}}.value == 1) {
                                            outputAbility{{$evidence->id}}{{$ability->id}}{{$contAbility}}.style.color = "#a89600";
                                            outputAbility{{$evidence->id}}{{$ability->id}}{{$contAbility}}.innerHTML = 'Desenvolveu Parcialmente';
                                        } else if (sliderAbility{{$evidence->id}}{{$ability->id}}{{$contAbility}}.value == 2) {
                                            outputAbility{{$evidence->id}}{{$ability->id}}{{$contAbility}}.style.color = "#00a808";
                                            outputAbility{{$evidence->id}}{{$ability->id}}{{$contAbility}}.innerHTML = 'Desenvolveu';
                                        }


                                        sliderAbility{{$evidence->id}}{{$ability->id}}{{$contAbility}}.oninput = function () {

                                            if (sliderAbility{{$evidence->id}}{{$ability->id}}{{$contAbility}}.value == 0) {
                                                outputAbility{{$evidence->id}}{{$ability->id}}{{$contAbility}}.style.color = "rgb(202, 19, 19)";
                                                outputAbility{{$evidence->id}}{{$ability->id}}{{$contAbility}}.innerHTML = 'Não Desenvolveu';
                                            } else if (sliderAbility{{$evidence->id}}{{$ability->id}}{{$contAbility}}.value == 1) {
                                                outputAbility{{$evidence->id}}{{$ability->id}}{{$contAbility}}.style.color = "#a89600";
                                                outputAbility{{$evidence->id}}{{$ability->id}}{{$contAbility}}.innerHTML = 'Desenvolveu Parcialmente';
                                            } else if (sliderAbility{{$evidence->id}}{{$ability->id}}{{$contAbility}}.value == 2) {
                                                outputAbility{{$evidence->id}}{{$ability->id}}{{$contAbility}}.style.color = "#00a808";
                                                outputAbility{{$evidence->id}}{{$ability->id}}{{$contAbility}}.innerHTML = 'Desenvolveu';
                                            }
                                        }


                                    </script>

                                </tr>

                            @endforeach

                            </tbody>
                        </table>

                    </div>
                </div>
            @endforeach
        @else
            <div class="alert alert-warning">@lang('status.ability.none')</div>

        @endif



        <hr>
        {{--Conhecimentos--}}
        <h2 align="center">@lang('strings.knowledge_s')</h2>

        @if(! $lesson->knowledge_s->isEmpty())
            @foreach($lesson->knowledge_s as $knowledge)

                <div class="well">

                    <p align="center"><strong>{{ $knowledge->description }}</strong></p>

                    <div class="table-responsive">

                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th style="text-align: center; width: 45%" scope="col">@lang('strings.evidences')</th>
                                <th style="text-align: center" scope="col" colspan="2">@lang('strings.evaluation')</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php
                            $contKnowledge = 0;
                            ?>

                            @foreach($knowledge->evidences as $evidence)

                                <?php $contKnowledge ++  ?>

                                <tr>

                                    {{--pega o id do Conhecimento--}}
                                    <div class="form-group">
                                        <input style="display: none" name="knowledge[]" type="text" class="form-control"
                                               value="{{ $knowledge->id }}" readonly>
                                    </div>

                                    <td>{{ $evidence->description }}</td>

                                    <td style="width: 20%; min-width: 50px">
                                        <div class="form-group">
                                            @if(! ($evidence->results)->isEmpty())
                                                @foreach($evidence->results as $result)
                                                    @if($result->student_id == $student->id)
                                                        <input type="range" min="0" max="2" step="1" value="{{ old('result') ?? $result->result }}"
                                                               id="rangeKnowledge{{$evidence->id}}{{$knowledge->id}}{{$contKnowledge}}" name="range[]"
                                                               class="form-control-range">
                                                    @endif
                                                @endforeach
                                            @else
                                                <input type="range" min="0" max="2" step="1" value="0"
                                                       id="rangeKnowledge{{$evidence->id}}{{$knowledge->id}}{{$contKnowledge}}" name="range[]"
                                                       class="form-control-range">
                                            @endif
                                        </div>
                                    </td>

                                    <td style=" width: 22rem; text-align: center">
                                        {{--pega o id da evidencia--}}
                                        <div class="form-group">
                                            <input style="display: none" name="evidence_id[]" type="text" class="form-control"
                                                   value="{{ $evidence->id }}" readonly>
                                        </div>
                                        <p><span class="range" id="outKnowledge{{$evidence->id}}{{$knowledge->id}}{{$contKnowledge}}"></span></p>
                                    </td>

                                    <script>
                                        var sliderKnowledge{{$evidence->id}}{{$knowledge->id}}{{$contKnowledge}} = document.getElementById("rangeKnowledge{{$evidence->id}}{{$knowledge->id}}{{$contKnowledge}}");
                                        var outputKnowledge{{$evidence->id}}{{$knowledge->id}}{{$contKnowledge}} = document.getElementById("outKnowledge{{$evidence->id}}{{$knowledge->id}}{{$contKnowledge}}");


                                        if (sliderKnowledge{{$evidence->id}}{{$knowledge->id}}{{$contKnowledge}}.value == 0) {
                                            outputKnowledge{{$evidence->id}}{{$knowledge->id}}{{$contKnowledge}}.style.color = "rgb(202, 19, 19)";
                                            outputKnowledge{{$evidence->id}}{{$knowledge->id}}{{$contKnowledge}}.innerHTML = 'Não Desenvolveu';
                                        } else if (sliderKnowledge{{$evidence->id}}{{$knowledge->id}}{{$contKnowledge}}.value == 1) {
                                            outputKnowledge{{$evidence->id}}{{$knowledge->id}}{{$contKnowledge}}.style.color = "#a89600";
                                            outputKnowledge{{$evidence->id}}{{$knowledge->id}}{{$contKnowledge}}.innerHTML = 'Desenvolveu Parcialmente';
                                        } else if (sliderKnowledge{{$evidence->id}}{{$knowledge->id}}{{$contKnowledge}}.value == 2) {
                                            outputKnowledge{{$evidence->id}}{{$knowledge->id}}{{$contKnowledge}}.style.color = "#00a808";
                                            outputKnowledge{{$evidence->id}}{{$knowledge->id}}{{$contKnowledge}}.innerHTML = 'Desenvolveu';
                                        }
                                        sliderKnowledge{{$evidence->id}}{{$knowledge->id}}{{$contKnowledge}}.oninput = function () {

                                            if (sliderKnowledge{{$evidence->id}}{{$knowledge->id}}{{$contKnowledge}}.value == 0) {
                                                outputKnowledge{{$evidence->id}}{{$knowledge->id}}{{$contKnowledge}}.style.color = "rgb(202, 19, 19)";
                                                outputKnowledge{{$evidence->id}}{{$knowledge->id}}{{$contKnowledge}}.innerHTML = 'Não Desenvolveu';
                                            } else if (sliderKnowledge{{$evidence->id}}{{$knowledge->id}}{{$contKnowledge}}.value == 1) {
                                                outputKnowledge{{$evidence->id}}{{$knowledge->id}}{{$contKnowledge}}.style.color = "#a89600";
                                                outputKnowledge{{$evidence->id}}{{$knowledge->id}}{{$contKnowledge}}.innerHTML = 'Desenvolveu Parcialmente';
                                            } else if (sliderKnowledge{{$evidence->id}}{{$knowledge->id}}{{$contKnowledge}}.value == 2) {
                                                outputKnowledge{{$evidence->id}}{{$knowledge->id}}{{$contKnowledge}}.style.color = "#00a808";
                                                outputKnowledge{{$evidence->id}}{{$knowledge->id}}{{$contKnowledge}}.innerHTML = 'Desenvolveu';
                                            }
                                        }
                                    </script>

                                </tr>

                            @endforeach

                            </tbody>
                        </table>

                    </div>

                </div>

            @endforeach

        @else
            <div class="alert alert-warning">@lang('status.knowledge.none')</div>

        @endif



        <hr>
        {{--Competências--}}
        <h2 align="center">@lang('strings.competences')</h2>

        @if(! $lesson->competencies->isEmpty())
            @foreach($lesson->competencies as $competence)

                <div class="well">

                    <p align="center"><strong>{{ $competence->description }}</strong></p>


                    <div class="table-responsive">

                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th style="text-align: center; width: 45%" scope="col">@lang('strings.evidences')</th>
                                <th style="text-align: center" scope="col" colspan="2">@lang('strings.evaluation')</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php
                            $contCompetence = 0;
                            ?>

                            @foreach($competence->evidences as $evidence)

                                <?php $contCompetence ++  ?>

                                <tr>

                                    {{--pega o id da Competência--}}
                                    <div class="form-group">
                                        <input style="display: none" name="competence[]" type="text" class="form-control"
                                               value="{{ $competence->id }}" readonly>
                                    </div>

                                    <td>{{ $evidence->description }}</td>

                                    <td style="width: 20%; min-width: 50px">
                                        <div class="form-group">
                                            @if(! ($evidence->results)->isEmpty())
                                                @foreach($evidence->results as $result)
                                                    @if($result->student_id == $student->id)
                                                        <input type="range" min="0" max="2" step="1" value="{{ old('result') ?? $result->result }}"
                                                               id="rangeCompetence{{$evidence->id}}{{$competence->id}}{{$contCompetence}}" name="range[]"
                                                               class="form-control-range">
                                                    @endif
                                                @endforeach
                                            @else
                                                <input type="range" min="0" max="2" step="1" value="0"
                                                       id="rangeCompetence{{$evidence->id}}{{$competence->id}}{{$contCompetence}}" name="range[]"
                                                       class="form-control-range">
                                            @endif
                                        </div>
                                    </td>

                                    <td style="width: 22rem; text-align: center">
                                        {{--pega o id da evidencia--}}
                                        <div class="form-group">
                                            <input style="display: none" name="evidence_id[]" type="text" class="form-control"
                                                   value="{{$evidence->id}}" readonly>
                                        </div>
                                        <p><span class="range" id="outCompetence{{$evidence->id}}{{$competence->id}}{{$contCompetence}}"></span></p>
                                    </td>

                                    <script>
                                        var sliderCompetence{{$evidence->id}}{{$competence->id}}{{$contCompetence}} = document.getElementById("rangeCompetence{{$evidence->id}}{{$competence->id}}{{$contCompetence}}");
                                        var outputCompetence{{$evidence->id}}{{$competence->id}}{{$contCompetence}} = document.getElementById("outCompetence{{$evidence->id}}{{$competence->id}}{{$contCompetence}}");

                                        if (sliderCompetence{{$evidence->id}}{{$competence->id}}{{$contCompetence}}.value == '0') {
                                            outputCompetence{{$evidence->id}}{{$competence->id}}{{$contCompetence}}.style.color = "rgb(202, 19, 19)";
                                            outputCompetence{{$evidence->id}}{{$competence->id}}{{$contCompetence}}.innerHTML = 'Não Desenvolveu';
                                        } else if (sliderCompetence{{$evidence->id}}{{$competence->id}}{{$contCompetence}}.value == '1') {
                                            outputCompetence{{$evidence->id}}{{$competence->id}}{{$contCompetence}}.style.color = "#a89600";
                                            outputCompetence{{$evidence->id}}{{$competence->id}}{{$contCompetence}}.innerHTML = 'Desenvolveu Parcialmente';
                                        } else if (sliderCompetence{{$evidence->id}}{{$competence->id}}{{$contCompetence}}.value == '2') {
                                            outputCompetence{{$evidence->id}}{{$competence->id}}{{$contCompetence}}.style.color = "#00a808";
                                            outputCompetence{{$evidence->id}}{{$competence->id}}{{$contCompetence}}.innerHTML = 'Desenvolveu';
                                        }

                                        sliderCompetence{{$evidence->id}}{{$competence->id}}{{$contCompetence}}.oninput = function () {

                                            if (sliderCompetence{{$evidence->id}}{{$competence->id}}{{$contCompetence}}.value == '0') {
                                                outputCompetence{{$evidence->id}}{{$competence->id}}{{$contCompetence}}.style.color = "rgb(202, 19, 19)";
                                                outputCompetence{{$evidence->id}}{{$competence->id}}{{$contCompetence}}.innerHTML = 'Não Desenvolveu';
                                            } else if (sliderCompetence{{$evidence->id}}{{$competence->id}}{{$contCompetence}}.value == '1') {
                                                outputCompetence{{$evidence->id}}{{$competence->id}}{{$contCompetence}}.style.color = "#a89600";
                                                outputCompetence{{$evidence->id}}{{$competence->id}}{{$contCompetence}}.innerHTML = 'Desenvolveu Parcialmente';
                                            } else if (sliderCompetence{{$evidence->id}}{{$competence->id}}{{$contCompetence}}.value == '2') {
                                                outputCompetence{{$evidence->id}}{{$competence->id}}{{$contCompetence}}.style.color = "#00a808";
                                                outputCompetence{{$evidence->id}}{{$competence->id}}{{$contCompetence}}.innerHTML = 'Desenvolveu';
                                            }

                                        }
                                    </script>

                                </tr>

                            @endforeach

                            </tbody>
                        </table>

                    </div>

                </div>

            @endforeach
        @else
            <div class="alert alert-warning">@lang('status.competence.none')</div>

        @endif


        <input type="submit" value="@lang('strings.save')" class="btn btn-block btn-success">

    </form>


@endsection

