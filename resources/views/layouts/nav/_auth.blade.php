<nav class="navbar navbar-default navbar-static-top">

    <div class="container" id="menu">

        <div class="navbar-header">
        @if(!(Auth::user()->isStudent()))
            @include('layouts.nav._hamburger')
        @endif
        <!-- Branding Image -->
            <a class="navbar-brand" href="{{ route('dashboard.index')}}">
                {{ config('app.name', 'Laravel') }}
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">

                @if(Auth::user()->isAdmin())
                    <li><a href="{{ route('users.index') }}">@lang('strings.users')</a></li>
                    <li><a href="{{ route('student.index') }}">@lang('strings.students')</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false">@lang('strings.classes') <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">

                            <li>
                                <a id="anchor-classes" href="{{ route('classes.index') }}">

                                    @lang('strings.all_classes')
                                </a>
                                <a id="anchor-myclasses" href="{{ route('classes.mine.index') }}">

                                    @lang('strings.my_classes')
                                </a>

                            </li>

                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false">@lang('strings.lessons') <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">

                            <li>
                                <a id="anchor-mylessons" href="{{ route('lessons.index') }}">

                                    @lang('strings.all_lessons')
                                </a>
                                <a id="anchor-alllessons" href="{{ route('lessons.mine.index') }}">

                                    @lang('strings.my_lessons')
                                </a>

                            </li>

                        </ul>
                    </li>

                    <li><a href="{{ route('graphics_of_user') }}">@lang('strings.graphics')</a></li>

                    <li><a href="{{ route('statistic_module') }}">@lang('strings.statistic_module')</a></li>

                @endif

                @if(Auth::user()->isCollaborator())
                    <li><a href="{{ route('student.index') }}">@lang('strings.students')</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false">@lang('strings.classes') <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">

                            <li>
                                <a id="anchor-classes" href="{{ route('classes.index') }}">

                                    @lang('strings.all_classes')
                                </a>
                                <a id="anchor-myclasses" href="{{ route('classes.mine.index') }}">

                                    @lang('strings.my_classes')
                                </a>

                            </li>

                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false">@lang('strings.lessons') <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">

                            <li>
                                <a id="anchor-mylessons" href="{{ route('lessons.index') }}">

                                    @lang('strings.all_lessons')
                                </a>
                                <a id="anchor-alllessons" href="{{ route('lessons.mine.index') }}">

                                    @lang('strings.my_lessons')
                                </a>

                            </li>

                        </ul>
                    </li>
                    <li><a href="{{ route('graphics_of_user') }}">@lang('strings.graphics')</a></li>

                        <li><a href="{{ route('statistic_module') }}">@lang('strings.statistic_module')</a></li>

                @endif

                @if(Auth::user()->isStudent())
                    <li><a href="{{ route('graphics_of_student') }}">@lang('strings.my_results')</a></li>
                @endif

            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                       aria-expanded="false">@lang('strings.manual') <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">

                        <li>
                            <a target="_blank" href="{{ url('manual/CADAP - Manual de Instruções Professorv2.pdf') }}">

                                @lang('strings.teacher')
                            </a>
                            <a target="_blank" href="{{ url('manual/CADAP - Manual de Instruções Estudante.pdf') }}">

                                @lang('strings.student')
                            </a>

                        </li>

                    </ul>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu" role="menu">

                        <li>
                            <a id="anchor-changepwd" href="{{ route('page.password') }}">

                                @lang('auth.change_password')
                            </a>
                            <a id="anchor-logout" href="{{ route('logout') }}">

                                @lang('auth.logout')
                            </a>

                        </li>

                    </ul>

                </li>

            </ul>

        </div>

    </div>

</nav>
