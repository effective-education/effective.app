<nav class="navbar navbar-default navbar-static-top">

    <div class="container" id = "menu">

        <div class="navbar-header">

        @include('layouts.nav._hamburger')

        <!-- Branding Image -->
            <a class="navbar-brand" href="{{ route('pages.index') }}">
                {{ config('app.name', 'Laravel') }}
            </a>

        </div>

        <ul class="nav navbar-nav">
            <li><a href="{{ route('student.register') }}">@lang('strings.register_self')</a></li>
        </ul>



        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Right Side Of Navbar -->


            <ul class="nav navbar-nav navbar-right">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                       aria-expanded="false">@lang('strings.manual') <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">

                        <li>
                            <a target="_blank" href="{{ url('manual/CADAP - Manual de Instruções Professorv2.pdf') }}">

                                @lang('strings.teacher')
                            </a>
                            <a target="_blank" href="{{ url('manual/CADAP - Manual de Instruções Estudante.pdf') }}">

                                @lang('strings.student')
                            </a>

                        </li>

                    </ul>
                </li>

                <!-- Authentication Links -->
                <li><a href="{{ route('login') }}">@lang('auth.login')</a></li>
                @if(! App\Models\User::existsAtLeastOne())
                    <li><a href="{{ route('register') }}">@lang('auth.register')</a></li>
                @endif

            </ul>

        </div>

    </div>

</nav>
