<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Method: GET, POST, PUT, OPTIONS');
header('Access-Control-Allow-Credentials: true');
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="url" content="{{ url('/') }}">

    <link rel="shortcut icon" href="{{url('images/logo.ico')}}" type="image/x-icon" />

    <title>{{ config('app.name', 'Laravel') }}</title>

    <meta name="Access-Control-Allow-Origin: *" >

    <?php header('Access-Control-Allow-Origin: *'); ?>

    <!--Scripts-->
    <script src="{{ asset('js/high-contrast.js') }}"></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/footer.css') }}">
    <link rel="stylesheet" href="{{ asset('css/contrast.css') }}">

</head>

{{--Valida a versão do Navegador--}}
@if( !(\App\Models\User::browser_is_ok()) )
    <div class="container">
        <div class="alert alert-danger " role="alert"
             style="padding: 0.3rem !important; background-color: #dc3545 !important; ">
            <div class="row">
                <div class="col-sm-11">
                    <p align="center" style="color: white; margin-left: 5em; margin-right: 1rem">
                        Aparentemente você está utilizando uma versão de Browser não suportada. Por favor utilize
                        uma versão
                        anterior do seu navegador!
                        {{\App\Models\User::type_and_version()}}
                    </p>
                </div>

                <div class="col-sm-1">
                    <button style="margin-top: 1.4rem; margin-right: 1rem; margin-bottom: 1rem" type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
@endif


<body>
<div id="app" style="padding-top: 5em;">

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2" style="position: static">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @yield('heading')
                        <div class="pull-right">
                            @yield('actions')
                        </div>
                    </div>

                    <div class="panel-body">

                        @include('partials._erros')
                        @include('partials._status')

                        @yield('content')
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                @yield('right_content')
            </div>
        </div>
    </div>

</div>

</body>

@stack('before-scripts')

<script src="{{ asset('js/app.js') }}"></script>

@stack('after-scripts')



</html>
