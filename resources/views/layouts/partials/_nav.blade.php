@if(Auth::guest())
    @include('layouts.nav._guest')
@else
    @include('layouts.nav._auth')
@endif