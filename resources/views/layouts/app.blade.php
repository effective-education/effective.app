<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Method: GET, POST, PUT, OPTIONS');
header('Access-Control-Allow-Credentials: true');
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="url" content="{{ url('/') }}">

    <link rel="shortcut icon" href="{{url('images/logo.ico')}}" type="image/x-icon" />

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!--Scripts-->
    <script src="{{ asset('js/high-contrast.js') }}"></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/footer.css') }}">
    <link rel="stylesheet" href="{{ asset('css/contrast.css') }}">

</head>

{{--Valida a Versão do Navegador--}}
    @if( !(\App\Models\User::browser_is_ok()) )
        <div class="container">
            <div class="alert alert-danger " role="alert"
                 style="padding: 0.3rem !important; background-color: #dc3545 !important; ">
            <div class="row">
                    <div class="col-sm-11">
                        <p align="center" style="color: white; margin-left: 5em; margin-right: 1rem">
                            Aparentemente você está utilizando uma versão de Browser não suportada. Por favor utilize
                            uma versão
                            anterior do seu navegador!
                            {{\App\Models\User::type_and_version()}}
                        </p>
                    </div>

                    <div class="col-sm-1">
                        <button style="margin-top: 1.4rem; margin-right: 1rem; margin-bottom: 1rem" type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    @endif



<!--Accessibility-->
    <div id="acessibilidade" class="hidden-xs">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="nav nav-pills">
                        <li>
                            <a accesskey="w" href="#menu" id="link-nav">@lang('strings.go_to_menu')
                                <span class="button_accessibility">[ w ]</span></a>
                        </li>
                        <li>
                            <a accesskey="t" href="#container" id="link-nav">@lang('strings.go_to_content')
                                <span class="button_accessibility">[ t ]</span></a>
                        </li>
                        <li>
                            <a accesskey="a" href="#footer" id="link-nav">@lang('strings.go_to_footer')
                                <span class="button_accessibility">[ a ]</span></a>
                        </li>
                        <li>
                            <a accesskey="q" href="{{route('map.index')}}"
                               id="link-nav">@lang('strings.go_to_map') <span class="button_accessibility">[ q ]</span></a>
                        </li>
                        <li>
                            <a accesskey="s" class="ativa-contraste" href="#" id="is-normal-contrast"
                               onclick="window.toggleContrast()"
                               onkeydown="window.toggleContrast()">@lang('strings.hight_contrast')<span class="button_accessibility">[ s ]</span></a>
                        </li>
                        <li>
                            <a accesskey="0" href="#" class="res-font" onclick='textoNormal()'>A</a>
                        </li>
                        <li>
                            <a accesskey="-" href="#" class="dec-font" onclick='textoMenor()'>A-</a>
                        </li>
                        <li>
                            <a accesskey="=" href="#" class="inc-font" onclick='textoMaior()'>A+</a>
                        </li>

                        <!--Plugin of Translation-->
                        <li class="text-right pull-right">
                            <div id="google_translate_element" style="margin-top: 1rem"></div>
                            <script type="text/javascript">
                                function googleTranslateElementInit() {
                                    new google.translate.TranslateElement({
                                        pageLanguage: 'pt',
                                        includedLanguages: 'de,en,es,fr,it',
                                        layout: google.translate.TranslateElement.InlineLayout.SIMPLE
                                    }, 'google_translate_element');
                                }
                            </script>
                            <script type="text/javascript"
                                    src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit">

                            </script>

                        </li>

                        {{--<li class="text-right pull-right">
                            <a accesskey="z" href="{{route('setlocale', 'es')}}" class="nodec">
                                <img src="{{ url('icons/img-es-small.png') }}" alt="es"/>
                            </a>
                        </li>
                        <li class="text-right pull-right">
                            <a accesskey="x" href="{{route('setlocale', 'pt')}}" style="text-decoration: none">
                                <img src="{{ url('icons/img-br-small.png') }}" alt="pt"/>
                            </a>
                        </li>--}}

                    </ul>

                    <div id="google_translate_element" class="text-right pull-right"></div>
                </div>
            </div>
        </div>
    </div>


    <script>
        function textoMenor() {
            var size = $("body").css('font-size');
            size = size.replace('px', '');
            size = parseInt(size) - 1.4;
            var h1 = size + 22;
            var h2 = size + 16;
            var h3 = size + 10;
            var h4 = size + 4;
            var h5 = size;
            var h6 = size - 2;
            var title = size + 50;
            $("body, th, td, #footer p, #footer a").animate({'font-size': size + 'px'});
            $(".btn").animate({'font-size': size + 'px'});
            $(".h1, h1").animate({'font-size': h1 + 'px'});
            $(".h2, h2").animate({'font-size': h2 + 'px'});
            $(".h3, h3").animate({'font-size': h3 + 'px'});
            $(".h4, h4").animate({'font-size': h4 + 'px'});
            $(".h5, h5").animate({'font-size': h5 + 'px'});
            $(".h6, h6").animate({'font-size': h6 + 'px'});
            $(".title").animate({'font-size': title + 'px'});
        }
    </script>

    <script>
        function textoMaior() {
            var size = $("body").css('font-size');
            size = size.replace('px', '');
            size = parseInt(size) + 1.4;
            var h1 = size + 22;
            var h2 = size + 16;
            var h3 = size + 10;
            var h4 = size + 4;
            var h5 = size;
            var h6 = size - 2;
            var title = size + 70;
            $("body, th, td, #footer p, #footer a").animate({'font-size': size + 'px'});
            $(".btn").animate({'font-size': size + 'px'});
            $(".h1, h1").animate({'font-size': h1 + 'px'});
            $(".h2, h2").animate({'font-size': h2 + 'px'});
            $(".h3, h3").animate({'font-size': h3 + 'px'});
            $(".h4, h4").animate({'font-size': h4 + 'px'});
            $(".h5, h5").animate({'font-size': h5 + 'px'});
            $(".h6, h6").animate({'font-size': h6 + 'px'});
            $(".title").animate({'font-size': title + 'px'});
        }
    </script>

    <script>
        function textoNormal() {
            $("body, th, td, #footer p, #footer a").animate({'font-size': '14px'});
            $(".btn").animate({'font-size': '14px'});
            $(".h1, h1").animate({'font-size': '36px'});
            $(".h2, h2").animate({'font-size': '30px'});
            $(".h3, h3").animate({'font-size': '24px'});
            $(".h4, h4").animate({'font-size': '18px'});
            $(".h5, h5").animate({'font-size': '14px'});
            $(".h6, h6").animate({'font-size': '12px'});
            $(".title").animate({'font-size': '84px'});
        }
    </script>



<body class="contraste">
<div id="app">

    @include('layouts.partials._nav')

    <div class="container" id="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        @yield('heading')
                        <div class="pull-right">
                            @yield('actions')
                        </div>
                    </div>

                    <div class="panel-body">

                        @include('partials._erros')
                        @include('partials._status')

                        @yield('content')
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                @yield('right_content')
            </div>
        </div>
    </div>

</div>

</body>


<footer class="footer" id="footer">
    <div class="container-fluid">

        <div class="row conteudo-rodape">

            {{--<div class="col-md-4">--}}
                {{--<div style="align-items: center;  display: flex;  flex-direction: row;  flex-wrap: wrap;  justify-content: center;">--}}
                    {{--<a href="https://acacia.digital/pt/">--}}
                        {{--<img style="margin: 5px;" src="{{url('images/logo_acacia_small.png')}}">--}}
                    {{--</a>--}}
                    {{--<a href="https://europa.eu/european-union/index_pt">--}}
                        {{--<img style="margin: 5px;" src="{{url('images/union_europea.png')}}">--}}
                    {{--</a>--}}
                {{--</div>--}}
            {{--</div>--}}


            <div class="col-md-5 offset-md-1">

                <h5 style="text-align: center">@lang('footer.tested_on')</h5>

                <div style="align-items: center;  display: flex;  flex-direction: row;  flex-wrap: wrap;  justify-content: center; border-top: #f4fcfb 1.2px solid;">
                    <a href="https://www.tawdis.net" target="_blank">
                        <img style="margin-top: 1rem; margin-bottom: 1rem; margin-right: 2em; margin-left: 2em; " src="{{url('icons/taw_1_A.png')}}">
                    </a>

                    <a href="http://www.sidar.org/hera/index.php.pt" target="_blank">
                        <img style="margin-top: 1rem; margin-bottom: 1rem; margin-right: 2em; margin-left: 2em; width: 40px; height: 33px" src="{{url('images/hera.gif')}}">
                    </a>
                    <a href="http://www.dasilva.org.br/">
                        <img style="margin-top: 1rem; margin-bottom: 1rem; margin-right: 2em; margin-left: 2em; width: 70px; height: 25px" src="{{url('images/daSilva.png')}}">
                    </a>
                </div>
            </div>


            <div class="col-6 col-md-3" style="text-align: center; line-height: 2 ">

                <a href="{{route('dashboard.index')}}">
                    <i class="fas fa-home"></i>
                    @lang('footer.home')<br>
                </a>

                <a href="{{route('map.index')}}" >
                    <i class="far fa-map"></i>
                    @lang('strings.go_to_map')<br>
                </a>

                <a href="{{route('accessibility.index')}}">
                    <i class="fab fa-accessible-icon"></i>
                    @lang('footer.accessibility')<br>
                </a>

            </div>

            <div class="col-6 col-md-3" style="text-align: center; line-height: 2 ">
                <a href="mailto:carlamarina@gmail.com?subject=Contato%20Acacia">
                    <i class="far fa-envelope"></i>
                    @lang('footer.contact')<br>
                </a>


                <p>
                    @lang('footer.version') {{config('app.version')}}
                </p>
            </div>


        </div>

    </div>


</footer>


<!-- Scripts -->


@stack('before-scripts')

<script src="{{ asset('js/app.js') }}"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">

@stack('after-scripts')

@if(Auth::check())
    {{-- Form to logout user. --}}
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
@endif


</html>
