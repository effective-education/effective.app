@extends('layouts.app')

@section('heading', $class->name)

@section('content')

    <button type="button" name="id_class" class="btn btn-primary btn-block" data-toggle="modal" data-target="#ilsModal">
        @lang('strings.add_lessons')
    </button>

    <form id="lesson">

        {{--Aulas--}}
        <label><h3>@lang('strings.lessons')</h3></label>

        @if(! $std->isEmpty())
            <div class="table-responsive">

                <table class="table">

                    <thead>
                    <tr>
                        <th>@lang('strings.title')</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($std as $st)
                        <tr>
                            <td>{{ $st->title }}</td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>
                <a href="{{route('classes.index')}}" class="btn btn-block btn-default"> @lang('strings.back')</a>
            </div>

            {{ $std->links() }}

        @else

            <div class="alert alert-warning">@lang('status.class_lesson.none')</div>

        @endif


    </form>


    <form method="POST" action="{{ route('class_lesson.store') }}" class="form" id="framework_form">


        {{ csrf_field() }}


        <div class="modal fade" id="ilsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

            <div class="modal-dialog" role="document">

                <div class="modal-content">




                    {{--pega o id da turma--}}
                    <div class="form-group">
                        <input style="display: none" name="classe_id" type="text" class="form-control"
                               value="{{ $class->id }}" readonly>
                    </div>


                    <div class="modal-body" align="center">
                        <label style="text-align: center">@lang('strings.lessons')</label>
                        <br>

                        <div class="form-group">
                            <select id="framework" name="lesson_id[]" multiple class="form-control select2"
                                    style="width: 100%" required>
                                @if(! $lessons->isEmpty())
                                    @foreach($lessons as $lesson)
                                        <option value="{{ $lesson->id }}">
                                            {{ $lesson->title }}
                                        </option>
                                    @endforeach
                                @else
                                    <div class="alert alert-warning">@lang('status.class_lesson.none')</div>
                                @endif
                            </select>
                        </div>

                    </div>

                    <input type="submit" value="@lang('strings.save')" class="btn btn-block btn-success">

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default"
                                data-dismiss="modal">@lang('strings.close')</button>
                    </div>

                </div>

            </div>

        </div>

    </form>

    {{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>--}}
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>--}}
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/>--}}
    {{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>--}}
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>--}}
    {{--<link rel="stylesheet"--}}
          {{--href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css"/>--}}

@endsection
