@extends('layouts.app')

@section('heading', trans('strings.impressions'))

@section('content')

    <h2>{{ $impression->result->student->name }}</h2>
    <label>{{ $impression->result->lesson->title  }}</label>

    <hr>

    @foreach(trans('impressions.types') as $type => $label)

        <div class="row">
            <div class="col-md-6"><label>{{ $label }}</label></div>
            <div class="col-md-6"><p>{{ $impression->getImpression($type) }}</p></div>
        </div>
        <hr>

    @endforeach

@endsection