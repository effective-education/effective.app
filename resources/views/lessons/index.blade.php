@extends('layouts.app')

@section('heading', trans('strings.lessons'))

@section('actions')
    <a href="{{ route('lessons.create') }}" class="btn btn-sm btn-success">@lang('strings.create')</a>
@endsection

@section('content')

    @if(! $lessons->isEmpty())

        <div class="table-responsive">

            <table class="table">

                <thead>
                <tr>
                    <th>@lang('strings.title')</th>
                    <th>@lang('strings.category')</th>
                    <th>@lang('strings.created_by')</th>
                    <th>@lang('strings.action')</th>
                </tr>
                </thead>

                <tbody>
                @foreach($lessons as $lesson)
                    <tr>
                        <td>{{ $lesson->title }}</td>
                        <td>{{ $lesson->knowledgeArea->title }}</td>
                        <td>{{ $lesson->creator->name }}</td>
                        <td>
                            <a href="{{ route('lessons.edit', [$lesson->id]) }}">@lang('strings.edit')</a>
                            <a href="{{ route('lesson.results', [$lesson->id]) }}">@lang('strings.results')</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>

            </table>

        </div>

        {{ $lessons->links() }}

    @else

        <div class="alert alert-warning">@lang('status.lessons.none')</div>

    @endif

@endsection