<template v-for="(knowledge, knowledgeIndex) in knowledge_s.all()">

    <div class="panel well">

        <div class="panel-heading pull-right">

            {{-- Remove knowledge_s --}}
            <button type="button" class="btn btn-danger btn-sm" @click="knowledge_s.remove(knowledgeIndex)">
            @lang('strings.remove')
            </button>

        </div>

        <div class="panel-body">

            <input
                    type="hidden"
                    v-model="knowledge.id"
                    :name="'knowledge_s['+knowledgeIndex+'][id]'">

            <div class="form-group">

                <div class="form-group">

                    <label>@lang('strings.knowledge')</label>

                    {{-- Abilities descriptions --}}
                    <textarea
                            cols="30"
                            rows="2"
                            class="form-control"
                            v-model="knowledge.description"
                            :name="'knowledge_s['+knowledgeIndex+'][description]'">

                </textarea>

                </div>


            </div>

            <div class="form-group">

                <label>@lang('strings.evidences')</label>

                @include('lessons.partials._evidenceKnowledge')

            </div>

        </div>

        <div class="panel-footer">

            {{-- Add new evidence --}}
            <button type="button" class="btn btn-success btn-block btn-sm" @click="knowledge.evidences.create()">
                @lang('strings.add_evidence')
            </button>

        </div>

    </div>

</template>