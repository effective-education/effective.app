<div class="form-group">

    <label>@lang('strings.contents')</label>

    @foreach($lesson->contents as $content)

        <div class="well">

            <p><strong>@lang('strings.start_time'):</strong><span>{{ $content->start_time }}</span></p>
            <p><strong>@lang('strings.end_time'):</strong><span>{{ $content->end_time }}</span></p>
            <p><strong>@lang('strings.content_tags'):</strong><span>{{ $content->tags }}</span></p>

        </div>

    @endforeach

    <button type="button" class="btn btn-danger btn-block" v-show="!contents_were_changed"
    @click="toggleContentsEdit">
    @lang('strings.change_contents')
    </button>

</div>

<div class="form-group" v-show="contents_were_changed">

    <label>@lang('strings.contents')</label>

    @include('lessons.partials._content')

    <button type="button" class="btn btn-success btn-block btn-sm" @click="contents.create()">
    @lang('strings.add_content')
    </button>

    <button type="button" class="btn btn-danger btn-block" @click="toggleContentsEdit">
    @lang('strings.cancel')
    </button>

</div>