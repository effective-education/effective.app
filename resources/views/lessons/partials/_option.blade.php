<template v-for="(option, optionIndex) in question.options.all()">

    <input
            type="hidden"
            v-model="option.id"
            :name="'questions['+questionIndex+'][options]['+optionIndex+'][id]'">

    <div class="row">

        <div class="col-md-8">

            {{-- Option title --}}
            <textarea
                    cols="30"
                    rows="2"
                    class="form-control"
                    v-model="option.text"
                    :name="'questions['+questionIndex+'][options]['+optionIndex+'][text]'">
            </textarea>

        </div>

        <div class="col-md-2">

            <label>@lang('strings.is_right')</label>

            {{-- Is this option the right answer? --}}
            <input
                    type="checkbox"
                    v-model="option.is_right"
                    :name="'questions['+questionIndex+'][options]['+optionIndex+'][is_right]'">

        </div>

        <div class="col-md-2">

            {{-- Remove option --}}
            <button type="button" class="btn btn-danger btn-block btn-sm" @click="question.options.remove(optionIndex)">
            @lang('strings.remove')
            </button>

        </div>

    </div>

    <br>

</template>