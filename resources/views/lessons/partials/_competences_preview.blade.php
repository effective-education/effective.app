<div class="form-group">

    <label>@lang('strings.competences')</label>

    @foreach($lesson->competencies as $competence)

        <div class="well">

            <p><strong>{{ $competence->description }}</strong></p>
            <ul>
                @foreach($competence->evidences as $evidence)
                    <li>
                        {{ $evidence->description }}
                    </li>
                @endforeach
            </ul>

        </div>

    @endforeach

    <button type="button" class="btn btn-danger btn-block" v-show="!competencies_were_changed"
    @click="toggleCompetenciesEdit">
    @lang('strings.change_competencies')
    </button>

</div>

<div class="form-group" v-show="competencies_were_changed">

    <label>@lang('strings.competences')</label>

    @include('lessons.partials._competence')

    <button type="button" class="btn btn-success btn-block btn-sm" @click="competences.create()">
    @lang('strings.add_competence')
    </button>

    <button type="button" class="btn btn-danger btn-block" @click="toggleCompetenciesEdit">
    @lang('strings.cancel')
    </button>

</div>