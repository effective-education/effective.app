<div class="form-group" xmlns:v-bind="http://www.w3.org/1999/xhtml" xmlns:v-bind="http://www.w3.org/1999/xhtml">

    <label>@lang('strings.video')</label>

    @if($lesson->videoIsFromFile())

        <video width="100%" height="100%" controls>
            <source src="{{ \Illuminate\Support\Facades\Storage::url($lesson->video_uri) }}" type="video/mp4">
            @lang('status.dont_support_video')
        </video>

    @else

        <iframe width="100%" height="400" src="https://www.youtube.com/embed/{{$lesson->video_uri}}"
                frameborder="0" sandbox="allow-forms allow-scripts allow-same-origin allow-forms allow-presentation" enablejsapi=1 allowfullscreen>
        </iframe>
    
    @endif

</div>

<div class="form-group">

    <button type="button" class="btn btn-danger btn-block"
            v-show="!video_was_changed" @click='toggleVideoEdit'>@lang('strings.change_video')</button>

</div>

<div v-if="video_was_changed">

    {{-- Video Type --}}
    <div class="form-group" style="display: none;">
        <label>@lang('strings.video_type')</label>
        <select name="video_type" class="form-control" v-model="videoType" required>
            <option value="{{ \App\Models\Lesson::VIDEO_TYPE_YOUTUBE }}">@lang('strings.youtube')</option>
            <option value="{{ \App\Models\Lesson::VIDEO_TYPE_FILE }}">@lang('strings.file')</option>
        </select>
    </div>

    {{-- Video file --}}
    <div class="form-group" v-show="!isVideoTypeYoutube">
        <label>@lang('strings.send_file')</label>
        <input type="file" name="video_file" class="form-control" v-bind:required="(!isVideoTypeYoutube)">
    </div>

    {{-- Video ID from youtube --}}
    <div class="form-group" v-show="isVideoTypeYoutube">
        <label>@lang('strings.video_id_youtube')</label>
        <input type="text" name="video_id" class="form-control" v-bind:required="isVideoTypeYoutube"
               value="{{ old('video_id') }}">
    </div>

    <div class="form-group">

        <button type="button" class="btn btn-danger btn-block" @click="toggleVideoEdit">@lang('strings.cancel')</button>

    </div>

</div>