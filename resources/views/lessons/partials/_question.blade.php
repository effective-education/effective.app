<template v-for="(question, questionIndex) in questions.all()">

    <div class="panel well">

        <div class="panel-heading pull-right">

            {{-- Remove question --}}
            <button type="button" class="btn btn-danger btn-sm" @click="questions.remove(questionIndex)">
            @lang('strings.remove')
            </button>

        </div>

        <div class="panel-body">

            <input
                    type="hidden"
                    v-model="question.id"
                    :name="'questions['+questionIndex+'][id]'">

            <div class="form-group">

                <div class="form-group">

                    <label>@lang('strings.question')</label>

                    {{-- Question's text --}}
                    <textarea
                            cols="30"
                            rows="2"
                            class="form-control"
                            v-model="question.title"
                            :name="'questions['+questionIndex+'][title]'">

                </textarea>

                </div>

                <div class="form-group">

                    <label>@lang('strings.video_timestamp')</label>

                    {{-- Question's timestamp --}}
                    <input type="text"
                           class="form-control"
                           v-model="question.timestamp"
                    @keyup="question.timestamp = $event.target.value"
                    v-mask-hour
                    :name="'questions['+questionIndex+'][timestamp]'">

                </div>

            </div>

            <div class="form-group">

                <label>@lang('strings.options')</label>

                @include('lessons.partials._option')

            </div>

        </div>

        <div class="panel-footer">

            {{-- Add new option --}}
            <button type="button" class="btn btn-success btn-block btn-sm" @click="question.options.create()">
            @lang('strings.add_option')
            </button>

        </div>

    </div>

</template>