<div class="form-group">

    <label>@lang('strings.knowledge_s')</label>

    @foreach($lesson->knowledge_s as $knowledge)

        <div class="well">

            <p><strong>{{ $knowledge->description }}</strong></p>
            <ul>
                @foreach($knowledge->evidences as $evidence)
                    <li>
                        {{ $evidence->description }}
                    </li>
                @endforeach
            </ul>

        </div>

    @endforeach

    <button type="button" class="btn btn-danger btn-block" v-show="!knowledge_s_were_changed"
    @click="toggleKnowledge_sEdit">
    @lang('strings.change_knowledge_s')
    </button>

</div>

<div class="form-group" v-show="knowledge_s_were_changed">

    <label>@lang('strings.knowledge_s')</label>

    @include('lessons.partials._knowledge')

    <button type="button" class="btn btn-success btn-block btn-sm" @click="knowledge_s.create()">
    @lang('strings.add_knowledge')
    </button>

    <button type="button" class="btn btn-danger btn-block" @click="toggleKnowledge_sEdit">
    @lang('strings.cancel')
    </button>

</div>