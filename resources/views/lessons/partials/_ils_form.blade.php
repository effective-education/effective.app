<button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#ilsModal">
    @lang('strings.see_ils_form')
</button>

<div class="modal fade" id="ilsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog" role="document">

        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">@lang('strings.ils_form')</h4>
            </div>

            <div class="modal-body">

                @foreach($ilsQuestions as $question)

                    <strong><label>{{ $question->number }}</label>. {{ $question->text }}</strong>
                    <ul>
                        @foreach($question->options as $option)
                            <li><label>{{ $option->letter }}</label>. {{ $option->text }}</li>
                        @endforeach
                    </ul>

                @endforeach

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">@lang('strings.close')</button>
            </div>

        </div>

    </div>

</div>