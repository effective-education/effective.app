<template v-for="(competence, competenceIndex) in competences.all()">

    <div class="panel well">

        <div class="panel-heading pull-right">

            {{-- Remove competences --}}
            <button type="button" class="btn btn-danger btn-sm" @click="competences.remove(competenceIndex)">
            @lang('strings.remove')
            </button>

        </div>

        <div class="panel-body">

            <input
                    type="hidden"
                    v-model="competence.id"
                    :name="'competences['+competenceIndex+'][id]'">

            <div class="form-group">

                <div class="form-group">

                    <label>@lang('strings.competence')</label>

                    {{-- Question's text --}}
                    <textarea
                            cols="30"
                            rows="2"
                            class="form-control"
                            v-model="competence.description"
                            :name="'competences['+competenceIndex+'][description]'">

                </textarea>

                </div>


            </div>

            <div class="form-group">

                <label>@lang('strings.evidences')</label>

                @include('lessons.partials._evidenceCompetence')

            </div>

        </div>

        <div class="panel-footer">

            {{-- Add new evidence --}}
            <button type="button" class="btn btn-success btn-block btn-sm" @click="competence.evidences.create()">
            @lang('strings.add_evidence')
            </button>

        </div>

    </div>

</template>