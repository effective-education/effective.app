<div class="form-group">

    <label>@lang('strings.abilities')</label>

    @foreach($lesson->abilities as $ability)

        <div class="well">

            <p><strong>{{ $ability->description }}</strong></p>
            <ul>
                @foreach($ability->evidences as $evidence)
                    <li>
                        {{ $evidence->description }}
                    </li>
                @endforeach
            </ul>

        </div>

    @endforeach

    <button type="button" class="btn btn-danger btn-block" v-show="!abilities_were_changed"
    @click="toggleAbilitiesEdit">
    @lang('strings.change_abilities')
    </button>

</div>

<div class="form-group" v-show="abilities_were_changed">

    <label>@lang('strings.abilities')</label>

    @include('lessons.partials._ability')

    <button type="button" class="btn btn-success btn-block btn-sm" @click="abilities.create()">
    @lang('strings.add_ability')
    </button>

    <button type="button" class="btn btn-danger btn-block" @click="toggleAbilitiesEdit">
    @lang('strings.cancel')
    </button>

</div>