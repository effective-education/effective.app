<template v-for="(evidence, evidenceAbilityIndex) in ability.evidences.all()">

    <input
            type="hidden"
            v-model="evidence.id"
            :name="'abilities['+abilityIndex+'][evidences]['+evidenceAbilityIndex+'][id]'">

    <div class="row">

        <div class="col-md-10">

            {{-- Evidence description --}}
            <textarea
                    cols="30"
                    rows="2"
                    class="form-control"
                    v-model="evidence.description"
                    :name="'abilities['+abilityIndex+'][evidences]['+evidenceAbilityIndex+'][description]'">
            </textarea>

        </div>


        <div class="col-md-2">

            {{-- Remove evidence --}}
            <button type="button" class="btn btn-danger btn-block btn-sm" @click="ability.evidences.remove(evidenceAbilityIndex)">
            @lang('strings.remove')
            </button>

        </div>

    </div>

    <br>

</template>