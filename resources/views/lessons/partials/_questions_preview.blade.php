<div class="form-group">

    <label>@lang('strings.questions')</label>

    @foreach($lesson->questions as $question)

        <div class="well">

            <p><strong>{{ $question->title }}</strong><span>{{ '('.$question->timestamp.')' }}</span></p>
            <ul>
                @foreach($question->options as $option)
                    <li>
                        {{ $option->text }}
                        @if($option->is_right)
                            &#9745;
                        @endif
                    </li>
                @endforeach
            </ul>

        </div>

    @endforeach

    <button type="button" class="btn btn-danger btn-block" v-show="!questions_were_changed"
    @click="toggleQuestionsEdit">
    @lang('strings.change_questions')
    </button>

</div>

<div class="form-group" v-show="questions_were_changed">

    <label>@lang('strings.questions')</label>

    @include('lessons.partials._question')

    <button type="button" class="btn btn-success btn-block btn-sm" @click="questions.create()">
    @lang('strings.add_question')
    </button>

    <button type="button" class="btn btn-danger btn-block" @click="toggleQuestionsEdit">
    @lang('strings.cancel')
    </button>

</div>