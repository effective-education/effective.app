<template v-for="(content, contentIndex) in contents.all()">

    <div class="panel well">

        <div class="panel-heading">

            {{-- Remove content --}}
            <button type="button" class="btn btn-danger btn-sm pull-right" @click="contents.remove(contentIndex)">
            @lang('strings.remove')
            </button>

        </div>

        <div class="panel-body">

            <input
                    type="hidden"
                    v-model="content.id"
                    :name="'contents['+contentIndex+'][id]'">

            <div class="form-group">

                <div class="form-group">

                    {{-- Content's times --}}
                    <div class="row">

                        <div class="col-md-6">
                            <label>@lang('strings.start_time')</label>
                            <input
                                    type="text"
                                    class="form-control"
                                    v-model="content.start_time"
                                    @keyup="content.start_time = $event.target.value"
                                    v-mask-hour
                                    :name="'contents['+contentIndex+'][start_time]'">
                        </div>

                        <div class="col-md-6">
                            <label>@lang('strings.end_time')</label>
                            <input
                                    type="text"
                                    class="form-control"
                                    v-model="content.end_time"
                                    @keyup="content.end_time = $event.target.value"
                                    v-mask-hour
                                    :name="'contents['+contentIndex+'][end_time]'">
                        </div>

                    </div>

                    <label>@lang('strings.content_tags')</label>
                    <small>@lang('strings.separate_tags_by_comma')</small>

                    <input
                            type="text"
                            class="form-control"
                            v-model="content.tags"
                            :name="'contents['+contentIndex+'][tags]'">

                </div>

            </div>

        </div>

    </div>

</template>