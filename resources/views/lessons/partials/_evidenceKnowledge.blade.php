<template v-for="(evidence, evidenceKnowledgeIndex) in knowledge.evidences.all()">

    <input
            type="hidden"
            v-model="evidence.id"
            :name="'knowledge_s['+knowledgeIndex+'][evidences]['+evidenceKnowledgeIndex+'][id]'">

    <div class="row">

        <div class="col-md-10">

            {{-- Evidence description --}}
            <textarea
                    cols="30"
                    rows="2"
                    class="form-control"
                    v-model="evidence.description"
                    :name="'knowledge_s['+knowledgeIndex+'][evidences]['+evidenceKnowledgeIndex+'][description]'">
            </textarea>

        </div>


        <div class="col-md-2">

            {{-- Remove evidence --}}
            <button type="button" class="btn btn-danger btn-block btn-sm" @click="knowledge.evidences.remove(evidenceKnowledgeIndex)">
            @lang('strings.remove')
            </button>

        </div>

    </div>

    <br>

</template>