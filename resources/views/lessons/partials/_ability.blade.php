<template v-for="(ability, abilityIndex) in abilities.all()">

    <div class="panel well">

        <div class="panel-heading pull-right">

            {{-- Remove abilities --}}
            <button type="button" class="btn btn-danger btn-sm" @click="abilities.remove(abilityIndex)">
            @lang('strings.remove')
            </button>

        </div>

        <div class="panel-body">

            <input
                    type="hidden"
                    v-model="ability.id"
                    :name="'abilities['+abilityIndex+'][id]'">

            <div class="form-group">

                <div class="form-group">

                    <label>@lang('strings.ability')</label>

                    {{-- Abilities descriptions --}}
                    <textarea
                            cols="30"
                            rows="2"
                            class="form-control"
                            v-model="ability.description"
                            :name="'abilities['+abilityIndex+'][description]'">

                </textarea>

                </div>


            </div>

            <div class="form-group">

                <label>@lang('strings.evidences')</label>

                @include('lessons.partials._evidenceAbility')

            </div>

        </div>

        <div class="panel-footer">

            {{-- Add new evidence --}}
            <button type="button" class="btn btn-success btn-block btn-sm" @click="ability.evidences.create()">
            @lang('strings.add_evidence')
            </button>

        </div>

    </div>

</template>