<template v-for="(evidence, evidenceCompetenceIndex) in competence.evidences.all()">

    <input
            type="hidden"
            v-model="evidence.id"
            :name="'competences['+competenceIndex+'][evidences]['+evidenceCompetenceIndex+'][id]'">

    <div class="row">

        <div class="col-md-10">

            {{-- Evidence description --}}
            <textarea
                    cols="30"
                    rows="2"
                    class="form-control"
                    v-model="evidence.description"
                    :name="'competences['+competenceIndex+'][evidences]['+evidenceCompetenceIndex+'][description]'">
            </textarea>

        </div>


        <div class="col-md-2">

            {{-- Remove evidence --}}
            <button type="button" class="btn btn-danger btn-block btn-sm" @click="competence.evidences.remove(evidenceCompetenceIndex)">
            @lang('strings.remove')
            </button>

        </div>

    </div>

    <br>

</template>