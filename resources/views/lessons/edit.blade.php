@extends('layouts.app')

@section('heading', trans('strings.edit'))

@section('content')

    @include('lessons.partials._ils_form')

    <br>

    <div class="alert alert-info">

        <p><label>@lang('strings.lesson_link')</label></p>
        <a href="{{ $lesson->link }}" target="_blank">{{ $lesson->link }}</a>

    </div>

    <form id="lessonEdit" method="POST" action="{{ route('lessons.update', [$lesson->id]) }}" class="form"
          enctype="multipart/form-data">

        {{ csrf_field() }}

        {{ method_field('PUT') }}

        {{-- Title --}}
        <div class="form-group">
            <label>@lang('strings.title')</label>
            <input type="text" name="title" class="form-control" value="{{ old('title') ?? $lesson->title }}" required>
        </div>

        {{-- Knowledge area --}}
        <div class="form-group">
            <label>@lang('strings.category')</label>
            <select name="knowledge_area" class="form-control select2" required>
                @foreach($knowledgeAreas as $knowledgeArea)
                    <optgroup label="{{ trans('knowledge-areas.' . $knowledgeArea->code) }}">
                        @foreach($knowledgeArea->children as $child)
                            <option value="{{ $child->id }}">
                                {{ trans('knowledge-areas.' . $child->code) }}
                            </option>
                        @endforeach
                    </optgroup>
                @endforeach
            </select>
        </div>

        {{-- Description --}}
        <div class="form-group">
            <label>@lang('strings.description')</label>
            <textarea name="description" cols="30" rows="10"
                      class="form-control">{{ old('description') ?? $lesson->description }}</textarea>
        </div>

        {{-- Video display --}}
        @include('lessons.partials._video')

        {{-- Questions --}}
        @include('lessons.partials._questions_preview')



        {{-- Abilities --}}
        @include('lessons.partials._abilities_preview')

        {{-- Knowledge_s --}}
        @include('lessons.partials._knowledge_s_preview')

        {{-- Competences --}}
        @include('lessons.partials._competences_preview')



        {{-- Contents --}}
        @include('lessons.partials._contents_preview')

        {{-- Outro --}}
        <div class="form-group">
            <label>@lang('strings.outro')</label>
            <textarea name="outro" cols="30" rows="10"
                      class="form-control editor">{{ old('outro') ?? $lesson->outro }}</textarea>
        </div>

        <input type="submit" value="@lang('strings.update')" class="btn btn-block btn-success">

        <input type="hidden" name="video_was_changed" v-model="video_was_changed">
        <input type="hidden" name="questions_were_changed" v-model="questions_were_changed">
        <input type="hidden" name="contents_were_changed" v-model="contents_were_changed">

        <input type="hidden" name="abilities_were_changed" v-model="abilities_were_changed">
        <input type="hidden" name="knowledge_s_were_changed" v-model="knowledge_s_were_changed">
        <input type="hidden" name="competencies_were_changed" v-model="competencies_were_changed">

    </form>

    <form id="formDeleteLesson" action="{{ route('lessons.destroy', [$lesson->id]) }}" method="POST">

        {{ csrf_field() }}

        {{ method_field('DELETE') }}

        <br>

        <input type="submit" class="btn btn-danger btn-block" value="@lang('strings.delete')">

    </form>

@endsection

@push('before-scripts')

<script>

    const data = {

        videoType: '{!! old('video_type') ?? $lesson->video_type !!}',
        questions: {!! old('questions') ? json_encode(old('questions')) : $lesson->questions->toJson() !!},
        contents: {!! old('contents') ? json_encode(old('contents')) : $lesson->contents->toJson() !!},
        knowledge_area: '{!! old('knowledge_area') ?? $lesson->knowledgeArea->id !!}',
        video_was_changed: {!! old('video_was_changed') ?? 'false' !!},
        questions_were_changed: {!! old('questions_were_changed') ?? 'false' !!},
        contents_were_changed: {!! old('contents_were_changed') ?? 'false' !!},


        abilities_were_changed:  {!! old('abilities_were_changed') ?? 'false' !!},
        knowledge_s_were_changed:  {!! old('knowledge_s_were_changed') ?? 'false' !!},
        competencies_were_changed: {!! old('competencies_were_changed') ?? 'false' !!},

        abilities: {!! old('abilities') ? json_encode(old('abilities')) : $lesson->abilities->toJson() !!},
        knowledge_s: {!! old('knowledge_s') ? json_encode(old('knowledge_s')) : $lesson->knowledge_s->toJson() !!},
        competences: {!! old('competencies') ? json_encode(old('competences')) : $lesson->competencies->toJson() !!},

    };

    document.getElementById('formDeleteLesson').onsubmit = function () {

        return confirm('{{ trans('strings.delete_this_lesson') }}');

    };

</script>

@endpush