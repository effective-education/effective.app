@extends('layouts.clear')

@section('heading', trans('strings.lessons'))

@section('content')


    <h1>
        @lang('strings.lessons')

        @if(Auth::check())
            @if(!Auth::user()->isStudent())
                <a href="{{ route('lessons.index') }}" id = "menu" class="btn btn-default">@lang('strings.manage_lessons')</a>
            @elseif(Auth::user()->isStudent())
                <a href="{{ route('dashboard.index') }}" id = "menu" class="btn btn-default">@lang('strings.back')</a>
            @endif
        @endif
        <h5>

        </h5>
    </h1>
    <hr>
    <template>
        <div class="row">
            <div class="col-md-12">
                <div id="app">
                    @if(\Illuminate\Support\Facades\Auth::check())
                        @if($flag)
                            <lessons isStudent="{{\Illuminate\Support\Facades\Auth::user()->isStudent()}}"
                                     all="{{$flag}}"></lessons>
                        @else
                            <lessons isStudent="{{\Illuminate\Support\Facades\Auth::user()->isStudent()}}"
                                     all="0"></lessons>
                        @endif
                    @else
                        @if($flag)
                            <lessons isStudent="{{true}}" all="{{$flag}}"></lessons>
                        @else
                            <lessons isStudent="{{true}}" all="0"></lessons>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </template>


@endsection

@push('after-scripts')


    <script type="application/javascript" src="{{ asset('js/public-lessons.js') }}"></script>


@endpush
