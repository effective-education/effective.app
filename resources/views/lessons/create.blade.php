@extends('layouts.app')

@section('heading', trans('strings.create'))

@section('content')

    @include('lessons.partials._ils_form')

    <form id="lessonCreate" method="POST" action="{{ route('lessons.store') }}" class="form" enctype="multipart/form-data">

        {{ csrf_field() }}

        {{-- Title --}}
        <div class="form-group">
            <label>@lang('strings.title')</label>
            <input type="text" name="title" class="form-control" value="{{ old('title') }}" required>
        </div>

        {{-- Knowledge area --}}
        <div class="form-group">
            <label>@lang('strings.category')</label>
            <select name="knowledge_area" class="form-control select2" required>
                @foreach($knowledgeAreas as $knowledgeArea)
                    <optgroup label="{{ trans('knowledge-areas.' . $knowledgeArea->code) }}">
                        @foreach($knowledgeArea->children as $child)
                            <option value="{{ $child->id }}" {{ old('knowledge_area') == $child->id ? 'selected' : ''}}>
                                {{ trans('knowledge-areas.' . $child->code) }}
                            </option>
                        @endforeach
                    </optgroup>
                @endforeach
            </select>
        </div>

        {{-- Description --}}
        <div class="form-group">
            <label>@lang('strings.description')</label>
            <textarea name="description" cols="30" rows="10" class="form-control editor">
{{ old('description') }}
            </textarea>
        </div>

        {{-- Video Type --}}
        <div class="form-group" style="display: none;">
            <label>@lang('strings.video_type')</label>
            <select name="video_type" class="form-control" v-model="videoType" required>
                <option value="{{ \App\Models\Lesson::VIDEO_TYPE_YOUTUBE }}">@lang('strings.youtube')</option>
                <option value="{{ \App\Models\Lesson::VIDEO_TYPE_FILE }}">@lang('strings.file')</option>
            </select>
        </div>

        {{-- Video file --}}
        <div class="form-group" v-show="!isVideoTypeYoutube">
            <label>@lang('strings.send_file')</label>
            <input type="file" name="video_file" class="form-control" v-bind:required="!isVideoTypeYoutube">
        </div>

        {{-- Video ID from youtube --}}
        <div class="form-group" v-show="isVideoTypeYoutube">
            <label>@lang('strings.video_id_youtube')</label>
            <input type="text" name="video_id" class="form-control" v-bind:required="isVideoTypeYoutube" value="{{ old('video_id') }}">
        </div>

        {{-- Questions --}}
        <div class="form-group">

            <label>@lang('strings.questions')</label>

            @include('lessons.partials._question')

            <button type="button" class="btn btn-success btn-block btn-sm" @click="questions.create()">
            @lang('strings.add_question')
            </button>

        </div>


        {{-- Contents --}}
        <div class="form-group">

            <label>@lang('strings.contents')</label>

            @include('lessons.partials._content')

            <button type="button" class="btn btn-success btn-block btn-sm" @click="contents.create()">
            @lang('strings.add_content')
            </button>

        </div>



        {{-- Competences --}}
        <div class="form-group">

            <label>@lang('strings.competences')</label>

            @include('lessons.partials._competence')

            <button type="button" class="btn btn-success btn-block btn-sm" @click="competences.create()">
                @lang('strings.add_competence')
            </button>

        </div>


        {{-- Abilities --}}
        <div class="form-group">

            <label>@lang('strings.abilities')</label>

            @include('lessons.partials._ability')

            <button type="button" class="btn btn-success btn-block btn-sm" @click="abilities.create()">
                @lang('strings.add_ability')
            </button>

        </div>


        {{-- Knowledge_s --}}
        <div class="form-group">

            <label>@lang('strings.knowledge_s')</label>

            @include('lessons.partials._knowledge')

            <button type="button" class="btn btn-success btn-block btn-sm" @click="knowledge_s.create()">
                @lang('strings.add_knowledge')
            </button>

        </div>




        {{-- Outro --}}
        <div class="form-group">
            <label>@lang('strings.outro')</label>
            <textarea name="outro" cols="30" rows="10" class="form-control editor">
{{ old('outro') }}
            </textarea>
        </div>

        <input type="submit" value="@lang('strings.create')" class="btn btn-block btn-success">

    </form>

@endsection

@push('before-scripts')

<script>

    const data = {

        videoType: '{!! old('video_type') ? old('video_type') : 'youtube' !!}',
        questions: {!! old('questions') ? json_encode(old('questions')) : '[]' !!},
        contents: {!! old('contents') ? json_encode(old('contents')) : '[]' !!},
        competences: {!! old('competences') ? json_encode(old('competences')) : '[]' !!},
        abilities: {!! old('abilities') ? json_encode(old('abilities')) : '[]' !!},
        knowledge_s: {!! old('knowledge_s') ? json_encode(old('knowledge_s')) : '[]' !!}


    };

</script>

@endpush