@extends('layouts.app')

@section('heading', trans('strings.accessibility'))

@section('content')

    <a style="align-items: center;  display: flex;  flex-direction: row;  flex-wrap: wrap;  justify-content: center;">
        <img height="100px" src="{{url('images/logo.png')}}">
    </a>

    <div class="page-content" style="text-align: justify; margin-bottom: 1rem; margin-top: 1rem">
        <p>
            @lang('accessibility.page_of_accessibility.p1')
        </p>
        <p>
            @lang('accessibility.page_of_accessibility.p2')
        </p>
        <p>
            @lang('accessibility.page_of_accessibility.p3')
        </p>

        <p>
            @lang('accessibility.page_of_accessibility.p4')
        </p>

        <ul>
            <li>
                <p>
                    @lang('accessibility.page_of_accessibility.p5')
                </p>
            </li>
            <li>
                <p>
                    @lang('accessibility.page_of_accessibility.p6')
                </p>
            </li>
            <li>
                <p>
                    @lang('accessibility.page_of_accessibility.p7')
                </p>
            </li>
            <li>
                <p>
                    @lang('accessibility.page_of_accessibility.p8')
                </p>
            </li>
            <li>
                <p>
                    @lang('accessibility.page_of_accessibility.p9')
                </p>
            </li>
            <li>
                <p>
                    @lang('accessibility.page_of_accessibility.p10')
                </p>
            </li>
            <li>
                <p>
                    @lang('accessibility.page_of_accessibility.p11')
                </p>
            </li>
            <li>
                <p>
                    @lang('accessibility.page_of_accessibility.p12')
                </p>
            </li>
        </ul>

        <p>@lang('accessibility.page_of_accessibility.p13')</p>
        <ul>
            <li>
                <p>
                    @lang('accessibility.page_of_accessibility.p14')
                </p>
            </li>
            <li>
                <p>
                    @lang('accessibility.page_of_accessibility.p15')
                </p>
            </li>
            <li>
                <p>
                    @lang('accessibility.page_of_accessibility.p16')
                </p>
            </li>
            <li>
                <p>
                    @lang('accessibility.page_of_accessibility.p17')
                </p>
            </li>
        </ul>
    </div>


@endsection
