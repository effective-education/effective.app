@extends('layouts.app')

@section('heading', trans('strings.my_classes'))

@section('actions')
    <a href="{{ route('classes.create') }}" class="btn btn-sm btn-success">@lang('strings.create')</a>
@endsection

@section('content')

    @if(! $classes->isEmpty())

        <div class="table-responsive">

            <table class="table">

                <thead>
                <tr>
                    <th>@lang('strings.name')</th>
                    <th>@lang('strings.created_by')</th>
                    <th>@lang('strings.students')</th>
                    <th>@lang('strings.lessons')</th>
                    <th>@lang('strings.action')</th>

                </tr>
                </thead>

                <tbody>
                @foreach($classes as $class)
                    <tr>
                        <td>{{ $class->name }}</td>
                        <td>{{ $class->creator->name }}</td>
                        <td><a href="{{route('add_user', ['id' => $class->id]) }}">@lang('strings.add_students')</a></td>
                        <td><a href="{{route('add_lesson', ['id' => $class->id]) }}">@lang('strings.add_lessons')</a></td>
                        <td><a href="{{ route('classes.edit', [$class->id]) }}">@lang('strings.edit')</a></td>

                    </tr>
                @endforeach
                </tbody>

            </table>
        </div>

        {{ $classes->links() }}


    @else

        <div class="alert alert-warning">@lang('status.classes.none')</div>

    @endif

@endsection