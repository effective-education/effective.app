@extends('layouts.app')

@section('heading', trans('strings.create'))

@section('content')

    <form method="POST" action="{{ route('classes.store') }}" class="form">

        {{ csrf_field() }}

        <div class="form-group">
            <label>@lang('strings.name')</label>
            <input type="text" name="name" class="form-control">
        </div>

        <div class="form-group">
            <label>@lang('strings.description')</label>
            <textarea name="description" rows="10" class="form-control">
            {{ old('description') }}
            </textarea>
        </div>

        <input type="submit" value="@lang('strings.create')" class="btn btn-block btn-success">
    </form>
@endsection