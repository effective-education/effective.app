<button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#ilsModal">
    @lang('strings.add_users')
</button>


    <form action="{{ route('classes.update', [$class->id]) }}" method="POST" class="form">

        {{ method_field('PATCH') }}
        {{ csrf_field() }}

        <div class="modal fade" id="ilsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

            <div class="modal-dialog" role="document">

                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">@lang('strings.add_users')</h4>
                    </div>

                    <div class="modal-body">


                        {{-- Name --}}
                        <div class="form-group">
                            <label>@lang('strings.name')</label>
                            <input type="text" name="name" class="form-control" value="{{ old('name') ?? $class->name }}" required>
                        </div>


                        {{-- Description --}}
                        <div class="form-group">
                            <label>@lang('strings.description')</label>
                            <textarea name="description" cols="30" rows="10"
                                      class="form-control editor">{{ old('description') ?? $class->description }}</textarea>
                        </div>






                    </div>


                    <input type="submit" class="btn btn-block btn-success" value="@lang('strings.update')">


                    <div class="modal-footer">
                        <button type="submit" class="btn btn-default" data-dismiss="modal">@lang('strings.close')</button>
                    </div>

                </div>

            </div>

        </div>
    </form>