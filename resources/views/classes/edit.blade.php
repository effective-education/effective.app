@extends('layouts.app')

@section('heading', trans('strings.edit'))

@section('content')

    <form action="{{ route('classes.update', [$class->id]) }}" method="POST" class="form">

        {{ method_field('PATCH') }}
        {{ csrf_field() }}

        {{-- Name --}}
        <div class="form-group">
            <label>@lang('strings.name')</label>
            <input type="text" name="name" class="form-control" value="{{ old('name') ?? $class->name }}" required>
        </div>


        {{-- Description --}}
        <div class="form-group">
            <label>@lang('strings.description')</label>
            <textarea name="description" cols="30" rows="10" class="form-control ">
                {{ old('description') ?? $class->description }}
            </textarea>
        </div>


        <input type="submit" class="btn btn-block btn-success" value="@lang('strings.update')"><br>

    </form>



    <form id="student">

        {{--Estudantes--}}
        <label><h3>@lang('strings.students')</h3></label>

        @if(! $students->isEmpty())
            <div class="table-responsive">

                <table class="table">

                    <thead>
                    <tr>
                        <th>@lang('strings.name')</th>
                        <th>@lang('strings.action')</th>

                    </tr>
                    </thead>

                    <tbody>


                    @foreach($students as $studen)
                        <tr>
                            <td>{{ $studen->name }}</td>
                            <td>
                                <a href="{{ route('rem_student',['classe_id' => $class->id, 'student_id' => $studen->id]) }}">@lang('strings.delete')</a>
                            </td>

                        </tr>
                    @endforeach
                    </tbody>

                </table>

            </div>

        @else

            <div class="alert alert-warning">@lang('status.class_student.none')</div>

        @endif

    </form>


    <form id="lesson">

        {{--Aulas--}}
        <label><h3>@lang('strings.lessons')</h3></label>

        @if(! $lessons->isEmpty())
            <div class="table-responsive">

                <table class="table">

                    <thead>
                    <tr>
                        <th>@lang('strings.title')</th>
                        <th>@lang('strings.action')</th>

                    </tr>
                    </thead>

                    <tbody>
                    @foreach($lessons as $lesson)
                        <tr>
                            <td>{{ $lesson->title }}</td>
                            <td>
                                <a href="{{ route('rem_lesson',['classe_id' => $class->id, 'lesson_id' => $lesson->id]) }}">@lang('strings.delete')</a>
                            </td>

                        </tr>
                    @endforeach
                    </tbody>

                </table>

            </div>

        @else

            <div class="alert alert-warning">@lang('status.class_lesson.none')</div>

        @endif


    </form>

    <br>

    {{--Delte Class--}}

    <form id="formDeleteClass" action="{{ route('classes.destroy', [$class->id]) }}" method="POST">

        {{ csrf_field() }}

        {{ method_field('DELETE') }}

        <br>

        <input type="submit" class="btn btn-danger btn-block" value="@lang('strings.delete_class')">

    </form>


@endsection