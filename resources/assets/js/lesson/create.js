import {QuestionCollection} from './../classes/QuestionCollection';
import {ContentsCollection} from './../classes/ContentsCollection';
import {CompetenceCollection} from "../classes/CompetenceCollection";
import {AbilityCollection} from "../classes/AbilityCollection";
import {KnowledgeCollection} from "../classes/KnowledgeCollection";

if (document.getElementById("lessonCreate")) {

    new Vue({

        el: '#lessonCreate',

        data: {

            videoType: data.videoType,
            questions: new QuestionCollection(data.questions),
            contents: new ContentsCollection(data.contents),
            competences: new CompetenceCollection(data.competences),
            abilities: new AbilityCollection(data.abilities),
            knowledge_s: new KnowledgeCollection(data.knowledge_s),

        },

        computed: {

            isVideoTypeYoutube: function () {

                return this.videoType == 'youtube';

            }

        }

    });

}