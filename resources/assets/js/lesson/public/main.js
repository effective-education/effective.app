import Vue from 'vue';

import Lessons from './Lessons.vue';
new Vue({
    el: '#app',

    components: {
        lessons: Lessons,
    }

});
