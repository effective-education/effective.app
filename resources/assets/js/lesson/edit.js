import {QuestionCollection} from "./../classes/QuestionCollection";
import {ContentsCollection} from "./../classes/ContentsCollection";
import {CompetenceCollection} from "../classes/CompetenceCollection";
import {AbilityCollection} from "../classes/AbilityCollection";
import {KnowledgeCollection} from "../classes/KnowledgeCollection";

if (document.getElementById("lessonEdit")) {

    new Vue({

        el: '#lessonEdit',

        data: {

            videoType: data.videoType,
            questions: new QuestionCollection(data.questions),
            contents: new ContentsCollection(data.contents),
            knowledge_area: data.knowledge_area,
            video_was_changed: data.video_was_changed,
            questions_were_changed: data.questions_were_changed,
            contents_were_changed: data.contents_were_changed,


            abilities_were_changed: data.abilities_were_changed,
            knowledge_s_were_changed: data.knowledge_s_were_changed,
            competencies_were_changed: data.competencies_were_changed,

            competences: new CompetenceCollection(data.competences),
            abilities: new AbilityCollection(data.abilities),
            knowledge_s: new KnowledgeCollection(data.knowledge_s),

        },

        computed: {

            isVideoTypeYoutube() {

                return this.videoType == 'youtube';

            }

        },

        methods: {

            toggleVideoEdit() {

                this.video_was_changed = !this.video_was_changed;

            },

            toggleQuestionsEdit() {

                this.questions_were_changed = !this.questions_were_changed;

            },

            toggleContentsEdit() {

                this.contents_were_changed = !this.contents_were_changed;

            },

            toggleAbilitiesEdit() {

                this.abilities_were_changed = !this.abilities_were_changed;

            },

            toggleKnowledge_sEdit() {

                this.knowledge_s_were_changed = !this.knowledge_s_were_changed;

            },

            toggleCompetenciesEdit() {

                this.competencies_were_changed = !this.competencies_were_changed;

            },

        }

    });

}