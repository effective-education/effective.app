/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.onload = function () {

    prepareLogoutLink();

    loadEditor();

    loadSelect2();

};

/**
 * Logouts the user from the dashboard.
 */
function prepareLogoutLink() {

    let anchorLogout = document.getElementById('anchor-logout');

    if(anchorLogout) {

        anchorLogout.addEventListener("click", function (event) {

            event.preventDefault();
            document.getElementById('logout-form').submit();

        }, true);

    }

}

function loadEditor() {

    tinymce.init({
        selector: '.editor',
        skin: false
    });

}

function loadSelect2() {

    $(".select2").select2();

}

const baseUrl = document.head.querySelector('meta[name="url"]').content;

window.url = function (path = '') {

    return baseUrl + '/' + path;

};









