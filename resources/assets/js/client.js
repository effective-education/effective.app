import axios from 'axios';
import toastr from 'toastr';
import nprogress from 'nprogress';

export default {

    searchLessons(search = '', page = 1) {

        return new Promise(function (resolve, reject) {

            nprogress.start();

            axios.get(window.url('api/lessons?page='+page+'&search=' + search)).then(function (response) {

                nprogress.done();

                resolve(response.data);

            }).catch(function (error) {

                nprogress.done();

                console.log(error);

            });

        });

    },

    searchAllLessons(search = '', page = 1) {

        return new Promise(function (resolve, reject) {

            nprogress.start();

            axios.get(window.url('api/lessons/all?page='+page+'&search=' + search)).then(function (response) {

                nprogress.done();

                resolve(response.data);

            }).catch(function (error) {

                nprogress.done();

                console.log(error);

            });

        });

    },

    getLesson(slug) {

        return new Promise(function (resolve) {

            nprogress.start();

            axios.get(window.url('lesson/' + slug)).then(function(response) {

                resolve(response.data.lesson);

                nprogress.done();

            });

        });

    },

    verifyEmail(email) {

        return new Promise(function (resolve, reject) {

            nprogress.start();

            axios.post(window.url('students/email'), {email}).then(function (response) {

                resolve(response.data.student);

                nprogress.done();

            }).catch(function (error) {

                toastr.error(error.response.data.errors[0]);

                reject(error);

                nprogress.done();

            });

        });

    },

    registerStudent(uuid, data) {

        return new Promise(function (resolve, reject) {

            nprogress.start();

            axios.put(window.url('student/' + uuid), data).then(function (response) {

                resolve(response.data.student);

                nprogress.done();

            }).catch(function (error) {

                toastr.error(error.response.data.errors[0]);

                reject(error);

                nprogress.done();

            });

        });

    },

    getIls() {

        return new Promise(function (resolve) {

            nprogress.start();

            axios.get(window.url('ils')).then(function (response) {

                resolve(response.data.ils);

                nprogress.done();

            });

        });

    },

    storeIlsResult(uuid, data) {

        return new Promise(function (resolve, reject) {

            nprogress.start();

            axios.post(window.url('student/ils/' + uuid), data).then(function (response) {

                resolve(response.data.results);

                nprogress.done();

            }).catch(function (error) {

                toastr.error(error.response.data.errors[0]);

                reject(error);

                nprogress.done();

            });

        });

    },

    storeLessonResults(data) {

        return new Promise(function (resolve, reject) {

            nprogress.start();

            axios.post(window.url('studentsresults'), data).then(function (response) {

                resolve(response.data);

                nprogress.done();

            }).catch(function (error) {

                toastr.error(error.response.data.errors[0]);

                reject(error);

                nprogress.done();

            });

        });

    },

    storeImpression(studentResultUuid, answers) {

        return new Promise(function (resolve, reject) {

            nprogress.start();

            axios.post(window.url('impressions'), {

                student_result_uuid: studentResultUuid,
                answers: answers

            }).then(function (response) {

                resolve(response.data);

                nprogress.done();

            }).catch(function (error) {

                toastr.error(error.response.data.errors[0]);

                reject(error);

                nprogress.done();

            });

        });

    },

    finish() {

        return new Promise(function (resolve) {

            nprogress.start();

            axios.get(window.url('lessons')).then(function (response) {

                nprogress.done();

            });


        });

    },

};
