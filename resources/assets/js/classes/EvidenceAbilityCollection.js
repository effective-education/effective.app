class EvidenceAbilityCollection {

    constructor(evidences) {

        this.evidences = [];

        evidences.forEach(function (evidence) {

            this.add(evidence.id, evidence.description);

        }.bind(this));

    }

    all() {

        return this.evidences;

    }

    add(id, description) {

        id = id ? id : 0;
        description = description ? description : '';

        this.evidences.push({

            id: id,
            description: description,

        });

    }

    create() {

        this.add();

    }

    remove(index) {

        this.evidences.splice(index, 1);

    }

}

export {EvidenceAbilityCollection}