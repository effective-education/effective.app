import {OptionsCollection} from './OptionsCollection';

class QuestionCollection {

    constructor(questions) {

        this.questions = [];

        questions.forEach(function (question) {

            this.add(question.id, question.title, question.timestamp, question.options);

        }.bind(this));

    }

    all() {

        return this.questions;

    }

    add(id, title, timestamp, options) {

        id = id ? id : 0;
        title = title ? title : '';
        timestamp = timestamp ? timestamp : '00:00:00';
        options = options ? options : [];

        this.questions.push({

            id: id,
            title: title,
            timestamp: timestamp,
            options: new OptionsCollection(options)

        });

    }

    create() {

        this.add();

    }

    remove(index) {

        this.questions.splice(index, 1);

    }

}

export {QuestionCollection}