import {EvidenceCompetenceCollection} from './EvidenceCompetenceCollection';

class CompetenceCollection {

    constructor(competences) {

        this.competences = [];

        competences.forEach(function (competence) {

            this.add(competence.id, competence.description, competence.evidences);

        }.bind(this));

    }

    all() {

        return this.competences;

    }

    add(id, description, evidences) {

        id = id ? id : 0;
        description = description ? description : '';
        evidences = evidences ? evidences : [];

        this.competences.push({

            id: id,
            description: description,
            evidences: new EvidenceCompetenceCollection(evidences)

        });

    }

    create() {

        this.add();

    }

    remove(index) {

        this.competences.splice(index, 1);

    }

}

export {CompetenceCollection}