import {EvidenceKnowledgeCollection} from './EvidenceKnowledgeCollection';

class KnowledgeCollection {

    constructor(knowledge_s) {

        this.knowledge_s = [];

        knowledge_s.forEach(function (knowledge) {

            this.add(knowledge.id, knowledge.description, knowledge.evidences);

        }.bind(this));

    }

    all() {

        return this.knowledge_s;

    }

    add(id, description, evidences) {

        id = id ? id : 0;
        description = description ? description : '';
        evidences = evidences ? evidences : [];

        this.knowledge_s.push({

            id: id,
            description: description,
            evidences: new EvidenceKnowledgeCollection(evidences)

        });

    }

    create() {

        this.add();

    }

    remove(index) {

        this.knowledge_s.splice(index, 1);

    }

}

export {KnowledgeCollection}