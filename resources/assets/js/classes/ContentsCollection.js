class ContentsCollection {

    constructor(contents) {

        this.contents = [];

        contents.forEach(function (content) {

            this.add(content.id, content.start_time, content.end_time, content.tags);

        }.bind(this));

    }

    all() {

        return this.contents;

    }

    add(id, start_time, end_time, tags) {

        id = id ? id : 0;
        start_time = start_time ? start_time : '';
        end_time = end_time ? end_time : '';
        tags = tags ? tags : '';

        this.contents.push({

            id: id,
            start_time: start_time,
            end_time: end_time,
            tags: tags,

        });

    }

    create() {

        this.add();

    }

    remove(index) {

        this.contents.splice(index, 1);

    }

}

export {ContentsCollection}