import {EvidenceAbilityCollection} from './EvidenceAbilityCollection';

class AbilityCollection {

    constructor(abilities) {

        this.abilities = [];

        abilities.forEach(function (ability) {

            this.add(ability.id, ability.description, ability.evidences);

        }.bind(this));

    }

    all() {

        return this.abilities;

    }

    add(id, description, evidences) {

        id = id ? id : 0;
        description = description ? description : '';
        evidences = evidences ? evidences : [];

        this.abilities.push({

            id: id,
            description: description,
            evidences: new EvidenceAbilityCollection(evidences)

        });

    }

    create() {

        this.add();

    }

    remove(index) {

        this.abilities.splice(index, 1);

    }

}

export {AbilityCollection}