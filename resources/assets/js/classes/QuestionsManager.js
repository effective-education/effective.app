class QuestionsManager {

    constructor(questions) {

        questions.forEach(function (question) {

            question.answers = [];
            question.seconds = this.timeStampToSeconds(question.timestamp);

        }.bind(this));

        this.questions = questions;
        this.currentQuestions = [];

    }

    timeStampToSeconds(timestamp) {

        let parts = timestamp.split(':');
        return (+parts[0]) * 60 * 60 + (+parts[1]) * 60 + (+parts[2]);

    }

    getBySeconds(seconds) {

        this.currentQuestions = [];

        this.questions.forEach(function (question) {

            //the question time must be between 'seconds' and 'seconds+2'
            //a range is more scure than a direct comparison.
            if (seconds >= question.seconds) {

                //if there are no answers
                if (question.answers.length <= 0)
                    this.currentQuestions.push(question);

            }

        }.bind(this));

        return this.currentQuestions;

    }

    current () {

        return this.currentQuestions;

    }

    validateAnswers() {

        this.questions.forEach(function (question) {

            question.is_right = this.isQuestionRight(question);

        }.bind(this));

    }

    rightPercentage() {

        this.validateAnswers();

        let amountOfQuestions = this.questions.length;

        let amountOfRightOnes = 0;

        this.questions.forEach(function (question) {

            if (question.is_right)
                amountOfRightOnes++;

        });

        return Math.ceil((amountOfRightOnes * 100) / amountOfQuestions);

    }

    wrongOptions(question) {

        let wrong = [];

        question.options.forEach(function (option) {

            if (! option.is_right)
                wrong.push(option.text);

        });

        return wrong;

    }

    isQuestionRight(question) {

        let wrongs = this.wrongOptions(question);

        let aWrongOptionIsChecked = false;

        question.answers.forEach(function (answer) {

            if (wrongs.indexOf(answer) == -1)
                aWrongOptionIsChecked = true;

        });

        return aWrongOptionIsChecked;

    }

    areThereQuestionsLeft() {

        let thereIsOneLeft = false;

        this.questions.forEach(function (question) {

            if(question.answers.length == 0)
                thereIsOneLeft = true;

        }.bind(this));

        return thereIsOneLeft;

    }

    setUnansweredQuestions() {

        this.currentQuestions = [];

        this.questions.forEach(function (question) {

            if(question.answers.length == 0)
                this.currentQuestions.push(question);

        }.bind(this));

    }

}

export {QuestionsManager};