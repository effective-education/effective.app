class OptionsCollection {

    constructor(options) {

        this.options = [];

        options.forEach(function (option) {

            this.add(option.id, option.text, option.is_right);

        }.bind(this));

    }

    all() {

        return this.options;

    }

    add(id, text, is_right) {

        id = id ? id : 0;
        text = text ? text : '';
        is_right = is_right ? is_right : false;

        this.options.push({

            id: id,
            text: text,
            is_right: is_right,

        });

    }

    create() {

        this.add();

    }

    remove(index) {

        this.options.splice(index, 1);

    }

}

export {OptionsCollection}