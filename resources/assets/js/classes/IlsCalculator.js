class IlsCalculator {

    constructor(questions) {

        this.questions = questions ? questions : [];

        this.resultsMatrix = {

            ACT_REF: {a: 0, b: 0},
            SEN_INT: {a: 0, b: 0},
            VIS_VER: {a: 0, b: 0},
            SEQ_GLO: {a: 0, b: 0}

        };

        this.convertToMatrix();

    }

    convertToMatrix() {

        this.questions.forEach(function (question) {

            if (question.scale == 'ACT/REF') {

                if (question.answer == 'a') {

                    this.resultsMatrix.ACT_REF.a++;

                } else {

                    this.resultsMatrix.ACT_REF.b++;

                }

            }

            if (question.scale == 'SEN/INT') {

                if (question.answer == 'a') {

                    this.resultsMatrix.SEN_INT.a++;

                } else {

                    this.resultsMatrix.SEN_INT.b++;

                }

            }

            if (question.scale == 'VIS/VER') {

                if (question.answer == 'a') {

                    this.resultsMatrix.VIS_VER.a++;

                } else {

                    this.resultsMatrix.VIS_VER.b++;

                }

            }

            if (question.scale == 'SEQ/GLO') {

                if (question.answer == 'a') {

                    this.resultsMatrix.SEQ_GLO.a++;

                } else {

                    this.resultsMatrix.SEQ_GLO.b++;

                }

            }

        }.bind(this));

    }

    results() {

        let results = {

            ACT_REF: '',
            SEN_INT: '',
            VIS_VER: '',
            SEQ_GLO: ''

        };

        results.ACT_REF = Math.abs(this.resultsMatrix.ACT_REF.a - this.resultsMatrix.ACT_REF.b);
        if (this.resultsMatrix.ACT_REF.a > this.resultsMatrix.ACT_REF.b) {
            results.ACT_REF += 'a';
        } else {
            results.ACT_REF += 'b';
        }

        results.SEN_INT = Math.abs(this.resultsMatrix.SEN_INT.a - this.resultsMatrix.SEN_INT.b);
        if (this.resultsMatrix.SEN_INT.a > this.resultsMatrix.SEN_INT.b) {
            results.SEN_INT += 'a';
        } else {
            results.SEN_INT += 'b';
        }

        results.VIS_VER = Math.abs(this.resultsMatrix.VIS_VER.a - this.resultsMatrix.VIS_VER.b);
        if (this.resultsMatrix.VIS_VER.a > this.resultsMatrix.VIS_VER.b) {
            results.VIS_VER += 'a';
        } else {
            results.VIS_VER += 'b';
        }

        results.SEQ_GLO = Math.abs(this.resultsMatrix.SEQ_GLO.a - this.resultsMatrix.SEQ_GLO.b);
        if (this.resultsMatrix.SEQ_GLO.a > this.resultsMatrix.SEQ_GLO.b) {
            results.SEQ_GLO += 'a';
        } else {
            results.SEQ_GLO += 'b';
        }

        return results;

    }

}

export {IlsCalculator}