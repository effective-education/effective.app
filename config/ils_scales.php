<?php

    return [

        App\Models\IlsQuestion::SCALE_ACT_REF => [

            1, 5, 9, 13, 17, 21, 25, 29, 33, 37, 41

        ],

        App\Models\IlsQuestion::SCALE_SEN_INT => [

            2, 6, 10, 14, 18, 22, 26, 30, 34, 38, 42

        ],

        App\Models\IlsQuestion::SCALE_VIS_VER => [

            3, 7, 11, 15, 19, 23, 27, 31, 35, 39, 43

        ],

        App\Models\IlsQuestion::SCALE_SEQ_GLO => [

            4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44

        ],

    ];