<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('', 'PagesController@index')->name('pages.index');

Auth::routes();

Route::group(['prefix' => 'dashboard', 'middleware' => ['auth', 'active']], function () {

    Route::get('', 'DashboardController@index')->name('dashboard.index');

    Route::patch('users/{id}/status', 'UsersController@status')->name('users.status');

    Route::patch('students/{id}/status', 'StudentsController@status')->name('students.status');

    Route::resource('users', 'UsersController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'updatedata']]);

    //alterar senha
    Route::post('change-password', 'StudentsController@changePwd')->name('change.password');
    Route::get('change-password', 'StudentsController@pagePwd')->name('page.password');

    // Turma
    Route::resource('classes', 'ClassesController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']]);
    Route::get('my_classes', 'ClassesController@myIndex')->name('classes.mine.index');
    Route::resource('class_student', 'Class_StudentController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']]);
    Route::resource('class_lesson', 'Class_LessonController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']]);

    Route::get('/add_user/{id}', 'Class_StudentController@create')->name('add_user');
    Route::get('/add_lesson/{id}', 'Class_LessonController@create')->name('add_lesson');

    Route::get('/rem_student/{classe_id}/{student_id}', 'Class_StudentController@destroy')->name('rem_student');
    Route::get('/rem_lesson/{classe_id}/{lesson_id}', 'Class_LessonController@destroy')->name('rem_lesson');

    Route::get('/classes/{id}/close', 'ClassesController@close')->name('close_classe');

    Route::resource('lessons', 'LessonsController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']]);

    Route::get('my_lessons', 'LessonsController@myIndex')->name('lessons.mine.index');

    Route::get('lesson/results/{lessonid}', 'ResultsController@lesson')->name('lesson.results');
    Route::get('lesson/results/{lessonid}/student/{studentid}', 'ResultsController@student')->name('lesson.results.student');
    Route::get('lesson/results/{lessonid}/student/{studentid}/download/lastframes', 'ResultsController@lastFrames')->name('lesson.results.student.download.lastFrames');
    Route::get('lesson/results/{lessonid}/student/{studentid}/download/lastframesxml', 'ResultsController@lastFramesXml')->name('lesson.results.student.download.lastFramesXml');



    Route::resource('evaluation_results', 'EvaluationsController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']]);

    Route::get('lesson/results/evaluation/index/lesson/{lessonid}/student/{studentid}', 'EvaluationsController@route')->name('lesson.results.evaluation_results.route');

    Route::get('lesson/results/evaluation/create/{lessonid}/student/{studentid}', 'EvaluationsController@create')->name('lesson.results.evaluation_results.create');
    Route::get('lesson/results/evaluation/edit/{lessonid}/student/{studentid}', 'EvaluationsController@edit')->name('lesson.results.evaluation_results.edit');


    Route::get('impression/{id}', 'ImpressionsController@show')->name('impressions.show');


    Route::resource('student', 'StudentsController', ['only' => ['index', 'create', 'store', 'edit', 'update']]);

    Route::group(['prefix' => 'graphics'], function () {
        Route::get('student', 'GraphicsController@index_student')->name('graphics_of_student');
        Route::get('user', 'GraphicsController@index_user')->name('graphics_of_user');

    });


    Route::group(['prefix' => 'statistic'], function () {
        Route::get('user', 'StatisticsModuleController@index')->name('statistic_module');

    });



    Route::get('graphics/index','GraphicsController@index')->name('graphics.index');

    Route::post('graphics/index','GraphicsController@plot_Student')->name('plot.student');

    Route::get('graphics/plotEmotionOfStudentByLesson/{lessonId}/{studentId}','GraphicsController@plotVariationOfStudentEmotionByLesson')->name('plot.student.lesson');
    Route::get('graphics/plotAverageOfEmotionsByStudent/{studentId}','GraphicsController@plotAverageOfEmotionsByStudent')->name('plot.average.student');
    Route::get('graphics/plotAverageOfEmotionsByLesson/{lessonId}','GraphicsController@plotAverageOfEmotionsByLesson')->name('plot.average.lesson');
    Route::get('graphics/plotAverageOfEmotionsByClass/{classId}','GraphicsController@plotAverageOfEmotionsByClass')->name('plot.average.class');

    Route::get('graphics/plotHitRateByStudent/{studentId}','GraphicsController@plotHitRateByStudent')->name('plot.hitRate.student');
    Route::get('graphics/plotHitRateByLesson/{lessonId}','GraphicsController@plotHitRateByLesson')->name('plot.hitRate.lesson');
    Route::get('graphics/plotHitRateByClass/{classId}','GraphicsController@plotHitRateByClass')->name('plot.hitRate.class');

    //webServer
    Route::get('graphics/filtered/{student_id}','GraphicsController@student_lessons');
    Route::get('statistic/filtered/{student_id}','GraphicsController@student_lessons');


    Route::get('statistic/emotion/{emotion}','StatisticsModuleController@maior_e_menor_indice_emocao');
    Route::get('statistic/lesson/{lesson}/emotion/{emotion}','StatisticsModuleController@maior_e_menor_indice_aluno_emocao');
    Route::get('statistic/student/{student_id}','StatisticsModuleController@individual_aluno');
    Route::get('statistic/lesson/{lesson_id}','StatisticsModuleController@individual_aula');
    Route::get('statistic/report/lesson/{lesson_id}','StatisticsModuleController@relatorio_resultados_aula');
    Route::get('statistic/emotion_lesson/student/{student_id}','StatisticsModuleController@maior_menor_indice_aula_emocao_aluno');
    Route::get('statistic/evaluation/student/{student_id}/lesson/{lesson_id}','StatisticsModuleController@results_evaluation');

});

Route::get('setlocale/{locale}', function ($locale) {
    if (in_array($locale, \Config::get('app.locales'))) {
        Session::put('locale', $locale);
    }
    return redirect()->back();
})->name('setlocale');


Route::get('ils', 'IlsController@index');

Route::resource('tests', 'TestsController', ['only' => ['show', 'store']]);

Route::get('lesson/{slug}', 'LessonsController@show');

Route::post('students/email', 'StudentsController@store')->name('students.email');

Route::post('students/add', 'StudentsController@secStore')->name('students.register.email');

Route::get('student/register', function () {
    return view('student.create');
})->name('student.register');


Route::put('student/{uuid}', 'StudentRegistrationController@update');

Route::post('student/ils/{uuid}', 'StudentsIlsController@store');

Route::post('studentsresults', 'StudentsResultsController@store');

Route::get('api/lessons', 'PublicLessonsController@index');

Route::get('api/lessons/all', 'PublicLessonsController@indexAll');

Route::get('lessons', 'PublicLessonsController@view')->name('lessons');

Route::get('lessons/all', 'PublicLessonsController@viewAll')->name('lessons.all');

Route::post('impressions', 'ImpressionsController@store');

Route::get('ontoafter/lesson/{lessonid}/student/{studentid}/download/lastframesxml', 'ResultsController@lastFramesXmlAfterLesson')->name('lesson.results.student.download.lastFramesXmlAfter');


Route::resource('accessibility', 'AccessibilityController', ['only' => ['index']]);

Route::resource('map', 'MapController', ['only' => ['index']]);

//Route::get('onto', 'GenerateUsersFromStudentsController@generate')->name('onto');

// fase II
//Route::get('onto/{uuid}', 'XmlExportationController@storeStudentIdIntoOnto');
//
//Route::get('testing/', 'StudentRegistrationController@testAcademic');
