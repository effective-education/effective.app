<?php

use App\Models\IlsQuestion;
use Illuminate\Database\Seeder;

class IlsQuestionsTableSeeder extends Seeder
{

    protected $scales;

    /**
     * IlsQuestionsTableSeeder constructor.
     */
    public function __construct()
    {

        $this->scales = config('ils_scales');

    }


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for ($number = 1; $number <= 44; $number++) {

            factory(IlsQuestion::class)->create([
                'number' => $number,
                'scale' => $this->findScale($number),
            ]);

        }

    }

    /**
     * @param int $number
     * @return string
     * @throws Exception
     */
    protected function findScale(int $number)
    {

        foreach ($this->scales as $scale => $questions){

            if (array_search($number, $questions) !== false)
                return $scale;

        }

        throw new Exception("A scale was not found for question number {$number}.");

    }

}