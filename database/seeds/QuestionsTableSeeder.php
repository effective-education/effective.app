<?php

use App\Models\Lesson;
use App\Models\Question;
use Illuminate\Database\Seeder;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $lessons = Lesson::get();

        foreach ($lessons as $lesson) {

            factory(Question::class)->create([
                'lesson_id' => $lesson->id,
                'timestamp' => '00:00:04',
            ]);

            factory(Question::class)->create([
                'lesson_id' => $lesson->id,
                'timestamp' => '00:00:04',
            ]);

            factory(Question::class)->create([
                'lesson_id' => $lesson->id,
                'timestamp' => '00:00:04',
            ]);

            factory(Question::class)->create([
                'lesson_id' => $lesson->id,
                'timestamp' => '00:00:04',
            ]);

        }

    }

}