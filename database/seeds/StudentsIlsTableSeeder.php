<?php

use App\Models\Student;
use App\Models\StudentIls;
use Illuminate\Database\Seeder;

class StudentsIlsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(StudentIls::class)->create([
            'student_id' => Student::first()->id,
        ]);

    }

}