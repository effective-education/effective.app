<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->createUserToLoginAs();

        factory(User::class, 5)->create();


    }

    public function createUserToLoginAs()
    {

        factory(User::class)->create([
            'email' => 'email@email.com',
            'password' => bcrypt('senha'),
        ]);

    }

}