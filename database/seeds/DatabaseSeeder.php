<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->call(KnowledgeAreasTableSeeder::class);
        $this->call(IlsQuestionsTableSeeder::class);
        $this->call(IlsOptionsTableSeeder::class);

        if (env('APP_ENV') != 'production') {

            $this->call(UsersTableSeeder::class);
            $this->call(LessonsTableSeeder::class);
            $this->call(QuestionsTableSeeder::class);
            $this->call(OptionsTableSeeder::class);
            $this->call(ContentsTableSeeder::class);
            $this->call(StudentsTableSeeder::class);
            $this->call(StudentsIlsTableSeeder::class);
            $this->call(LessonMetricTableSeeder::class);

        }

    }

}