<?php

use App\Models\Student;
use Illuminate\Database\Seeder;

class StudentsTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(Student::class)->create([
            'email' => 'email@email.com',
        ]);

        factory(Student::class, 4)->create();

    }

}