<?php

use App\Models\Option;
use App\Models\Question;
use Illuminate\Database\Seeder;

class OptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $questions = Question::get();

        foreach ($questions as $question) {

            factory(Option::class, 2)->create([
                'question_id' => $question->id,
            ]);

        }

    }

}