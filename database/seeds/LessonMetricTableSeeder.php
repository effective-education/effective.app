<?php

use App\Models\Lesson;
use App\Models\LessonMetric;
use Illuminate\Database\Seeder;

class LessonMetricTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach (Lesson::all() as $lesson) {

            factory(LessonMetric::class)->create([
                'lesson_id' => $lesson->id,
            ]);

        }

    }
}
