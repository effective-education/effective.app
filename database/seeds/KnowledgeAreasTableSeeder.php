<?php

use App\Models\KnowledgeArea;
use Illuminate\Database\Seeder;

class KnowledgeAreasTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $areasCode = config('knowledge_areas');

        foreach ($areasCode as $parentCode => $children) {

            $parent = factory(KnowledgeArea::class)->create(['code' => $parentCode]);

            foreach ($children as $childrenCode) {

                factory(KnowledgeArea::class)->create([
                    'code' => $childrenCode,
                    'parent_id' => $parent->id,
                ]);

            }

        }

    }

}