<?php

use App\Models\IlsOption;
use App\Models\IlsQuestion;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class IlsOptionsTableSeeder extends Seeder
{

    protected $now;

    public function __construct()
    {

        $this->now = Carbon::now();

    }


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $questions = IlsQuestion::get();

        foreach ($questions as $question) {

            $this->insertLettersWithQuestion($question, ['a', 'b']);

        }

    }

    protected function insertLettersWithQuestion(IlsQuestion $question, $letters = [])
    {

        $query = [];

        foreach ($letters as $letter) {

            $query[] = [
                'ils_question_id' => $question->id,
                'letter' => $letter,
                'created_at' => $this->now,
                'updated_at' => $this->now,
            ];

        }

        IlsOption::insert($query);

    }

}