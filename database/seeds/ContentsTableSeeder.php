<?php

use App\Models\Content;
use App\Models\Lesson;
use Illuminate\Database\Seeder;

class ContentsTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $lessons = Lesson::all();

        foreach ($lessons as $lesson) {

            factory(Content::class)->create([
                'lesson_id' => $lesson->id,
            ]);

        }

    }

}