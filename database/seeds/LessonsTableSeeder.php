<?php

use App\Models\KnowledgeArea;
use App\Models\Lesson;
use App\Models\User;
use Illuminate\Database\Seeder;

class LessonsTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $users = User::get();
        $areas = KnowledgeArea::get();

        $videos = collect([
            'pkHZTWOoTLM',
            'jwoK5WKVXGw',
            'LFU5ZlrR21E',
            '0jjeOYMjmDU',
            'IKB1hWWedMk',
            'f0lkz2gSsIk',
            'BV9ny785UNc',
            '6z7GQewK-Ks',
            'jrk_lOg_pVA',
            '17WoOqgXsRM'
        ]);

        foreach (range(1, 50) as $count) {

            factory(Lesson::class)->create([
                'user_id' => $users->random()->id,
                'knowledge_area_id' => $areas->random()->id,
                'video_type' => Lesson::VIDEO_TYPE_YOUTUBE,
                'video_uri' => $videos->random()
            ]);

        }

    }

}
