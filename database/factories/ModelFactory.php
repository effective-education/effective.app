<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'role' => \App\Models\User::ADMIN,
        'active' => true,
    ];
});

$factory->define(App\Models\KnowledgeArea::class, function (Faker\Generator $faker) {

    return [
        'code' => str_random(3),
    ];

});

$factory->define(App\Models\Lesson::class, function (Faker\Generator $faker) {

    return [
        'user_id' => function () {

            return factory(\App\Models\User::class)->create()->id;

        },
        'user_editor_id' => function () {

            return factory(\App\Models\User::class)->create()->id;

        },
        'knowledge_area_id' => function () {

            return factory(\App\Models\KnowledgeArea::class)->create()->id;

        },
        'uuid' => $faker->uuid,
        'title' => $faker->name,
        'slug' => $faker->slug,
        'description' => $faker->paragraph,
        'outro' => $faker->paragraph,
        'video_uri' => $faker->url,
        'video_type' => \App\Models\Lesson::VIDEO_TYPE_YOUTUBE,

    ];

});

$factory->define(App\Models\Question::class, function (Faker\Generator $faker) {

    return [

        'lesson_id' => function() {

            return factory(\App\Models\Lesson::class)->create()->id;

        },

        'title' => $faker->name,
        'timestamp' => '00:00:05',

    ];

});

$factory->define(App\Models\Option::class, function (Faker\Generator $faker) {

    return [

        'question_id' => function() {

            return factory(\App\Models\Question::class)->create()->id;

        },

        'text' => $faker->name,
        'is_right' => $faker->boolean,

    ];

});

$factory->define(App\Models\IlsQuestion::class, function (Faker\Generator $faker) {

    return [

        'number' => $faker->randomNumber(3),

    ];

});

$factory->define(App\Models\IlsOption::class, function (Faker\Generator $faker) {

    return [

        'ils_question_id' => function () {

            return factory(\App\Models\IlsQuestion::class)->create()->id;

        },

        'letter' => $faker->randomLetter,

    ];

});

$factory->define(App\Models\Content::class, function (Faker\Generator $faker) {

    return [

        'lesson_id' => function () {

            return factory(\App\Models\Lesson::class)->create()->id;

        },

        'start_time' => '00:00:00',
        'end_time' => '00:09:00',
        'tags' => 'php,python,ruby',

    ];

});

$factory->define(App\Models\Student::class, function (Faker\Generator $faker) {

    return [

        'uuid' => $faker->uuid,
        'name' => $faker->name,
        'email' => $faker->email,
        'age' => 21,
        'gender' => 'F',
        'college' => 'UFOPA',
        'course' => 'Ciências da computação',
        'handicap' => 'deaf',
        'musical_instrument' => 'Guitar',

    ];

});

$factory->define(App\Models\StudentIls::class, function (Faker\Generator $faker) {

    return [

        'student_id' => function () {

            return factory(\App\Models\Student::class)->create()->id;

        },
        'act_ref' => '11a',
        'sen_int' => '11a',
        'vis_ver' => '11a',
        'seq_glo' => '11a',

    ];

});

$factory->define(App\Models\LessonMetric::class, function (Faker\Generator $faker) {

    return [

        'lesson_id' => function () {

            return factory(\App\Models\Lesson::class)->create()->id;

        },

    ];

});

$factory->define(App\Models\Impression::class, function (Faker\Generator $faker) {

    return [

        'lesson_id' => function () {

            return factory(\App\Models\Lesson::class)->create()->id;

        },

        'student_id' => function () {

            return factory(\App\Models\Student::class)->create()->id;

        },

    ];

});

$factory->define(App\Models\StudentResult::class, function (Faker\Generator $faker) {

    return [

        'lesson_id' => function () {

            return factory(\App\Models\Lesson::class)->create()->id;

        },

        'student_id' => function () {

            return factory(\App\Models\Student::class)->create()->id;

        },

        'uuid' => $faker->uuid,
        'questions' => [ ['id' => 69, 'answers' => ['blue']] ],
        'frames' => [ ['seconds' => 34, 'emotions' => [], 'questions' => []] ],
        'total_questions' => 1,
        'right_answers' => 1,

    ];

});
