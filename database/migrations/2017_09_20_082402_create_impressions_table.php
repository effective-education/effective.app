<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImpressionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('impressions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('student_result_id');

            $table->unsignedTinyInteger('surprised')->default(1);
            $table->unsignedTinyInteger('proud')->default(1);
            $table->unsignedTinyInteger('sad')->default(1);
            $table->unsignedTinyInteger('excited')->default(1);
            $table->unsignedTinyInteger('interesting')->default(1);
            $table->unsignedTinyInteger('devalued')->default(1);
            $table->unsignedTinyInteger('ashamed')->default(1);
            $table->unsignedTinyInteger('happy')->default(1);
            $table->unsignedTinyInteger('sorry')->default(1);
            $table->unsignedTinyInteger('expectancy')->default(1);
            $table->unsignedTinyInteger('fear')->default(1);
            $table->unsignedTinyInteger('quiet')->default(1);
            $table->unsignedTinyInteger('aversion')->default(1);
            $table->unsignedTinyInteger('satisfied')->default(1);
            $table->unsignedTinyInteger('angry')->default(1);
            $table->unsignedTinyInteger('jealous')->default(1);
            $table->unsignedTinyInteger('indifferent')->default(1);

            $table->foreign('student_result_id')->references('id')->on('students_results')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('impressions');
    }
}
