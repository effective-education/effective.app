<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvidenceKnowledgeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evidence_knowledge', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('lesson_id');
            $table->integer('knowledge_id');
            $table->string('description');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('lesson_id')->references('id')->on('lessons')->onUpdate('cascade');
            $table->foreign('knowledge_id')->references('id')->on('knowledge')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evidence_knowledge');
    }
}
