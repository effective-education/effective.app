<?php

use App\Models\IlsQuestion;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsIlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students_ils', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('student_id');
            $table->char(IlsQuestion::SCALE_ACT_REF, 3);
            $table->char(IlsQuestion::SCALE_SEN_INT, 3);
            $table->char(IlsQuestion::SCALE_VIS_VER, 3);
            $table->char(IlsQuestion::SCALE_SEQ_GLO, 3);
            $table->timestamps();
            $table->foreign('student_id')->references('id')->on('students')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students_ils');
    }
}
