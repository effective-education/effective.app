<?php

use App\Models\Lesson;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lessons', function (Blueprint $table) {

            $table->increments('id');
            $table->uuid('uuid');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('user_editor_id');
            $table->unsignedInteger('knowledge_area_id');
            $table->string('title');
            $table->string('slug')->unique();
            $table->longText('description');
            $table->longText('outro');
            $table->string('video_uri');
            $table->enum('video_type', [Lesson::VIDEO_TYPE_FILE, Lesson::VIDEO_TYPE_YOUTUBE]);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade');
            $table->foreign('user_editor_id')->references('id')->on('users')->onUpdate('cascade');
            $table->foreign('knowledge_area_id')->references('id')->on('knowledge_areas')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::drop('lessons');

    }

}