<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultEvaluationStudentKnowledgeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('result_evaluation_student_knowledge', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('evidence_knowledge_id');
            $table->integer('student_id');
            $table->integer('lesson_id');

            $table->string('result');

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('evidence_knowledge_id')->references('id')->on('evidence_knowledge')->onUpdate('cascade');
            $table->foreign('student_id')->references('id')->on('students')->onUpdate('cascade');
            $table->foreign('lesson_id')->references('id')->on('lessons')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('result_evaluation_student_knowledge');
    }
}
