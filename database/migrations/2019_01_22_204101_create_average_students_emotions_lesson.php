<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAverageStudentsEmotionsLesson extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('average_students_emotions_lesson', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('student_id');
            $table->unsignedInteger('lesson_id');

            $table->float('joy', 3, 4);
            $table->float('fear', 3, 4);
            $table->float('rage', 3, 4);
            $table->float('disgust', 3, 4);
            $table->float('sadness', 3, 4);
            $table->float('contempt', 3, 4);
            $table->float('surprise', 3, 4);

            $table->softDeletes();
            $table->timestamps();
            $table->foreign('student_id')->references('id')->on('students')->onUpdate('cascade');
            $table->foreign('lesson_id')->references('id')->on('lessons')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('average_students_emotions_lesson');
    }
}
