<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIlsOptionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('ils_options', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('ils_question_id');
            $table->char('letter', 1);
            $table->timestamps();

            $table->foreign('ils_question_id')->references('id')->on('ils_questions')->onUpdate('cascade')->onDelete('cascade');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::drop('ils_options');

    }

}