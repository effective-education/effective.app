<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassLessonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classe_lesson', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('classe_id');
            $table->unsignedinteger('lesson_id');
            $table->integer('position');
            $table->timestamps();
            $table->foreign('classe_id')->references('id')->on('classes')->onUpdate('cascade');
            $table->foreign('lesson_id')->references('id')->on('lessons')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('classe_lesson', function (Blueprint $table) {
            Schema::dropIfExists('classe_lesson');
        });
    }
}
