<?php

use App\Models\IlsQuestion;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIlsQuestionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('ils_questions', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedSmallInteger('number')->unique();
            $table->enum('scale', [
                IlsQuestion::SCALE_ACT_REF,
                IlsQuestion::SCALE_SEN_INT,
                IlsQuestion::SCALE_VIS_VER,
                IlsQuestion::SCALE_SEQ_GLO,
            ]);
            $table->timestamps();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::drop('ils_questions');

    }

}