<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassStudentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('classe_student', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedinteger('classe_id');
            $table->unsignedinteger('student_id');
            $table->timestamps();
            $table->foreign('classe_id')->references('id')->on('classes')->onUpdate('cascade');
            $table->foreign('student_id')->references('id')->on('students')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('classe_student', function (Blueprint $table) {
            Schema::dropIfExists('classe_student');
        });
    }
}
