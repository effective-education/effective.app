<?php

namespace App\Transformers;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Transforms public lessons data.
 *
 * @package App\Transformers
 */
class PublicLessonsTransformer
{

    /**
     * @var LengthAwarePaginator
     */
    protected $lessons;

    /**
     * @param $lessons
     */
    public function __construct(LengthAwarePaginator $lessons)
    {

        $this->lessons = $lessons;

    }

    /**
     * @return array
     */
    public function transform()
    {

        return [

            'lessons' => $this->lessons->map(function ($lesson) {

                return [

                    'title' => $lesson->title,
                    'teacher' => $lesson->creator->name,
                    'area' => $lesson->knowledgeArea->title,
                    'url' => $lesson->link,
                    'youtube_id' => $lesson->video_uri,

                ];

            })->all(),

            'pagination' => $this->getPaginationData(),

        ];

    }

    /**
     * @return array
     */
    protected function getPaginationData()
    {

        $data = $this->lessons->toArray();
        unset($data['data']);
        return $data;

    }

}
