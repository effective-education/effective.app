<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Only admins can manage users.
     *
     * @param User $user
     * @param string $ability
     * @return bool
     */
    public function before(User $user, string $ability)
    {

        if ($ability == 'update')
            return null;

        return $user->isAdmin();

    }

    /**
     * A user cannot update its own register.
     *
     * @param User $user
     * @param User $editedUser
     * @return bool
     */
    public function update(User $user, User $editedUser)
    {

        return $user->id != $editedUser->id;

    }

    /**
     * Only an admin can manage others users.
     */
    public function manage()
    {

        //Requirement of 5.5 to trigger the before method.

    }

}
