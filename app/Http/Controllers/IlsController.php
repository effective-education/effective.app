<?php

namespace App\Http\Controllers;

use App\Models\IlsQuestion;
use Illuminate\Http\JsonResponse;

/**
 * Ils form controller.
 *
 * @package App\Http\Controllers
 */
class IlsController extends Controller
{

    /**
     * Shows all questions to the current Ils form.
     *
     * @return JsonResponse
     */
    public function index()
    {

        return response()->json(['ils' => IlsQuestion::getQuiz()]);

    }
    
}
