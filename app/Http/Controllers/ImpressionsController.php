<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreImpressionRequest;
use App\Models\Impression;
use App\Models\StudentResult;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\View\View;

/**
 * Controller for impressions that students gave at lessons.
 *
 * @package App\Http\Controllers
 */
class ImpressionsController extends Controller
{

    /**
     * Show an impression.
     *
     * @param $id
     * @return View
     */
    public function show($id)
    {

        return view('impressions.show', [

            'impression' => Impression::with('result.student', 'result.lesson')->findOrFail($id)

        ]);

    }

    /**
     * Stores an impression about a lesson.
     *
     * @param StoreImpressionRequest $request
     * @return JsonResponse
     */
    public function store(StoreImpressionRequest $request)
    {

        Impression::create(array_merge($request->get('answers'), [

            'student_result_id' => StudentResult::findOrFailByUuid($request->get('student_result_uuid'))->id,

        ]));

        return response()->json('', Response::HTTP_CREATED);

    }

}
