<?php

namespace App\Http\Controllers;


use App\Models\Average_Students_Emotions_Lesson;
use App\Models\Lesson;
use App\Models\Student;
use App\Models\StudentResult;
use App\Models\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Illuminate\Support\Facades\Auth;

/**
 * Class DashboardController
 * @package App\Http\Controllers
 */
class StatisticsModuleController extends Controller
{

    /**
     * Show the page .
     *
     * @return Factory|View
     */
    public function index()
    {
        if (Auth::check()) {
            if (Auth::user()->isAdmin() || Auth::user()->isCollaborator()) {

                $students = Student::students_with_emotion();
                $lessons = Lesson::findLessonsWatched();


                return view('statistics.index', compact('lessons', 'students'));
            }
        }

    }


    public function translate_emotion($emotion)
    {

        switch ($emotion) {
            case 'joy':
                return 'Alegria';
                break;
            case 'fear':
                return 'Medo';
                break;
            case 'rage':
                return 'Raiva';
                break;
            case 'disgust':
                return 'Desgosto';
                break;
            case 'sadness':
                return 'Tristeza';
                break;
            case 'contempt':
                return 'Desprezo';
                break;
            case 'surprise':
                return 'Surpresa';
                break;
            default:
                return 'Nenhuma';
        }


    }


    public function maior_e_menor_indice_emocao($emotion)
    {


        //arruma uma das emoções que no banco tá diferente
        if ($emotion == 'anger') {
            $emotion = 'rage';
        }

        $todas_medias = Average_Students_Emotions_Lesson::with('lesson')
            ->join('lessons', 'average_students_emotions_lesson.lesson_id', '=', 'lessons.id')
            ->get();
        $todos_ids_aulas = $todas_medias->pluck('lesson_id');

        $maior = null;
        $menor = null;
        $id_maior = null;
        $id_menor = null;
        $i = 0;
        $aula_maior = null;
        $aula_menor = null;
        $qtd_views_maior = null;
        $qtd_views_menor = null;

        foreach ($todas_medias as $medias) {

            $average = new Average_Students_Emotions_Lesson();
            $media = $average::all()->where('lesson_id', $todos_ids_aulas[$i])->avg($emotion);

            if ($i == 0) {
                $maior = $media;
                $aula_maior = $medias;
                $menor = $media;
                $aula_menor = $medias;
            }

            if ($maior <= $media) {
                $maior = $media;
                $aula_maior = $medias;
            } elseif ($menor >= $media) {
                $menor = $media;
                $aula_menor = $medias;
            }
            $i++;
        }


        if ($aula_maior) {
            $qtd_views_maior = StudentResult::all('lesson_id')
                ->where('lesson_id', $aula_maior->lesson_id)
                ->count();

            $qtd_views_menor = StudentResult::all('lesson_id')
                ->where('lesson_id', $aula_menor->lesson_id)
                ->count();
        } else {

            $qtd_views_maior = null;

            $qtd_views_menor = null;
        }

        $emotion_name = self::translate_emotion($emotion);

        return view('statistics.classes_with_greater_lesser_emotion.index', compact('emotion', 'aula_maior', 'aula_menor',
            'qtd_views_maior', 'qtd_views_menor', 'emotion_name', 'maior', 'menor'));
    }


    public function maior_e_menor_indice_aluno_emocao($lesson, $emotion)
    {

        //arruma uma das emoções que no banco tá diferente
        if ($emotion == 'anger') {
            $emotion = 'rage';
        }

        $lesson_details = Lesson::all()->where('id', $lesson)->first();

        $maior = Average_Students_Emotions_Lesson::all('lesson_id', 'student_id', $emotion)
            ->where('lesson_id', $lesson)
            ->sortBy($emotion)
            ->last();

        $menor = Average_Students_Emotions_Lesson::all('lesson_id', 'student_id', $emotion)
            ->where('lesson_id', $lesson)
            ->sortBy($emotion)
            ->first();


        $qtd_views = StudentResult::all('lesson_id')
            ->where('lesson_id', $lesson)
            ->count();

        if ($maior) {
            $aluno_maior = User::all()->where('student_id', $maior->student_id)->first();
            $aluno_menor = User::all()->where('student_id', $menor->student_id)->first();
        } else {
            $aluno_maior = null;
            $aluno_menor = null;
        }

        $emotion_name = self::translate_emotion($emotion);

        return view('statistics.students_with_higher_and_lower_index_of_emotion.index', compact('emotion',
            'maior', 'menor', 'aluno_maior', 'aluno_menor', 'qtd_views', 'lesson_details', 'emotion_name'));

    }


    public function individual_aluno($student_id)
    {

        $student = Average_Students_Emotions_Lesson::with('lesson')
            ->join('lessons', 'average_students_emotions_lesson.lesson_id', '=', 'lessons.id')
            ->where('average_students_emotions_lesson.student_id', $student_id)
            ->get();

        $user = User::all()->where('student_id', $student_id)->first();

        if ($student) {
            $qtd_aulas_assistidas = sizeof($student);
        } else {
            $qtd_aulas_assistidas = null;
        }

        return view('statistics.individual_per_student.index', compact('student', 'user',
            'qtd_aulas_assistidas'));

    }


    public function individual_aula($lesson_id)
    {

        $student = Average_Students_Emotions_Lesson::with('lesson', 'student')
            ->join('lessons', 'average_students_emotions_lesson.lesson_id', '=', 'lessons.id')
            ->join('students', 'average_students_emotions_lesson.student_id', '=', 'students.id')
            ->where('average_students_emotions_lesson.lesson_id', $lesson_id)
            ->get();

        $lesson = Lesson::all()->find($lesson_id);

       //dd($student);

        return view('statistics.individual_per_lesson.index', compact('student', 'lesson'));

    }




    public function maior_menor_indice_aula_emocao_aluno($student_id)
    {

        $maior_alegria = Average_Students_Emotions_Lesson::with('lesson')
            ->join('lessons', 'average_students_emotions_lesson.lesson_id', '=', 'lessons.id')
            ->where('average_students_emotions_lesson.student_id', $student_id)
            ->orderBy('average_students_emotions_lesson.joy', 'desc')
            ->first();

        $menor_alegria = Average_Students_Emotions_Lesson::with('lesson')
            ->join('lessons', 'average_students_emotions_lesson.lesson_id', '=', 'lessons.id')
            ->where('average_students_emotions_lesson.student_id', $student_id)
            ->orderBy('average_students_emotions_lesson.joy', 'asc')
            ->first();


        $maior_medo = Average_Students_Emotions_Lesson::with('lesson')
            ->join('lessons', 'average_students_emotions_lesson.lesson_id', '=', 'lessons.id')
            ->where('average_students_emotions_lesson.student_id', $student_id)
            ->orderBy('average_students_emotions_lesson.fear', 'desc')
            ->first();


        $menor_medo = Average_Students_Emotions_Lesson::with('lesson')
            ->join('lessons', 'average_students_emotions_lesson.lesson_id', '=', 'lessons.id')
            ->where('average_students_emotions_lesson.student_id', $student_id)
            ->orderBy('average_students_emotions_lesson.fear', 'asc')
            ->first();


        $maior_raiva = Average_Students_Emotions_Lesson::with('lesson')
            ->join('lessons', 'average_students_emotions_lesson.lesson_id', '=', 'lessons.id')
            ->where('average_students_emotions_lesson.student_id', $student_id)
            ->orderBy('average_students_emotions_lesson.rage', 'desc')
            ->first();


        $menor_raiva = Average_Students_Emotions_Lesson::with('lesson')
            ->join('lessons', 'average_students_emotions_lesson.lesson_id', '=', 'lessons.id')
            ->where('average_students_emotions_lesson.student_id', $student_id)
            ->orderBy('average_students_emotions_lesson.rage', 'asc')
            ->first();


        $maior_desgosto = Average_Students_Emotions_Lesson::with('lesson')
            ->join('lessons', 'average_students_emotions_lesson.lesson_id', '=', 'lessons.id')
            ->where('average_students_emotions_lesson.student_id', $student_id)
            ->orderBy('average_students_emotions_lesson.disgust', 'desc')
            ->first();


        $menor_desgosto = Average_Students_Emotions_Lesson::with('lesson')
            ->join('lessons', 'average_students_emotions_lesson.lesson_id', '=', 'lessons.id')
            ->where('average_students_emotions_lesson.student_id', $student_id)
            ->orderBy('average_students_emotions_lesson.disgust', 'asc')
            ->first();


        $maior_tristeza = Average_Students_Emotions_Lesson::with('lesson')
            ->join('lessons', 'average_students_emotions_lesson.lesson_id', '=', 'lessons.id')
            ->where('average_students_emotions_lesson.student_id', $student_id)
            ->orderBy('average_students_emotions_lesson.sadness', 'desc')
            ->first();


        $menor_tristeza = Average_Students_Emotions_Lesson::with('lesson')
            ->join('lessons', 'average_students_emotions_lesson.lesson_id', '=', 'lessons.id')
            ->where('average_students_emotions_lesson.student_id', $student_id)
            ->orderBy('average_students_emotions_lesson.sadness', 'asc')
            ->first();


        $maior_desprezo = Average_Students_Emotions_Lesson::with('lesson')
            ->join('lessons', 'average_students_emotions_lesson.lesson_id', '=', 'lessons.id')
            ->where('average_students_emotions_lesson.student_id', $student_id)
            ->orderBy('average_students_emotions_lesson.contempt', 'desc')
            ->first();


        $menor_desprezo = Average_Students_Emotions_Lesson::with('lesson')
            ->join('lessons', 'average_students_emotions_lesson.lesson_id', '=', 'lessons.id')
            ->where('average_students_emotions_lesson.student_id', $student_id)
            ->orderBy('average_students_emotions_lesson.contempt', 'asc')
            ->first();


        $maior_surpresa = Average_Students_Emotions_Lesson::with('lesson')
            ->join('lessons', 'average_students_emotions_lesson.lesson_id', '=', 'lessons.id')
            ->where('average_students_emotions_lesson.student_id', $student_id)
            ->orderBy('average_students_emotions_lesson.surprise', 'desc')
            ->first();


        $menor_surpresa = Average_Students_Emotions_Lesson::with('lesson')
            ->join('lessons', 'average_students_emotions_lesson.lesson_id', '=', 'lessons.id')
            ->where('average_students_emotions_lesson.student_id', $student_id)
            ->orderBy('average_students_emotions_lesson.surprise', 'asc')
            ->first();


        $user = User::all()->where('student_id', $student_id)->first();


        if ($maior_alegria) {

            $media_emocao_aula_maior_alegria = Average_Students_Emotions_Lesson::all()
                ->where('lesson_id', $maior_alegria->lesson_id)->avg('joy');

            $media_emocao_aula_menor_alegria = Average_Students_Emotions_Lesson::all()
                ->where('lesson_id', $menor_alegria->lesson_id)->avg('joy');


            $media_emocao_aula_maior_medo = Average_Students_Emotions_Lesson::all()
                ->where('lesson_id', $maior_medo->lesson_id)->avg('fear');

            $media_emocao_aula_menor_medo = Average_Students_Emotions_Lesson::all()
                ->where('lesson_id', $menor_medo->lesson_id)->avg('fear');


            $media_emocao_aula_maior_raiva = Average_Students_Emotions_Lesson::all()
                ->where('lesson_id', $maior_raiva->lesson_id)->avg('rage');

            $media_emocao_aula_menor_raiva = Average_Students_Emotions_Lesson::all()
                ->where('lesson_id', $menor_raiva->lesson_id)->avg('rage');

            $media_emocao_aula_maior_desgosto = Average_Students_Emotions_Lesson::all()
                ->where('lesson_id', $maior_desgosto->lesson_id)->avg('disgust');

            $media_emocao_aula_menor_desgosto = Average_Students_Emotions_Lesson::all()
                ->where('lesson_id', $menor_desgosto->lesson_id)->avg('disgust');

            $media_emocao_aula_maior_tristeza = Average_Students_Emotions_Lesson::all()
                ->where('lesson_id', $maior_tristeza->lesson_id)->avg('sadness');

            $media_emocao_aula_menor_tristeza = Average_Students_Emotions_Lesson::all()
                ->where('lesson_id', $menor_tristeza->lesson_id)->avg('sadness');


            $media_emocao_aula_maior_desprezo = Average_Students_Emotions_Lesson::all()
                ->where('lesson_id', $maior_desprezo->lesson_id)->avg('contempt');

            $media_emocao_aula_menor_desprezo = Average_Students_Emotions_Lesson::all()
                ->where('lesson_id', $menor_desprezo->lesson_id)->avg('contempt');


            $media_emocao_aula_maior_surpresa = Average_Students_Emotions_Lesson::all()
                ->where('lesson_id', $maior_surpresa->lesson_id)->avg('surprise');

            $media_emocao_aula_menor_surpresa = Average_Students_Emotions_Lesson::all()
                ->where('lesson_id', $menor_surpresa->lesson_id)->avg('surprise');

//            $qtd_views_maior_alegria = StudentResult::all('lesson_id')->where('lesson_id', $maior_alegria->lesson_id)->count();
//            $qtd_views_menor_alegria = StudentResult::all('lesson_id')->where('lesson_id', $menor_alegria->lesson_id)->count();
//
//            $qtd_views_maior_medo = StudentResult::all('lesson_id')->where('lesson_id', $maior_medo->lesson_id)->count();
//            $qtd_views_menor_medo = StudentResult::all('lesson_id')->where('lesson_id', $menor_medo->lesson_id)->count();
//
//            $qtd_views_maior_raiva = StudentResult::all('lesson_id')->where('lesson_id', $maior_raiva->lesson_id)->count();
//            $qtd_views_menor_raiva = StudentResult::all('lesson_id')->where('lesson_id', $menor_raiva->lesson_id)->count();
//
//            $qtd_views_maior_desgosto = StudentResult::all('lesson_id')->where('lesson_id', $maior_desgosto->lesson_id)->count();
//            $qtd_views_menor_desgosto = StudentResult::all('lesson_id')->where('lesson_id', $menor_desgosto->lesson_id)->count();
//
//            $qtd_views_maior_tristeza = StudentResult::all('lesson_id')->where('lesson_id', $maior_tristeza->lesson_id)->count();
//            $qtd_views_menor_tristeza = StudentResult::all('lesson_id')->where('lesson_id', $menor_tristeza->lesson_id)->count();
//
//            $qtd_views_maior_desprezo = StudentResult::all('lesson_id')->where('lesson_id', $maior_desprezo->lesson_id)->count();
//            $qtd_views_menor_desprezo = StudentResult::all('lesson_id')->where('lesson_id', $menor_desprezo->lesson_id)->count();
//
//            $qtd_views_maior_surpresa = StudentResult::all('lesson_id')->where('lesson_id', $maior_surpresa->lesson_id)->count();
//            $qtd_views_menor_surpresa = StudentResult::all('lesson_id')->where('lesson_id', $menor_surpresa->lesson_id)->count();

        } else {

            $media_emocao_aula_maior_alegria = null;
            $media_emocao_aula_menor_alegria = null;

            $media_emocao_aula_maior_medo = null;
            $media_emocao_aula_menor_medo = null;

            $media_emocao_aula_maior_raiva = null;
            $media_emocao_aula_menor_raiva = null;

            $media_emocao_aula_maior_desgosto = null;
            $media_emocao_aula_menor_desgosto = null;

            $media_emocao_aula_maior_tristeza = null;
            $media_emocao_aula_menor_tristeza = null;

            $media_emocao_aula_maior_desprezo = null;
            $media_emocao_aula_menor_desprezo = null;

            $media_emocao_aula_maior_surpresa = null;
            $media_emocao_aula_menor_surpresa = null;

//            $qtd_views_maior_alegria = null;
//            $qtd_views_menor_alegria =  null;
//            $qtd_views_maior_medo =  null;
//            $qtd_views_menor_medo =  null;
//            $qtd_views_maior_raiva =  null;
//            $qtd_views_menor_raiva =  null;
//            $qtd_views_maior_desgosto = null;
//            $qtd_views_menor_desgosto = null;
//            $qtd_views_maior_tristeza = null;
//            $qtd_views_menor_tristeza = null;
//            $qtd_views_maior_desprezo = null;
//            $qtd_views_menor_desprezo = null;
//            $qtd_views_maior_surpresa = null;
//            $qtd_views_menor_surpresa = null;

        }


        //dd($user);

        return view('statistics.emotion_upper_lower_by_lesson.index',
            compact('user', 'maior_alegria', 'menor_alegria', 'maior_medo', 'menor_medo',
                'maior_raiva', 'menor_raiva', 'maior_desgosto', 'menor_desgosto', 'maior_desprezo', 'menor_desprezo',
                'maior_tristeza', 'menor_tristeza', 'maior_surpresa', 'menor_surpresa',
                'media_emocao_aula_maior_alegria',
                'media_emocao_aula_menor_alegria',
                'media_emocao_aula_maior_medo',
                'media_emocao_aula_menor_medo',
                'media_emocao_aula_maior_raiva',
                'media_emocao_aula_menor_raiva',
                'media_emocao_aula_maior_desgosto',
                'media_emocao_aula_menor_desgosto',
                'media_emocao_aula_maior_tristeza',
                'media_emocao_aula_menor_tristeza',
                'media_emocao_aula_maior_desprezo',
                'media_emocao_aula_menor_desprezo',
                'media_emocao_aula_maior_surpresa',
                'media_emocao_aula_menor_surpresa'

            ));

    }



    public function results_evaluation($student_id, $lesson_id)
    {

        $student = Average_Students_Emotions_Lesson::with('lesson')
            ->join('lessons', 'average_students_emotions_lesson.lesson_id', '=', 'lessons.id')
            ->where('average_students_emotions_lesson.student_id', $student_id)
            ->where('average_students_emotions_lesson.lesson_id', $lesson_id)
            ->get();

        $user = User::all()->where('student_id', $student_id)->first();

        $lesson = Lesson::with('abilities.evidences.results', 'knowledge_s.evidences.results', 'competencies.evidences.results')
            ->findOrFail($lesson_id);

        return view('statistics.evaluation_results.index', compact('student', 'user',
            'lesson'));

    }


    public function relatorio_resultados_aula($lesson_id)
    {

        $lesson = Lesson::with('abilities.evidences.results', 'knowledge_s.evidences.results', 'competencies.evidences.results')->findOrFail($lesson_id);

        $students = Student::students_who_watched_lesson($lesson_id);



        //Pega as médias diretamente do banco
        $med_Joy =  Average_Students_Emotions_Lesson::all()
            ->where('lesson_id', $lesson_id)->avg('joy');


        $med_Fear =  Average_Students_Emotions_Lesson::all()
            ->where('lesson_id', $lesson_id)->avg('fear');

        $med_Anger =  Average_Students_Emotions_Lesson::all()
            ->where('lesson_id', $lesson_id)->avg('rage');

        $med_Disgust =  Average_Students_Emotions_Lesson::all()
            ->where('lesson_id', $lesson_id)->avg('disgust');

        $med_Sadness =  Average_Students_Emotions_Lesson::all()
            ->where('lesson_id', $lesson_id)->avg('sadness');

        $med_Contempt =  Average_Students_Emotions_Lesson::all()
            ->where('lesson_id', $lesson_id)->avg('contempt');

        $med_Surprise =  Average_Students_Emotions_Lesson::all()
            ->where('lesson_id', $lesson_id)->avg('surprise');


        $lesson = Lesson::findLessonById($lesson_id);


        //verifica se existem resultados - caso não tenha, aparece a msg dizendo que não teve resultado na tela do grático
        if ($med_Joy){
            $subtitle = 'Todos os Alunos' . ' / ' . $lesson->title;
        } else{
            $subtitle = null;
        }


        //Reduz as casas decimais
        $med_Joy = number_format($med_Joy, 2, '.', '');
        $med_Anger = number_format($med_Anger, 2, '.', '');
        $med_Contempt = number_format($med_Contempt, 2, '.', '');
        $med_Disgust = number_format($med_Disgust, 2, '.', '');
        $med_Fear = number_format($med_Fear, 2, '.', '');
        $med_Sadness = number_format($med_Sadness, 2, '.', '');
        $med_Surprise = number_format($med_Surprise, 2, '.', '');

        //Formata em array
        $str_Media = '[' . $med_Joy . ', ' . $med_Fear . ', ' . $med_Anger . ', ' .
            $med_Disgust . ', ' . $med_Sadness . ', ' .
            $med_Contempt . ', ' . $med_Surprise . ']';






        return view('statistics.report_results_lesson.index', compact('lesson', 'students', 'subtitle', 'str_Media'));

    }



}