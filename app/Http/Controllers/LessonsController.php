<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreLessonRequest;
use App\Http\Requests\UpdateLessonRequest;
use App\Models\IlsQuestion;
use App\Models\KnowledgeArea;
use App\Models\Lesson;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LessonsController extends Controller
{

    public function index()
    {

        $lessons = Lesson::with('knowledgeArea', 'creator')->latest()->paginate();

        return view('lessons.index', compact('lessons'));

    }

    public function myIndex()
    {

        $lessons = Lesson::with('knowledgeArea', 'creator')->where('user_id','=', Auth::user()->id)->latest()->paginate();

        return view('lessons.my_index', compact('lessons'));

    }

    public function create()
    {

        $knowledgeAreas = KnowledgeArea::parentsWithChildren();
        $ilsQuestions = IlsQuestion::with('options.question')->get();

        return view('lessons.create', compact('knowledgeAreas', 'ilsQuestions'));

    }

    public function store(StoreLessonRequest $request)
    {

        $data = $request->only([

            'knowledge_area',
            'title',
            'description',
            'outro',
            'video_type',
            'questions',
            'contents',
            'competences',
            'abilities',
            'knowledge_s',


        ]);

        $data['video_uri'] = $this->getVideoUri($data['video_type'], $request);

        $data['slug'] = slugify($data['title']);

        DB::transaction(function () use ($data) {

            Lesson::createNew(Auth::user(), $data);

        });

        return redirect()->route('lessons.index')->with('status', trans('status.lessons.created'));

    }

    public function edit($id)
    {

        $lesson = Lesson::with('knowledgeArea', 'creator', 'editor', 'questions.options',
            'abilities.evidences', 'knowledge_s.evidences', 'competencies.evidences')->findOrFail($id);

        $ilsQuestions = IlsQuestion::with('options.question')->get();

        $knowledgeAreas = KnowledgeArea::parentsWithChildren();

        return view('lessons.edit', compact('knowledgeAreas', 'lesson', 'ilsQuestions'));

    }

    public function update(UpdateLessonRequest $request, $id)
    {

        $lesson = Lesson::findOrFail($id);

        $data = $request->only([

            'knowledge_area',
            'title',
            'description',
            'outro',
            'video_was_changed',
            'questions_were_changed',
            'contents_were_changed',

            'abilities_were_changed',
            'knowledge_s_were_changed',
            'competencies_were_changed',

        ]);

        if (filter_var($request->get('video_was_changed'), FILTER_VALIDATE_BOOLEAN)) {

            $data['video_type'] = $request->get('video_type');

            $data['video_uri'] = $this->getVideoUri($data['video_type'], $request);

        }

        if (filter_var($request->get('questions_were_changed'), FILTER_VALIDATE_BOOLEAN)) {

            $data['questions'] = $request->get('questions');

        }




        if (filter_var($request->get('abilities_were_changed'), FILTER_VALIDATE_BOOLEAN)) {

            $data['abilities'] = $request->get('abilities');

        }

        if (filter_var($request->get('knowledge_s_were_changed'), FILTER_VALIDATE_BOOLEAN)) {

            $data['knowledge_s'] = $request->get('knowledge_s');

        }

        if (filter_var($request->get('competencies_were_changed'), FILTER_VALIDATE_BOOLEAN)) {

            $data['competences'] = $request->get('competences');

        }




        if (filter_var($request->get('contents_were_changed'), FILTER_VALIDATE_BOOLEAN)) {

            $data['contents'] = $request->get('contents');

        }

        DB::transaction(function () use ($lesson, $data) {

            $lesson->updateData(Auth::user(), $data);

        });


        return redirect()->route('lessons.index')->with('status', trans('status.lessons.updated'));

    }

    public function destroy($id)
    {

        Lesson::deleteLesson($id);

//        $lesson = Lesson::findOrFail($id);
//        $lesson->delete();

        return redirect()->route('lessons.index')->with('status', trans('status.lessons.deleted'));

    }

    private function getVideoUri(string $videoType, Request $request)
    {

        if ($videoType == Lesson::VIDEO_TYPE_FILE)
            return $request->file('video_file')->store('public/videos');

        return $request->get('video_id');

    }

    /**
     * Shows a Lesson and its details.
     *
     * @param $slug
     * @return JsonResponse
     */
    public function show($slug)
    {
        return response()->json([
            'lesson' => Lesson::findOrFailBySlug($slug)->toArray(),
        ]);
    }

}
