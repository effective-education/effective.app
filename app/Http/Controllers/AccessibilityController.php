<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

/**
 * Class DashboardController
 * @package App\Http\Controllers
 */
class AccessibilityController extends Controller
{

    /**
     * Show the page of Accessibility.
     *
     * @return Factory|View
     */
    public function index()
    {

        return view('accessibility.index');

    }

}