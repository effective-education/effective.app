<?php

namespace App\Http\Controllers;

use App\Models\Average_Students_Emotions_Lesson;


/**
 *
 *
 * @package App\Http\Controllers
 */
class Average_Students_Emotions_LessonController extends Controller
{

    public static function store($student_id, $lesson_id)
    {
        $average = new Average_Students_Emotions_Lesson();
        $average::createNew($student_id, $lesson_id, StudentsResultsController::medias_aluno_aula($lesson_id, $student_id));

    }
}
