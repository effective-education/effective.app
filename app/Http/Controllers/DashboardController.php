<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Sinergi\BrowserDetector\Browser;


/**
 * Class DashboardController
 * @package App\Http\Controllers
 */
class DashboardController extends Controller
{

    /**
     * Show the application's dashboard.
     *
     * @return Factory|View
     */
    public function index()
    {


        return view('dashboard.index');

    }

}