<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTestResultsRequest;
use App\Models\IlsQuestion;
use App\Models\Lesson;
use App\Models\Student;
use App\Models\StudentResult;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TestsController extends Controller
{

    /**
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($slug)
    {

        $lesson = Lesson::findOrFailBySlug($slug);

        $lesson->countView();

        $translation = trans('strings');

        $ils = IlsQuestion::getQuiz();

        return view('tests.show', compact('lesson', 'translation', 'ils'));

    }

    public function store(StoreTestResultsRequest $request)
    {

        $questions = $request->get('questions');
        $frames = $request->get('frames');

        $student = Student::findOrFail($request->get('student'));
        $lesson = Lesson::findOrFail($request->get('lesson'));

        StudentResult::createNew($student, $lesson, $questions, $frames);

        $lesson->countComplete();

        return response()->json();

    }

}