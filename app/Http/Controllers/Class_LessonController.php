<?php

namespace App\Http\Controllers;


use App\Http\Requests\StoreClass_LessonRequest;

use App\Models\Classe;
use App\Models\ClasseLesson;
use App\Models\Lesson;
use App\Policies\TaskPolicy;
use Illuminate\Support\Facades\Auth;


/**
 * Class ClassesController
 * @package App\Http\Controllers
 */
class Class_LessonController extends Controller
{

    public function create($id)
    {
        if (Auth::check()) {
            if (Auth::user()->isAdmin() || Auth::user()->isCollaborator()) {
                $lessons = Lesson::all();
                $std = Classe::find($id)->lessons()->orderBy('title')->paginate(5);

                foreach ($std as $st) {
                    $lessons = $lessons->except($st->id);
                }

                $class = $this->getEditedClass($id);

                return view('class_lesson.add', compact('class', 'lessons', 'std'));

            }
        }

    }


    public function destroy($classe_id, $lesson_id)
    {
        $classes = ClasseLesson::where('classe_id','=',$classe_id);
        $classe_lesson = $classes->where('lesson_id','=',$lesson_id);

        $classe_lesson->delete();


        return redirect()->route('classes.edit', [$classe_id])->with('status', trans('status.class_lesson.deleted'));

    }


    public function store(StoreClass_LessonRequest $request)
    {

        $data = $request->only([

            'classe_id',
            'lesson_id'

        ]);


        ClasseLesson::createNew($data);

        return redirect()->route('add_lesson', ['id' => $data['classe_id']])->with('status', trans('status.class_lesson.add'));

    }


    private function getEditedClass($id)
    {

        $class = Classe::findOrFail($id);
        return $class;

    }

}