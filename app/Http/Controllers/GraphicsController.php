<?php

namespace App\Http\Controllers;

use App\Http\Requests\PlotGraphicsRequest;
use App\Models\Average_Students_Emotions_Lesson;
use App\Models\Classe;
use App\Models\ClasseLesson;
use App\Models\ClasseStudent;
use App\Models\Lesson;
use App\Models\Student;
use App\Models\StudentResult;
use App\Models\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Illuminate\Support\Facades\Auth;

/**
 * Class DashboardController
 * @package App\Http\Controllers
 */
class GraphicsController extends Controller
{

    /**
     * Show the page .
     *
     * @return Factory|View
     */
    public function index_student()
    {
        if (Auth::check()) {
            if (Auth::user()->isStudent()) {
                $lessons = Lesson::findLessonsByUser(Auth::user());
                return view('graphics.index_of_student', compact('lessons'));
            }
        }
    }


    public function index_user()
    {
        if (Auth::check()) {
            if (Auth::user()->isAdmin() || Auth::user()->isCollaborator()) {
                $students = Student::students_with_emotion();
                $lessons = Lesson::findLessonsWatched();

                $lessons_ar = $lessons->pluck('id');
                $class_lesson = ClasseLesson::all()->whereIn('lesson_id', $lessons_ar)->pluck('classe_id');
                $classes = Classe::all()->whereIn('id', $class_lesson);

                return view('graphics.index_of_user', compact('students', 'lessons', 'classes'));
            }
        }
    }


    public function index()
    {
        if (Auth::check()) {
            if (Auth::user()->isStudent()) {
                $lessons = Lesson::findLessonsByUser(Auth::user());
                return view('graphics.index_of_student', compact('lessons'));
            } elseif (Auth::user()->isAdmin() || Auth::user()->isCollaborator()) {
                $lessons = Lesson::findLessonsByUser(Auth::user());
                return view('graphics.index_of_user', compact('lessons'));
            }
        }
    }


    /////////////////////////////////////
    // Graficos do Student
    ////////////////////////////////////////////////////////////////////////////////


    public function plot_Student(PlotGraphicsRequest $request)
    {
        $data = $request->only([
            'lesson_id',
            'type'
        ]);

        $student = Student::findByEmail(Auth::user()->email);

        $results = StudentResult::findResults($data['lesson_id'], $student->id);

        $frames = $results->frames;

        $lesson = Lesson::findLessonById($data['lesson_id']);

        $cont = 0;
        $joy = array();
        $fear = array();
        $anger = array();
        $disgust = array();
        $sadness = array();
        $valence = array();
        $contempt = array();
        $surprise = array();
        $engagement = array();


        //echo "Time - Emotion <br>";
        foreach ($frames as $frame) {
            //echo $frame['seconds'] . ' - ';
            foreach ($frame['emotions'] as $emotions) {
                //echo $emotions;

                if ($cont == 0) {
                    $joy[] = $emotions;
                } elseif ($cont == 1) {
                    $fear[] = $emotions;
                } elseif ($cont == 2) {
                    $anger[] = $emotions;
                } elseif ($cont == 3) {
                    $disgust[] = $emotions;
                } elseif ($cont == 4) {
                    $sadness[] = $emotions;
                } elseif ($cont == 5) {
                    $valence[] = $emotions;
                } elseif ($cont == 6) {
                    $contempt[] = $emotions;
                } elseif ($cont == 7) {
                    $surprise[] = $emotions;
                } elseif ($cont == 8) {
                    $engagement[] = $emotions;
                }

                if ($cont == 8) {
                    // echo '*';
                    $cont = 0;
                } else {
                    $cont++;
                }
            }
            //echo 'Joy = '. $joy;
            //dd($frame);
            //echo "<br>";
        }


        if ($data['type'] == '1') { //Variaçao de Emoçoes por Aula


            //Formata os valores do Array para o grafico [x, y, z]

            $str_Joy = '[';
            foreach ($joy as $j) {
                $str_Joy = $str_Joy . $j . ', ';
            }
            $str_Joy = $str_Joy . ']';


            $str_Fear = '[';
            foreach ($fear as $j) {
                $str_Fear = $str_Fear . $j . ', ';
            }
            $str_Fear = $str_Fear . ']';


            $str_Anger = '[';
            foreach ($anger as $j) {
                $str_Anger = $str_Anger . $j . ', ';
            }
            $str_Anger = $str_Anger . ']';


            $str_Disgust = '[';
            foreach ($disgust as $j) {
                $str_Disgust = $str_Disgust . $j . ', ';
            }
            $str_Disgust = $str_Disgust . ']';


            $str_Sadness = '[';
            foreach ($sadness as $j) {
                $str_Sadness = $str_Sadness . $j . ', ';
            }
            $str_Sadness = $str_Sadness . ']';


//            $str_Valence = '[';
//            foreach ($valence as $j) {
//                $str_Valence = $str_Valence . $j . ', ';
//            }
//            $str_Valence = $str_Valence . ']';


            $str_Contempt = '[';
            foreach ($contempt as $j) {
                $str_Contempt = $str_Contempt . $j . ', ';
            }
            $str_Contempt = $str_Contempt . ']';


            $str_Surprise = '[';
            foreach ($surprise as $j) {
                $str_Surprise = $str_Surprise . $j . ', ';
            }
            $str_Surprise = $str_Surprise . ']';


//            $str_Engagement = '[';
//            foreach ($engagement as $j) {
//                $str_Engagement = $str_Engagement . $j . ', ';
//            }
//            $str_Engagement = $str_Engagement . ']';


            $subtitle = $lesson->title;

            return view('graphics.variation_of_emotions_per_lesson', compact(
                'str_Joy', 'str_Fear', 'str_Anger', 'str_Disgust', 'str_Sadness', 'str_Contempt', 'str_Surprise', 'subtitle'));


        } elseif ($data['type'] == '2') { //Media das Emoçoes

            //recupera as médias direto do banco de dados
//            $student_results = new StudentsResultsController();
//            $media = $student_results::medias_aluno_aula($lesson->id, $student->id);
//

//            $str_Media = '[' . $media['joy'] . ', ' . $media['fear'] . ',' . $media['rage'] . ', ' .
//                $media['disgust'] . ', ' . $media['sadness'] . ', ' .
//                $media['contempt'] . ', ' . $media['surprise'] . ']';

            //Pega as médias diretamente do banco
            $med_Joy =  Average_Students_Emotions_Lesson::all()
                ->where('student_id', $student->id)->avg('joy');


            $med_Fear =  Average_Students_Emotions_Lesson::all()
                ->where('student_id', $student->id)->avg('fear');

            $med_Anger =  Average_Students_Emotions_Lesson::all()
                ->where('student_id', $student->id)->avg('rage');

            $med_Disgust =  Average_Students_Emotions_Lesson::all()
                ->where('student_id', $student->id)->avg('disgust');

            $med_Sadness =  Average_Students_Emotions_Lesson::all()
                ->where('student_id', $student->id)->avg('sadness');

            $med_Contempt =  Average_Students_Emotions_Lesson::all()
                ->where('student_id', $student->id)->avg('contempt');

            $med_Surprise =  Average_Students_Emotions_Lesson::all()
                ->where('student_id', $student->id)->avg('surprise');


            //Reduz as casas decimais
            $med_Joy = number_format($med_Joy, 2, '.', '');
            $med_Anger = number_format($med_Anger, 2, '.', '');
            $med_Contempt = number_format($med_Contempt, 2, '.', '');
            $med_Disgust = number_format($med_Disgust, 2, '.', '');
            $med_Fear = number_format($med_Fear, 2, '.', '');
            $med_Sadness = number_format($med_Sadness, 2, '.', '');
            $med_Surprise = number_format($med_Surprise, 2, '.', '');


            //Formata em array
            $str_Media = '[' . $med_Joy . ', ' . $med_Fear . ',' . $med_Anger . ', ' .
                $med_Disgust . ', ' . $med_Sadness . ', ' .
                $med_Contempt . ', ' . $med_Surprise . ']';


            //verifica se existem resultados - caso não tenha, aparece a msg dizendo que não teve resultado na tela do grático
            if ($med_Joy){
                $subtitle = $lesson->title;
            } else{
                $subtitle = null;
            }

            return view('graphics.average_chart_of_emotions_per_lesson', compact('str_Media', 'subtitle'));


        } elseif ($data['type'] == '3') { //Taxa de Acertos


            $total = $results->total_questions;

            $certas = $results->right_answers;
            $erradas = $total - $certas;

            $subtitle = $lesson->title;
            return view('graphics.hit_rate_graph_per_lesson', compact('certas', 'erradas', 'total', 'subtitle'));
        }

    }


    /////////////////////////////////////
    // Graficos do Admin
    ////////////////////////////////////////////////////////////////////////////////

    //usado pelo JavaScript para encontrar via Ajax as aulas de um aluno selecionado na checkBox
    public static function student_lessons($student_id)
    {
        $user = User::findByStudentId($student_id);
        $lessons = Lesson::findLessonsByUser($user);

        $ar = array();
        $al = array();
        $i = 0;

        foreach ($lessons as $lesson) {
            $al['Id'] = $lesson->id;
            $al['Name'] = $lesson->title;
            $ar[$i] = $al;
            $i++;
        }

        if (Auth::check()){
            if (! Auth::user()->isStudent()){
                return $ar;
            }else{
                return null;
            }
        }else{
            return null;
        }

    }


    public function plotVariationOfStudentEmotionByLesson($lesson_id, $student_id)
    {
        $results = StudentResult::findResults($lesson_id, $student_id);

        $frames = $results->frames;

        $lesson = Lesson::findLessonById($lesson_id);

        $cont = 0;
        $joy = array();
        $fear = array();
        $anger = array();
        $disgust = array();
        $sadness = array();
        $valence = array();
        $contempt = array();
        $surprise = array();
        $engagement = array();


        foreach ($frames as $frame) {
            foreach ($frame['emotions'] as $emotions) {

                if ($cont == 0) {
                    $joy[] = $emotions;
                } elseif ($cont == 1) {
                    $fear[] = $emotions;
                } elseif ($cont == 2) {
                    $anger[] = $emotions;
                } elseif ($cont == 3) {
                    $disgust[] = $emotions;
                } elseif ($cont == 4) {
                    $sadness[] = $emotions;
                } elseif ($cont == 5) {
                    $valence[] = $emotions;
                } elseif ($cont == 6) {
                    $contempt[] = $emotions;
                } elseif ($cont == 7) {
                    $surprise[] = $emotions;
                } elseif ($cont == 8) {
                    $engagement[] = $emotions;
                }

                if ($cont == 8) {
                    $cont = 0;
                } else {
                    $cont++;
                }
            }
        }

        $str_Joy = '[';
        foreach ($joy as $j) {
            $str_Joy = $str_Joy . $j . ', ';
        }
        $str_Joy = $str_Joy . ']';


        $str_Fear = '[';
        foreach ($fear as $j) {
            $str_Fear = $str_Fear . $j . ', ';
        }
        $str_Fear = $str_Fear . ']';


        $str_Anger = '[';
        foreach ($anger as $j) {
            $str_Anger = $str_Anger . $j . ', ';
        }
        $str_Anger = $str_Anger . ']';


        $str_Disgust = '[';
        foreach ($disgust as $j) {
            $str_Disgust = $str_Disgust . $j . ', ';
        }
        $str_Disgust = $str_Disgust . ']';


        $str_Sadness = '[';
        foreach ($sadness as $j) {
            $str_Sadness = $str_Sadness . $j . ', ';
        }
        $str_Sadness = $str_Sadness . ']';


        $str_Contempt = '[';
        foreach ($contempt as $j) {
            $str_Contempt = $str_Contempt . $j . ', ';
        }
        $str_Contempt = $str_Contempt . ']';


        $str_Surprise = '[';
        foreach ($surprise as $j) {
            $str_Surprise = $str_Surprise . $j . ', ';
        }
        $str_Surprise = $str_Surprise . ']';

        $student_name = Student::all()->where('id', $student_id)->first();
        $subtitle = $student_name->name . ' / ' . $lesson->title;

        return view('graphics.variation_of_emotions_per_lesson', compact(
            'str_Joy', 'str_Fear', 'str_Anger', 'str_Disgust', 'str_Sadness', 'str_Contempt', 'str_Surprise', 'subtitle'));


    }


    public function plotAverageOfEmotionsByStudent($student_id)
    {
//        $med_Joy = 0;
//        $cont_Joy = 0;
//        $med_Fear = 0;
//        $cont_Fear = 0;
//        $med_Anger = 0;
//        $cont_Anger = 0;
//        $med_Disgust = 0;
//        $cont_Disgust = 0;
//        $med_Sadness = 0;
//        $cont_Sadness = 0;
//        $med_Contempt = 0;
//        $cont_Contempt = 0;
//        $med_Surprise = 0;
//        $cont_Surprise = 0;
//
//        $lessons = self::student_lessons($student_id);
//
//        $frames = array();
//        $i = 0;
//        //pega os frames de todas as aulas assistidas pelo aluno selecionado
//        for ($i; $i < sizeof($lessons); $i++) {
//            $results = StudentResult::findResults($lessons[$i]['Id'], $student_id);
//            $frames[] = $results->frames;
//        }
//
//        $cont = 0;
//        $j = 0;
//        for ($j; $j < sizeof($frames); $j++) {
//            foreach ($frames[$j] as $frame) {
//                foreach ($frame['emotions'] as $emotions) {
//                    if ($cont == 0) {
//                        $med_Joy += $emotions;
//                        $cont_Joy++;
//                    } elseif ($cont == 1) {
//                        $med_Fear += $emotions;
//                        $cont_Fear++;
//                    } elseif ($cont == 2) {
//                        $med_Anger += $emotions;
//                        $cont_Anger++;
//                    } elseif ($cont == 3) {
//                        $med_Disgust += $emotions;
//                        $cont_Disgust++;
//                    } elseif ($cont == 4) {
//                        $med_Sadness += $emotions;
//                        $cont_Sadness++;
//                    } elseif ($cont == 5) {
//                        //$med_Valence += $emotions;
//                    } elseif ($cont == 6) {
//                        $med_Contempt += $emotions;
//                        $cont_Contempt++;
//                    } elseif ($cont == 7) {
//                        $med_Surprise += $emotions;
//                        $cont_Surprise++;
//                    } elseif ($cont == 8) {
//                        //$med_Engagement += $emotions;
//                    }
//
//                    if ($cont == 8) {
//                        $cont = 0;
//                    } else {
//                        $cont++;
//                    }
//                }
//            }
//        }
//
//        //Calcula as médias
//        $med_Joy /= $cont_Joy;
//        $med_Fear /= $cont_Fear;
//        $med_Anger /= $cont_Anger;
//        $med_Disgust /= $cont_Disgust;
//        $med_Sadness /= $cont_Sadness;
//        $med_Contempt /= $cont_Contempt;
//        $med_Surprise /= $cont_Surprise;

        //Pega as médias diretamente do banco
        $med_Joy =  Average_Students_Emotions_Lesson::all()
            ->where('student_id', $student_id)->avg('joy');


        $med_Fear =  Average_Students_Emotions_Lesson::all()
            ->where('student_id', $student_id)->avg('fear');

        $med_Anger =  Average_Students_Emotions_Lesson::all()
            ->where('student_id', $student_id)->avg('rage');

        $med_Disgust =  Average_Students_Emotions_Lesson::all()
            ->where('student_id', $student_id)->avg('disgust');

        $med_Sadness =  Average_Students_Emotions_Lesson::all()
            ->where('student_id', $student_id)->avg('sadness');

        $med_Contempt =  Average_Students_Emotions_Lesson::all()
            ->where('student_id', $student_id)->avg('contempt');

        $med_Surprise =  Average_Students_Emotions_Lesson::all()
            ->where('student_id', $student_id)->avg('surprise');


        $student = Student::all()->where('id', $student_id)->first();

        //verifica se existem resultados - caso não tenha, aparece a msg dizendo que não teve resultado na tela do grático
        if ($med_Joy){
            $subtitle = $student->name . ' / Todas as Aulas';
        } else{
            $subtitle = null;
        }


        //Reduz as casas decimais
        $med_Joy = number_format($med_Joy, 2, '.', '');
        $med_Anger = number_format($med_Anger, 2, '.', '');
        $med_Contempt = number_format($med_Contempt, 2, '.', '');
        $med_Disgust = number_format($med_Disgust, 2, '.', '');
        $med_Fear = number_format($med_Fear, 2, '.', '');
        $med_Sadness = number_format($med_Sadness, 2, '.', '');
        $med_Surprise = number_format($med_Surprise, 2, '.', '');

        //Formata em array
        $str_Media = '[' . $med_Joy . ', ' . $med_Fear . ',' . $med_Anger . ', ' .
            $med_Disgust . ', ' . $med_Sadness . ', ' .
            $med_Contempt . ', ' . $med_Surprise . ']';


        return view('graphics.average_chart_of_emotions_per_lesson', compact('str_Media', 'subtitle'));

    }


    public function plotAverageOfEmotionsByLesson($lesson_id)
    {
//        $med_Joy = 0;
//        $cont_Joy = 0;
//        $med_Fear = 0;
//        $cont_Fear = 0;
//        $med_Anger = 0;
//        $cont_Anger = 0;
//        $med_Disgust = 0;
//        $cont_Disgust = 0;
//        $med_Sadness = 0;
//        $cont_Sadness = 0;
//        $med_Contempt = 0;
//        $cont_Contempt = 0;
//        $med_Surprise = 0;
//        $cont_Surprise = 0;
//
//        $frames = array();
//        //pega os alunos que assistiram a aula selecionada
//        $students = Student::students_who_watched_lesson($lesson_id);
//
//        //pega os frames da aula de acordo com o Id do Aluno que a assistiu
//        foreach ($students as $student) {
//            $results = StudentResult::findResults($lesson_id, $student->id);
//            $frames[] = $results->frames;
//            //echo $student->name . '<br>';
//        }
//
//        $cont = 0;
//        $j = 0;
//        for ($j; $j < sizeof($frames); $j++) {
//            foreach ($frames[$j] as $frame) {
//                foreach ($frame['emotions'] as $emotions) {
//                    if ($cont == 0) {
//                        $med_Joy += $emotions;
//                        $cont_Joy++;
//                    } elseif ($cont == 1) {
//                        $med_Fear += $emotions;
//                        $cont_Fear++;
//                    } elseif ($cont == 2) {
//                        $med_Anger += $emotions;
//                        $cont_Anger++;
//                    } elseif ($cont == 3) {
//                        $med_Disgust += $emotions;
//                        $cont_Disgust++;
//                    } elseif ($cont == 4) {
//                        $med_Sadness += $emotions;
//                        $cont_Sadness++;
//                    } elseif ($cont == 5) {
//                        //$med_Valence += $emotions;
//                    } elseif ($cont == 6) {
//                        $med_Contempt += $emotions;
//                        $cont_Contempt++;
//                    } elseif ($cont == 7) {
//                        $med_Surprise += $emotions;
//                        $cont_Surprise++;
//                    } elseif ($cont == 8) {
//                        //$med_Engagement += $emotions;
//                    }
//
//                    if ($cont == 8) {
//                        $cont = 0;
//                    } else {
//                        $cont++;
//                    }
//                }
//            }
//        }
//        //Calcula as médias
//        $med_Joy /= $cont_Joy;
//        $med_Fear /= $cont_Fear;
//        $med_Anger /= $cont_Anger;
//        $med_Disgust /= $cont_Disgust;
//        $med_Sadness /= $cont_Sadness;
//        $med_Contempt /= $cont_Contempt;
//        $med_Surprise /= $cont_Surprise;

        //Pega as médias diretamente do banco
        $med_Joy =  Average_Students_Emotions_Lesson::all()
            ->where('lesson_id', $lesson_id)->avg('joy');


        $med_Fear =  Average_Students_Emotions_Lesson::all()
            ->where('lesson_id', $lesson_id)->avg('fear');

        $med_Anger =  Average_Students_Emotions_Lesson::all()
            ->where('lesson_id', $lesson_id)->avg('rage');

        $med_Disgust =  Average_Students_Emotions_Lesson::all()
            ->where('lesson_id', $lesson_id)->avg('disgust');

        $med_Sadness =  Average_Students_Emotions_Lesson::all()
            ->where('lesson_id', $lesson_id)->avg('sadness');

        $med_Contempt =  Average_Students_Emotions_Lesson::all()
            ->where('lesson_id', $lesson_id)->avg('contempt');

        $med_Surprise =  Average_Students_Emotions_Lesson::all()
            ->where('lesson_id', $lesson_id)->avg('surprise');


        $lesson = Lesson::findLessonById($lesson_id);


        //verifica se existem resultados - caso não tenha, aparece a msg dizendo que não teve resultado na tela do grático
        if ($med_Joy){
            $subtitle = 'Todos os Alunos' . ' / ' . $lesson->title;
        } else{
            $subtitle = null;
        }


        //Reduz as casas decimais
        $med_Joy = number_format($med_Joy, 2, '.', '');
        $med_Anger = number_format($med_Anger, 2, '.', '');
        $med_Contempt = number_format($med_Contempt, 2, '.', '');
        $med_Disgust = number_format($med_Disgust, 2, '.', '');
        $med_Fear = number_format($med_Fear, 2, '.', '');
        $med_Sadness = number_format($med_Sadness, 2, '.', '');
        $med_Surprise = number_format($med_Surprise, 2, '.', '');

        //Formata em array
        $str_Media = '[' . $med_Joy . ', ' . $med_Fear . ', ' . $med_Anger . ', ' .
            $med_Disgust . ', ' . $med_Sadness . ', ' .
            $med_Contempt . ', ' . $med_Surprise . ']';

        return view('graphics.average_chart_of_emotions_per_lesson', compact('str_Media', 'subtitle'));

    }


    public function plotAverageOfEmotionsByClass($class_id)
    {
        $med_Joy = 0;
        $cont_Joy = 0;
        $med_Fear = 0;
        $cont_Fear = 0;
        $med_Anger = 0;
        $cont_Anger = 0;
        $med_Disgust = 0;
        $cont_Disgust = 0;
        $med_Sadness = 0;
        $cont_Sadness = 0;
        $med_Contempt = 0;
        $cont_Contempt = 0;
        $med_Surprise = 0;
        $cont_Surprise = 0;

        $frames = array();
        $lessons = array();

        $class = Classe::all()->where('id', '=', $class_id)->first();

        $class_student = ClasseStudent::all()->where('classe_id', $class_id)->pluck('student_id');
        $students_with_emotions = Student::students_with_emotion();
        $students_class = $students_with_emotions->whereIn('id', $class_student);
        $students_ar = $students_class->pluck('id');


        //Somente os alunos da Turma que assitiram alguma aula
        $students = Student::all()->whereIn('id', $students_ar);

        //Somente as aulas assistidas pelos alunos selecionados acima
        foreach ($students as $student) {
            $lessons[] = self::student_lessons($student->id);
        }


        $ids = array();
        $results = array();

        //pega os ids das aulas
        for ($i = 0; $i < sizeof($lessons); $i++) {
            for ($j = 0; $j < sizeof($lessons[$i]); $j++) {
                $ids[] = $lessons[$i][$j]['Id'];
            }
        }


        $m_les = 0;
        //Pega os Results
        foreach ($students as $student) {
            for ($x = 0; $x < sizeof($ids); $x++) {
                if ($m_les != $ids[$x]) {
                    $m_les = $ids[$x];
                    $r = StudentResult::findResults($ids[$x], $student->id);
                    if ($r != null) {
                        $results[] = $r;
                    }
                }
            }
        }

        //Pega os Frames
        for ($y = 0; $y < sizeof($results); $y++) {
            $frames[] = $results[$y]->frames;
        }

        $cont = 0;
        for ($j = 0; $j < sizeof($frames); $j++) {
            foreach ($frames[$j] as $frame) {
                foreach ($frame['emotions'] as $emotions) {
                    if ($cont == 0) {
                        $med_Joy += $emotions;
                        $cont_Joy++;
                    } elseif ($cont == 1) {
                        $med_Fear += $emotions;
                        $cont_Fear++;
                    } elseif ($cont == 2) {
                        $med_Anger += $emotions;
                        $cont_Anger++;
                    } elseif ($cont == 3) {
                        $med_Disgust += $emotions;
                        $cont_Disgust++;
                    } elseif ($cont == 4) {
                        $med_Sadness += $emotions;
                        $cont_Sadness++;
                    } elseif ($cont == 5) {
                        //$med_Valence += $emotions;
                    } elseif ($cont == 6) {
                        $med_Contempt += $emotions;
                        $cont_Contempt++;
                    } elseif ($cont == 7) {
                        $med_Surprise += $emotions;
                        $cont_Surprise++;
                    } elseif ($cont == 8) {
                        //$med_Engagement += $emotions;
                    }

                    if ($cont == 8) {
                        $cont = 0;
                    } else {
                        $cont++;
                    }
                }
            }
        }

        //Calcula as médias
        $med_Joy /= $cont_Joy;
        $med_Fear /= $cont_Fear;
        $med_Anger /= $cont_Anger;
        $med_Disgust /= $cont_Disgust;
        $med_Sadness /= $cont_Sadness;
        $med_Contempt /= $cont_Contempt;
        $med_Surprise /= $cont_Surprise;


        //verifica se existem resultados - caso não tenha, aparece a msg dizendo que não teve resultado na tela do grático
        if ($med_Joy){
            $subtitle = 'Turma: ' . $class->name;
        } else{
            $subtitle = null;
        }

        //Reduz as casas decimais
        $med_Joy = number_format($med_Joy, 2, '.', '');
        $med_Anger = number_format($med_Anger, 2, '.', '');
        $med_Contempt = number_format($med_Contempt, 2, '.', '');
        $med_Disgust = number_format($med_Disgust, 2, '.', '');
        $med_Fear = number_format($med_Fear, 2, '.', '');
        $med_Sadness = number_format($med_Sadness, 2, '.', '');
        $med_Surprise = number_format($med_Surprise, 2, '.', '');

        //Formata em array
        $str_Media = '[' . $med_Joy . ', ' . $med_Fear . ', ' . $med_Anger . ', ' .
            $med_Disgust . ', ' . $med_Sadness . ', ' .
            $med_Contempt . ', ' . $med_Surprise . ']';

        //dd($str_Media);

        return view('graphics.average_chart_of_emotions_per_lesson', compact('str_Media', 'subtitle'));

    }


    public function plotHitRateByStudent($student_id)
    {

        $student = Student::all()->where('id', '=', $student_id)->first();

        $lessons = self::student_lessons($student_id);

        $results = array();

        for ($j = 0; $j < sizeof($lessons); $j++) {
            $results[] = StudentResult::findResults($lessons[$j]['Id'], $student_id);
        }

        $total = 0;
        $certas = 0;

        //Pega os Totais
        for ($y = 0; $y < sizeof($results); $y++) {
            $total += $results[$y]->total_questions;
            $certas += $results[$y]->right_answers;
        }
        $erradas = $total - $certas;


        $subtitle = $student->name . ' / Todas as Aulas';
        return view('graphics.hit_rate_graph_per_lesson', compact('certas', 'erradas', 'total', 'subtitle'));

    }


    public function plotHitRateByLesson($lesson_id){

        $students = Student::students_who_watched_lesson($lesson_id);
        $lesson = Lesson::findLessonById($lesson_id);

        $total = 0;
        $certas = 0;
        //pega os frames da aula de acordo com o Id do Aluno que a assistiu
        foreach ($students as $student) {
            $results = StudentResult::findResults($lesson_id, $student->id);
            $total += $results->total_questions;
            $certas += $results->right_answers;
        }
        $erradas = $total - $certas;

        $subtitle = 'Todos os Alunos / '. $lesson->title;
        return view('graphics.hit_rate_graph_per_lesson', compact('certas', 'erradas', 'total', 'subtitle'));

    }



    public function plotHitRateByClass($class_id){

        $class = Classe::all()->where('id', '=', $class_id)->first();

        $class_student = ClasseStudent::all()->where('classe_id', $class_id)->pluck('student_id');
        $students_with_emotions = Student::students_with_emotion();
        $students_class = $students_with_emotions->whereIn('id', $class_student);
        $students_ar = $students_class->pluck('id');


        //Somente os alunos da Turma que assitiram alguma aula
        $students = Student::all()->whereIn('id', $students_ar);

        //Somente as aulas assistidas pelos alunos selecionados acima
        foreach ($students as $student) {
            $lessons[] = self::student_lessons($student->id);
        }


        $ids = array();
        $results = array();

        //pega os ids das aulas
        for ($i = 0; $i < sizeof($lessons); $i++) {
            for ($j = 0; $j < sizeof($lessons[$i]); $j++) {
                $ids[] = $lessons[$i][$j]['Id'];
            }
        }


        $m_les = 0;
        //Pega os Results
        foreach ($students as $student) {
            for ($x = 0; $x < sizeof($ids); $x++) {
                if ($m_les != $ids[$x]) {
                    $m_les = $ids[$x];
                    $r = StudentResult::findResults($ids[$x], $student->id);
                    if ($r != null) {
                        $results[] = $r;
                    }
                }
            }
        }

        $total = 0;
        $certas = 0;
        //Pega os Frames
        for ($y = 0; $y < sizeof($results); $y++) {
            $total += $results[$y]->total_questions;
            $certas += $results[$y]->right_answers;
        }
        $erradas = $total - $certas;

        $subtitle = 'Turma: ' . $class->name;

        return view('graphics.hit_rate_graph_per_lesson', compact('certas', 'erradas', 'total', 'subtitle'));


    }


}