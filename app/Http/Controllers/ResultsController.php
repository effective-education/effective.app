<?php

namespace App\Http\Controllers;


use App\Models\Lesson;
use App\Reports\Frames;
use App\Models\StudentResult;
use App\Reports\XmlFrames;
use Illuminate\Support\Facades\Auth;


class ResultsController extends Controller
{

    public function lesson($id)
    {
        if (Auth::check()) {
            if (Auth::user()->isAdmin() || Auth::user()->isCollaborator()) {
                $lesson = Lesson::with('metric', 'creator', 'results.student')->findOrFail($id);
                return view('results.lesson', compact('lesson'));
            }
        }
    }

    public function student($lessonId, $studentId)
    {
        if (Auth::check()) {
            if (Auth::user()->isAdmin() || Auth::user()->isCollaborator()) {
                $results = StudentResult::with('lesson', 'student.ils')->where([
                    ['lesson_id', $lessonId],
                    ['student_id', $studentId]
                ])->get();

                $student = $results->first()->student;
                $lesson = $results->first()->lesson;

                return view('results.student', compact('results', 'student', 'lesson'));
            }
        }
    }

    public function lastFrames($lessonId, $studentId)
    {

        $lastResult = StudentResult::with('student')->where([
            ['lesson_id', $lessonId],
            ['student_id', $studentId]
        ])->orderBy('created_at', 'DESC')->first();

        $report = new Frames($lastResult->student->name, $lastResult->frames);

        return response()->stream([$report, 'generate'], 200, $report->headers());

    }

    public function lastFramesXml($lessonId, $studentId)
    {
        $lastResult = StudentResult::with('student')->where([
            ['lesson_id', $lessonId],
            ['student_id', $studentId]
        ])->orderBy('created_at', 'DESC')->first();

        //TO DO faz uma querie pra buscar a session da lesson e mandar pro xml
        $report = new XmlFrames($lastResult->student->name, $lastResult->frames, $studentId, $lastResult->updated_at, $lastResult->student->onto_id);

        return response()->stream([$report, 'generate'], 200, $report->headers());
    }
}