<?php

namespace App\Http\Controllers;

use App\Models\Lesson;
use App\Transformers\PublicLessonsTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

/**
 * Controller for public lessons.
 *
 * @package App\Http\Controllers
 */
class PublicLessonsController extends Controller
{

    /**
     * Show all public lessons page.
     *
     * @return View
     */
    public function view()
    {
        $flag = false;
        return view('lessons.public', compact('flag'));

    }

    public function viewAll()
    {
        $flag = true;
        return view('lessons.public', compact('flag'));

    }

    /**
     * Shows all lessons.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {

        if (Auth::check()) {

            if (Auth::user()->isStudent()) {

                return response()->json((new PublicLessonsTransformer(Lesson::student_lessons($request->get('search', ''))))->transform());

            } else {

                return response()->json((new PublicLessonsTransformer(Lesson::search($request->get('search', ''))))->transform());

            }

        } else {

            return response()->json((new PublicLessonsTransformer(Lesson::search($request->get('search', ''))))->transform());

        }

    }

    public function indexAll(Request $request)
    {
        if (Auth::check()) {
            return response()->json((new PublicLessonsTransformer(Lesson::search($request->get('search', ''))))->transform());
        }else{
            return response()->json((new PublicLessonsTransformer(Lesson::search($request->get('search', ''))))->transform());
        }
    }

}
