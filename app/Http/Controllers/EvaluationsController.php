<?php

namespace App\Http\Controllers;


use App\Http\Requests\StoreEvaluationRequest;
use App\Http\Requests\UpdateEvaluationRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\Lesson;
use App\Models\ResultEvaluationStudentAbility;
use App\Models\ResultEvaluationStudentCompetence;
use App\Models\ResultEvaluationStudentKnowledge;
use App\Models\Student;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;


class EvaluationsController extends Controller
{


    public function route($lesson_id, $student_id){

        //verifica se já há resultados e encaminha para criar ou editar
        if(!(ResultEvaluationStudentAbility::isNew($lesson_id, $student_id) && ResultEvaluationStudentCompetence::isNew($lesson_id, $student_id) && ResultEvaluationStudentKnowledge::isNew($lesson_id, $student_id))){
            return redirect(route('lesson.results.evaluation_results.edit', [$lesson_id, $student_id]));
        }else{
            return redirect(route('lesson.results.evaluation_results.create', [$lesson_id, $student_id]));
        }

    }


    public function create($lesson_id, $student_id)
    {

        if (Auth::check()){
            if (Auth::user()->isAdmin() || Auth::user()->isCollaborator()) {
                $student = Student::all()->where('id', $student_id)->first();
                $lesson = Lesson::with('abilities.evidences', 'knowledge_s.evidences', 'competencies.evidences')->findOrFail($lesson_id);

                return view('results.evaluation.create', compact('student', 'lesson'));
            }
        }

    }


    public function edit($lesson_id, $student_id)
    {

        if (Auth::check()){
            if (Auth::user()->isAdmin() || Auth::user()->isCollaborator()) {
                $student = Student::all()->where('id', $student_id)->first();

                $lesson = Lesson::with('abilities.evidences.results', 'knowledge_s.evidences.results', 'competencies.evidences.results')->findOrFail($lesson_id);

               // dd($lesson);

                return view('results.evaluation.edit', compact('student', 'lesson'));
            }
        }

    }



    /**
     * Update an evaluation data.
     *
     * @param UpdateEvaluationRequest $request
     * @param $lesson_id
     * @return RedirectResponse
     */
    public function update(UpdateEvaluationRequest $request, $lesson_id)
    {

        $data = $request->only([
            'range',
            'evidence_id',
            'competence',
            'ability',
            'knowledge',
            'student_id',
            'lesson_id'
        ]);

        $ability = array();
        $knowledge = array();
        $competence = array();
        $evidence = array();
        $range = array();

        if (isset($data['ability'])){
            $ability = $data['ability'];
        }
        if (isset($data['knowledge'])){
            $knowledge = $data['knowledge'];
        }
        if (isset($data['competence'])){
            $competence = $data['competence'];
        }
        if (isset($data['evidence_id'])){
            $evidence = $data['evidence_id'];
        }
        if (isset($data['range'])){
            $range = $data['range'];
        }

        $union = array_merge($ability, $knowledge, $competence);


        $j = 0;
        $k = 0;

        for ($i = 0; $i < sizeof($union); $i++){

            if ($i < sizeof($ability)){

                ResultEvaluationStudentAbility::createNew($evidence[$i], $data['student_id'], $lesson_id, $range[$i]);

            }elseif ($j < sizeof($knowledge)){

                ResultEvaluationStudentKnowledge::createNew($evidence[$i], $data['student_id'], $lesson_id, $range[$i]);
                $j++;

            }elseif ($k < sizeof($competence)){

                ResultEvaluationStudentCompetence::createNew($evidence[$i], $data['student_id'], $lesson_id, $range[$i]);
                $k++;

            }

        }

        return redirect()->route('lesson.results', $lesson_id)->with('status', trans('status.evaluation_results.updated'));

    }


    public function store(StoreEvaluationRequest $request)
    {

        $data = $request->only([
            'range',
            'evidence_id',
            'competence',
            'ability',
            'knowledge',
            'student_id',
            'lesson_id'
        ]);

        $ability = array();
        $knowledge = array();
        $competence = array();
        $evidence = array();
        $range = array();

        if (isset($data['ability'])){
            $ability = $data['ability'];
        }
        if (isset($data['knowledge'])){
            $knowledge = $data['knowledge'];
        }
        if (isset($data['competence'])){
            $competence = $data['competence'];
        }
        if (isset($data['evidence_id'])){
            $evidence = $data['evidence_id'];
        }
        if (isset($data['range'])){
            $range = $data['range'];
        }

        $union = array_merge($ability, $knowledge, $competence);


        $j = 0;
        $k = 0;

        for ($i = 0; $i < sizeof($union); $i++){

            if ($i < sizeof($ability)){

                ResultEvaluationStudentAbility::createNew($evidence[$i], $data['student_id'], $data['lesson_id'], $range[$i]);

            }elseif ($j < sizeof($knowledge)){

                ResultEvaluationStudentKnowledge::createNew($evidence[$i], $data['student_id'], $data['lesson_id'], $range[$i]);
                $j++;

            }elseif ($k < sizeof($competence)){

                ResultEvaluationStudentCompetence::createNew($evidence[$i], $data['student_id'], $data['lesson_id'], $range[$i]);
                $k++;

            }

        }

        return redirect()->route('lesson.results', [$data['lesson_id']])->with('status', trans('status.evaluation_results.created'));

    }


}