<?php

namespace App\Http\Controllers;


use App\Http\Requests\StoreClassRequest;
use App\Http\Requests\UpdateClassRequest;

use App\Models\Classe;
use App\Models\ClasseLesson;
use App\Models\ClasseStudent;
use App\Models\Student;
use Illuminate\Support\Facades\Auth;


/**
 * Class ClassesController
 * @package App\Http\Controllers
 */
class ClassesController extends Controller
{

    public function index()
    {
        $classes = Classe::with('creator')->orderBy('name')->paginate('10');
        return view('classes.index', compact('classes'));

    }

    public function myIndex()
    {
        $classes = Classe::with('creator')->where('user_id', '=', Auth::user()->id)->orderBy('name')->paginate('10');
        return view('classes.my_index', compact('classes'));

    }

    public function create()
    {
        return view('classes.create');
    }


    public function edit($id)
    {

        $students = Classe::find($id)->students()->orderBy('name')->get();

        $lessons = Classe::find($id)->lessons()->orderBy('title')->get();

        $class = $this->getEditedClass($id);

        return view('classes.edit', compact('class', 'students', 'lessons'));

    }


    private function getEditedClass($id)
    {

        $class = Classe::findOrFail($id);
        return $class;

    }


    public function destroy($id)
    {
        //bsucar na tabela class_student e class_lesson
        $class_lesson = ClasseLesson::where('classe_id', '=', $id);
        $class_lesson->delete();

        $class_student = ClasseStudent::where('classe_id', '=', $id);
        $class_student->delete();

        $class = Classe::findOrFail($id);
        $class->delete();

        return redirect()->route('classes.index')->with('status', trans('status.classes.deleted'));

    }

    public function store(StoreClassRequest $request)
    {

        $data = $request->only([
            'name',
            'description',
        ]);

        Classe::createNew(Auth::user(), $data);


        return redirect()->route('classes.index')->with('status', trans('status.classes.created'));

    }


    public function update(UpdateClassRequest $request, $id)
    {

        $class = Classe::findOrFail($id);

        $data = $request->only([
            'name',
            'description',
            'students',
        ]);

        $class->updateData($data);

        return redirect()->route('classes.index')->with('status', trans('status.classes.updated'));

    }
}