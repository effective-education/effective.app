<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreStudentsIlsRequest;
use App\Models\Student;
use App\Models\StudentIls;
use Illuminate\Http\Response;
use App\Models\Ils;

/**
 * Controller to manage ils results from students.
 *
 * @package App\Http\Controllers
 */
class StudentsIlsController extends Controller
{

    /**
     * Store a new ils results from a student.
     *
     * @param StoreStudentsIlsRequest $request
     * @param $uuid
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreStudentsIlsRequest $request, $uuid)
    {
        $student = Student::findOrFailByUuid($uuid);
        $results = (new Ils())->answer($request->get('answers'));


        if ($student->hasIls()) {

            $student->ils->update($results);
            $status = Response::HTTP_OK;

        } else {

            StudentIls::createNew($student, $results);
            $status = Response::HTTP_CREATED;
        }

        return response()->json(compact('results'), $status);
    }
}
