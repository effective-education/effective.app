<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateStudentRegistrationRequest;
use App\Models\Student;
use App\Models\User;
use App\Notifications\WelcomeToEffective;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

/**
 * Controller for registration of students.
 *
 * @package App\Http\Controllers
 */
class StudentRegistrationController extends Controller
{

    /**
     * Stores a student's registration.
     *
     * @param UpdateStudentRegistrationRequest $request
     * @param $uuid
     * @return JsonResponse
     */
    public function update(UpdateStudentRegistrationRequest $request, $uuid)
    {

        $student = Student::findOrFailByUuid($uuid);
        $id = $student->id;
        $data = $request->only([
            'name', 'email', 'age', 'gender', 'college', 'course', 'handicap_bool', 'musical_instrument',
            'educational_level','terms_of_use', 'password'
        ]);

        //bloco de função para descobrir a idade baseado na data de nascimento
        $bdate = $request['birth_date'];
        list($dia, $mes, $ano) = explode('/', $bdate);
        $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
        $nascimento = mktime(0, 0, 0, $mes, $dia, $ano);
        $idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);

        $data['age'] = $idade;

        if ($data['educational_level'] == 0) {
            $data['educational_level'] = 'elementary';
        } else if ($data['educational_level'] == 1) {
            $data['educational_level'] = 'high';
        } else if ($data['educational_level'] == 2) {
            $data['educational_level'] = 'bachelor';
        } else if ($data['educational_level'] == 3) {
            $data['educational_level'] = 'master';
        } else if ($data['educational_level'] == 4) {
            $data['educational_level'] = 'doctor';
        }

        $student->uuid = uuid();
        $student->name = $data['name'];
        $student->email = $data['email'];
        $student->age = $data['age'];
        $student->gender = $data['gender'];
        $student->college = $data['college'];
        $student->course = $data['course'];
        $student->handicap = $data['handicap_bool'];
        $student->musical_instrument = $data['musical_instrument'];
        $student->educational_level = $data['educational_level'];
        $student->active = true;
        $student->terms = $data['terms_of_use'];

        $student->update();


        $dataUser = $request->only([
            'name',
            'email',
            'password',
            'role',
            'gender',
            'birth_date',
        ]);


        $user = User::createNew($dataUser, $dataUser['role'], $id);
        $user->student_id = $student->id;
        $user->terms = $data['terms_of_use'];
        $user->save();
        //$user->notify(new WelcomeToEffective($user, $dataUser['password']));
        return response()->json('', Response::HTTP_NO_CONTENT);

    }
}
