<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\StoreStudentRequest;
use App\Http\Requests\UpdateStudentRequest;
use App\Models\Student;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use phpDocumentor\Reflection\Types\Null_;

/**
 * Students controller.
 *
 * @package App\Http\Controllers
 */
class StudentsController extends Controller
{

    /**
     * Returns a student with the given email.
     * If none is found, a new one will be created.
     *
     * @param StoreStudentRequest $request
     * @return JsonResponse
     */
    public function store(StoreStudentRequest $request)
    {

        $student = Student::findOrCreateByEmail($request->get('email'));

        $status = $student->needs_to_register ? Response::HTTP_CREATED : Response::HTTP_OK;

        return response()->json(['student' => $student->toArray()], $status);
    }


    public function secStore(StoreStudentRequest $request)
    {

        $student = Student::findOrCreateByEmail($request->get('email'));

        $status = $student->needs_to_register ? Response::HTTP_CREATED : Response::HTTP_OK;

        $res = response()->json(['student' => $student->toArray()], $status);
        return route('student/' . $student->uuid);
    }

    public function changePwd(ChangePasswordRequest $request)
    {
        $password = $request['password'];
        Auth::user()->password = bcrypt($password);
        Auth::user()->save();
        return view('student.sucsses');
    }

    public function pagePwd()
    {
        return view('student.pwdchange');
    }


    public function status($id)
    {

        $user = $this->getEditedStudent($id);

        $user->toggleStatus();

        return redirect()->route('student.index')->with('status', trans('status.student.updated'));

    }


    public function index()
    {
        $students = Student::where('id', '<>', Auth::user()->id)->orderBy('created_at', 'DESC')->get();
        return view('student.index', compact('students'));
    }


    public function create()
    {

        return view('student.create');

    }


    public function update(UpdateStudentRequest $request, $uuid)
    {

        $student = Student::findOrFailByUuid($uuid);
        $id = $student->id;

        $data = $request->only([
            'name', 'email', 'birth_date', 'age', 'gender',
            'college', 'course',
            'col_serie', 'col_name', 'uni_course', 'uni_name',
            'course_default', 'college_default',
            'default_handicap_custom_type', 'default_handicap','default_musical',
            'handicap', 'musical_instrument',
            'educational_level', 'handicap_custom_type'
        ]);


        //pega os valores corretos relacionados ao Educational Level
        if ($data['college_default'] != '') {
            $data['college'] = $data['college_default'];
            $data['course'] = $data['course_default'];
        } elseif ($data['col_name'] != '') {
            $data['college'] = $data['col_name'];
            $data['course'] = $data['col_serie'];
        } elseif ($data['uni_name'] != '') {
            $data['college'] = $data['uni_name'];
            $data['course'] = $data['uni_course'];
        }


        //faz a troca do valor selecionado pelo seu respectivo em String
        if ($data['educational_level'] == 0) {
            $data['educational_level'] = 'elementary';
        } elseif ($data['educational_level'] == 1) {
            $data['educational_level'] = 'high school';
        } elseif ($data['educational_level'] == 2) {
            $data['educational_level'] = 'bachelor';
        } elseif ($data['educational_level'] == 3) {
            $data['educational_level'] = 'master';
        } elseif ($data['educational_level'] == 4) {
            $data['educational_level'] = 'doctor';
        }


        //pega o valor (caso seja outro) do Handicap
        if (empty($data['default_handicap']) || $data['default_handicap'] == null) {
            if ($data['handicap'] == 'other') {
                $data['handicap'] = $data['handicap_custom_type'];
            }
        } else {
            $data['handicap'] = $data['default_handicap'];
            if ($data['handicap'] == 'other') {
                $data['handicap'] = $data['default_handicap_custom_type'];
            }
        }

        //dd($data);

        //pega o valor do Musical instrument
        if (empty($data['musical_instrument']) || $data['musical_instrument'] == null) {

            $data['musical_instrument'] = $request['default_musical'];

        }


        //arruma a data para salvar no banco do USER
        $data_nasc_user = implode('-', array_reverse(explode('/', $data['birth_date'])));


        //bloco de função para descobrir a idade baseado na data de nascimento
        $bdate = $request['birth_date'];
        list($dia, $mes, $ano) = explode('/', $bdate);
        $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
        $nascimento = mktime(0, 0, 0, $mes, $dia, $ano);
        $idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);

        $data['age'] = $idade;


        //atualiza os valores no Student Editado
        $student->name = $data['name'];
        $student->gender = $data['gender'];
        $student->age = $data['age'];
        $student->college = $data['college'];
        $student->course = $data['course'];
        $student->handicap = $data['handicap'];
        $student->musical_instrument = $data['musical_instrument'];
        $student->educational_level = $data['educational_level'];

        $student->update();


        $user = User::findByStudentId($id);

        $user->name = $data['name'];
        $user->gender = $data['gender'];
        $user->birth_date = $data_nasc_user;

        $user->update();

        return redirect()->route('student.index')->with('status', trans('status.student.updated'));

    }


    public function edit($id)
    {

        $student = $this->getEditedStudent($id);

        $user = User::findByStudentId($id);

        $data_nasc = implode('/', array_reverse(explode('-', $user->birth_date)));

        $has_disability = $student->handicap == '' ? false : true;
        $has_musical = $student->musical_instrument == '' ? false : true;



        $handicap = $student->handicap;
        $custom_handicap = null;
        if ($handicap == 'blind') {
            $custom_handicap = false;
            $handicap = 'Cego';
        } elseif ($student->handicap == 'visual') {
            $custom_handicap = false;
            $handicap = 'Deficiência visual';
        } elseif ($student->handicap == 'deaf_mute') {
            $custom_handicap = false;
            $handicap = 'Surdo/Mudo';
        } elseif ($student->handicap == 'auditory') {
            $custom_handicap = false;
            $handicap = 'Deficiência auditiva (perda auditiva)';
        } elseif ($student->handicap == 'motor') {
            $custom_handicap = false;
            $handicap = 'Deficiência motora';
        } elseif ($student->handicap == 'comunication') {
            $custom_handicap = false;
            $handicap = 'Problemas de comunicação (distúrbios da fala e linguagem)';
        } else {
            $custom_handicap = true;
        }

        $musical = $student->musical_instrument;


        return view('student.edit', compact('student', 'data_nasc', 'user',
            'has_disability', 'custom_handicap', 'handicap',
            'has_musical','musical'));

    }


    private function getEditedStudent($id)
    {

        $student = Student::findOrFail($id);

        return $student;

    }


}
