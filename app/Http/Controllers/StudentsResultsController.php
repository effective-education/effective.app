<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreStudentResultRequest;
use App\Models\Average_Students_Emotions_Lesson;
use App\Models\Lesson;
use App\Models\Student;
use App\Models\StudentIls;
use App\Models\StudentResult;
use App\Models\User;
use Illuminate\Http\Response;
use App\Models\Quiz;

/**
 * Controller to manage results of lessons.
 *
 * @package App\Http\Controllers
 */
class StudentsResultsController extends Controller
{


    public static function medias_aluno_aula ($lesson_id, $student_id){
        $results = StudentResult::findResults($lesson_id, $student_id);
        $frames = $results->frames;

        $cont = 0;
        $joy = array();
        $fear = array();
        $anger = array();
        $disgust = array();
        $sadness = array();
        $valence = array();
        $contempt = array();
        $surprise = array();
        $engagement = array();
        //separa as emoções - todas as emoções do aluno durante uma aula em formato de array individual
        foreach ($frames as $frame) {
            foreach ($frame['emotions'] as $emotions) {
                if ($cont == 0) {
                    $joy[] = $emotions;
                } elseif ($cont == 1) {
                    $fear[] = $emotions;
                } elseif ($cont == 2) {
                    $anger[] = $emotions;
                } elseif ($cont == 3) {
                    $disgust[] = $emotions;
                } elseif ($cont == 4) {
                    $sadness[] = $emotions;
                } elseif ($cont == 5) {
                    $valence[] = $emotions;
                } elseif ($cont == 6) {
                    $contempt[] = $emotions;
                } elseif ($cont == 7) {
                    $surprise[] = $emotions;
                } elseif ($cont == 8) {
                    $engagement[] = $emotions;
                }
                if ($cont == 8) {
                    $cont = 0;
                } else {
                    $cont++;
                }
            }
        }

        //calcula a média de cada emoção daquele aluno naquela aula
        $med_Joy = 0;
        $cont_Joy = 0;
        foreach ($joy as $j) {
            $med_Joy = $med_Joy + $j;
            $cont_Joy++;
        }
        $med_Joy = $med_Joy / $cont_Joy;


        $med_Fear = 0;
        $cont_Fear = 0;
        foreach ($fear as $j) {
            $med_Fear = $med_Fear + $j;
            $cont_Fear++;
        }
        $med_Fear = $med_Fear / $cont_Fear;


        $med_Anger = 0;
        $cont_Anger = 0;
        foreach ($anger as $j) {
            $med_Anger = $med_Anger + $j;
            $cont_Anger++;
        }
        $med_Anger = $med_Anger / $cont_Anger;


        $med_Disgust = 0;
        $cont_Disgust = 0;
        foreach ($disgust as $j) {
            $med_Disgust = $med_Disgust + $j;
            $cont_Disgust++;
        }
        $med_Disgust = $med_Disgust / $cont_Disgust;


        $med_Sadness = 0;
        $cont_Sadness = 0;
        foreach ($sadness as $j) {
            $med_Sadness = $med_Sadness + $j;
            $cont_Sadness++;
        }
        $med_Sadness = $med_Sadness / $cont_Sadness;


        $med_Contempt = 0;
        $cont_Contempt = 0;
        foreach ($contempt as $j) {
            $med_Contempt = $med_Contempt + $j;
            $cont_Contempt++;
        }
        $med_Contempt = $med_Contempt / $cont_Contempt;


        $med_Surprise = 0;
        $cont_Surprise = 0;
        foreach ($surprise as $j) {
            $med_Surprise = $med_Surprise + $j;
            $cont_Surprise++;
        }
        $med_Surprise = $med_Surprise / $cont_Surprise;

        //Formata os valores das médias para Float
        $med_Joy = number_format($med_Joy, 3, '.', '');
        $med_Anger = number_format($med_Anger, 3, '.', '');
        $med_Contempt = number_format($med_Contempt, 3, '.', '');
        $med_Disgust = number_format($med_Disgust, 3, '.', '');
        $med_Fear = number_format($med_Fear, 3, '.', '');
        $med_Sadness = number_format($med_Sadness, 3, '.', '');
        $med_Surprise = number_format($med_Surprise, 3, '.', '');
    
        $medias = array(
            "joy" => $med_Joy,
            "rage" => $med_Anger,
            "contempt" => $med_Contempt,
            "disgust" =>  $med_Disgust,
            "fear" => $med_Fear,
            "sadness" => $med_Sadness,
            "surprise" => $med_Surprise,
        );

        return $medias;
    }


    /**
     * Stores a new student lesson results.
     *
     * @param StoreStudentResultRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreStudentResultRequest $request)
    {

        $lesson = Lesson::findOrFailByUuid($request->get('lesson_uuid'));
        $student = Student::findOrFailByUuid($request->get('student_uuid'));

        $results = (new Quiz($lesson))->answer($request->get('questions'));

        $result = new StudentResult();
        $result = $result->createNew($student, $lesson, $results['questions'], $request->get('frames'));

        //calcula a média e salva/atualiza no banco
        Average_Students_Emotions_LessonController::store($student->id, $lesson->id);

        return response()->json([
            'uuid' => $result->uuid,
            'right_percentage' => $results['right_percentage']
        ], Response::HTTP_CREATED);

    }
}
