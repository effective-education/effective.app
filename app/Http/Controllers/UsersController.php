<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\User;
use App\Notifications\WelcomeToEffective;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

/**
 * Class UsersController
 * @package App\Http\Controllers
 */
class UsersController extends Controller
{

    /**
     * UsersController constructor.
     */
    public function __construct()
    {

        $this->middleware('can:manage,' . User::class);

    }

    /**
     * Show all users registered in application.
     *
     * @return Factory|View
     */
    public function index()
    {

        $users = $this->getAllUsersExceptLogedUser();

        return view('users.index', compact('users'));

    }

    /**
     * Show the create user form.
     *
     * @return View
     */
    public function create()
    {

        return view('users.create');

    }

    /**
     * Store a user in database and fires a notification.
     *
     * @param StoreUserRequest $request
     * @return RedirectResponse
     */
    public function store(StoreUserRequest $request)
    {

        $data = $request->only([
            'name',
            'email',
            'password',
            'role',
            'gender',
            'birth_date'
        ]);

        $student_id = '';

        $user = User::createNew($data, $data['role'], $student_id);
        //$user->notify(new WelcomeToEffective($user, $data['password']));

        return redirect()->route('users.index')->with('status', trans('status.user.created'));

    }

    /**
     * Show the edit user form.
     *
     * @param $id
     * @return View
     */
    public function edit($id)
    {

        $user = $this->getEditedUser($id);

        return view('users.edit', compact('user'));

    }

    /**
     * Update an user data.
     *
     * @param UpdateUserRequest $request
     * @param $id
     * @return RedirectResponse
     */
    public function update(UpdateUserRequest $request, $id)
    {

        $user = $this->getEditedUser($id);

        $role = $request->get('role');

        $user->setRole($role);

        return redirect()->route('users.index')->with('status', trans('status.user.updated'));

    }

    /**
     * Updates an user's status.
     *
     * @param $id
     * @return RedirectResponse
     */
    public function status($id)
    {

        $user = $this->getEditedUser($id);

        $user->toggleStatus();

        return redirect()->route('users.index')->with('status', trans('status.user.updated'));

    }

    /**
     * Get user being edited and checks if it is not the loged user.
     *
     * @param $id
     * @return User
     */
    private function getEditedUser($id)
    {

        $user = User::findOrFail($id);

        $this->authorize('update', [$user]);

        return $user;

    }

    /**
     * Get all users except the loged user.
     *
     * @return Collection
     */
    private function getAllUsersExceptLogedUser()
    {

        return User::where('id', '<>', Auth::user()->id)->orderBy('created_at', 'DESC')->get();

    }



}