<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreClass_StudentRequest;

use App\Models\Classe;
use App\Models\ClasseStudent;
use App\Models\Student;

use App\Policies\TaskPolicy;
use Illuminate\Support\Facades\Auth;


/**
 * Class ClassesController
 * @package App\Http\Controllers
 */
class Class_StudentController extends Controller
{


    public function create($id)
    {
        if (Auth::check()) {
            if (Auth::user()->isAdmin() || Auth::user()->isCollaborator()) {
                $students = Student::all();
                $std = Classe::find($id)->students()->orderBy('name')->paginate(5);

                foreach ($std as $st) {
                    $students = $students->except($st->id);
                }


                $class = $this->getEditedClass($id);

                return view('class_student.add', compact('students', 'class', 'std'));
            }
        }
    }


    public function destroy($classe_id, $student_id)
    {
        $classes = ClasseStudent::where('classe_id', '=', $classe_id);
        $classe_student = $classes->where('student_id', '=', $student_id);

        $classe_student->delete();

        return redirect()->route('classes.edit', [$classe_id])->with('status', trans('status.class_student.deleted'));
    }


    public function store(StoreClass_StudentRequest $request)
    {

        $data = $request->only([

            'classe_id',
            'student_id'

        ]);


        ClasseStudent::createNew($data);

        return redirect()->route('add_user', ['id' => $data['classe_id']])->with('status', trans('status.class_student.add'));

    }


    private function getEditedClass($id)
    {

        $class = Classe::findOrFail($id);
        return $class;

    }
}