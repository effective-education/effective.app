<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Student;
use App\Models\User;

class GenerateUsersFromStudentsController extends Controller
{
    public static function generate(){

        $userList = User::all()->where('student_id', '!=', null);
        foreach ($userList as $user){
            $studentList  = Student::all()->where('id', '!=', $user->student_id);
        }
        $pass = '123';

        foreach ($studentList as $student) {
            $newUser = new User();
            $newUser->name = $student->name;
            $newUser->email = $student->email;
            $newUser->password = bcrypt($pass);
            $newUser->role = 'student';
            $newUser->active = true;
            $newUser->student_id = $student->id;

            $newUser->save();

            $resp[] = $newUser;
        }
        return view('tests.onto', compact('resp'));
    }
}
