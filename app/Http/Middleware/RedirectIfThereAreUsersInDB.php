<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;

class RedirectIfThereAreUsersInDB
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (User::existsAtLeastOne())
            return redirect()->route('pages.index');

        return $next($request);

    }

}