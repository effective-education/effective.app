<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsActive
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(!Auth::user()->isActive() && Auth::user()->isStudent()){

            Auth::logout();

            return redirect()->route('login')->withErrors(trans('status.user.blocked'));

        }elseif (! Auth::user()->isActive()) {

            Auth::logout();

            return redirect()->route('login')->withErrors(trans('status.user.blocked'));

        }

        return $next($request);

    }

}