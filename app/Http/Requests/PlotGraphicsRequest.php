<?php

namespace App\Http\Requests;


class PlotGraphicsRequest extends LessonRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'lesson_id' => 'required',
            'type' => 'required'

        ];
    }


}