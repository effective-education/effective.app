<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreTestResultsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'student' => 'required|exists:students,id',
            'lesson' => 'required|exists:lessons,id',
            'questions' => 'required|array|min:1',
            'frames' => 'required|array|min:1',

        ];
    }
}
