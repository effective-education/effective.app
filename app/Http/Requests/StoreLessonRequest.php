<?php

namespace App\Http\Requests;

use Illuminate\Validation\Validator;

class StoreLessonRequest extends LessonRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return $this->baseRules();

    }

    protected function withValidator(Validator $validator)
    {

        $validator->after(function (Validator $validator) {

            $this->isSlugUnique($validator);
            $this->allQuestionsHasAtLeastOneRight($validator);

        });

    }

}