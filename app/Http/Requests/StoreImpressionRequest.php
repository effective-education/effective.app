<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Validates the request to store a student impression about a lesson.
 *
 * @package App\Http\Requests
 */
class StoreImpressionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'student_result_uuid' => 'required|exists:students_results,uuid',
            'answers' => 'required|array|size:17',
            'answers.surprised' => 'required|integer|between:1,5',
            'answers.proud' => 'required|integer|between:1,5',
            'answers.sad' => 'required|integer|between:1,5',
            'answers.excited' => 'required|integer|between:1,5',
            'answers.interesting' => 'required|integer|between:1,5',
            'answers.devalued' => 'required|integer|between:1,5',
            'answers.ashamed' => 'required|integer|between:1,5',
            'answers.happy' => 'required|integer|between:1,5',
            'answers.sorry' => 'required|integer|between:1,5',
            'answers.expectancy' => 'required|integer|between:1,5',
            'answers.fear' => 'required|integer|between:1,5',
            'answers.quiet' => 'required|integer|between:1,5',
            'answers.aversion' => 'required|integer|between:1,5',
            'answers.satisfied' => 'required|integer|between:1,5',
            'answers.angry' => 'required|integer|between:1,5',
            'answers.jealous' => 'required|integer|between:1,5',
            'answers.indifferent' => 'required|integer|between:1,5',
        ];
    }
}
