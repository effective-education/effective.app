<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Stores the registration data of a student.
 *
 * @package App\Http\Requests
 */
class UpdateStudentRegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'name' =>'required|max:255',
            'email' => ['required', 'email', Rule::unique('students')->ignore($this->route('uuid'), 'uuid')],
            'birth_date' => 'required|date_format:d/m/Y',
            'gender' => 'required|in:M,F',
            'educational_level'=>'required|max:255',
            'college' => 'required|max:255',
            'course' => 'required|max:255',
            'handicap' => 'nullable|string|max:255',
            'musical_instrument' => 'nullable|string|max:255',
            'password'=>'required|max:20',
            'terms_of_use' =>'required'
        ];
    }

}
