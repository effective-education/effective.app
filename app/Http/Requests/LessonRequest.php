<?php

namespace App\Http\Requests;

use App\Models\Lesson;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Validator;

class LessonRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

        return Auth::check();

    }

    protected function videoTypes()
    {

        return Lesson::VIDEO_TYPE_YOUTUBE . ',' . Lesson::VIDEO_TYPE_FILE;

    }

    protected function baseRules()
    {

        $videoTypes = $this->videoTypes();

        return [

            'knowledge_area' => 'required|exists:knowledge_areas,id',
            'title' => 'required|max:255',
            'description' => 'required',
            'outro' => 'required',
            'video_type' => 'required|in:' . $videoTypes,
            'video_id' => 'nullable|required_if:video_type,' . Lesson::VIDEO_TYPE_YOUTUBE,
            'video_file' => 'nullable|required_if:video_type,' . Lesson::VIDEO_TYPE_FILE . '|file|mimes:mp4,mp4s,m4a,mp4a,mp4v,mpg4',

            'questions' => 'required',
            'questions.*.title' => 'required',
            'questions.*.timestamp' => 'required|date_format:H:i:s',
            'questions.*.options' => 'required',
            'questions.*.options.*.text' => 'required',

            'contents' => 'required',
            'contents.*.start_time' => 'required|date_format:H:i:s',
            'contents.*.end_time' => 'required|date_format:H:i:s',
            'contents.*.tags' => 'required',

            'competences' => 'max:255',
            'competences.*.description' => 'required',
            'competences.*.evidences' => 'required',
            'competences.*.evidences.*.description' => 'required',

            'abilities' => 'max:255',
            'abilities.*.description' => 'required',
            'abilities.*.evidences' => 'required',
            'abilities.*.evidences.*.description' => 'required',

            'knowledge_s' => 'max:255',
            'knowledge_s.*.description' => 'required',
            'knowledge_s.*.evidences' => 'required',
            'knowledge_s.*.evidences.*.description' => 'required',


        ];

    }

    protected function isSlugUnique(Validator $validator)
    {

        $slug = slugify($validator->getData()['title']);

        if (Lesson::where('slug', $slug)->first())
            $validator->errors()->add('title', trans('validation.unique', ['attribute' => trans('strings.title')]));

    }

    protected function allQuestionsHasAtLeastOneRight(Validator $validator)
    {

        $questions = $validator->getData()['questions'] ?? [];

        foreach ($questions as $question) {

            $atLeastOneIsRight = false;

            $options = $question['options'] ?? [];

            foreach ($options as $option) {

                if (isset($option['is_right'])) {

                    $atLeastOneIsRight = true;
                    break;

                }

            }

            if (!$atLeastOneIsRight) {

                $validator->errors()->add('questions', trans('validation.at_least_one_right'));
                break;

            }

        }

    }

}