<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Request validation to store the ils answers for a student.
 *
 * @package App\Http\Requests
 */
class StoreStudentsIlsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'answers' => 'required|array|size:44',
            'answers.*.number' => 'required|integer',
            'answers.*.answer' => 'required|in:a,b',

        ];
    }

    /**
     * @return array
     */
    public function messages()
    {

        return [

            '*' => trans('strings.ils_result_error'),

        ];

    }

}
