<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Validates the request to verify if a student gave his impressions about a lesson.
 *
 * @package App\Http\Requests
 */
class VerifyImpressionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lesson_uuid' => 'required|exists:lessons,uuid',
            'student_uuid' => 'required|exists:students,uuid'
        ];
    }
}
