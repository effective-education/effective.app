<?php

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateStudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'name' =>'required|max:255',
            'birth_date' => 'max:10|required|date_format:d/m/Y',
            'gender' => 'required|in:M,F',
            'educational_level' => 'required|max:255',
            'college' => 'nullable|max:255',
            'college_default' => 'nullable|max:255',
            'col_name' => 'nullable|max:255',
            'uni_name' => 'nullable|max:255',
            'course' => 'nullable|max:255',
            'course_default' => 'nullable|max:255',
            'col_serie' => 'nullable|max:255',
            'uni_course' => 'nullable|max:255',
            'handicap' => 'nullable|string|max:255',
            'default_handicap_custom_type' => 'nullable|string|max:255',
            'handicap_custom_type' => 'nullable|string|max:255',
            'musical_instrument' => 'nullable|string|max:255'

        ];
    }
}
