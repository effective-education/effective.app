<?php

namespace App\Http\Requests;

use Illuminate\Validation\Validator;

class UpdateLessonRequest extends LessonRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = $this->baseRules();

        $rules['video_type'] = 'required_if:video_was_changed,true|in:' . $this->videoTypes();
        $rules['video_file'] = 'nullable|file|mimes:mp4,mp4s,m4a,mp4a,mp4v,mpg4';

        $rules['questions.*.title'] = 'required_if:questions_were_changed,true';
        $rules['questions.*.timestamp'] = 'required_if:questions_were_changed,true|date_format:H:i:s';
        $rules['questions.*.options'] = 'required_if:questions_were_changed,true';
        $rules['questions.*.options.*.text'] = 'required_if:questions_were_changed,true';


        // Editar Abilidades / Competências / Conhecimentos

        $rules['competences'] = 'required_if:competencies_were_changed,true';
        $rules['competences.*.description'] = 'required_if:competencies_were_changed,true';
        $rules['competences.*.evidences'] = 'required_if:competencies_were_changed,true';
        $rules['competences.*.evidences.*.description'] = 'required_if:competencies_were_changed,true';

        $rules['abilities'] = 'required_if:abilities_were_changed,true';
        $rules['abilities.*.description'] = 'required_if:abilities_were_changed,true';
        $rules['abilities.*.evidences'] = 'required_if:abilities_were_changed,true';
        $rules['abilities.*.evidences.*.description'] = 'required_if:abilities_were_changed,true';

        $rules['knowledge_s'] = 'required_if:knowledge_s_were_changed,true';
        $rules['knowledge_s.*.description'] = 'required_if:knowledge_s_were_changed,true';
        $rules['knowledge_s.*.evidences'] = 'required_if:knowledge_s_were_changed,true';
        $rules['knowledge_s.*.evidences.*.description'] = 'required_if:knowledge_s_were_changed,true';

        // -  - - - - - -


        $rules['contents'] = 'required_if:contents_were_changed,true';
        $rules['contents.*.start_time'] = 'required_if:contents_were_changed,true|date_format:H:i:s';
        $rules['contents.*.end_time'] = 'required_if:contents_were_changed,true|date_format:H:i:s';
        $rules['contents.*.tags'] = 'required_if:contents_were_changed,true';

        return $rules;

    }

    protected function withValidator(Validator $validator)
    {

        $validator->after(function (Validator $validator) {

            $questionWereChanged = filter_var($validator->getData()['questions_were_changed'], FILTER_VALIDATE_BOOLEAN);

            if ($questionWereChanged)
                $this->allQuestionsHasAtLeastOneRight($validator);

        });

    }

}