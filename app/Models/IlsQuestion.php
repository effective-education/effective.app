<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IlsQuestion extends Model
{

    const SCALE_ACT_REF = 'act_ref';
    const SCALE_SEN_INT = 'sen_int';
    const SCALE_VIS_VER = 'vis_ver';
    const SCALE_SEQ_GLO = 'seq_glo';

    protected $appends = ['text', 'answer'];
    protected $hidden = ['id', 'created_at', 'updated_at'];

    protected $casts = [

        'id' => 'integer',
        'number' => 'integer',

    ];

    public static function getQuiz()
    {

        return IlsQuestion::with('options.question')->orderBy('number', 'ASC')->get();

    }

    public function getTextAttribute()
    {

        return trans('ils.' . $this->number . '.text');

    }

    public function getAnswerAttribute()
    {

        return null;

    }

    public function options()
    {

        return $this->hasMany(IlsOption::class, 'ils_question_id');

    }

}