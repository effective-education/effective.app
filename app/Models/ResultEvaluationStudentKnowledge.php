<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResultEvaluationStudentKnowledge extends Model
{

    /**
     * @var string
     */
    protected $table = 'result_evaluation_student_knowledge';


    public static function createNew($evidence_knowledge_id, $student_id, $lesson_id, $result)
    {

        $old_result = ResultEvaluationStudentKnowledge::all()
            ->where('evidence_knowledge_id', $evidence_knowledge_id)
            ->where('student_id', $student_id)
            ->where('lesson_id', $lesson_id)
            ->first();

        if ($old_result !== null){

            $old_result->result = $result;
            return $old_result->update();

        }else {

            $result_evaluation = new ResultEvaluationStudentKnowledge();

            $result_evaluation->evidence_knowledge_id = $evidence_knowledge_id;
            $result_evaluation->student_id = $student_id;
            $result_evaluation->lesson_id = $lesson_id;
            $result_evaluation->result = $result;

            return $result_evaluation->save();
        }
    }


    public static function deleteByLesson($lesson_id){

        $result_knowledge_s = ResultEvaluationStudentKnowledge::all()->where('lesson_id', $lesson_id);

        if (! $result_knowledge_s->isEmpty()){
            foreach ($result_knowledge_s as $result_knowledge){
                $result_knowledge->delete();
            }
        }

    }

    public static function isNew($lesson_id, $student_id){

        $x = ResultEvaluationStudentKnowledge::all()
            ->where('student_id', $student_id)
            ->where('lesson_id', $lesson_id)->first();
        return $x == null;
    }


    public function evidence_knowledge()
    {

        return $this->belongsTo(EvidenceKnowledge::class);

    }

}