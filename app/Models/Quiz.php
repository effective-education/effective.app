<?php

namespace App\Models;

/**
 * Quiz of questions from a lesson.
 *
 * @package App\Models
 */
class Quiz
{

    /**
     * @var Lesson
     */
    protected $lesson;

    /**
     * @var int
     */
    protected $rightOnes = 0;

    /**
     * @param Lesson $lesson
     */
    public function __construct(Lesson $lesson)
    {
        $this->lesson = $lesson;
    }

    /**
     * Answer the quiz
     *
     * @param $questionsWithAnswers
     * @return array
     */
    public function answer($questionsWithAnswers)
    {

        $questionsWithAnswers = collect($questionsWithAnswers)->map(function ($questionWithAnswer) {

            $isRight = $this->lesson->questions->where('id', $questionWithAnswer['id'])->first()->isRight($questionWithAnswer['answers']);

            $questionWithAnswer['is_right'] = $isRight;

            if ($isRight) $this->rightOnes++;

            return $questionWithAnswer;

        })->all();

        return [

            'questions' => $questionsWithAnswers,
            'total_questions' => count($questionsWithAnswers),
            'right_answers' => $this->rightOnes,
            'right_percentage' => intval(($this->rightOnes * 100) / count($questionsWithAnswers)),
            //'right_percentage' => '5',

        ];

    }

}
