<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * A lesson's Knowledge_s.
 *
 * @package App\Models
 */
class   Knowledge extends Model
{
    /**
     * @var string
     */
    protected $table = 'knowledge';

    /**
     * @var array
     */
    protected $guarded = [];


    /**
     * @var array
     */
    protected $hidden = ['lesson_id', 'created_at', 'updated_at'];

    /**
     * @var array
     */
    protected $casts = [

        'id' => 'integer',
        'lesson_id' => 'integer',

    ];

    /**
     * Creates a new Knowledge.
     *
     * @param Lesson $lesson
     * @param $data
     * @return Competence
     */
    public static function createNew(Lesson $lesson, $data)
    {

        $knowledge = static::create([

            'lesson_id' => $lesson->id,
            'description' => $data['description']

        ]);

        $evidence_knowledge_s = $data['evidences'];

        foreach ($evidence_knowledge_s as $evidence_knowledge)
            EvidenceKnowledge::createNew($knowledge, $evidence_knowledge, $lesson->id);

        return $knowledge;

    }


    public static function deleteByLesson($lesson_id){

        $knowledge_s = Knowledge::all()->where('lesson_id', $lesson_id);

        if ($knowledge_s !== null){
            foreach ($knowledge_s as $knowledge){
                $knowledge->delete();
            }
        }

    }


    /**
     * Gets the related lesson.
     *
     * @return BelongsTo
     */
    public function lesson()
    {

        return $this->belongsTo(Lesson::class);

    }

    /**
     * Get the options for the question.
     *
     * @return HasMany
     */
    public function evidences()
    {

        return $this->hasMany(EvidenceKnowledge::class);

    }


}
