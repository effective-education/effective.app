<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * A impression that a student had for a lesson.
 *
 * @package App\Models
 */
class Impression extends Model
{

    const VERY_WEAK = 1;
    const WEAK = 2;
    const NOR_WEAK_OR_STRONG = 3;
    const STRONG = 4;
    const VERY_STRONG = 5;

    /**
     * @var array
     */
    protected $fillable = [

        'student_result_id',
        'surprised',
        'proud',
        'sad',
        'excited',
        'interesting',
        'devalued',
        'ashamed',
        'happy',
        'sorry',
        'expectancy',
        'fear',
        'quiet',
        'aversion',
        'satisfied',
        'angry',
        'jealous',
        'indifferent',

    ];

    /**
     * Checks if the student gave an impression to the given lesson.
     *
     * @param $lessonUuid
     * @param $studentUuid
     * @return bool
     */
    public static function studentGaveImpressionForLesson($studentUuid, $lessonUuid)
    {

        return Impression::join('students', 'students.id', '=', 'impressions.student_id')
                ->join('lessons', 'lessons.id', '=', 'impressions.lesson_id')
                ->where([
                    ['lessons.uuid', $lessonUuid],
                    ['students.uuid', $studentUuid]
                ])->first() !== null;

    }

    /**
     * Gets a translated impression.
     *
     * @param $type
     * @return string
     */
    public function getImpression($type)
    {

        return trans("impressions.degrees.{$this->{$type}}");

    }

    /**
     * Gets the result associated with this impression.
     *
     * @return BelongsTo
     */
    public function result()
    {

        return $this->belongsTo(StudentResult::class, 'student_result_id');

    }

    public static function deleteByStudentResult($student_result_id){

        $impressions = Impression::all()->where('student_result_id', $student_result_id);

        if (! $impressions->isEmpty()){
            foreach ($impressions as $impression){

                $impression->delete();
            }
        }

    }


}
