<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class EvidenceCompetence extends Model
{

    /**
     * @var string
     */
    protected $table = 'evidence_competence';


    protected $guarded = [];

    protected $hidden = ['competence_id', 'created_at', 'updated_at'];

    protected $casts = [

        'id' => 'integer',
        'competence_id' => 'integer',

    ];

    public static function createNew(Competence $competence, array $data, $lesson_id)
    {

        return static::create([

            'competence_id' => $competence->id,
            'description' => $data['description'],
            'lesson_id' => $lesson_id,

        ]);

    }

    public static function deleteByLesson($lesson_id){

        $evidence_competences = EvidenceCompetence::all()->where('lesson_id', $lesson_id);

        if ($evidence_competences !== null){
            foreach ($evidence_competences as $evidence_competence){
                $evidence_competence->delete();
            }
        }

    }


    public function competence()
    {

        return $this->belongsTo(Competence::class);

    }




    /**
     * Get the results for the evidences.
     *
     * @return HasMany
     */
    public function results()
    {

        return $this->hasMany(ResultEvaluationStudentCompetence::class);

    }


}