<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResultEvaluationStudentAbility extends Model
{

    /**
     * @var string
     */
    protected $table = 'result_evaluation_student_ability';


    public static function createNew($evidence_ability_id, $student_id, $lesson_id, $result)
    {

        $old_result = ResultEvaluationStudentAbility::all()
            ->where('evidence_ability_id', $evidence_ability_id)
        ->where('student_id', $student_id)
        ->where('lesson_id', $lesson_id)
        ->first();

        if ($old_result !== null){
            $old_result->result = $result;
            return $old_result->update();
        }else{
            $result_evaluation = new ResultEvaluationStudentAbility();

            $result_evaluation->evidence_ability_id = $evidence_ability_id;
            $result_evaluation->student_id = $student_id;
            $result_evaluation->lesson_id = $lesson_id;
            $result_evaluation->result = $result;

            return $result_evaluation->save();
        }

    }


    public static function deleteByLesson($lesson_id){

        $result_abilities = ResultEvaluationStudentAbility::all()->where('lesson_id', $lesson_id);

        if (! $result_abilities->isEmpty()){
            foreach ($result_abilities as $result_ability){
                $result_ability->delete();
            }
        }

    }


    public static function isNew($lesson_id, $student_id){

        $x = ResultEvaluationStudentAbility::all()
            ->where('student_id', $student_id)
            ->where('lesson_id', $lesson_id)->first();
        return $x == null;
    }



    public function evidence_ability()
    {

        return $this->belongsTo(EvidenceAbility::class);

    }

}