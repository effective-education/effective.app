<?php

namespace App\Models;

use App\Notifications\ResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Sinergi\BrowserDetector\Browser;

/**
 * Class User
 * @package App\Models
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * User's roles.
     */
    const ADMIN = 'admin';
    const COLLABORATOR = 'collaborator';
    const STUDENT = 'student';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role', 'active', 'gender', 'birth_date', 'terms'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [

        'id' => 'integer',
        'active' => 'boolean',
        'terms' => 'boolean',

    ];

    /**
     * Send the password reset notification.
     *
     * @param  string $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    /**
     * Create a new user instance and store it in the db.
     *
     * @param $data
     * @param $role
     * @return mixed
     */
    public static function createNew($data, $role, $student_id)
    {

        if ($student_id !== '') {
            $bdate = implode('-', array_reverse(explode('/', $data['birth_date'])));

            return static::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'role' => $data['role'],
                'active' => 1,
                'gender' => $data['gender'],
                'birth_date' => $bdate,
                'terms' => 1,
                'student_id' => $student_id,
            ]);
        } else{

            return static::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'role' => $role,
                'active' => 1,
                'terms' => 1,
            ]);

        }
    }

    //public function setStudentId(){}

    /**
     * Check if there is at least one user in the database.
     *
     * @return bool
     */
    public static function existsAtLeastOne()
    {

        return static::count() > 0;

    }

    /**
     * Checks if the user is an admin.
     *
     * @return bool
     */
    public function isAdmin()
    {

        return $this->role == static::ADMIN;

    }

    /**
     * Checks if the user is a collaborator.
     *
     * @return bool
     */
    public function isCollaborator()
    {

        return $this->role == static::COLLABORATOR;

    }

    /**
     * Checks if the user is a student.
     *
     * @return bool
     */
    public function isStudent()
    {

        return $this->role == static::STUDENT;

    }

    /**
     * Sets the user role.
     *
     * @param $role
     * @return bool
     */
    public function setRole($role)
    {

        $this->role = $role;

        return $this->save();

    }

    /**
     * Toggle the current user's status.
     *
     * @return bool
     */
    public function toggleStatus()
    {

        $this->active = !$this->isActive();

        return $this->save();

    }

    /**
     * Checks if the user is active or not.
     *
     * @return bool
     */
    public function isActive()
    {

        return $this->active == true;

    }

    public function findByEmail($email){
        return static::where('email', $email)->firstOrFail();
    }

    public function student(){
        return $this->belongsTo(Student::class);
    }

    public static function findByStudentId($id){
        return User::all()->where('student_id','=',$id)->first();
    }


    public static function browser_is_ok()
    {
        $browser = new Browser();
        $browser_is_ok = true;
        if ($browser->getName() === Browser::FIREFOX && $browser->getVersion() >= '64') {
            $browser_is_ok = false;
        }
        return $browser_is_ok;
    }


    public static function type_and_version(){
        $browser = new Browser();
        if ($browser->getName() === Browser::CHROME){
            return 'Você está utilizando a versão '. $browser->getVersion() . '. Utilize uma versão inferior a 71.0.  Estamos trabalhando para garantir o funcionamento na ultima versão do ' . $browser->getName() . ' :)';
        }elseif ($browser->getName() === Browser::FIREFOX){
            return 'Você está utilizando a versão '. $browser->getVersion() . '. Utilize uma versão inferior a 64.0. Estamos trabalhando para garantir o funcionamento na ultima versão do ' . $browser->getName() . ' :)';
        }else{
            return 'Utilize uma Versão do Chrome ou FireFox';
        }
    }


}