<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KnowledgeArea extends Model
{

    protected $guarded = [];

    protected $casts = [

        'id' => 'integer',
        'parent_id' => 'integer',

    ];

    public static function parentsWithChildren()
    {

        return static::with('children')->whereNull('parent_id')->get();

    }

    /**
     * Search a knowledge area by name in the translations strings.
     *
     * @param $name
     * @return array|false
     */
    public static function searchByName($name)
    {

        if(empty($name))
            return false;

        $found = collect(trans('knowledge-areas'))->filter(function ($value) use ($name) {

            return strpos(normToSearch($value), normToSearch($name)) !== false;

        });

        return $found->keys()->all();

    }

    public function getTitleAttribute()
    {

        return trans('knowledge-areas.' . $this->code);

    }

    public function children()
    {

        return $this->hasMany(KnowledgeArea::class, 'parent_id', 'id');

    }

}