<?php

namespace App\Models;

use App\Models\StudentResult;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Lesson extends Model
{

    use SoftDeletes;

    const VIDEO_TYPE_FILE = 'file';
    const VIDEO_TYPE_YOUTUBE = 'youtube';

    protected $guarded = [];
    protected $dates = ['deleted_at'];

    protected $hidden = [
        'created_at', 'deleted_at', 'id', 'knowledge_area_id', 'updated_at', 'user_editor_id', 'user_id', 'video_type',
    ];

    protected $casts = [

        'id' => 'integer',
        'user_id' => 'integer',
        'user_editor_id' => 'integer',
        'knowledge_area_id' => 'integer'

    ];

    /**
     * Search for lessons.
     *
     * @param string $search
     * @return LengthAwarePaginator
     */

    public static function search($search = '')
    {

        $codes = KnowledgeArea::searchByName($search) ?: [$search];

        return Lesson::with('knowledgeArea', 'creator')
            ->select('lessons.*')
            ->join('users', 'lessons.user_id', '=', 'users.id')
            ->join('knowledge_areas', 'lessons.knowledge_area_id', '=', 'knowledge_areas.id')
            ->where(DB::raw('lower(lessons.title)'), 'like', DB::raw("lower('%{$search}%')"))
            ->orWhere(DB::raw('lower(users.name)'), 'like', DB::raw("lower('%{$search}%')"))
            ->orWhereIn('knowledge_areas.code', $codes)
            ->latest()
            ->paginate();

    }

        public static function student_lessons($search = '')
    {

        $user = Auth::user();
//        $search = 'Theresa';

        $codes = KnowledgeArea::searchByName($search) ?: [$search];
        $cs = ClasseStudent::where('student_id', '=', $user->student_id)->pluck('classe_id');
        $cl = ClasseLesson::whereIn('classe_id', $cs)->pluck('lesson_id');

        return Lesson::with('knowledgeArea', 'creator', 'classes')
            ->select('lessons.*')
            ->join('users', 'lessons.user_id', '=', 'users.id')
            ->join('knowledge_areas', 'lessons.knowledge_area_id', '=', 'knowledge_areas.id')
            ->join('classe_lesson', 'lessons.id', '=','classe_lesson.lesson_id')
            ->join('classes','classe_lesson.classe_id', '=', 'classes.id')
            ->WhereIn('classe_lesson.classe_id', $cs)
            ->whereIn('lessons.id',$cl)
            ->where(DB::raw('lower(lessons.title)'), 'like', DB::raw("lower('%{$search}%')"))
//            ->where(DB::raw('lower(users.name)'), 'like', DB::raw("lower('%{$search}%')"))
//            ->orWhereIn('knowledge_areas.code', $codes)
            ->latest()
            ->paginate();

    }

    public static function createNew($user, array $data)
    {

        $lesson = static::create([

            'uuid' => uuid(),
            'user_id' => $user->id,
            'user_editor_id' => $user->id,
            'knowledge_area_id' => $data['knowledge_area'],
            'title' => $data['title'],
            'slug' => $data['slug'],
            'description' => $data['description'],
            'outro' => $data['outro'],
            'video_uri' => $data['video_uri'],
            'video_type' => $data['video_type'],

        ]);

        $questions = $data['questions'];

        foreach ($questions as $question)
            Question::createNew($lesson, $question);

        $contents = $data['contents'];

        foreach ($contents as $content)
            Content::createNew($lesson, $content);

        LessonMetric::create([
            'lesson_id' => $lesson->id,
        ]);

        if (isset($data['competences'])) {
            $competences = $data['competences'];
            foreach ($competences as $competence)
                Competence::createNew($lesson, $competence);
        }


        if (isset($data['abilities'])) {
            $abilities = $data['abilities'];
            foreach ($abilities as $ability)
                Ability::createNew($lesson, $ability);
        }

        if (isset($data['knowledge_s'])) {
            $knowledge_s = $data['knowledge_s'];
            foreach ($knowledge_s as $knowledge)
                Knowledge::createNew($lesson, $knowledge);
        }


        return $lesson;

    }

    public function updateData($user, array $data)
    {

        $this->user_editor_id = $user->id;
        $this->knowledge_area_id = $data['knowledge_area'];
        $this->title = $data['title'];
        $this->description = $data['description'];
        $this->outro = $data['outro'];

        if (isset($data['video_type']) && isset($data['video_uri'])) {

            $this->video_type = $data['video_type'];
            $this->video_uri = $data['video_uri'];

        }

        if (isset($data['questions'])) {

            $this->questions()->delete();

            foreach ($data['questions'] as $question) {

                Question::createNew($this, $question);

            }

        }


        if (isset($data['abilities'])) {

            ResultEvaluationStudentAbility::deleteByLesson($this->id);
            EvidenceAbility::deleteByLesson($this->id);
            Ability::deleteByLesson($this->id);

            foreach ($data['abilities'] as $ability) {

                Ability::createNew($this, $ability);

            }
        }

        if (isset($data['knowledge_s'])) {

            ResultEvaluationStudentKnowledge::deleteByLesson($this->id);
            EvidenceKnowledge::deleteByLesson($this->id);
            Knowledge::deleteByLesson($this->id);

            foreach ($data['knowledge_s'] as $knowledge) {

                Knowledge::createNew($this, $knowledge);

            }
        }


        if (isset($data['competences'])) {

            ResultEvaluationStudentCompetence::deleteByLesson($this->id);
            EvidenceCompetence::deleteByLesson($this->id);
            Competence::deleteByLesson($this->id);

            foreach ($data['competences'] as $competence) {

                Competence::createNew($this, $competence);

            }
        }




        if (isset($data['contents'])) {

            $this->contents()->delete();

            foreach ($data['contents'] as $content) {

                Content::createNew($this, $content);

            }

        }

        return $this->save();

    }


    public static function deleteLesson($lesson_id){

        ResultEvaluationStudentAbility::deleteByLesson($lesson_id);
        ResultEvaluationStudentCompetence::deleteByLesson($lesson_id);
        ResultEvaluationStudentKnowledge::deleteByLesson($lesson_id);

        EvidenceAbility::deleteByLesson($lesson_id);
        EvidenceCompetence::deleteByLesson($lesson_id);
        EvidenceKnowledge::deleteByLesson($lesson_id);

        Ability::deleteByLesson($lesson_id);
        Competence::deleteByLesson($lesson_id);
        Knowledge::deleteByLesson($lesson_id);

        ClasseLesson::deleteByLesson($lesson_id);
        Question::deleteByLesson($lesson_id);
        StudentResult::deleteByLesson($lesson_id);


        StudentResult::deleteByLesson($lesson_id);
        Content::deleteByLesson($lesson_id);

        LessonMetric::deleteByLesson($lesson_id);

        $lesson = Lesson::findOrFail($lesson_id);
        $lesson->delete();
    }





    public function countView()
    {

        $this->metric->views++;
        $this->metric->save();

    }

    public function countComplete()
    {

        $this->metric->finalized++;
        $this->metric->save();

    }

    public function classes()
    {

        return $this->belongsToMany(Classe::class);

    }

    /**
     * Searchs for a lesson by its slug.
     *
     * @param $slug
     * @return Lesson
     */
    public static function findOrFailBySlug($slug)
    {

        return static::with('questions.options')->where('slug', $slug)->firstOrFail();

    }

    /**
     * Searchs for a lesson by its uuid.
     *
     * @param $slug
     * @return Lesson
     */
    public static function findOrFailByUuid($uuid)
    {

        return static::with('questions.options')->where('uuid', $uuid)->firstOrFail();

    }


    public function videoIsFromFile()
    {

        return $this->video_type == static::VIDEO_TYPE_FILE;

    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function editor()
    {

        return $this->belongsTo(User::class, 'user_editor_id');

    }

    public function knowledgeArea()
    {

        return $this->belongsTo(KnowledgeArea::class);

    }

    public function questions()
    {

        return $this->hasMany(Question::class);

    }


    public function abilities()
    {

        return $this->hasMany(Ability::class);

    }

    public function knowledge_s()
    {

        return $this->hasMany(Knowledge::class);

    }


    public function competencies()
    {

        return $this->hasMany(Competence::class);

    }


    public function contents()
    {

        return $this->hasMany(Content::class);

    }

    public function metric()
    {

        return $this->hasOne(LessonMetric::class, 'lesson_id');

    }

    public function results()
    {

        return $this->hasMany(StudentResult::class, 'lesson_id');

    }

    public function getLinkAttribute()
    {

        return url("tests/{$this->slug}", [], true);

    }

    public function students()
    {
        $studentsIds = $this->results()->pluck('student_id');
        return Student::whereIn('id', $studentsIds)->get();
    }

    public function studentsIds()
    {
        return $this->results()->pluck('student_id');
    }


    //busca aulas assistida por um usuario/aluno
    public static function findLessonsByUser($user)
    {

        $student = Student::findByEmail($user->email);

        //$student_results = StudentResult::all()->where('student_id', $student->id)->pluck('lesson_id');
        $student_results = Average_Students_Emotions_Lesson::all()->where('student_id', $student->id)->pluck('lesson_id');

        $lessons = Lesson::all()->whereIn('id',$student_results);

        return $lessons;
    }

    //busca todas as aulas assistida
    public static function findLessonsWatched()
    {

        $lessons_results = StudentResult::all()->pluck('lesson_id');

        $lessons = Lesson::all()->whereIn('id',$lessons_results);

        return $lessons;
    }

    public static function findLessonById($id){
        return Lesson::all()->where('id','=',$id)->first();
    }


}