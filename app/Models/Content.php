<?php

namespace App\Models;

use App\Models\Lesson;
use Illuminate\Database\Eloquent\Model;

class Content extends Model
{

    protected $guarded = [];

    protected $casts = [

        'id' => 'integer',
        'lesson_id' => 'integer',

    ];

    public static function createNew(Lesson $lesson, array $data)
    {

        $tags = trim($data['tags'], ',');

        return static::create([

            'lesson_id' => $lesson->id,
            'start_time' => $data['start_time'],
            'end_time' => $data['end_time'],
            'tags' => $tags,

        ]);

    }

    public function lesson()
    {

        return $this->belongsTo(Lesson::class);

    }


    public static function deleteByLesson($lesson_id){

        $contents = Content::all()->where('lesson_id', $lesson_id);

        if (! $contents->isEmpty()){
            foreach ($contents as $content){
                $content->delete();
            }
        }

    }

}