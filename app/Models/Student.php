<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * A student that can make tests.
 *
 * @package App\Models
 */
class Student extends Model
{

    protected $table = 'students';

    /**
     * @var array
     */
    protected $appends = [
        'hasIls', 'needs_to_register'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'id', 'name', 'age', 'gender', 'college', 'course', 'handicap', 'musical_instrument', 'created_at', 'updated_at', 'ils'
    ];

    /**
     * @var array
     */
    protected $casts = [

        'id' => 'integer',
        'age' => 'integer',

        'play_musical_instrument' => 'boolean',
        'hasIls' => 'boolean',


    ];

    /**
     * @var array
     */
    protected $guarded = [];



    public function user(){

        return $this->belongsToMany(User::class);

    }

//Está usando o do StudentRegistrationController
//    public static function createNew(array $data)
//    {
//
//        $student = new Student();
//
//        $student->uuid = uuid();
//        $student->name = $data['name'];
//        $student->email = $data['email'];
//        $student->age = $data['age'];
//        $student->gender = $data['gender'];
//        $student->college = $data['college'];
//        $student->course = $data['course'];
//
//        $has_handicap = null;
//
//        if (empty($data['handicap_bool']) || $data['handicap_bool'] == null) {
//            $student->handicap = $has_handicap;
//        } else {
//            $student->handicap = $data['handicap_bool'];
//        }
//
//
//        $student->play_musical_instrument = empty($data['play_musical_instrument']) ? false : filter_var($data['play_musical_instrument'], FILTER_VALIDATE_BOOLEAN);
//
//        if ($student->play_musical_instrument)
//            $student->which_instrument = $data['which_instrument'];
//
//        $student->save();
//
//        return $student;
//
//    }

    /**
     * Search a Student by its uuid.
     *
     * @param $uuid
     * @return Student
     */
    public static function findOrFailByUuid($uuid)
    {

        return static::where('uuid', $uuid)->firstOrFail();

    }

    /**
     * Finds or creates a student by its email.
     *
     * @param $email
     * @return Student
     */
    public static function findOrCreateByEmail($email)
    {

        $student = static::where('email', $email)->first();

        if ($student === null)
            return static::create(['email' => $email, 'uuid' => uuid()]);

        return $student;

    }

    /**
     * Verifies if the student needs to finish registration.
     *
     * @return bool
     */
    public function getNeedsToRegisterAttribute()
    {

        return $this->name === null;

    }

    public function getHasIlsAttribute()
    {

        return $this->ils != null;

    }

    public function getReadableHandicapAttribute()
    {

        if (!$this->handicap)
            return trans('strings.none');

        switch ($this->handicap) {

            case 'blind':
                return trans('strings.handicap_types.1');
            case 'visual':
                return trans('strings.handicap_types.2');
            case 'deaf_mute':
                return trans('strings.handicap_types.3');
            case 'auditory':
                return trans('strings.handicap_types.4');
            case 'motor':
                return trans('strings.handicap_types.5');
            case 'comunication':
                return trans('strings.handicap_types.6');
            default:
                return $this->handicap;

        }

    }

    public function getReadableMusicalInstrumentAttribute()
    {

        return $this->musical_instrument ?? trans('strings.none');

    }

    /**
     * Verifies if student has an Ils.
     *
     * @return bool
     */
    public function hasIls()
    {

        return $this->ils != null;

    }

    public function ils()
    {

        return $this->hasOne(StudentIls::class, 'student_id');

    }

    protected function extractBoolean(array $data, string $key)
    {

        if (empty($data[$key]))
            return false;

        return filter_var($data[$key], FILTER_VALIDATE_BOOLEAN);

    }

    public function hasClasse(){
        return current($this->classe);
    }

    public function classe(){

        return $this->belongsToMany(Classe::class);

    }

    /**
     * Checks if the student is active or not.
     *
     * @return bool
     */
    public function isActive()
    {

        return $this->active == true;

    }

    /**
     * Toggle the current student's status.
     *
     * @return bool
     */
    public function toggleStatus()
    {

        $this->active = !$this->isActive();


        $user = User::all()->where('student_id','=',$this->id)->first();
        $user->active = $this->active;
        $user->update();

        return $this->save();

    }


    public static function findByEmail($email)
    {
        $student = static::where('email', $email)->first();
        return $student;
    }



    public static function findStudentByUser($user)
    {
        return Student::findByEmail($user->email);
    }


    public static function students_with_emotion(){
        $results = StudentResult::all()->pluck('student_id');
        return Student::all()->whereIn('id',$results);
    }

    public static function students_who_watched_lesson($lesson_id){
        $student_results =  StudentResult::all()->where('lesson_id','=',$lesson_id)->pluck('student_id');
        return Student::all()->whereIn('id',$student_results);
    }


}