<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ClasseStudent extends Model
{
    protected $table = 'classe_student';



    public function getClassByStudent($studentId)
    {
        $classes = DB::table('classe_student')->select('classe_id')->where('student_id', $studentId);
        return $classes;
    }


    public function updateData($data)
    {
        $this->student_id = $data['id_student'];
        $this->classe_id = $data['id_class'];
        return $this->save();
    }

    /**
     * Create a new instance and store it in the db.
     *
     * @param $data
     * @return mixed
     */
    public static function createNew($data)
    {

        $ids = $data['student_id'];

        foreach ($ids as $id){

            $class_student = new ClasseStudent();
            $class_student->classe_id = $data['classe_id'];
            $class_student->student_id = $id;
            $x = $class_student->save();
        }

        return $x;

    }

}
