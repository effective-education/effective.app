<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class EvidenceAbility extends Model
{

    /**
     * @var string
     */
    protected $table = 'evidence_ability';


    protected $guarded = [];

    protected $hidden = ['ability_id', 'created_at', 'updated_at'];

    protected $casts = [

        'id' => 'integer',
        'ability_id' => 'integer',

    ];

    public static function createNew(Ability $ability, array $data, $lesson_id)
    {

        return static::create([

            'ability_id' => $ability->id,
            'description' => $data['description'],
            'lesson_id' => $lesson_id,

        ]);

    }

    public static function deleteByLesson($lesson_id){

        $evidence_abilities = EvidenceAbility::all()->where('lesson_id', $lesson_id);

        if ($evidence_abilities !== null){
            foreach ($evidence_abilities as $evidence_ability){
                $evidence_ability->delete();
            }
        }

    }


    public function ability()
    {

        return $this->belongsTo(Ability::class);

    }


    /**
     * Get the results for the evidences.
     *
     * @return HasMany
     */
    public function results()
    {

        return $this->hasMany(ResultEvaluationStudentAbility::class);

    }


}