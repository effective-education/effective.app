<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Ils results for students.
 *
 * @package App\Models
 */
class StudentIls extends Model
{

    /**
     * @var string
     */
    protected $table = 'students_ils';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     */
    protected $casts = [

        'id' => 'integer',
        'student_id' => 'integer',


    ];

    /**
     * Create a new ils result for a student.
     *
     * @param Student $student
     * @param array $data
     * @return StudentIls
     */
    public static function createNew(Student $student, array $data)
    {

        return static::create([

            'student_id' => $student->id,
            IlsQuestion::SCALE_ACT_REF => $data[IlsQuestion::SCALE_ACT_REF],
            IlsQuestion::SCALE_SEN_INT => $data[IlsQuestion::SCALE_SEN_INT],
            IlsQuestion::SCALE_VIS_VER => $data[IlsQuestion::SCALE_VIS_VER],
            IlsQuestion::SCALE_SEQ_GLO => $data[IlsQuestion::SCALE_SEQ_GLO],

        ]);

    }






}
