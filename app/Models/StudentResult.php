<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\Auth;

/**
 * A result of a lesson watched by a student.
 *
 * @package App\Models
 */
class StudentResult extends Model
{

    /**
     * @var string
     */
    protected $table = 'students_results';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     */
    protected $casts = [

        'questions' => 'array',
        'frames' => 'array',

    ];

    /**
     * Finds a result by its uuid.
     *
     * @param $uuid
     * @return StudentResult
     */
    public static function findOrFailByUuid($uuid)
    {

        return static::where('uuid', $uuid)->firstOrFail();

    }

    public function findByStudentId($studentId)
    {
        return $this->where('student_id', $studentId)->get();
    }


    public static function findResults($lessonId, $studentId)
    {

        $results = StudentResult::all()
            ->where('lesson_id', $lessonId);

        return $results->where('student_id', $studentId)->last();
    }

    /**
     * Creates a new result for a student.
     *
     * @param Student $student
     * @param Lesson $lesson
     * @param array $questions
     * @param array $frames
     * @return StudentResult
     */
    public function createNew(Student $student, Lesson $lesson, array $questions, array $frames)
    {

        $lesson->countComplete();

        $totalQuestions = count($questions);
        $rightAnswers = array_reduce($questions, function ($total, $question) {

            return $question['is_right'] ? ++$total : $total;

        }, 0);

        return static::create([

            'uuid' => uuid(),
            'student_id' => $student->id,
            'lesson_id' => $lesson->id,
            'questions' => $questions,
            'frames' => $frames,
            'total_questions' => $totalQuestions,
            'right_answers' => $rightAnswers,

        ]);

    }



        public static function deleteByLesson($lesson_id){

            $student_results = StudentResult::all()->where('lesson_id', $lesson_id);

            if (! $student_results->isEmpty()){
                foreach ($student_results as $student_result){
                    Impression::deleteByStudentResult($student_result->id);
                    $student_result->delete();
                }
            }

        }


    /**
     * Get the student associated with this result.
     *
     * @return BelongsTo
     */
    public function student()
    {
        return $this->belongsTo(Student::class, 'student_id');
    }

    public function students()
    {
        $studentsIds = $this->student()->pluck('student_id');
        return Student::whereIn('id', $studentsIds)->get();
    }

    /**
     * Get the lesson associated with this result.
     *
     * @return BelongsTo
     */
    public function lesson()
    {

        return $this->belongsTo(Lesson::class, 'lesson_id');

    }

    /**
     * Gets the impression of this result.
     *
     * @return HasOne
     */
    public function impression()
    {

        return $this->hasOne(Impression::class, 'student_result_id');

    }

}
