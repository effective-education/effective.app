<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;

/**
 * A Ils form.
 *
 * @package Models
 */
class Ils
{

    /**
     * @var Collection
     */
    protected $questions;

    /**
     * Ils constructor.
     * @param $questions
     */
    public function __construct()
    {

        $this->questions = IlsQuestion::getQuiz();

    }

    /**
     * Answer the Ils form and return the result.
     *
     * @param $answers
     * @return array
     */
    public function answer($answers)
    {

        $resultsMatrix = [

            IlsQuestion::SCALE_ACT_REF => [ 'a' => 0, 'b' => 0 ],
            IlsQuestion::SCALE_SEN_INT => [ 'a' => 0, 'b' => 0 ],
            IlsQuestion::SCALE_VIS_VER => [ 'a' => 0, 'b' => 0 ],
            IlsQuestion::SCALE_SEQ_GLO => [ 'a' => 0, 'b' => 0 ],

        ];

        foreach ($answers as $answer) {

            $scale = strtolower($this->questions->where('number', $answer['number'])->first()->scale);
            $letter = $answer['answer'];

            $resultsMatrix[$scale][$letter]++;
            
        }

        return array_map(function ($result) {

            return $result['a'] > $result['b'] ? "{$result['a']}a" : "{$result['b']}b";

        }, $resultsMatrix);

    }

}
