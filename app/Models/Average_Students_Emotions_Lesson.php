<?php

namespace App\Models;

use App\Http\Controllers\GraphicsController;
use App\Http\Controllers\StudentsResultsController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\Auth;

/**
 * A result of a lesson watched by a student.
 *
 * @package App\Models
 */
class Average_Students_Emotions_Lesson extends Model
{

    /**
     * @var string
     */
    protected $table = 'average_students_emotions_lesson';

    /**
     * @var array
     */
    protected $guarded = [];



    public static function findByStudentId($studentId)
    {
        return Average_Students_Emotions_Lesson::all()->where('student_id', $studentId)->first();
    }

    public static function findByLessonId($lessonId)
    {
       return Average_Students_Emotions_Lesson::all()->where('lesson_id', $lessonId)->first();
    }


    public static function isNew($student_id, $lesson_id){

        $x = Average_Students_Emotions_Lesson::all()
            ->where('student_id',$student_id)
            ->firstWhere('lesson_id',$lesson_id);

        if ($x === null){
            return true;
        }else{
            return false;
        }

    }


    public static function findAverages($lessonId, $studentId)
    {

        $results = Average_Students_Emotions_Lesson::all()
            ->where('lesson_id', $lessonId)
        ->pluck('lesson_id');

        //return $results->whereIn('student_id', $studentId);
        return Average_Students_Emotions_Lesson::all()->where('student_id', $studentId)->whereIn('lesson_id', $results)->first();
    }


    /**
     * Creates a new result for a student.
     *
     * @param Student $student
     * @param Lesson $lesson
     * @param array $questions
     * @param array $frames
     * @return StudentResult
     */
    public static function createNew($student_id, $lesson_id, array $medias)
    {


        //verificar se o aluno já assistiu a aula para atualizar a média dele

        if (self::isNew($student_id,$lesson_id)){
            $average = new Average_Students_Emotions_Lesson();

            $average->student_id = $student_id;
            $average->lesson_id = $lesson_id;
            $average->joy = $medias['joy'];
            $average->fear = $medias['fear'];
            $average->rage = $medias['rage'];
            $average->disgust = $medias['disgust'];
            $average->sadness = $medias['sadness'];
            $average->contempt = $medias['contempt'];
            $average->surprise = $medias['surprise'];
            $average->save();

        }else{

            $x = new Average_Students_Emotions_Lesson();

            $average = $x::findAverages($lesson_id, $student_id);

            $average->joy = $medias['joy'];
            $average->fear = $medias['fear'];
            $average->rage = $medias['rage'];
            $average->disgust = $medias['disgust'];
            $average->sadness = $medias['sadness'];
            $average->contempt = $medias['contempt'];
            $average->surprise = $medias['surprise'];
            $average->update();

        }


    }

    /**
     * Get the student associated with this result.
     *
     * @return BelongsTo
     */
    public function student()
    {
        return $this->belongsTo(Student::class, 'student_id');
    }


    /**
     * Get the lesson associated with this result.
     *
     * @return BelongsTo
     */
    public function lesson()
    {

        return $this->belongsTo(Lesson::class, 'lesson_id');

    }


    public function students()
    {
        $studentsIds = $this->student()->pluck('student_id');
        return Student::whereIn('id', $studentsIds)->get();
    }

    public function lessons()
    {
        $lessons_id = $this->lesson()->pluck('lesson_id');
        return Lesson::whereIn('id', $lessons_id)->get();
    }




}
