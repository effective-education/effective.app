<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * A lesson's question.
 *
 * @package App\Models
 */
class Question extends Model
{

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     */
    protected $appends = ['answers', 'seconds'];

    /**
     * @var array
     */
    protected $hidden = ['lesson_id', 'created_at', 'updated_at'];

    /**
     * @var array
     */
    protected $casts = [

        'id' => 'integer',
        'lesson_id' => 'integer',

    ];

    /**
     * Creates a new question.
     *
     * @param Lesson $lesson
     * @param $data
     * @return Question
     */
    public static function createNew(Lesson $lesson, $data)
    {

        $question = static::create([

            'lesson_id' => $lesson->id,
            'title' => $data['title'],
            'timestamp' => $data['timestamp'],

        ]);

        $options = $data['options'];

        foreach ($options as $option)
            Option::createNew($question, $option);

        return $question;

    }


    public static function deleteByLesson($lesson_id){

        $questions = Question::all()->where('lesson_id', $lesson_id);

        if ($questions !== null){
            foreach ($questions as $question){
                $question->delete();
            }
        }

    }



    /**
     * Gets the timestamp as seconds.
     *
     * @return int
     */
    public function getSecondsAttribute()
    {

        $parts = explode(':', $this->timestamp);
        return (($parts[0] * 60) * 60) + ($parts[1] * 60) + $parts[2];

    }

    /**
     * Helper to store answers for this question.
     *
     * @return array
     */
    public function getAnswersAttribute()
    {

        return [];

    }

    /**
     * Gets the related lesson.
     *
     * @return BelongsTo
     */
    public function lesson()
    {

        return $this->belongsTo(Lesson::class);

    }

    /**
     * Get the options for the question.
     *
     * @return HasMany
     */
    public function options()
    {

        return $this->hasMany(Option::class);

    }

    /**
     * @return Collection
     */
    public function rightAnswers()
    {

        return $this->options->where('is_right', true);

    }

    /**
     * @return Collection
     */
    public function wrongAnswers()
    {

        return $this->options->where('is_right', false);

    }

    /**
     * Checks if an array of answers (in text, not indexes)
     * are right to this question.
     *
     * @param $answers
     * @return bool
     */
    public function isRight($answers)
    {

        return $answers == $this->rightAnswers()->pluck('text')->all();

    }

}
