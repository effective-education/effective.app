<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Classe extends Model
{
    protected $table = 'classes';

    public function students() {
        return $this->belongsToMany(Student::class);
    }

    public function lessons() {
        return $this->belongsToMany(Lesson::class);
    }


    /**
     * Create a new class instance and store it in the db.
     *
     * @param $data
     * @return mixed
     */
    public static function createNew($user, $data)
    {

        $class = new Classe();
        $class->name = $data['name'];
        $class->description = $data['description'];
        $class->user_id = $user->id;
        $class->save();

        return $class;

    }

    public function updateData($data)
    {
        $this->name = $data['name'];
        $this->description = $data['description'];
        return $this->save();
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
