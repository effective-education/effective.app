<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * A lesson's abilities.
 *
 * @package App\Models
 */
class Ability extends Model
{
    /**
     * @var string
     */
    protected $table = 'abilities';

    /**
     * @var array
     */
    protected $guarded = [];


    /**
     * @var array
     */
    protected $hidden = ['lesson_id', 'created_at', 'updated_at'];

    /**
     * @var array
     */
    protected $casts = [

        'id' => 'integer',
        'lesson_id' => 'integer',

    ];

    /**
     * Creates a new ability.
     *
     * @param Lesson $lesson
     * @param $data
     * @return Competence
     */
    public static function createNew(Lesson $lesson, $data)
    {

        $ability = static::create([

            'lesson_id' => $lesson->id,
            'description' => $data['description']

        ]);

        $evidence_abilities = $data['evidences'];

        foreach ($evidence_abilities as $evidence_ability)
            EvidenceAbility::createNew($ability, $evidence_ability, $lesson->id);

        return $ability;

    }


    public static function deleteByLesson($lesson_id){

        $abilities = Ability::all()->where('lesson_id', $lesson_id);

        if ($abilities !== null){
            foreach ($abilities as $ability){
                $ability->delete();
            }
        }

    }



    /**
     * Gets the related lesson.
     *
     * @return BelongsTo
     */
    public function lesson()
    {

        return $this->belongsTo(Lesson::class);

    }

    /**
     * Get the options for the question.
     *
     * @return HasMany
     */
    public function evidences()
    {

        return $this->hasMany(EvidenceAbility::class);

    }


}
