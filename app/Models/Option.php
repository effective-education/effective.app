<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{

    protected $guarded = [];

    protected $hidden = ['question_id', 'created_at', 'updated_at'];

    protected $casts = [

        'id' => 'integer',
        'question_id' => 'intger',
        'is_right' => 'boolean'

    ];

    public static function createNew(Question $question, array $data)
    {

        $isRight = $data['is_right'] ?? false;

        return static::create([

            'question_id' => $question->id,
            'text' => $data['text'],
            'is_right' => $isRight,

        ]);

    }

    public function question()
    {

        return $this->belongsTo(Question::class);

    }

}