<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IlsOption extends Model
{

    protected $appends = ['text'];
    protected $hidden = ['id', 'created_at', 'updated_at', 'question'];
    protected $casts = [

        'id' => 'integer',
        'ils_question_id' => 'integer',

    ];

    public function getTextAttribute()
    {

        return trans('ils.' . $this->question->number . '.options.' . $this->letter);

    }
    
    public function question()
    {

        return $this->belongsTo(IlsQuestion::class, 'ils_question_id');

    }

}