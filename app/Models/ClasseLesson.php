<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class ClasseLesson extends Model

{
    protected $table = 'classe_lesson';

    public function getClassByLesson($lessonId)
    {
        $classes = DB::table('classe_lesson')->select('classe_id')->where('lesson_id', $lessonId);

        return $classes;
    }



    public static function createNew($data)
    {
        $last_id = ClasseLesson::where('classe_id','=',$data['classe_id']);


        $max = $last_id->max('position');


        if($max == null)
        {
            $max = 0;
        }

        $ids = $data['lesson_id'];

        foreach ($ids as $id){
            $max++;
            $class_lesson = new ClasseLesson();
            $class_lesson->classe_id = $data['classe_id'];
            $class_lesson->lesson_id = $id;
            $class_lesson->position = $max;

            $x = $class_lesson->save();
        }

        return $x;

    }


    public static function deleteByLesson($lesson_id){
        $classe_lessons = ClasseLesson::all()->where('lesson_id', $lesson_id);
        if ($classe_lessons !== null){
            foreach ($classe_lessons as $classe_lesson){
                $classe_lesson->delete();
            }
        }
    }




}
