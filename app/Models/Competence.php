<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * A lesson's competences.
 *
 * @package App\Models
 */
class Competence extends Model
{
    /**
     * @var string
     */
    protected $table = 'competencies';

    /**
     * @var array
     */
    protected $guarded = [];


    /**
     * @var array
     */
    protected $hidden = ['lesson_id', 'created_at', 'updated_at'];

    /**
     * @var array
     */
    protected $casts = [

        'id' => 'integer',
        'lesson_id' => 'integer',

    ];

    /**
     * Creates a new competence.
     *
     * @param Lesson $lesson
     * @param $data
     * @return Competence
     */
    public static function createNew(Lesson $lesson, $data)
    {

        $competence = static::create([

            'lesson_id' => $lesson->id,
            'description' => $data['description']

        ]);

        $evidence_competencies = $data['evidences'];

        foreach ($evidence_competencies as $evidence_competency)
            EvidenceCompetence::createNew($competence, $evidence_competency, $lesson->id);

        return $competence;

    }



    public static function deleteByLesson($lesson_id){

        $competencies = Competence::all()->where('lesson_id', $lesson_id);

        if ($competencies !== null){
            foreach ($competencies as $competency){
                $competency->delete();
            }
        }

    }


    /**
     * Gets the related lesson.
     *
     * @return BelongsTo
     */
    public function lesson()
    {

        return $this->belongsTo(Lesson::class);

    }

    /**
     * Get the options for the question.
     *
     * @return HasMany
     */
    public function evidences()
    {

        return $this->hasMany(EvidenceCompetence::class);

    }


}
