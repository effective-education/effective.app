<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResultEvaluationStudentCompetence extends Model
{

    /**
     * @var string
     */
    protected $table = 'result_evaluation_student_competency';


    public static function createNew($evidence_competence_id, $student_id, $lesson_id, $result)
    {

        $old_result = ResultEvaluationStudentCompetence::all()
            ->where('evidence_competence_id', $evidence_competence_id)
            ->where('student_id', $student_id)
            ->where('lesson_id', $lesson_id)
            ->first();

        if ($old_result !== null){

            $old_result->result = $result;
            return $old_result->update();

        }else {


            $result_evaluation = new ResultEvaluationStudentCompetence();

            $result_evaluation->evidence_competence_id = $evidence_competence_id;
            $result_evaluation->student_id = $student_id;
            $result_evaluation->lesson_id = $lesson_id;
            $result_evaluation->result = $result;

            return $result_evaluation->save();
        }
    }


    public static function deleteByLesson($lesson_id){

        $result_competences = ResultEvaluationStudentCompetence::all()->where('lesson_id', $lesson_id);

        if (! $result_competences->isEmpty()){
            foreach ($result_competences as $result_competence){
                $result_competence->delete();
            }
        }

    }


    public static function isNew($lesson_id, $student_id){

        $x = ResultEvaluationStudentCompetence::all()
            ->where('student_id', $student_id)
            ->where('lesson_id', $lesson_id)->first();
        return $x == null;
    }


    public function evidence_competence()
    {

        return $this->belongsTo(EvidenceCompetence::class);

    }

}