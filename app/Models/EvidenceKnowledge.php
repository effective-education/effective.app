<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class EvidenceKnowledge extends Model
{

    /**
     * @var string
     */
    protected $table = 'evidence_knowledge';


    protected $guarded = [];

    protected $hidden = ['knowledge_id', 'created_at', 'updated_at'];

    protected $casts = [

        'id' => 'integer',
        'knowledge_id' => 'integer',

    ];

    public static function createNew(Knowledge $knowledge, array $data, $lesson_id)
    {

        return static::create([

            'knowledge_id' => $knowledge->id,
            'description' => $data['description'],
            'lesson_id' => $lesson_id,

        ]);

    }

    public static function deleteByLesson($lesson_id){

        $evidence_knowledge_s = EvidenceKnowledge::all()->where('lesson_id', $lesson_id);

        if ($evidence_knowledge_s !== null){
            foreach ($evidence_knowledge_s as $evidence_knowledge){
                $evidence_knowledge->delete();
            }
        }

    }


    public function knowledge()
    {

        return $this->belongsTo(Knowledge::class);

    }


    /**
     * Get the results for the evidences.
     *
     * @return HasMany
     */
    public function results()
    {

        return $this->hasMany(ResultEvaluationStudentKnowledge::class);

    }


}