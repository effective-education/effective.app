<?php

namespace App\Reports;


class Frames
{

    private $frames;

    private $studentName;

    public function __construct($studentName, $frames)
    {

        $this->studentName = $studentName;
        $this->frames = $frames;

    }

    public function headers()
    {

        return [
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
            'Content-type' => 'text/csv',
            'Content-Disposition' => "attachment; filename={$this->studentName}.csv",
            'Expires' => '0',
            'Pragma' => 'public',
        ];

    }

    public function generate()
    {

        $csv = fopen('php://output', 'w');
        fprintf($csv, chr(0xEF) . chr(0xBB) . chr(0xBF)); //UTF-8

        fputcsv($csv, [
            trans('strings.time'),
            trans('strings.joy'),
            trans('strings.fear'),
            trans('strings.anger'),
            trans('strings.disgust'),
            trans('strings.sadness'),
            trans('strings.valence'),
            trans('strings.contempt'),
            trans('strings.surprise'),
            trans('strings.engagement'),
            trans('strings.during_video'),
        ], ';');

        foreach ($this->frames as $frame) {

            fputcsv($csv, [
                !empty($frame['seconds']) ? $frame['seconds'] : '0',
                !empty($frame['emotions']['joy']) ? floor($frame['emotions']['joy']) : '0',
                !empty($frame['emotions']['fear']) ? floor($frame['emotions']['fear']) : '0',
                !empty($frame['emotions']['anger']) ? floor($frame['emotions']['anger']) : '0',
                !empty($frame['emotions']['disgust']) ? floor($frame['emotions']['disgust']) : '0',
                !empty($frame['emotions']['sadness']) ? floor($frame['emotions']['sadness']) : '0',
                !empty($frame['emotions']['valence']) ? floor($frame['emotions']['valence']) : '0',
                !empty($frame['emotions']['contempt']) ? floor($frame['emotions']['contempt']) : '0',
                !empty($frame['emotions']['surprise']) ? floor($frame['emotions']['surprise']) : '0',
                !empty($frame['emotions']['engagement']) ? floor($frame['emotions']['engagement']) : '0',
                !empty($frame['questions']) ? 'true' : 'false',
            ], ';');

        }

        fclose($csv);

    }

}
