<?php

namespace App\Reports;


class XmlFramesAfter
{
    private $frames;
    private $updatedDate;
    private $studentName;
    private $studentID;
    private $fileName;
    private $ontoId;

    public function __construct($studentName, $frames, $studentID, $updatedDate, $ontoId)
    {
        $this->studentID = $studentID;
        $this->updatedDate = $updatedDate;
        $this->studentName = $studentName;
        $this->frames = $frames;
        $this->fileName = 'Affectiva ' . str_replace(':', '_', $this->updatedDate) . ' '.$this->studentName . '_' . $this->studentID;
        $this->ontoId = $ontoId;
    }

    public function generate()
    {

        $xmlFile = fopen('xml/'. $this->fileName .'.xml', 'w');
        $br = PHP_EOL;
        $tb1 = "\t";
        $tb2 = "\t\t";
        $botEmotion =  $tb1. '</emotion>'. $br;
        $botEmotionml =  '</emotionml>'. $br;
        $botBehavior = $tb1. '</BEHAVIOR>'. $br;
        $headerEmotionML = '<emotionml version="1.0" xmlns="http://www.w3.org/2009/10/emotionml">' . $br;

        if ($this->ontoId != null) {
            $infoTag = $tb1 . '<info>' . $br .
                $tb2 . '<user_id value="' . $this->ontoId . '"/>' . $br .
                $tb2 . '<dateTime value="' . $this->updatedDate . '"/>' . $br .
                $tb2 . '<sensor value="Affectiva"/>' . $br .
                $tb1 . '</info>' . $br;
        }else{
            $infoTag = $tb1 . '<info>'. $br .
                $tb2 . '<user_id value="CADAP '. $this->studentName . '_' . $this->studentID . '"/>' . $br .
                $tb2 . '<dateTime value="' . $this->updatedDate . '"/>' . $br .
                $tb2 . '<sensor value="Affectiva"/>'. $br .
                $tb1 . '</info>' . $br;
        }

        fwrite($xmlFile, $headerEmotionML);
        fwrite($xmlFile, $infoTag);

        foreach ($this->frames as $frame) {
            $headerEmotion = $tb1.'<emotion xmlns="http://www.w3.org/2009/10/emotionml" category-set="http://www.w3.org/TR/emotion-voc/xml#fsre-categories" start="' . ($frame['unixTime']) . '" duration="30">'. $br;
            $headerBehavior = $tb1. '<BEHAVIOR start="' . ($frame['unixTime']) . '" duration="30">'. $br;

            $catEmotion =
                $tb2 . '<category name="anger" value="' . ($frame['emotions']['anger'])/100  . '"/>' . $br .
                $tb2 . '<category name="contempt" value="' . ($frame['emotions']['contempt'])/100 . '"/>' . $br .
                $tb2 . '<category name="disgust" value="' . ($frame['emotions']['disgust'])/100 . '"/>' . $br .
                $tb2 . '<category name="fear" value="' . ($frame['emotions']['fear'])/100 . '"/>' . $br .
                $tb2 . '<category name="joy" value="' . ($frame['emotions']['joy'])/100 . '"/>' . $br .
                $tb2 . '<category name="sadness" value="' . ($frame['emotions']['sadness'])/100 . '"/>' . $br .
                $tb2 . '<category name="surprise" value="' . ($frame['emotions']['surprise'])/100 . '"/>' . $br;

            $catBehavior = $tb2 . '<category name="Engaged" value="'. ($frame['emotions']['engagement'])/100 . '"/>' . $br;

            fwrite($xmlFile, $headerEmotion);
            fwrite($xmlFile, $catEmotion);
            fwrite($xmlFile, $botEmotion);
            fwrite($xmlFile, $headerBehavior);
            fwrite($xmlFile, $catBehavior);
            fwrite($xmlFile, $botBehavior);

        }

        fwrite($xmlFile, $botEmotionml);
        fclose($xmlFile);

        return $xmlFile;

    }

}