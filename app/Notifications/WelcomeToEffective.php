<?php

namespace App\Notifications;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class WelcomeToEffective extends Notification
{
    use Queueable;

    public $user;
    public $password;

    /**
     * Create a new notification instance.
     *
     * @param User $user
     * @param string $password
     */
    public function __construct(User $user, string $password)
    {

        $this->user = $user;
        $this->password = $password;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting(trans('notifications.welcome_to_effective.greeting', ['app' => config('app.name')]))
            ->line(trans('notifications.welcome_to_effective.intro'))
            ->line(trans('notifications.welcome_to_effective.email', ['email' => $this->user->email]))
            ->line(trans('notifications.welcome_to_effective.password', ['password' => $this->password]))
            ->action(trans('notifications.welcome_to_effective.action'), route('login'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

}