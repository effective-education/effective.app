# Effective

Web application to evaluate the engagement of students with didactic material through the analysis of their facial expressions.

## Structure

Effective was built in PHP using Laravel 5.5. It needs PHP 7.0 to run (without its dev dependencies) and it is recommended
to use it with Postgres as database. 

## Install
First of all, pay attention in which version of cadap you want to install. There are 2 versions:

At the **master branch**, you will install and change the main version.

At the **ontology branch**, you will install and change the version required to send data to this [Ontology API](https://doc.arca.acacia.red/).

For production

``` bash
$ git clone git@bitbucket.org:effective-education/effective.app.git effective
$ cd effective
$ composer install --no-dev
$ cp .env.example .env
$ php artisan key:generate
```

Edit your .env

``` txt
APP_ENV=production
APP_DEBUG=false
APP_URL={your url}

DB_CONNECTION={db connection}
DB_HOST={db host}
DB_PORT={db port}
DB_DATABASE={db name}
DB_USERNAME={db user}
DB_PASSWORD={db secret}
MAIL_DRIVER=mailgun
MAILGUN_DOMAIN={your mailgun domaing}
MAILGUN_SECRET={your mailgun key}
```

Then run

``` bash
$ php artisan migrate --seed
```

For development

Follow the same steps above but instead of running ``composer install --no-dev`` run

``` bash
$ composer install
```

And configure the .env for development

``` txt
APP_ENV=local
APP_DEBUG=true
MAIL_DRIVER=mailtrap.io
MAIL_HOST=mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME={your mailtrap username}
MAIL_PASSWORD={your mailtrap password}
```

You don't need to setup Mailgun for development, only Mailtrap. Don't forget to setup your database connection!

Note that to install the dev dependencies you need ``PHP 7.1`` 

## Usage in production

Currently the application in production is available at [https://10.203.24.96/effective/](https://10.203.24.96/effective/)
and for tests at [https://10.203.24.96/stage/effective](https://10.203.24.96/stage/effective).

## Testing

``` bash
$ php ./vendor/phpunit/phpunit/phpunit tests
```

## License

The MIT License (MIT).
