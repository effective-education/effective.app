<?php

namespace Tests\Integration\Models;

use App\Models\Lesson;
use App\Models\Option;
use App\Models\Question;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Models\Quiz;
use Tests\BrowserKitTestCase;

/**
 * Tests for a quiz of a lesson.
 *
 * @package Tests\Integration\Models
 */
class QuizTest extends BrowserKitTestCase
{

    use DatabaseMigrations;

    /** @test */
    public function returns_right_results_from_given_answers()
    {

        $lesson = factory(Lesson::class)->create();

        $questionA = factory(Question::class)->create(['lesson_id' => $lesson->id]);
        $questionB = factory(Question::class)->create(['lesson_id' => $lesson->id]);
        $questionC = factory(Question::class)->create(['lesson_id' => $lesson->id]);

        factory(Option::class)->create(['question_id' => $questionA->id, 'text' => 'red', 'is_right' => true]);
        factory(Option::class)->create(['question_id' => $questionA->id, 'text' => 'blue', 'is_right' => false]);

        factory(Option::class)->create(['question_id' => $questionB->id, 'text' => 'yellow', 'is_right' => false]);
        factory(Option::class)->create(['question_id' => $questionB->id, 'text' => 'orange', 'is_right' => true]);

        factory(Option::class)->create(['question_id' => $questionC->id, 'text' => 'black', 'is_right' => true]);
        factory(Option::class)->create(['question_id' => $questionC->id, 'text' => 'purple', 'is_right' => true]);



        $quiz = new Quiz($lesson);
        $questionsWithAnswers = [
            ['id' => $questionA->id, 'answers' => ['blue']],
            ['id' => $questionB->id, 'answers' => ['orange']],
            ['id' => $questionC->id, 'answers' => ['black']],
        ];
        $this->assertEquals([
            'questions' => [
                ['id' => $questionA->id, 'answers' => ['blue'], 'is_right' => false],
                ['id' => $questionB->id, 'answers' => ['orange'], 'is_right' => true],
                ['id' => $questionC->id, 'answers' => ['black'], 'is_right' => false],
            ],
            'total_questions' => 3,
            'right_answers' => 1,
            'right_percentage' => 33,

        ], $quiz->answer($questionsWithAnswers));



        $quiz = new Quiz($lesson);
        $questionsWithAnswers = [
            ['id' => $questionA->id, 'answers' => ['black']],
            ['id' => $questionB->id, 'answers' => ['black']],
            ['id' => $questionC->id, 'answers' => ['black']],
        ];
        $this->assertEquals([
            'questions' => [
                ['id' => $questionA->id, 'answers' => ['black'], 'is_right' => false],
                ['id' => $questionB->id, 'answers' => ['black'], 'is_right' => false],
                ['id' => $questionC->id, 'answers' => ['black'], 'is_right' => false],
            ],
            'total_questions' => 3,
            'right_answers' => 0,
            'right_percentage' => 0,
        ], $quiz->answer($questionsWithAnswers));



        $quiz = new Quiz($lesson);
        $questionsWithAnswers = [
            ['id' => $questionA->id, 'answers' => ['red']],
            ['id' => $questionB->id, 'answers' => ['orange']],
            ['id' => $questionC->id, 'answers' => ['black', 'purple']],
        ];
        $this->assertEquals([
            'questions' => [
                ['id' => $questionA->id, 'answers' => ['red'], 'is_right' => true],
                ['id' => $questionB->id, 'answers' => ['orange'], 'is_right' => true],
                ['id' => $questionC->id, 'answers' => ['black', 'purple'], 'is_right' => true],
            ],
            'total_questions' => 3,
            'right_answers' => 3,
            'right_percentage' => 100,
        ], $quiz->answer($questionsWithAnswers));

    }

}
