<?php

namespace Tests\Integration\Models;

use App\Models\Student;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\BrowserKitTestCase;

/**
 * Student model test.
 * @package Tests\Integration\Models
 */
class StudentTest extends BrowserKitTestCase
{

    use DatabaseMigrations;

    /** @test */
    public function it_finds_a_student_by_its_email()
    {

        $student = factory(Student::class)->create([ 'email' => 'email@email.com' ]);

        $foundStudent = Student::findOrCreateByEmail($student->email);

        $this->assertTrue($student->is($foundStudent));

    }

    /** @test */
    public function it_creates_a_student_if_no_student_is_found_by_email()
    {

        $email = 'mail@bol.com';

        $this->dontSeeInDatabase('students', ['email' => $email]);

        Student::findOrCreateByEmail($email);

        $this->seeInDatabase('students', ['email' => $email]);

    }

    /** @test */
    public function checks_if_the_student_needs_to_finish_registration()
    {

        $student = factory(Student::class)->create(['name' => null]);

        $this->assertTrue($student->needs_to_register);

    }

}
