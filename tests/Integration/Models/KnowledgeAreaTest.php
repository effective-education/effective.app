<?php

namespace Tests\Integration\Models;


use App\Models\KnowledgeArea;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\BrowserKitTestCase;

/**
 * Test for KnowledgeAreas
 *
 * @package Tests\Integration\Models
 */
class KnowledgeAreaTest extends BrowserKitTestCase
{

    use DatabaseMigrations;

    /** @test */
    public function search_area_by_name_in_translation_files()
    {

        $this->createKnowledgeArea('Computer Science', '019381029321');

        $this->assertEquals(['019381029321'], KnowledgeArea::searchByName('Computer Science'));

        $this->assertEquals(['019381029321'], KnowledgeArea::searchByName('puter Scie'));

        $this->assertEquals(['019381029321'], KnowledgeArea::searchByName('computer'));

        $this->assertEquals(['019381029321'], KnowledgeArea::searchByName('science'));

        $this->assertEmpty(KnowledgeArea::searchByName('none'));

    }

    /** @test */
    public function search_area_with_special_characters()
    {

        $this->createKnowledgeArea('SOFTWARE BÁSICO', '01239203182');

        $this->assertEquals(['01239203182'], KnowledgeArea::searchByName('software básico'));

        $this->assertEquals(['01239203182'], KnowledgeArea::searchByName('básico'));

        $this->assertEquals(['01239203182'], KnowledgeArea::searchByName('bás'));

    }

}
