<?php

namespace Tests\Integration\Models;

use App\Models\Option;
use App\Models\Question;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\BrowserKitTestCase;

/**
 * Test for a question from a lesson.
 *
 * @package Tests\Integration\Models
 */
class QuestionTest extends BrowserKitTestCase
{

    use DatabaseMigrations;

    /** @test */
    public function it_converts_a_timestamp_to_seconds()
    {

        $question = new Question();

        $question->timestamp = '01:35:45';
        $this->assertEquals(5745, $question->seconds);

        $question->timestamp = '00:00:45';
        $this->assertEquals(45, $question->seconds);

        $question->timestamp = '00:02:45';
        $this->assertEquals(165, $question->seconds);

        $question->timestamp = '00:00:00';
        $this->assertEquals(0, $question->seconds);

    }

    /** @test */
    public function returns_the_right_answers()
    {

        $question = factory(Question::class)->create();

        factory(Option::class)->create(['question_id' => $question->id, 'is_right' => true]);
        factory(Option::class)->create(['question_id' => $question->id, 'is_right' => false]);
        factory(Option::class)->create(['question_id' => $question->id, 'is_right' => true]);
        factory(Option::class)->create(['question_id' => $question->id, 'is_right' => false]);
        factory(Option::class)->create(['question_id' => $question->id, 'is_right' => false]);

        $this->assertCount(2, $question->rightAnswers());

    }

    /** @test */
    public function checks_if_the_question_is_right_for_the_given_answers()
    {

        $question = factory(Question::class)->create();

        factory(Option::class)->create(['question_id' => $question->id, 'text' => 'red', 'is_right' => true]);
        factory(Option::class)->create(['question_id' => $question->id, 'text' => 'blue', 'is_right' => false]);
        factory(Option::class)->create(['question_id' => $question->id, 'text' => 'purple', 'is_right' => false]);
        factory(Option::class)->create(['question_id' => $question->id, 'text' => 'black', 'is_right' => true]);

        $this->assertFalse($question->isRight(['red']));
        $this->assertFalse($question->isRight(['blue']));
        $this->assertFalse($question->isRight([]));
        $this->assertFalse($question->isRight(['red', 'blue']));
        $this->assertTrue($question->isRight(['red', 'black']));


    }

}
