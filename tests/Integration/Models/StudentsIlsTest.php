<?php

namespace Tests\Integration\Models;

use App\Models\IlsQuestion;
use App\Models\Student;
use App\Models\StudentIls;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\BrowserKitTestCase;

/**
 * Student ils test result test.
 * @package Tests\Integration\Models
 */
class StudentsIlsTest extends BrowserKitTestCase
{

    use DatabaseMigrations;

    /** @test */
    public function it_creates_ils_results_for_a_student()
    {

        $student = factory(Student::class)->create();

        $results = StudentIls::createNew($student, [

            IlsQuestion::SCALE_ACT_REF => '11a',
            IlsQuestion::SCALE_SEN_INT => '11b',
            IlsQuestion::SCALE_VIS_VER => '11b',
            IlsQuestion::SCALE_SEQ_GLO => '11a',

        ]);

        $this->seeInDatabase('students_ils', [

            'id' => $results->id,
            IlsQuestion::SCALE_ACT_REF => '11a',
            IlsQuestion::SCALE_SEN_INT => '11b',
            IlsQuestion::SCALE_VIS_VER => '11b',
            IlsQuestion::SCALE_SEQ_GLO => '11a',

        ]);

    }

}
