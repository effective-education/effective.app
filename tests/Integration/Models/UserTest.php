<?php

namespace Tests\Integration\Models;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\BrowserKitTestCase;

class UserTest extends BrowserKitTestCase
{

    use DatabaseMigrations;

    /**
     * @test
     */
    public function it_should_return_true_when_there_is_at_least_one_user_in_db()
    {

        factory(User::class, 2)->create();

        $this->assertTrue(User::existsAtLeastOne());

    }

    /**
     * @test
     */
    public function it_should_return_false_when_there_are_no_one_users_in_db()
    {

        $this->assertFalse(User::existsAtLeastOne());

    }

    /**
     * @test
     */
    public function it_should_create_an_user_instance_and_save_it_to_the_db()
    {

        $data = [

            'name' => 'João Fernances',
            'email' => 'joao@bol.com',
            'password' => 'JesusTodoPoderoso123',

        ];

        $user = User::createNew($data, User::ADMIN);

        $this->assertInstanceOf(User::class, $user);

        $this->seeInDatabase('users', [
            'name' => $user->name,
            'email' => $user->email,
            'password' => $user->password,
            'role' => $user->role,
        ]);

    }

    /**
     * @test
     */
    public function it_should_check_the_user_role()
    {

        $user = createUser();

        $this->assertTrue($user->isAdmin());

        $user->role = User::COLLABORATOR;

        $this->assertTrue($user->isCollaborator());

    }

    /**
     * @test
     */
    public function it_should_set_the_user_role()
    {

        $user = createUser();

        $user->setRole(User::COLLABORATOR);

        $this->assertEquals(User::COLLABORATOR, $user->role);

        $this->seeInDatabase('users', [
            'id' => $user->id,
            'role' => User::COLLABORATOR
        ]);

    }

    /**
     * @test
     */
    public function it_should_toggle_user_status()
    {

        $user = createUser();

        $user->toggleStatus();

        $this->assertFalse($user->active);

        $this->seeInDatabase('users', ['id' => $user->id, 'active' => false]);

    }

    /**
     * @test
     */
    public function it_should_check_if_the_user_is_active()
    {

        $user = createUser();

        $this->assertTrue($user->isActive());

    }

}