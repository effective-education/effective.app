<?php

namespace Tests;

use App\Models\Ils;
use App\Models\IlsOption;
use App\Models\IlsQuestion;
use Illuminate\Foundation\Testing\DatabaseMigrations;

/**
 * Test for Ils form.
 *
 * @package Tests
 */
class IlsTest extends BrowserKitTestCase
{

    use DatabaseMigrations;

    /** @test */
    public function returns_ils_results()
    {

        $this->createIlsForm();

        $ils = new Ils();

        $results = $ils->answer([

            ['number' => 1, 'answer' => 'a'],
            ['number' => 2, 'answer' => 'b'],
            ['number' => 3, 'answer' => 'b'],
            ['number' => 4, 'answer' => 'a'],

        ]);

        $this->assertEquals([
            'act_ref' => '1a',
            'sen_int' => '1b',
            'vis_ver' => '1b',
            'seq_glo' => '1a',
        ], $results);

    }

    /**
     * Create a simple ils form with 4 questions.
     */
    protected function createIlsForm()
    {

        $question1 = factory(IlsQuestion::class)->create(['number' => 1, 'scale' => 'act_ref']);
        $question2 = factory(IlsQuestion::class)->create(['number' => 2, 'scale' => 'sen_int']);
        $question3 = factory(IlsQuestion::class)->create(['number' => 3, 'scale' => 'vis_ver']);
        $question4 = factory(IlsQuestion::class)->create(['number' => 4, 'scale' => 'seq_glo']);
        factory(IlsOption::class)->create(['ils_question_id' => $question1->id, 'letter' => 'a']);
        factory(IlsOption::class)->create(['ils_question_id' => $question1->id, 'letter' => 'b']);
        factory(IlsOption::class)->create(['ils_question_id' => $question2->id, 'letter' => 'a']);
        factory(IlsOption::class)->create(['ils_question_id' => $question2->id, 'letter' => 'b']);
        factory(IlsOption::class)->create(['ils_question_id' => $question3->id, 'letter' => 'a']);
        factory(IlsOption::class)->create(['ils_question_id' => $question3->id, 'letter' => 'b']);
        factory(IlsOption::class)->create(['ils_question_id' => $question4->id, 'letter' => 'a']);
        factory(IlsOption::class)->create(['ils_question_id' => $question4->id, 'letter' => 'b']);

    }

}
