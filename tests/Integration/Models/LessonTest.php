<?php

namespace Tests\Integration\Models;

use App\Models\Lesson;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\BrowserKitTestCase;

/**
 * Test for a lesson.
 *
 * @package Tests\Integration\Models
 */
class LessonTest extends BrowserKitTestCase
{

    use DatabaseMigrations;

    /** @test */
    public function it_finds_a_lesson_by_slug()
    {

        $lesson = factory(Lesson::class)->create();

        $foundLesson = Lesson::findOrFailBySlug($lesson->slug);

        $this->assertTrue($lesson->is($foundLesson));

    }

    /** @test */
    public function throws_exception_when_dont_find_lesson_by_slug()
    {

        $this->expectException(ModelNotFoundException::class);

        Lesson::findOrFailBySlug('dont exists');

    }

}
