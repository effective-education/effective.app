<?php

namespace Tests\Integration\Models;

use App\Models\Lesson;
use App\Models\LessonMetric;
use App\Models\Student;
use App\Models\StudentResult;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\BrowserKitTestCase;

/**
 * Student result test.
 *
 * @package Tests\Integration\Models
 */
class StudentsResultTest extends BrowserKitTestCase
{

    use DatabaseMigrations;

    /** @test */
    public function it_creates_a_student_result()
    {

        $student = factory(Student::class)->create();

        $lesson = factory(Lesson::class)->create();
        factory(LessonMetric::class)->create(['lesson_id' => $lesson->id]);

        $questions = [
            ['id' => 1, 'is_right' => true],
            ['id' => 2, 'is_right' => true],
            ['id' => 3, 'is_right' => true],
        ];
        $frames = [
            ['seconds' => 34, 'emotions' => [], 'questions' => []],
            ['seconds' => 120, 'emotions' => [], 'questions' => []],
            ['seconds' => 230, 'emotions' => [], 'questions' => []],
        ];

        $result = StudentResult::createNew($student, $lesson, $questions, $frames);

        $this->seeInDatabase('students_results', [
            'student_id' => $student->id,
            'lesson_id' => $lesson->id,
            'total_questions' => 3,
            'right_answers' => 3,
        ]);

        $this->seeInDatabase('lessons_metrics', [
            'lesson_id' => $lesson->id,
            'finalized' => 1,
        ]);

        $this->assertEquals($questions, $result->questions);
        $this->assertEquals($frames, $result->frames);

    }

}
