<?php

namespace Tests;

use App\Models\IlsOption;
use App\Models\IlsQuestion;
use App\Models\KnowledgeArea;
use App\Models\User;
use Illuminate\Contracts\Console\Kernel;
use Laravel\BrowserKitTesting\TestCase as BaseTestCase;

abstract class BrowserKitTestCase extends BaseTestCase
{

    /**
     * The base URL of the application.
     *
     * @var string
     */
    public $baseUrl = 'http://localhost';

    /**
     * The name of the route being tested.
     *
     * @var string
     */
    protected $routeToBeVisited = '';

    /**
     * The loged in user.
     *
     * @var User
     */
    protected $user = null;

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        return $app;
    }

    /**
     * Visits the tested page.
     */
    protected function visitPage()
    {

        $this->visitRoute($this->routeToBeVisited);

    }

    /**
     * Create a user and log him in.
     *
     * @param array $attributes
     */
    protected function logIn($attributes = [])
    {

        $this->user = factory(User::class)->create($attributes);

        $this->be($this->user);

    }

    protected function createKnowledgeArea($name, $code = null)
    {

        $data = $code === null ? [] : ['code' => $code];
        $knowledgeArea = factory(KnowledgeArea::class)->create($data);
        trans()->addLines(["knowledge-areas.{$knowledgeArea->code}" => $name], trans()->getLocale());
        return $knowledgeArea;

    }

    /**
     * Dumps the response to the terminal.
     */
    protected function showResponse()
    {

        dd($this->response->content());

    }

}
