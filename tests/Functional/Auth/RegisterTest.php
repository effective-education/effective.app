<?php

namespace Tests\Functional\Auth;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\BrowserKitTestCase;

class RegisterTest extends BrowserKitTestCase
{

    use DatabaseMigrations;

    /**
     * @test
     */
    public function it_should_not_show_register_link_when_there_are_users_in_db()
    {

        createUser();

        $this->visitRoute('pages.index');

        $this->dontSeeLink('Registrar', route('register'));

    }

    /**
     * @test
     */
    public function it_should_show_the_register_link_when_there_are_no_users_in_db()
    {

        $this->visitRoute('pages.index');

        $this->seeLink('Registrar', route('register'));

    }

    /**
     * @test
     */
    public function it_should_redirect_to_home_page_while_trying_to_access_register_route_when_there_are_users_in_db()
    {

        createUser();

        $this->visitRoute('register');

        $this->seeRouteIs('pages.index');

    }

}