<?php

namespace Tests\Funcional\Controller;

use App\Models\User;
use App\Notifications\WelcomeToEffective;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Notification;
use Laravel\BrowserKitTesting\HttpException;
use Tests\BrowserKitTestCase;

/**
 * Class UserControllerTest
 * @package Tests\Funcional\Controller
 */
class UserControllerTest extends BrowserKitTestCase
{

    use DatabaseMigrations;

    protected $userData = [

        'name' => 'Jesus',
        'email' => 'jesus@bol.com.br',

    ];

    public function setUp()
    {

        parent::setUp();

        $this->logIn();

        Notification::fake();

    }

    /**
     * @test
     */
    public function it_should_show_a_message_when_there_are_no_users()
    {

        $this->visitRoute('users.index');

        $this->see(trans('status.user.none'));

    }

    /**
     * @test
     */
    public function it_should_show_other_user_data_in_a_table()
    {

        $otherUser = createUser();

        $this->visitIndexRoute();

        $this->seeInElement('td', $otherUser->name);
        $this->seeInElement('td', $otherUser->email);
        $this->seeInElement('td', 'Sim');
        $this->seeLink('Editar', route('users.edit', [$otherUser->id]));

        $this->dontSeeInElement('td', $this->user->name);

    }

    /**
     * @test
     */
    public function it_should_fail_validation_while_creating_an_user()
    {

        $this->visitCreateRoute();

        $this->press("Criar");

        $this->seeRouteIs('users.create');

        $this->see('O campo Nome é obrigatório');

    }

    /**
     * @test
     */
    public function it_should_create_a_new_user_and_send_a_notification()
    {

        $this->visitCreateRoute();

        $this->fillAndSubmitCreateForm();

        $newUser = User::where('email', $this->userData['email'])->first();

        Notification::assertSentTo($newUser, WelcomeToEffective::class);

        $this->seeRouteIs('users.index');

        $this->see('Um e-mail foi enviado para o usuário avisando sobre seu cadastro');

    }

    /**
     * @test
     */
    public function it_should_update_user_role()
    {

        $user = createUser([
            'role' => User::ADMIN,
        ]);

        $this->visitEditRoute($user);

        $this->select(User::COLLABORATOR, 'role');

        $this->press('Salvar');

        $this->visitEditRoute($user);

        $this->seeElement('option[value=' . User::COLLABORATOR . ']', ['selected']);

    }

    /**
     * @test
     */
    public function it_should_toggle_user_status()
    {

        $user = createUser(['active' => true]);

        $this->visitEditRoute($user);

        $this->press('Bloquear');

        $this->seeInElement('td', 'não');

        $this->visitEditRoute($user);

        $this->press('Desbloquear');

        $this->seeInElement('td', 'sim');

    }

    /**
     * @test
     */
    public function collaborator_should_not_have_permission_to_manage_users()
    {

        $this->expectException(HttpException::class);

        $this->logIn(['role' => User::COLLABORATOR]);

        $this->visitRoute('users.index');

    }

    /**
     * @test
     */
    public function collaborator_should_not_see_users_link()
    {

        $this->logIn(['role' => User::COLLABORATOR]);

        $this->visitRoute('dashboard.index');

        $this->dontSeeLink('Usuários', route('users.index'));

    }

    protected function visitIndexRoute()
    {

        $this->visitRoute('users.index');

    }

    protected function visitCreateRoute()
    {

        $this->visitRoute('users.create');

    }

    protected function visitEditRoute(User $user)
    {

        $this->visitRoute('users.edit', [$user->id]);

    }

    private function fillAndSubmitCreateForm()
    {

        $this->type($this->userData['name'], 'name');
        $this->type($this->userData['email'], 'email');

        $this->press(trans('strings.create'));

    }

}