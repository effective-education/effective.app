<?php
/**
 * Created by PhpStorm.
 * User: Artenes
 * Date: 16/09/2017
 * Time: 17:38
 */

namespace Tests\Features;


use App\Models\Ils;
use App\Models\Student;
use DatabaseSeeder;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Http\Response;
use Tests\BrowserKitTestCase;

/**
 * Test to register an ils form answers.
 *
 * @package Tests\Features
 */
class RegisterIlsResultsTest extends BrowserKitTestCase
{

    use DatabaseMigrations;

    /** @test */
    public function answers_ils_form()
    {

        $this->seed(DatabaseSeeder::class);

        $student = factory(Student::class)->create();

        $answers = $this->makeAnswers();

        $this->postJson("student/ils/$student->uuid", ['answers' => $answers]);

        $this->assertResponseStatus(Response::HTTP_CREATED);

        $this->seeJson($this->getAnswersResults($answers));

    }

    /**
     * @return array
     */
    protected function makeAnswers()
    {

        $answers = [];

        foreach (range(1, 44) as $number) {

            $answers[] = ['number' => $number, 'answer' => 'a'];

        }

        return $answers;

    }

    /**
     * @param $answers
     * @return array
     */
    protected function getAnswersResults($answers)
    {

        return (new Ils())->answer($answers);

    }

}
