<?php

namespace Features;

use App\Models\Lesson;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Http\Response;
use Tests\BrowserKitTestCase;

class FindsALessonBySlugTest extends BrowserKitTestCase
{

    use DatabaseMigrations;

    /** @test */
    public function finds_lesson_by_slug()
    {

        $lesson = factory(Lesson::class)->create();

        $this->getJson("lesson/{$lesson->slug}");

        $this->assertResponseStatus(Response::HTTP_OK);

        $this->seeJson(['uuid' => $lesson->uuid]);

    }

}
