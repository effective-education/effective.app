<?php

namespace Tests\Funcional\Controller;

use App\Models\Student;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Http\Response;
use Tests\BrowserKitTestCase;

/**
 * Test for student data update.
 * @package Tests\Funcional\Controller
 */
class RegisterStudentTest extends BrowserKitTestCase
{

    use DatabaseMigrations;

    /** @test */
    public function updates_student_registration()
    {

        $student = factory(Student::class)->create();

        $data = [
            'name' => 'Jhon Doe',
            'email' => $student->email,
            'age' => 34,
            'gender' => 'M',
            'college' => 'UFOPA',
            'course' => 'CC',
            'handicap' => 'deaf',
            'musical_instrument' => 'Guitar',
        ];

        $this->putJson("student/{$student->uuid}", $data);

        $this->assertResponseStatus(Response::HTTP_NO_CONTENT);

        $data['uuid'] = $student->uuid;

        $this->seeInDatabase('students', $data);

    }

    /** @test */
    public function fails_registration_update_with_invalid_data()
    {

        $student = factory(Student::class)->create();

        $this->putJson("student/{$student->uuid}");
        $this->assertResponseStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $this->putJson("student/{$student->uuid}", [
            'gender' => 'invalid',
        ]);
        $this->assertResponseStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

    }

}
