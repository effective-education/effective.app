<?php

namespace Tests\Features;

use App\Models\Lesson;
use App\Models\LessonMetric;
use App\Models\Option;
use App\Models\Question;
use App\Models\Student;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Http\Response;
use Tests\BrowserKitTestCase;

/**
 * Test for registering a student's result test.
 *
 * @package Tests\Features
 */
class RegisterAStudentResultTest extends BrowserKitTestCase
{

    use DatabaseMigrations;

    /** @test */
    public function it_gets_results_right_percentage()
    {

        $student = factory(Student::class)->create();

        $lesson = factory(Lesson::class)->create();
        $question = factory(Question::class)->create(['lesson_id' => $lesson->id]);
        factory(Option::class)->create(['question_id' => $question->id, 'text' => 'red', 'is_right' => true]);
        factory(Option::class)->create(['question_id' => $question->id, 'text' => 'blue', 'is_right' => false]);
        factory(LessonMetric::class)->create(['lesson_id' => $lesson->id]);

        $questions = [
            ['id' => $question->id, 'answers' => ['blue']],
        ];

        $frames = [
            ['seconds' => 34, 'emotions' => [], 'questions' => []],
            ['seconds' => 120, 'emotions' => [], 'questions' => []],
            ['seconds' => 230, 'emotions' => [], 'questions' => []],
        ];

        $this->postJson('studentsresults', [
            'student_uuid' => $student->uuid,
            'lesson_uuid' => $lesson->uuid,
            'questions' => $questions,
            'frames' => $frames,
        ]);

        $this->assertResponseStatus(Response::HTTP_CREATED);

        $this->seeJson(['right_percentage' => 0]);

    }

    /** @test */
    public function fails_registration_with_invalid_data()
    {

        $this->postJson('studentsresults');
        $this->assertResponseStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $this->postJson('studentsresults', [
            'student_uuid' => 'dont exist',
            'lesson_uuid' => 'dont exist',
            'frames' => [],
            'questions' => [],
        ]);
        $this->assertResponseStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $this->postJson('studentsresults', [
            'student_uuid' => factory(Student::class)->create()->uuid,
            'lesson_uuid' => factory(Lesson::class)->create()->uuid,
            'frames' => [],
            'questions' => [],
        ]);
        $this->assertResponseStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

    }

}
