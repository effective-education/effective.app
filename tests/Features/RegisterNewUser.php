<?php

namespace Tests\Features;

use App\Models\User;
use App\Notifications\WelcomeToEffective;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Notification;
use Tests\BrowserKitTestCase;

/**
 * Test for new registered user.
 *
 * @package Tests\Features
 */
class RegisterNewUser extends BrowserKitTestCase
{

    use DatabaseMigrations;

    /** @test */
    public function send_email_to_new_registered_user()
    {

        Notification::fake();

        $this->logIn();

        $this->post('dashboard/users', [

            'name' => 'Jhon Doe',
            'email' => 'jhon@doe.com',
            'password' => 'randompassword',
            'role' => 'admin',

        ]);

        $this->assertRedirectedToRoute('users.index');

        $user = User::where('email', 'jhon@doe.com')->first();

        Notification::assertSentTo([$user], WelcomeToEffective::class, function ($notification) {

            return $notification->user->email === 'jhon@doe.com' && $notification->password === 'randompassword';

        });

    }

}
