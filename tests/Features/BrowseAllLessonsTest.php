<?php

namespace Tests\Features;

use App\Models\Lesson;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Http\Response;
use Tests\BrowserKitTestCase;

/**
 * Test for browsing public lessons.
 *
 * @package Tests\Features
 */
class BrowseAllLessonsTest extends BrowserKitTestCase
{

    use DatabaseMigrations;

    /** @test */
    public function show_paginated_lessons()
    {

        factory(Lesson::class, 30)->create();

        $this->getJson('api/lessons');

        $this->assertResponseStatus(Response::HTTP_OK);
        $this->assertCount(15, $this->response->getOriginalContent()['lessons']);

    }

    /** @test */
    public function go_to_next_page()
    {

        factory(Lesson::class, 20)->create();

        $this->getJson('api/lessons');
        $this->assertResponseStatus(Response::HTTP_OK);
        $this->assertCount(15, $this->response->getOriginalContent()['lessons']);

        $this->getJson('api/lessons?page=2');
        $this->assertResponseStatus(Response::HTTP_OK);
        $this->assertCount(5, $this->response->getOriginalContent()['lessons']);

    }

    /** @test */
    public function show_latest_lessons_first()
    {

        $lessonA = factory(Lesson::class)->create(['created_at' => '2017-09-19 10:58:00']);
        $lessonB = factory(Lesson::class)->create(['created_at' => '2017-09-19 09:58:00']);
        $lessonC = factory(Lesson::class)->create(['created_at' => '2017-09-19 08:58:00']);

        $this->getJson('api/lessons');
        $this->assertResponseStatus(Response::HTTP_OK);

        $this->assertEquals($lessonA->title, $this->response->getOriginalContent()['lessons'][0]['title']);
        $this->assertEquals($lessonB->title, $this->response->getOriginalContent()['lessons'][1]['title']);
        $this->assertEquals($lessonC->title, $this->response->getOriginalContent()['lessons'][2]['title']);

    }

    /** @test */
    public function show_lesson_details()
    {

        factory(Lesson::class, 30)->create(['created_at' => '2017-09-19 10:00:00']);

        factory(Lesson::class)->create([
            'title' => 'Object Oriented Programming 101',
            'knowledge_area_id' => $this->createKnowledgeArea('Computer Science')->id,
            'user_id' => factory(User::class)->create(['name' => 'Dr. Ana Hamic'])->id,
            'slug' => 'object-oriented-programming-101',
            'created_at' => '2017-09-19 12:00:00'
        ]);

        $this->getJson('api/lessons');
        $this->assertResponseStatus(Response::HTTP_OK);

        $lesson = $this->response->getOriginalContent()['lessons'][0];

        $this->assertEquals('Object Oriented Programming 101', $lesson['title']);
        $this->assertEquals('Dr. Ana Hamic', $lesson['teacher']);
        $this->assertEquals('Computer Science', $lesson['area']);
        $this->assertEquals(url('tests/object-oriented-programming-101', [], true), $lesson['url']);

    }

    /** @test */
    public function search_for_lessons()
    {

        factory(Lesson::class, 30)->create(['created_at' => '2017-09-19 10:00:00']);

        factory(Lesson::class)->create([
            'title' => 'Object Oriented Programming 101',
            'knowledge_area_id' => $this->createKnowledgeArea('Computer Science')->id,
            'user_id' => factory(User::class)->create(['name' => 'Dr. Ana Hamic'])->id,
        ]);

        $this->getJson('api/lessons?search=object oriented');
        $this->assertResponseStatus(Response::HTTP_OK);
        $this->assertCount(1, $this->response->getOriginalContent()['lessons']);
        $this->assertEquals('Object Oriented Programming 101', $this->response->getOriginalContent()['lessons'][0]['title']);

        $this->getJson('api/lessons?search=ana hamic');
        $this->assertResponseStatus(Response::HTTP_OK);
        $this->assertCount(1, $this->response->getOriginalContent()['lessons']);
        $this->assertEquals('Object Oriented Programming 101', $this->response->getOriginalContent()['lessons'][0]['title']);

        $this->getJson('api/lessons?search=computer science');
        $this->assertResponseStatus(Response::HTTP_OK);
        $this->assertCount(1, $this->response->getOriginalContent()['lessons']);
        $this->assertEquals('Object Oriented Programming 101', $this->response->getOriginalContent()['lessons'][0]['title']);

    }

    /** @test */
    public function search_lessons_with_special_characters()
    {

        factory(Lesson::class, 30)->create(['created_at' => '2017-09-19 10:00:00']);

        factory(Lesson::class)->create([
            'title' => 'Programação Orientáda a Objétos',
            'knowledge_area_id' => $this->createKnowledgeArea('SOFTWÁRE BÁSICO')->id,
            'user_id' => factory(User::class)->create(['name' => 'André Ferrêira Martíns'])->id,
        ]);

        $this->getJson('api/lessons?search=básico');
        $this->assertResponseStatus(Response::HTTP_OK);
        $this->assertCount(1, $this->response->getOriginalContent()['lessons']);
        $this->assertEquals('SOFTWÁRE BÁSICO', $this->response->getOriginalContent()['lessons'][0]['area']);

        $this->getJson('api/lessons?search=andré');
        $this->assertResponseStatus(Response::HTTP_OK);
        $this->assertCount(1, $this->response->getOriginalContent()['lessons']);
        $this->assertEquals('André Ferrêira Martíns', $this->response->getOriginalContent()['lessons'][0]['teacher']);

        $this->getJson('api/lessons?search=orientáda');
        $this->assertResponseStatus(Response::HTTP_OK);
        $this->assertCount(1, $this->response->getOriginalContent()['lessons']);
        $this->assertEquals('Programação Orientáda a Objétos', $this->response->getOriginalContent()['lessons'][0]['title']);

    }

}
