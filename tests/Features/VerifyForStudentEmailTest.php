<?php

namespace Tests\Funcional\Controller;

use App\Models\Student;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Http\Response;
use Tests\BrowserKitTestCase;

/**
 * Test for student email verification.
 * @package Tests\Funcional\Controller
 */
class VerifyForStudentEmailTest extends BrowserKitTestCase
{

    use DatabaseMigrations;

    /** @test */
    public function returns_a_student_regardless_if_email_exists_or_not()
    {

        $this->postJson('/students/email', ['email' => 'somerandom@email.com']);
        $this->assertResponseStatus(Response::HTTP_CREATED);
        $this->seeJson(['email' => 'somerandom@email.com']);

        $student = factory(Student::class)->create(['email' => 'exists@email.com']);
        $this->postJson('/students/email', ['email' => $student->email]);
        $this->assertResponseStatus(Response::HTTP_OK);
        $this->seeJson(['email' => $student->email]);

    }

    /** @test */
    public function fails_verification_with_invalid_email()
    {

        $this->postJson('/students/email');
        $this->assertResponseStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $this->postJson('/students/email', ['email' => 'invalid']);
        $this->assertResponseStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

    }

}
