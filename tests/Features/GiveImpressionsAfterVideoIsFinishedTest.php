<?php

namespace Tests\Features;

use App\Models\Impression;
use App\Models\StudentResult;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Http\Response;
use Tests\BrowserKitTestCase;

/**
 * Tests the impressions form after the video is complete.
 *
 * @package Tests\Features
 */
class GiveImpressionsAfterVideoIsFinishedTest extends BrowserKitTestCase
{

    use DatabaseMigrations;

    /** @test */
    public function give_an_impression_about_a_lesson()
    {

        $result = factory(StudentResult::class)->create();

        $this->postJson('impressions', [

            'student_result_uuid' => $result->uuid,
            'answers' => [

                'surprised' => Impression::VERY_WEAK,
                'proud' => Impression::WEAK,
                'sad' => Impression::NOR_WEAK_OR_STRONG,
                'excited' => Impression::STRONG,
                'interesting' => Impression::VERY_STRONG,
                'devalued' => Impression::VERY_WEAK,
                'ashamed' => Impression::WEAK,
                'happy' => Impression::NOR_WEAK_OR_STRONG,
                'sorry' => Impression::STRONG,
                'expectancy' => Impression::VERY_STRONG,
                'fear' => Impression::VERY_WEAK,
                'quiet' => Impression::WEAK,
                'aversion' => Impression::NOR_WEAK_OR_STRONG,
                'satisfied' => Impression::STRONG,
                'angry' => Impression::VERY_STRONG,
                'jealous' => Impression::VERY_WEAK,
                'indifferent' => Impression::WEAK,

            ]

        ]);

        $this->assertResponseStatus(Response::HTTP_CREATED);

        $this->seeInDatabase('impressions', [
            'student_result_id' => $result->id,
            'happy' => Impression::NOR_WEAK_OR_STRONG
        ]);

    }

}
