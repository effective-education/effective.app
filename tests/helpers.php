<?php

/**
 * @param array $attributes
 * @return \App\Models\User
 */
function createUser($attributes = [])
{

    return factory(\App\Models\User::class)->create($attributes);

}