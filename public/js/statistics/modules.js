// ----------------------- IMPORTANTE  -----------------------------
// para que o usuário não tenha acesso ao código JS puro, ele deve ser ofuscado
// Ofuscado em http://www.javascriptobfuscator.com/Javascript-Obfuscator.aspx#
//
// -----------------------------------------------------------------



//------------------------------------------------------
//  GLOBAL VARIABLES
//------------------------------------------------------

var selected_emotion = null;
var selected_emotion_2 = null;
var selected_lesson = null;
var selected_lesson_2 = null;
var selected_lesson_3 = null;
var selected_lesson_4 = null;
var selected_student = null;
var selected_student_2 = null;
var selected_student_3 = null;

var sessions = [];


//------------------------------------------------------
//  CONSTANTS
//------------------------------------------------------
const NOT_FOUND = -1;

//------------------------------------------------------
//  GLOBAL FUNCTIONS
//------------------------------------------------------

var initialize = function() {


    $('#larger_smaller_search').prop('disabled', true);
    $('#student_larger_smaller_emotion_search').prop('disabled', true);
    $('#individual_student_search').prop('disabled', true);
    $('#individual_lesson_search').prop('disabled', true);
    $('#lesson_report_results').prop('disabled', true);
    $('#lesson_upper_lower_emotion_search').prop('disabled', true);;
    $('#lesson').prop('disabled', true);
    $('#lesson_2').prop('disabled', true);
    $('#evaluation_button').prop('disabled', true);


};



var getListSessions = function(student_id, callback) {
    $.ajax({
        url: "filtered/" + student_id + "",
        method: 'GET',
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Content-Type", "application/json");
        },
        dataType: "json"
    })
        .done(callback)
        .fail(function(error) {
            console.error('Error', error);
        });
};


var renderSession = function() {
    let html = '<option value="">Choose Session</option>';
    for (let i = 0; i < sessions.length; i++) {
        html += '<option value="' + sessions[i]['Id'] + '">' + sessions[i]['Name'] + "</option>";
    }

    $('#lesson_2').html(html);
};



//Busca aulas com maior e menor indice de uma emoçao selecionada
var botaoBuscaMaiorMenorEmotion = function() {

    if (selected_emotion) {

            window.open("emotion/" + selected_emotion + "");

    } else {
        alert('Please select a Emotion');
    }
};


//Busca aluno com maior e menor indice em uma aula selecionada
var botaoBuscaAlunoMaiorMenorEmotionAula = function() {

    if (selected_emotion_2 && selected_lesson) {

        window.open("lesson/" + selected_lesson + "/emotion/" + selected_emotion_2 + "");

    } else {
        alert('Please select the filter');
    }
};



//individual por aluno - tras as aulas e suas respectivas médias de emoções
var botaoIndividualAluno = function() {

    if (selected_student ) {

        window.open("student/" + selected_student + "");

    } else {
        alert('Please select the filter');
    }
};



//individual por Aula - tras os alunos e suas respectivas médias de emoções
var botaoIndividualAula = function() {

    if (selected_lesson_3 ) {

        window.open("lesson/" + selected_lesson_3 + "");

    } else {
        alert('Please select the filter');
    }

};



//para cada emoção, tras a aula que teve meior e menor índice
var botaoMaiorMenorForEachEmotion = function() {

    if (selected_student_2 ) {

        window.open("emotion_lesson/student/" + selected_student_2 + "");

    } else {
        alert('Please select the filter');
    }
};


//Busca a avaliação de um aluno numa aula
var botaoAvaliacaoAlunoAula = function() {

    if (selected_student_3 && selected_lesson_2) {

        window.open("evaluation/student/" + selected_student_3 + "/lesson/" + selected_lesson_2 + "");

    } else {
        alert('Please select the filter');
    }
};



//relatório de uma aula com resultados
var botaoAvaliacaoAula = function() {

    if (selected_lesson_4 ) {

        window.open("report/lesson/" + selected_lesson_4 + "");

    } else {
        alert('Please select the filter');
    }

};



//------------------------------------------------------
//  DOCUMENT READY
//------------------------------------------------------
$(document).ready(function() {

    //------------------------------------------------------
    //  EVENTS
    //------------------------------------------------------

// Active button for search the emotion
    $('#emotion').change(function() {
        selected_emotion = $(this).val() !== "" ? $(this).val() : null;

        if (selected_emotion !== null) {
            $('#larger_smaller_search').prop('disabled', false);
        } else {
            $('#larger_smaller_search').prop('disabled', true);
        }
    });


// Active button for search the student by emotion by lesson
    $('#emotion_2').change(function() {
        selected_emotion_2 = $(this).val() !== "" ? $(this).val() : null;

        if (selected_emotion_2 !== null){
            $('#lesson').prop('disabled', false);
        } else {
            $('#lesson').prop('disabled', true);
        }

    });

    //aluno com maior e menor indice de emoção
    $('#lesson').change(function() {
        selected_lesson = $(this).val() !== "" ? $(this).val() : null;

        if (selected_lesson !== null && selected_emotion_2 !== null) {
            $('#student_larger_smaller_emotion_search').prop('disabled', false);
        } else {
            $('#student_larger_smaller_emotion_search').prop('disabled', true);
        }

    });


//Active activates the individual student search button
    $('#student').change(function() {
        selected_student = $(this).val() !== "" ? $(this).val() : null;

        if (selected_student !== null ) {
            $('#individual_student_search').prop('disabled', false);
        } else {
            $('#individual_student_search').prop('disabled', true);
        }

    });


    //Active activates the individual lesson search button
    $('#lesson_3').change(function() {
        selected_lesson_3 = $(this).val() !== "" ? $(this).val() : null;

        if (selected_lesson_3 !== null ) {
            $('#individual_lesson_search').prop('disabled', false);
        } else {
            $('#individual_lesson_search').prop('disabled', true);
        }

    });


    //Activate button to pick lesson with higher and lower index for each emotion
    $('#student_2').change(function() {
        selected_student_2 = $(this).val() !== "" ? $(this).val() : null;

        if (selected_student_2 !== null ) {
            $('#lesson_upper_lower_emotion_search').prop('disabled', false);
        } else {
            $('#lesson_upper_lower_emotion_search').prop('disabled', true);
        }

    });

    // Active button for search the student by emotion by lesson
    $('#student_3').change(function() {
        selected_student_3 = $(this).val() !== "" ? $(this).val() : null;

        if (selected_student_3 !== null){


            getListSessions(selected_student_3, function(_sessions) {

                if (_sessions.length) {
                    sessions = _sessions;
                    renderSession();
                }

            });

            $('#lesson_2').prop('disabled', false);
        } else {
            $('#lesson_2').prop('disabled', true);
        }

    });


//Resultado da Avaliação
    $('#lesson_2').change(function() {
        selected_lesson_2 = $(this).val() !== "" ? $(this).val() : null;

        if (selected_lesson_2 !== null && selected_student_3 !== null) {
            $('#evaluation_button').prop('disabled', false);
        } else {
            $('#evaluation_button').prop('disabled', true);
        }

    });



    //Active activates the individual lesson search button
    $('#lesson_4').change(function() {
        selected_lesson_4 = $(this).val() !== "" ? $(this).val() : null;

        if (selected_lesson_4 !== null ) {
            $('#lesson_report_results').prop('disabled', false);
        } else {
            $('#lesson_report_results').prop('disabled', true);
        }

    });

    //------------------------------------------------------
    //  INITIALIZATION
    //------------------------------------------------------
    initialize();
});

