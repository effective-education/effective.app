// ----------------------- IMPORTANTE  -----------------------------
// para que o usuário não tenha acesso ao código JS puro, ele deve ser ofuscado
// Ofuscado em http://www.javascriptobfuscator.com/Javascript-Obfuscator.aspx#
//
// -----------------------------------------------------------------



//------------------------------------------------------
//  GLOBAL VARIABLES
//------------------------------------------------------

var students = [];
var sessions = [];

var selected_student = null;
var selected_student_v3 = null;
var selected_student_v6 = null;


var selected_session = null;
var selected_lesson = null;
var selected_lesson_v7 = null;
var selected_class = null;
var selected_class_v8 = null;



var chart_v2 = null;


//------------------------------------------------------
//  CONSTANTS
//------------------------------------------------------
const NOT_FOUND = -1;

//------------------------------------------------------
//  GLOBAL FUNCTIONS
//------------------------------------------------------

var initialize = function() {
    $('#session_v2').prop('disabled', true);

    $('#plotGraphicBtn_v2').prop('disabled', true);
    $('#plotGraphicBtn_v3').prop('disabled', true);
    $('#plotGraphicBtn_v4').prop('disabled', true);
    $('#plotGraphicBtn_v5').prop('disabled', true);
    $('#plotGraphicBtn_v6').prop('disabled', true);
    $('#plotGraphicBtn_v7').prop('disabled', true);
    $('#plotGraphicBtn_v8').prop('disabled', true);

};



var getListSessions = function(student_id, callback) {
    $.ajax({
        url: "filtered/" + student_id + "",
        method: 'GET',
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Content-Type", "application/json");
        },
        //data: JSON.stringify([student_id]),
        dataType: "json"
    })
        .done(callback)
        .fail(function(error) {
            console.error('Error', error);
        });
};


var renderSession = function() {
    let html = '<option value="">Choose Session</option>';
    for (let i = 0; i < sessions.length; i++) {
        html += '<option value="' + sessions[i]['Id'] + '">' + sessions[i]['Name'] + "</option>";
    }

    $('#session').html(html);
    $('#session_v2').html(html);
};


//Variação de Emoções de um Estudante durante uma Aula
var plotGraphic_v2 = function() {

    if (selected_student) {
        if (selected_session) {

            window.open("plotEmotionOfStudentByLesson/" + selected_session + "/" + selected_student + "");


        } else {
            alert('Please select a Session');
        }
    } else {
        alert('Please select a Student');
    }
};



//Média de Emoções de um Estudante
var plotGraphic_v3 = function() {

    if (selected_student_v3) {

            window.open("plotAverageOfEmotionsByStudent/" + selected_student_v3 + "");

    } else {
        alert('Please select a Student');
    }
};


//Média de Emoções de uma Aula
var plotGraphic_v4 = function() {

    if (selected_lesson) {

        window.open("plotAverageOfEmotionsByLesson/" + selected_lesson + "");

    } else {
        alert('Please select a Lesson');
    }
};

//Média de Emoções de uma Turma
var plotGraphic_v5 = function() {

    if (selected_class) {

        window.open("plotAverageOfEmotionsByClass/" + selected_class + "");

    } else {
        alert('Please select a Class');
    }
};






//Taxa de Acertos por Estudante
var plotGraphic_v6 = function() {

    if (selected_student_v6) {

        window.open("plotHitRateByStudent/" + selected_student_v6 + "");

    } else {
        alert('Please select a Student');
    }
};



//Taxa de Acertos por Aula
var plotGraphic_v7 = function() {

    if (selected_lesson_v7) {

        window.open("plotHitRateByLesson/" + selected_lesson_v7 + "");

    } else {
        alert('Please select a Lesson');
    }
};


//Taxa de Acertos por Turma
var plotGraphic_v8 = function() {

    if (selected_class_v8) {

        window.open("plotHitRateByClass/" + selected_class_v8 + "");

    } else {
        alert('Please select a Class');
    }
};



//------------------------------------------------------
//  DOCUMENT READY
//------------------------------------------------------
$(document).ready(function() {

    //------------------------------------------------------
    //  EVENTS
    //------------------------------------------------------

    $('#student_v2').change(function() {
        selected_student = $(this).val() !== "" ? $(this).val() : null;


        $('#session_v2').prop('disabled', true);
        $('#plotGraphicBtn_v2').prop('disabled', true);

        $('#session_v2').html('<option value="">Choose Session</option>');
        if (chart_v2) {
            chart_v2 = null;
            //charta.destroy();
        }

        if (selected_student) {
            getListSessions(selected_student, function(_sessions) {

                if (_sessions.length) {
                    sessions = _sessions;
                    $('#session_v2').prop('disabled', false);
                    $('#plotGraphicBtn_v2').prop('disabled', true);
                    renderSession();
                }


            });
        }

    });

// Active button for plot first Graphic
    $('#session_v2').change(function() {
        selected_session = $(this).val() !== "" ? $(this).val() : null;

        if (chart_v2) {
            chart_v2.destroy();
            chart_v2 = null;
        }

        if (selected_session) {
            $('#plotGraphicBtn_v2').prop('disabled', false);
        } else {
            $('#plotGraphicBtn_v2').prop('disabled', true);
        }
    });



    // Active button for plot second Graphic
    $('#student_v3').change(function() {
        selected_student_v3 = $(this).val() !== "" ? $(this).val() : null;

        if (selected_student_v3) {
            $('#plotGraphicBtn_v3').prop('disabled', false);
        } else {
            $('#plotGraphicBtn_v3').prop('disabled', true);
        }

    });



    // Active button for plot third Graphic
    $('#lesson_v2').change(function() {
        selected_lesson = $(this).val() !== "" ? $(this).val() : null;

        if (selected_lesson) {
            $('#plotGraphicBtn_v4').prop('disabled', false);
        } else {
            $('#plotGraphicBtn_v4').prop('disabled', true);
        }

    });


    // Active button for plot third Graphic
    $('#class').change(function() {
        selected_class = $(this).val() !== "" ? $(this).val() : null;

        if (selected_class) {
            $('#plotGraphicBtn_v5').prop('disabled', false);
        } else {
            $('#plotGraphicBtn_v5').prop('disabled', true);
        }

    });


    // Active button for plot fourth Graphic
    $('#student_v6').change(function() {
        selected_student_v6 = $(this).val() !== "" ? $(this).val() : null;

        if (selected_student_v6) {
            $('#plotGraphicBtn_v6').prop('disabled', false);
        } else {
            $('#plotGraphicBtn_v6').prop('disabled', true);
        }

    });

    // Active button for plot fifth Graphic
    $('#lesson_v7').change(function() {
        selected_lesson_v7 = $(this).val() !== "" ? $(this).val() : null;

        if (selected_lesson_v7) {
            $('#plotGraphicBtn_v7').prop('disabled', false);
        } else {
            $('#plotGraphicBtn_v7').prop('disabled', true);
        }

    });


    // Active button for plot sixth Graphic
    $('#class_v8').change(function() {
        selected_class_v8 = $(this).val() !== "" ? $(this).val() : null;

        if (selected_class_v8) {
            $('#plotGraphicBtn_v8').prop('disabled', false);
        } else {
            $('#plotGraphicBtn_v8').prop('disabled', true);
        }

    });



    //------------------------------------------------------
    //  INITIALIZATION
    //------------------------------------------------------
    initialize();
});

